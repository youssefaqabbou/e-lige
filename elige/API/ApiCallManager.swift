//
//  ApiCallManager.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import SwiftUI
import Alamofire
import SwiftyJSON

let baseUrl = "https://e-lige.ajicreative.club/"
//let baseUrl = "http://a507-105-154-67-64.ngrok.io/"
//let baseUrl = "http://192.168.0.69:4000/"
let apiBaseUrl = baseUrl + "api/"


class APICallManager {
    
    static let shared = APICallManager()
    
    func createRequest(
            _ url: String,
            method: HTTPMethod,
            headers: HTTPHeaders? = AppFunctions.getHeader(),
            parameters: [String: Any]?,
            onSuccess successCallback: ((JSON) -> Void)?,
            onFailure failureCallback: ((String) -> Void)?
        ) {
        
        DebugHelper.debug("url : ", url)
        DebugHelper.debug("Params : ", parameters as Any)
        
            AF.request(url, method: method , parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<999).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    //DebugHelper.debug("Result", json)
                    successCallback?(json)
                case .failure(let error):
                    if let callback = failureCallback {
                        DebugHelper.debug("failureCreateRequest" , error.localizedDescription )
                        callback(error.localizedDescription)
                    }
                }
            }
        }
    
  var manager: Session!
      init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        //configuration.httpAdditionalHeaders = Session.default
        manager = Alamofire.Session(configuration: configuration)
      }
    
    func createRequestJSON(
        _ url: String,
        method: HTTPMethod,
        headers: HTTPHeaders?,
        parameters: [String: Any]?,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
      ) -> DataRequest {
        DebugHelper.debug(url)
        DebugHelper.debug(parameters as Any)
        return manager.request(url, method: method , parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
          switch response.result {
          case .success(let value):
            //DispatchQueue.main.async {
            let json = JSON(value)
            successCallback?(json)
            //DebugHelper.debug(json)
            //}
          case .failure(let error):
              DebugHelper.debug(error.localizedDescription)
              switch error {
                case .explicitlyCancelled:
                    DebugHelper.debug("explicitlyCancelled")
                  break;
                  default :
                      failureCallback?(error.localizedDescription)
                    break;
              }
          }
        }
      }
    
    func createUploadRequest(_ url: String,
                         method: HTTPMethod,
                         headers: HTTPHeaders?,
                         image: UIImage? = nil,
                         parameters: [String: Any],
                         onSuccess successCallback: ((JSON) -> Void)?,
                         onFailure failureCallback: ((String) -> Void)?) {
        DebugHelper.debug(url)
        DebugHelper.debug(parameters)
            AF.upload(multipartFormData: { multiPart in
                for p in parameters {
                    multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
                }
                multiPart.append(image!.jpegData(compressionQuality: 0.4)!, withName: "image", fileName: "file.jpg", mimeType: "image/jpg")
            }, to: url, method: method, headers: headers)
            .uploadProgress(queue: .main, closure: { progress in
            }).responseJSON(completionHandler: { data in
                //successCallback?(json)
            }).response { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value!)
                    successCallback?(json)
                case .failure(let error):
                    if let callback = failureCallback {
                        callback(error.localizedDescription)
                    }
                }
            }
        }
    
}
