//
//  ApiServices.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIService {
    
    enum StatusCode: Int {
        case code200 = 200 // OK
        case code201 = 201 // Created
        case code202 = 202 // Accepted
        
        case code400 = 400 // Bad Request
        case code401 = 401 // Unauthorized
        case code403 = 403 // Forbidden
        case code404 = 404 // Not Found
        case code409 = 409 // Conflict
        
        case code500 = 500 // Server Error
        
    }
    enum Endpoint: String {
        
        case login = "users/login" // POST
        case signup = "users/signup" // POST
        case verifyAccount = "users/verify-account" // POST
        case refreshToken = "users/refresh-token" // POST
        case resendCode = "users/resend-code" // POST
        case sendCode = "users/send-code" // POST
        
        
        case forgotPassword = "users/forget-password" // POST
        case resetPassword = "users/reset-password" // PUT
        case changePassword = "users/change-password" // PUT
        
        case profile = "users/profile" // GET: detail // PUT: Update (all infos)
        case uploadImage = "users/upload-image" // POST
        case updateEmail = "users/update-email" // PUT
        
        case completePaiementAccount = "users/generate-account-link" // POST
        case stripeAccount = "users/get-stripe-account" // POST
        
        /// Clients & Barber
        case getShopCategories = "home" // GET list barber shop & categories
        case getShops = "salons" // GET list, detail, filter
        
        case favorites = "favorites" // GET: list favorites // POST: add to favorites // DELETE: Remove from favorites
        
        case services = "services" // GET: list services, detail // PATCH: update // DETELE: remove
        
        //case schedules = "schedules" // GET: list schedules
        case updateSchedule = "schedules/update" // Put: update schedule
        
        case reviews = "reviews" // GET: reviews // POST: create review
        case comments = "comments" // GET: list reviews
        
        case orders = "orders" // GET: list orders
        case ordersByDate = "orders/by-date"
        case ordersByKeyword = "orders/search"
        case availableSchedules = "orders/schedules-by-date" // GET salonId & date
        
        case createOrder = "orders/add" // POST: create new order
        case updateOrders = "orders/update-status" // PATCH: update order
        
        case customers = "customers" // GET: list customers with search, detail // DELETE: remove customer
        case blockCustomer = "customers/block" // PATCH: block customer
        
        case images = "images" // GET, POST, PUT, DELETE
        
        case categories = "categories"
        
        case cards = "cards" // GET, PUT, DELETE
        
        case notifications = "notifications" // GET notification
        case settings = "settings" // GET notification
        
        case googleSignIn = "auth-social/google" //
        case facebookSignIn = "auth-social/facebook" //
        case appleSignIn = "auth-social/apple" //
    }

    
    public func signIn(params: [String: Any],
                       onSuccess successCallback: ((_ response: Bool) -> Void)?,
                       onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.login.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    // Store Token & Refrtesh Token & User
                    if let token = responseObject[JsonKeys.accessToken.rawValue].string{
                        UserDefaults.token = token
                        if let refreshToken = responseObject[JsonKeys.refreshToken.rawValue].string{
                            UserDefaults.refreshToken = refreshToken
                            if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                                
                                if let role = user[JsonKeys.role.rawValue]?.string{
                                    UserDefaults.isBarber =  role == "SALON" ? true : false
                                    let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                                    UserDefaults.user = userString!
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }
                else if(statusCode == StatusCode.code202.rawValue){
                    // Account must be verified
                    successCallback?(false)
                    return
                }else{
                    // code400, code401, code409,code500
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }
            failureCallback?(LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func googleSignIn(params: [String: Any],
                       onSuccess successCallback: ((_ response: Bool) -> Void)?,
                       onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.googleSignIn.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    // Store Token & Refrtesh Token & User
                    if let token = responseObject[JsonKeys.accessToken.rawValue].string{
                        UserDefaults.token = token
                        if let refreshToken = responseObject[JsonKeys.refreshToken.rawValue].string{
                            UserDefaults.refreshToken = refreshToken
                            if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                                
                                if let role = user[JsonKeys.role.rawValue]?.string{
                                    UserDefaults.isBarber =  role == "SALON" ? true : false
                                    let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                                    UserDefaults.user = userString!
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }
                else if(statusCode == StatusCode.code202.rawValue){
                    // Account must be verified
                    successCallback?(false)
                    return
                }else{
                    // code400, code401, code409,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func facebookSignIn(params: [String: Any],
                       onSuccess successCallback: ((_ response: Bool) -> Void)?,
                       onFailure failureCallback: (( _ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.facebookSignIn.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    // Store Token & Refrtesh Token & User
                    if let token = responseObject[JsonKeys.accessToken.rawValue].string{
                        UserDefaults.token = token
                        if let refreshToken = responseObject[JsonKeys.refreshToken.rawValue].string{
                            UserDefaults.refreshToken = refreshToken
                            if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                                
                                if let role = user[JsonKeys.role.rawValue]?.string{
                                    UserDefaults.isBarber =  role == "SALON" ? true : false
                                    let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                                    UserDefaults.user = userString!
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }
                else if(statusCode == StatusCode.code202.rawValue){
                    // Account must be verified
                    successCallback?(false)
                    return
                }else{
                    // code400, code401, code409,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func appleSignIn(params: [String: Any],
                       onSuccess successCallback: ((_ response: Bool) -> Void)?,
                       onFailure failureCallback: (( _ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.appleSignIn.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    // Store Token & Refrtesh Token & User
                    if let token = responseObject[JsonKeys.accessToken.rawValue].string{
                        UserDefaults.token = token
                        if let refreshToken = responseObject[JsonKeys.refreshToken.rawValue].string{
                            UserDefaults.refreshToken = refreshToken
                            if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                                
                                if let role = user[JsonKeys.role.rawValue]?.string{
                                    UserDefaults.isBarber =  role == "SALON" ? true : false
                                    let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                                    UserDefaults.user = userString!
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }
                else if(statusCode == StatusCode.code202.rawValue){
                    // Account must be verified
                    successCallback?(false)
                    return
                }else{
                    // code400, code401, code409,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func signUp(params: [String: Any],
                       onSuccess successCallback: ((_ response: Bool) -> Void)?,
                       onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.signup.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code201.rawValue){
                    
                    successCallback?(true)
                    return
                }else{
                    // code400, code409, code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func verifyAccount(params: [String: Any],
                              onSuccess successCallback: ((_ response: Bool) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.verifyAccount.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    // Store Token & Refrtesh Token & User
                    // store Role
                    if let token = responseObject[JsonKeys.accessToken.rawValue].string{
                        UserDefaults.token = token
                        if let refreshToken = responseObject[JsonKeys.refreshToken.rawValue].string{
                            UserDefaults.refreshToken = refreshToken
                            if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                                
                                if let role = user[JsonKeys.role.rawValue]?.string{
                                    UserDefaults.isBarber =  role == "SALON" ? true : false
                                    let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                                    UserDefaults.user = userString!
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }else{
                    // code400, code404, code409,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func refreshToken(params: [String: Any],
                             onSuccess successCallback: ((_ response: Bool) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.refreshToken.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    
                    successCallback?(true)
                    return
                }else{
                    // code401, code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func resendCode(params: [String: Any],
                           onSuccess successCallback: ((_ message: String) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.resendCode.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code202.rawValue){
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }else{
                    // code401, code400, code404, code409 ,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func sendCode(params: [String: Any],
                           onSuccess successCallback: ((_ message: String) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.sendCode.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code202.rawValue || statusCode == StatusCode.code200.rawValue){
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }else{
                    // code401, code400, code404, code409 ,code500
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
            }
            failureCallback?(LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func forgotPassword(params: [String: Any],
                               onSuccess successCallback: ((_ response: Bool) -> Void)?,
                               onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.forgotPassword.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .post, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code202.rawValue){
                    
                    successCallback?(true)
                    return
                }else{
                    // code401, code400, code404, code409 ,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func resetPassword(params: [String: Any],
                              onSuccess successCallback: ((_ response: Bool) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.resetPassword.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .put, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    
                    successCallback?(true)
                    return
                }else{
                    // code401, code404, code409 ,code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func changePassword(params: [String: Any],
                               onSuccess successCallback: ((_ message: String) -> Void)?,
                               onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.changePassword.rawValue.toApiUrl()
        APICallManager.shared.createRequest(url, method: .put, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }else{
                    // code400, code401, code500
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                }
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func shopCategories(params: [String: String],
                               onSuccess successCallback: ((_ shops: [BarberShopModel], _ categories:[TagModel]) -> Void)?,
                               onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.getShopCategories.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    
                    if let dataArray = responseObject["salons"].array{
                        
                        let salonList = JsonHelper.getBarberShops(items: dataArray)
                        
                        if let categoryArray = responseObject["categories"].array{
                            let categoriesString = JSON(categoryArray).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                            UserDefaults.categories = categoriesString!
                            let categoryList = JsonHelper.getCategories(items: categoryArray)
                            successCallback?(salonList, categoryList)
                            return
                        }else{
                            successCallback?(salonList, [TagModel]())
                            return
                        }
                        
                    }
                }
                successCallback?([BarberShopModel](), [TagModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func updateAccount(idAccount: Int, params: [String: Any],
                              onSuccess successCallback: ((_ message: String) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.profile.rawValue + "/" + String(idAccount)
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                        //let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                        //UserDefaults.user = userString!
                        AppFunctions.updateConnectedUser(user: JsonHelper.getUser(item: user))
                    }
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                    
                }else{
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                    
                }
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func updateEmail(params: [String: Any],
                              onSuccess successCallback: ((_ message: String) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
       
        let url = apiBaseUrl + Endpoint.updateEmail.rawValue
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                        //let userString = JSON(user).rawString([.jsonSerialization : JSONSerialization.WritingOptions.prettyPrinted,.encoding : String.Encoding.utf8])
                        //UserDefaults.user = userString!
                        AppFunctions.updateConnectedUser(user: JsonHelper.getUser(item: user))
                    }
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                    
                }else{
                    
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?( message)
                    return
                    
                }
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    
    public func getFavorites(params: [String: String],
                             onSuccess successCallback: ((_ favorite: [BarberShopModel]) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.favorites.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["salons"].array{
                        
                        let favoriteList = JsonHelper.getBarberShops(items: dataArray)
                        successCallback?(favoriteList)
                        return
                    }
                }
                successCallback?([BarberShopModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getShops(params: [String: String],
                         onSuccess successCallback: ((_ favorite: [BarberShopModel]) -> Void)?,
                         onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.getShops.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["salons"].array{
                        
                        let shopList = JsonHelper.getBarberShops(items: dataArray)
                        successCallback?(shopList)
                        return
                    }
                }
                successCallback?([BarberShopModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func fetchBarberShop(params: [String: String],
                                onSuccess successCallback: ((_ favorite: [BarberShopModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) -> DataRequest {
        
        let url = apiBaseUrl + Endpoint.getShops.rawValue
        
       return APICallManager.shared.createRequestJSON(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["salons"].array{
                        
                        let shopList = JsonHelper.getBarberShops(items: dataArray)
                        successCallback?(shopList)
                        return
                    }
                }
                successCallback?([BarberShopModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func addToFavorite(params: [String: String],
                             onSuccess successCallback: ((_ response: String) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.favorites.rawValue
        
        APICallManager.shared.createRequest(url, method: .post, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code201.rawValue{
                    
                    successCallback?("")
                    return
                }else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func deleteFavorites(idBarber: Int,
                                onSuccess successCallback: ((_ response: String) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.favorites.rawValue + "/" + String(idBarber)
        
        APICallManager.shared.createRequest(url, method: .delete, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    successCallback?("")
                    return
                }else if statusCode == StatusCode.code404.rawValue{
                    
                    successCallback?("")
                    return
                }
                else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func uploadImageProfile(image: UIImage?, params: [String: Any],
                                   onSuccess successCallback: ((_ response: Bool) -> Void)?,
                                   onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.uploadImage.rawValue
        APICallManager.shared.createUploadRequest(url, method: .post, headers: AppFunctions.getHeader(), image: image, parameters: params,
                                                  onSuccess: { (responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let user = responseObject[JsonKeys.user.rawValue].dictionary{
                        AppFunctions.updateConnectedUser(user: JsonHelper.getUser(item: user))
                        successCallback?(true)
                        return
                    }
                }
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
        }, onFailure: { (errorMessage: String) -> Void in
            
            failureCallback?("")
        })
        
    }
    
    public func getServices(params: [String: String]?,
                            onSuccess successCallback: ((_ services: [ServiceModel]) -> Void)?,
                            onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.services.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["services"].array{
                        
                        let serviceList = JsonHelper.getServices(items: dataArray)
                        successCallback?(serviceList)
                        return
                    }
                }
                successCallback?([ServiceModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getReviews(params: [String: String],
                           onSuccess successCallback: ((_ review: [ReviewModel]) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.reviews.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["reviews"].array{
                        
                        let reviewList = JsonHelper.getReviews(items: dataArray)
                        successCallback?(reviewList)
                        return
                    }
                }
                successCallback?([ReviewModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getBarberShop(idShop : Int,
                              onSuccess successCallback: ((_ shop: BarberShopModel) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.getShops.rawValue + "/" + String(idShop)
        
        APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let shopObject = responseObject["salon"].dictionary{
                        let barberShop = JsonHelper.getBarberShop(item: shopObject)
                        successCallback?(barberShop)
                        return
                    }
                }
            }
            
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?( message)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func uploadImageBarber(image: UIImage?, params: [String: Any],
                                  onSuccess successCallback: ((_ successMessage: String, _ media: MediaModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.images.rawValue
        APICallManager.shared.createUploadRequest(url, method: .post, headers: AppFunctions.getHeader(), image: image, parameters: params, onSuccess: { (responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code201.rawValue{
                    if let mediaObj = responseObject[JsonKeys.image.rawValue].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let media = JsonHelper.getMedia(item: mediaObj)
                        successCallback?(message, media)
                        return
                    }
                }
            }
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?(message)
            return
            
        }, onFailure: { (errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
        
    }
    
    public func updateImageBarber(idImage : Int, image: UIImage?, params: [String: Any],
                                  onSuccess successCallback: ((_ successMessage: String, _ media: MediaModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.images.rawValue + "/" + String(idImage)
        APICallManager.shared.createUploadRequest(url, method: .put, headers: AppFunctions.getHeader(), image: image, parameters: params, onSuccess: { (responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let mediaObj = responseObject[JsonKeys.image.rawValue].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let media = JsonHelper.getMedia(item: mediaObj)
                        successCallback?(message, media)
                        return
                    }
                }
            }
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?(message)
            return
            
        }, onFailure: { (errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
        
    }
    
    public func removeImageBarber(idImage : Int,   onSuccess successCallback: ((_ message: String) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.images.rawValue + "/" + String(idImage)
        
        APICallManager.shared.createRequest(url, method: .delete, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }
            }
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?(message)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getMedias(onSuccess successCallback: ((_ medias: [MediaModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.images.rawValue
        
        
        APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let imageArray = responseObject["images"].array{
                        let medias = JsonHelper.getMedias(items: imageArray)
                        successCallback?(medias)
                        return
                    }
                }
            }
            
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?( message)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getMediasByShop(params: [String: String], onSuccess successCallback: ((_ medias: [MediaModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.images.rawValue
        
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let imageArray = responseObject["images"].array{
                        let medias = JsonHelper.getMedias(items: imageArray)
                        successCallback?(medias)
                        return
                    }
                }
            }
            let message = responseObject[JsonKeys.message.rawValue].stringValue
            failureCallback?(message)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func addService(params: [String: Any],
                           onSuccess successCallback: ((_ message : String, _ service: ServiceModel) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.services.rawValue
        
        APICallManager.shared.createRequest(url, method: .post, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code201.rawValue{
                    
                    if let serviceObject = responseObject["service"].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let service = JsonHelper.getService(item: serviceObject)
                        successCallback?(message, service)
                        return
                    }
                }
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func updateService(idService: Int, params: [String: Any],
                              onSuccess successCallback: ((_ message : String ,_ service: ServiceModel) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.services.rawValue + "/" + String(idService)
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["service"].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let service = JsonHelper.getService(item: dataArray)
                        successCallback?(message, service)
                        return
                    }
                }
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func deleteService(idService: Int,
                              onSuccess successCallback: ((_ message : String ,_ response: String) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.services.rawValue + "/" + String(idService)
        
        APICallManager.shared.createRequest(url, method: .delete, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message, "")
                    return
                    
                }else if statusCode == StatusCode.code404.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message, "")
                    return
                }
                else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func fetchCustomers(params: [String: String],
                               onSuccess successCallback: ((_ customers: [UserModel]) -> Void)?,
                               onFailure failureCallback: ((_ errorMessage: String) -> Void)?) -> DataRequest  {
        
        let url = apiBaseUrl + Endpoint.customers.rawValue
        
        return APICallManager.shared.createRequestJSON(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["customers"].array{
                        
                        let customerList = JsonHelper.getUsers(items: dataArray)
                        successCallback?(customerList)
                        return
                    }
                }
                successCallback?([UserModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func deleteCustomer(idCustomer: Int,
                               onSuccess successCallback: ((_ message : String ,_ response: String) -> Void)?,
                               onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.customers.rawValue + "/" + String(idCustomer)
        
        APICallManager.shared.createRequest(url, method: .delete, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message, "")
                    return
                    
                }
                else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func blockCustomer(idCustomer: Int,
                              onSuccess successCallback: ((_ message : String ,_ response: String) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.customers.rawValue + "/block/" + String(idCustomer)
        
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message, "")
                    return
                    
                }
                else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func unblockCustomer(idCustomer: Int,
                                onSuccess successCallback: ((_ message : String ,_ response: String) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.customers.rawValue + "/unblock/" + String(idCustomer)
        
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message, "")
                    return
                    
                }
                else{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func updateSchedules(params: [String: Any],
                                onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url =  Endpoint.updateSchedule.rawValue.toApiUrl()
        
        APICallManager.shared.createRequest(url, method: .put, parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if(statusCode == StatusCode.code200.rawValue){
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }else{
                    // code401, code404, code409 ,code500
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    failureCallback?(message)
                    return
                }
            }
            failureCallback?(LocalizationKeys.error_serveur.localized)
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getCategories(onSuccess successCallback: ((_ categories: [TagModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
                
                let url = apiBaseUrl + Endpoint.categories.rawValue
                
                APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
                    
                    if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                        if statusCode == StatusCode.code200.rawValue{
                            
                            if let dataArray = responseObject["categories"].array{
                                
                                let categorieList = JsonHelper.getCategories(items: dataArray)
                                successCallback?(categorieList)
                                return
                            }
                        }
                        successCallback?([TagModel]())
                        return
                        
                    }else{
                        
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        failureCallback?( message)
                        return
                    }
                    
                }, onFailure: {(errorMessage: String) -> Void in
                    failureCallback?( LocalizationKeys.error_serveur.localized)
                })
            }
    
    public func fetchOrders(params: [String: String],onSuccess successCallback: ((_ order: [OrderModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.orders.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let orderArray = responseObject["orders"].array{
                        
                        let orderList = JsonHelper.getOrders(items: orderArray)
                        successCallback?(orderList)
                        return
                    }
                }
                successCallback?([OrderModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func fetchOrdersByKeyword(params: [String: String],onSuccess successCallback: ((_ order: [OrderModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) -> DataRequest {
        
        let url = apiBaseUrl + Endpoint.ordersByKeyword.rawValue
        
        return APICallManager.shared.createRequestJSON(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let orderArray = responseObject["orders"].array{
                        
                        let orderList = JsonHelper.getOrders(items: orderArray)
                        successCallback?(orderList)
                        return
                    }
                }
                successCallback?([OrderModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func updateOrder(idOrder: Int, params: [String: Any],
                              onSuccess successCallback: ((_ message : String ,_ order: OrderModel) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.updateOrders.rawValue + "/" + String(idOrder)
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            DebugHelper.debug(responseObject)
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let orderObj = responseObject["order"].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let order = JsonHelper.getOrder(item: orderObj)
                        successCallback?(message, order)
                        return
                    }
                }
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func createOrder(params: [String: Any],
                              onSuccess successCallback: ((_ message : String ,_ clientSecret : String ,_ order: OrderModel) -> Void)?,
                              onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.createOrder.rawValue
        APICallManager.shared.createRequest(url, method: .post, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            DebugHelper.debug(responseObject)
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code201.rawValue{
                    
                    if let orderObj = responseObject["order"].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let clientSecret = responseObject[JsonKeys.clientSecret.rawValue].stringValue
                        
                        let order = JsonHelper.getOrder(item: orderObj)
                        successCallback?(message,clientSecret, order)
                        return
                    }
                }
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func fetchOrdersByDate(params: [String: String], onSuccess successCallback: ((_ order: [OrderModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.ordersByDate.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let orderArray = responseObject["orders"].array{
                        
                        let orderList = JsonHelper.getOrders(items: orderArray)
                        successCallback?(orderList)
                        return
                    }
                }
                successCallback?([OrderModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    

    public func fetchCards(onSuccess successCallback: ((_ cards: [CardModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.cards.rawValue
        
        APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let cardArray = responseObject["cards"].array{
                        
                        let cardList = JsonHelper.getCards(items: cardArray)
                        successCallback?(cardList)
                        return
                    }
                }
                successCallback?([CardModel]())
                return
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func addReview(params: [String: Any],
                           onSuccess successCallback: ((_ message : String, _ review: ReviewModel) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.reviews.rawValue
        
        APICallManager.shared.createRequest(url, method: .post, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code201.rawValue{
                    
                    if let reviewObject = responseObject["review"].dictionary{
                        let message = responseObject[JsonKeys.message.rawValue].stringValue
                        let review = JsonHelper.getReview(item: reviewObject)
                        successCallback?(message, review)
                        return
                    }
                }
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func removeCard(pm: String,
                           onSuccess successCallback: ((_ message : String) -> Void)?,
                           onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.cards.rawValue + "/" + pm
        
        APICallManager.shared.createRequest(url, method: .delete, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    let message = responseObject[JsonKeys.message.rawValue].stringValue
                    successCallback?(message)
                    return
                }
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
                
            }
            failureCallback?( LocalizationKeys.error_serveur.localized)
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func completeStripeAccount(params: [String: String],
                        onSuccess successCallback: ((_ url: String) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.completePaiementAccount.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let linkObj = responseObject["link"].dictionary{
                        
                        let url = linkObj["url"]!.stringValue
                        successCallback?(url)
                        return
                    }
                }
               
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
            failureCallback?(LocalizationKeys.error_serveur.localized)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getStripeAccount( onSuccess successCallback: ((_ completed: Bool) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.stripeAccount.rawValue
        
        APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let accountObj = responseObject["account"].dictionary{
                        
                        if let submitted = accountObj["details_submitted"]?.bool{
                            if let enabled = accountObj["charges_enabled"]?.bool{
                                if(submitted && enabled){
                                    successCallback?(true)
                                    return
                                }
                            }
                        }
                    }
                }
            }
            
            failureCallback?(LocalizationKeys.error_serveur.localized)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getAvailableSchedules(params: [String: String],
                        onSuccess successCallback: ((_ availableSchedules: [AvailableScheduleModel]) -> Void)?,
                                onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.availableSchedules.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let scheduleArray = responseObject["schedules"].array{
                        
                        let schedules = JsonHelper.getAvailableSchedules(items: scheduleArray)
                        successCallback?(schedules)
                        return
                    }
                }
               
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
            failureCallback?(LocalizationKeys.error_serveur.localized)
            return
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    public func setDeviceToken(idAccount: Int) {

        let params = ["deviceType": "IOS", "deviceToken": UserDefaults.deviceToken]
        let url = apiBaseUrl + Endpoint.profile.rawValue + "/" + String(idAccount)
        APICallManager.shared.createRequest(url, method: .put, headers: AppFunctions.getHeader(), parameters: params, onSuccess: {(responseObject: JSON) -> Void in
            //DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    UserDefaults.deviceTokenSent = UserDefaults.deviceToken
                    return
                }
                
            }
        }, onFailure: {(errorMessage: String) -> Void in
        })
    }
    
    public func getNotifications(params: [String: String],
                             onSuccess successCallback: ((_ notification: [NotificationModel]) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.notifications.rawValue
        
        APICallManager.shared.createRequest(AppFunctions.getUrlWithParams(url: url, parameters: params), method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    
                    if let dataArray = responseObject["notifications"].array{
                        
                        let notificationList = JsonHelper.getNotifs(items: dataArray)
                        successCallback?(notificationList)
                        return
                    }
                }
                successCallback?([NotificationModel]())
                return
                
            }else{
                
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?( message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?( LocalizationKeys.error_serveur.localized)
        })
    }
    
    public func getSettings(onSuccess successCallback: ((_ success: Bool) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = apiBaseUrl + Endpoint.settings.rawValue
        
        APICallManager.shared.createRequest(url, method: .get, headers: AppFunctions.getHeader(), parameters: nil, onSuccess: {(responseObject: JSON) -> Void in
            DebugHelper.debug(responseObject)
            if let statusCode = responseObject[JsonKeys.statusCode.rawValue].int{
                if statusCode == StatusCode.code200.rawValue{
                    if let settingObj = responseObject["settings"].dictionary{
                        
                        let salonFees = settingObj["salonFees"]?.double
                        let homeFees = settingObj["homeFees"]?.double
                        let stripePk = settingObj["stripePk"]?.string
                        
                        AppConstants.barberFees = salonFees!
                        AppConstants.homeFees = homeFees!
                        AppConstants.stripePublishableKey = stripePk!
                        successCallback?(true)
                        return
                    }
                }
                successCallback?(false)
                return
                
            }else{
                let message = responseObject[JsonKeys.message.rawValue].stringValue
                failureCallback?(message)
                return
            }
            
        }, onFailure: {(errorMessage: String) -> Void in
            failureCallback?(LocalizationKeys.error_serveur.localized)
        })
    }
    
    
}
