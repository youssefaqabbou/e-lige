//
//  BarberBookingView.swift
//  elige
//
//  Created by macbook pro on 11/11/2021.
//

import SwiftUI
import HCalendar

struct BarberBookingView: View {
    @StateObject var vmBooking = BarberBookingViewModel()
    @State var isCalendarView = true
    
    var showOrderDetail : (OrderModel) -> () = { order in }
    
    func queryChanged(to value: String) {
        let delay = 0.2
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            vmBooking.resetPagination()
            vmBooking.fetchOrders()
        }
      }
    let NC = NotificationCenter.default
    
    var body: some View {
        
        VStack(){
            
            HStack(spacing: 25){
                Button(action: {
                    vmBooking.fetchOrders(vmBooking.selectedDate)
                    withAnimation {
                        isCalendarView = true
                    }
                }, label: {
                    Text("Calendrier")
                        .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                    
                }).buttonStyle(PrimaryButtonStyle(bgColor: isCalendarView ? Color.primaryColor:  Color.lightGray, fgColor: isCalendarView ? Color.primaryTextLightColor:  Color.primaryTextColor,shdowColor : Color.bgViewColor, raduis: 50))
                Button(action: {
                    vmBooking.resetPagination()
                    vmBooking.fetchOrders()
                    withAnimation {
                        isCalendarView = false
                    }
                }, label: {
                    Text("Liste")
                        .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                    
                }).buttonStyle(PrimaryButtonStyle(bgColor: !isCalendarView ? Color.primaryColor:  Color.lightGray, fgColor: !isCalendarView ? Color.primaryTextLightColor:  Color.primaryTextColor,shdowColor : Color.bgViewColor, raduis: 50))

            }.padding(.vertical, AppConstants.viewNormalMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
            ZStack(){
                VStack(spacing: 0){
                    
                    HCalendarView(config: vmBooking.hCalendarConfig, calendarStyle: vmBooking.hCalendarStyle) { selectedDate in
                        withAnimation(.spring()) {
                            vmBooking.selectedDate = selectedDate
                            DebugHelper.debug("On date selected",selectedDate.toString())
                            EventUtils.onDayChanged()
                            vmBooking.removeAllEvent()
                            vmBooking.fetchOrders(selectedDate)
                        }
                    }
                    Spacer(minLength: 0)
                    
                    ZStack(){
                        VStack(spacing: 20){
                            //Spacer()
                            CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_order_shop_empty_title.localized, message: LocalizationKeys.list_order_shop_empty_message.localized)
                            Spacer()
                            Spacer()
                            Spacer()
                        }.opacity(vmBooking.events.isEmpty ? 1 : 0)
                        
                        TimeLineView(config: vmBooking.timeLineConfig, style: vmBooking.timeLineStyle, date: $vmBooking.selectedDate, events: $vmBooking.events) { event in
                            let order = event.eventInfo!["order"] as! OrderModel
                                                       
                            EventItemView(order: .constant(order))
                            
                        } onEventTaped: { event in
                            let order = event.eventInfo!["order"] as! OrderModel
                            
                            showOrderDetail(order)
                        }
                        .opacity(vmBooking.events.isEmpty ? 0 : 1)
                    }
                }
                .opacity(isCalendarView ? 1 : 0)
                
                VStack(){
                    HStack{
                        CustomSearchTextField(placeholder: "Recherche réservation", text: $vmBooking.keyword.onChange(queryChanged))
                                    .disableAutocorrection(true)
                        
                        Image.iconSearch
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(Color.secondaryTextColor)
                            .padding(AppConstants.viewExtraMargin)
                                   
                    }.background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                    
                    CollectionLoadingView(loadingState: vmBooking.loadingAllOrderState) {
                        ListShimmeringView(count: 15){
                            ReservationItemHolderView()
                        }
                    } content: {
                        
                        GeometryReader { geometry in
                            ScrollView(.vertical, showsIndicators: false){
                           
                                LazyVStack(spacing: 20) {
                                    ForEach(vmBooking.allOrderList.indices, id: \.self) {index in   // << here !
                                    //ForEach(0..<vmLanding.barberShopList.count, id: \.self){ index in
                                        let order = vmBooking.allOrderList[index]
                                        CustomerOrderItemView(order: .constant(order), indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY )
                                            .onTapGesture {
                                                showOrderDetail(order)
                                            }.onAppear {
                                                vmBooking.loadMoreIfNeeded(currentItem: order)
                                            }
                                       
                                    }
                                }.padding(.bottom, AppConstants.viewNormalMargin)
                            }.padding(.top, AppConstants.viewExtraMargin)
                        }
                        /*
                        ScrollView(.vertical, showsIndicators: false){
                            
                            LazyVStack(spacing: 20){
                                
                                ForEach(vmBooking.allOrderList, id: \.id) { order in
                                
                                    CustomerOrderItemView(order: .constant(order))
                                        .onTapGesture {
                                            showOrderDetail(order)
                                        }.onAppear {
                                            vmBooking.loadMoreIfNeeded(currentItem: order)
                                        }
                                }
                            }.padding(.bottom, AppConstants.viewNormalMargin)
                        }
                        .padding(.top, AppConstants.viewExtraMargin)
                        */
                    } empty: {
                        VStack(spacing: 20){
                            Spacer()
                            CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_order_shop_empty_title.localized, message: LocalizationKeys.list_order_shop_empty_message.localized)
                            Spacer()
                        }
                        
                    } error: {
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_order_shop_empty_title.localized, message: LocalizationKeys.list_order_shop_empty_message.localized)
                    }
                    
                    
                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    .opacity(isCalendarView ? 0 : 1)
            }
        }.background(Color.bgViewColor)
        
        .onAppear {
            vmBooking.fetchOrders(vmBooking.selectedDate)
            self.NC.addObserver(forName: NSNotification.orderUpdated, object: nil, queue: nil,
                                using: self.orderUpdated)
        }
        
    }
    
    func orderUpdated(_ notification: Notification) {
        let orderUpdated = notification.userInfo!["order"] as! OrderModel
        vmBooking.updateOrder(order : orderUpdated)
    }
}

struct BarberBookingView_Previews: PreviewProvider {
    static var previews: some View {
        BarberBookingView()
    }
}
