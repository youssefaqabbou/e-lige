//
//  BarberBookingViewModel.swift
//  elige
//
//  Created by macbook pro on 11/11/2021.
//

import Foundation
import HCalendar
import SwiftUI
import Alamofire

class BarberBookingViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var selectedDate = DateUtils.getTodayDateWithTZ()
    @Published var events = [EventModel]()
    @Published var orderList = [OrderModel]()
    @Published var allOrderList = [OrderModel]()

    @Published var timeLineConfig = TimeLineConfig()
    @Published var timeLineStyle = customTimeLineStyle
    
    @Published var hCalendarConfig = HCalendarConfig()
    @Published var hCalendarStyle = customCalendarStyle
    
    @Published var keyword = ""
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingMore = false
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    @Published private(set) var loadingAllOrderState: CollectionLoadingState = .loading
    
    var ordersRequest : DataRequest!
    
    init() {
        timeLineConfig.nowLineEnabled = true
        timeLineConfig.isScaleEnabled = false
        timeLineConfig.minScale = 1
        timeLineConfig.maxScale = 5
        timeLineConfig.use24Format = true
        timeLineConfig.adaptTimeToEvent = true
        

        hCalendarConfig.showPastWeek = true
        hCalendarConfig.weekdaySymbolFormat = .short
        hCalendarConfig.firstWeekday = 2
        hCalendarConfig.showToday = true
        hCalendarConfig.locale = Locale(identifier: "Fr")
        
    }
    
    public static var customTimeLineStyle : TimeLineStyle  {
        var timeLineStyle = TimeLineStyle()
        timeLineStyle.background = Color.bgViewColor
        timeLineStyle.border = Color.clear
        
        timeLineStyle.dividerColor = Color.black.opacity(0.5)
        timeLineStyle.eventBackground = Color.bgViewColor
        
        timeLineStyle.timeColor = Color.primaryTextColor
        timeLineStyle.timeBackground = Color.bgViewColor
        
        timeLineStyle.timeSize = 12
        timeLineStyle.timeFont = FontStyle.brownStd.rawValue
        
        timeLineStyle.timeWidth = 50
        timeLineStyle.eventHeight = 180
        
        return timeLineStyle
    }
    
    public static var customCalendarStyle : HCalendarStyle  {
        var calendarStyle = HCalendarStyle()
        
        calendarStyle.background = Color.bgViewColor
        calendarStyle.border = Color.bgViewColor
        
        var dayStyle = HCalendarDayStyle()
        dayStyle.current = Color.primaryTextColor
        dayStyle.future = Color.primaryTextColor
        dayStyle.past = Color.gray
        dayStyle.selectedBg = Color.primaryTextColor
        dayStyle.selectedFg = Color.primaryTextLightColor
        
        dayStyle.selectedBg = Color.primaryTextColor
        dayStyle.selectedRadius = 10
        dayStyle.fontSize = 14
        calendarStyle.day = dayStyle
        
        var weekStyle =  HCalendarWeekStyle()
        weekStyle.foreground = Color.primaryTextColor
        weekStyle.font = FontStyle.brownStd.rawValue
        weekStyle.fontSize = 14
        weekStyle.forceUpperCase = true
        calendarStyle.weekSymbol = weekStyle
        
        var monthStyle = HCalendarMonthStyle()
        monthStyle.foreground =  Color.primaryTextColor
        //monthStyle.foreground = Color.primaryTextColor
        monthStyle.font = FontStyle.brownStd.rawValue
        monthStyle.fontSize = 16
        
        var navStyle =  HCalendarNavStyle()
        navStyle.navIconW = 5
        navStyle.navIconH = 8
        navStyle.navImageH = 25
        navStyle.navImageW = 25
        navStyle.navBg = Color.primaryTextColor
        navStyle.navStrokeColor = Color.primaryTextColor
        navStyle.navFg = Color.bgViewColor
        navStyle.navRadius = .infinity
        
        navStyle.showToday = true
        navStyle.todayStrokeColor = Color.primaryTextColor
        calendarStyle.nav =  navStyle
        
        return calendarStyle
    }
    
    func fetchOrders(){
        
        guard !isLoadingMore && canLoadMorePages else {
          return
        }
        
        if(currentPage == 0){
            self.loadingAllOrderState = .loading
        }else{
            isLoadingMore = true
        }
        
        let params = ["page": String(currentPage), "size" : String(size), "keyword" : keyword]
        ordersRequest = apiService.fetchOrdersByKeyword(params: params, onSuccess: { orders in
            if orders.count == 0 {
                self.canLoadMorePages = false
            }
            if( self.currentPage == 0){
                self.allOrderList = orders
            }else{
                self.allOrderList.append(contentsOf: orders)
            }
           
            self.currentPage += 1
            self.isLoadingMore = false
            if(self.allOrderList.isEmpty){
                self.loadingAllOrderState = .empty
            }else{
                self.loadingAllOrderState = .loaded
            }
        }, onFailure: { errorMessage in
            self.canLoadMorePages = false
            self.isLoadingMore = false
            if(self.allOrderList.isEmpty){
                self.loadingAllOrderState = .empty
            }else{
                self.loadingAllOrderState = .loaded
            }
        })
    }
    
    func resetPagination(){
        self.loadingAllOrderState = .loading
        isLoadingMore = false
        currentPage = 0
        canLoadMorePages = true
        self.allOrderList.removeAll()
        if(ordersRequest != nil){
            ordersRequest.cancel()
        }
    }
    
    func fetchOrders(_ date : Date){
        self.loadingState = .loading
        let params = ["date": date.toString(format: "yyyy-MM-dd", isConvertZone: false)]
        
        apiService.fetchOrdersByDate(params: params, onSuccess: { orders in
            //DebugHelper.debug(orders.count)
            if(orders.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
                self.orderList = orders
                self.fillEvents()
            }
            
        }, onFailure: { errorMessage in
            self.loadingState = .empty
        })
    }

    func fillEvents(){
        removeAllEvent()
        for order in orderList {
            events.append(convertOrderToEvent(order))
        }
        EventUtils.onEventAdded()
    }
    
    func convertOrderToEvent(_ order : OrderModel) -> EventModel{
        //let startH = Int.random(in: timeLineConfig.startHour < timeLineConfig.endHour ?  timeLineConfig.startHour ..< timeLineConfig.endHour :  timeLineConfig.endHour ..< timeLineConfig.startHour)
       
        var event = EventModel()
        event.id = order.id
        event.eName = order.customer.fullname
        event.eStartTime = order.orderDate
        event.eEndTime = order.orderDate.addMinutes(minutes: order.duration)
        event.eventInfo = ["order" :  order]
        //DebugHelper.debug("Event", event)
        
        return event
    }
    
    func removeAllEvent()  {
        events.removeAll()
        EventUtils.onEventRemovedAll()
    }
    
    func loadMoreIfNeeded(currentItem item: OrderModel?) {
        guard let item = item else {
            fetchOrders()
          return
        }

        let thresholdIndex = allOrderList.index(allOrderList.endIndex, offsetBy: AppConstants.offsetPagination)
        if allOrderList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            fetchOrders()
        }
      }

    func updateOrder(order: OrderModel){
               
        let index = getOrder(order.id, orders : allOrderList)
        if(index != -1){
            allOrderList[index].status = order.status
            DebugHelper.debug("allOrderList" , allOrderList[index].id, allOrderList[index].status)
        }
        
        let indexEvent = getOrder(order.id, orders : orderList)
        if(indexEvent != -1){
            orderList[indexEvent].status = order.status
            DebugHelper.debug("orderList", orderList[indexEvent].id, orderList[indexEvent].status)
            fillEvents()
        }
        
        objectWillChange.send()
    }
    
    func getOrder(_ id : Int , orders : [OrderModel]) -> Int{
       
        for (index, value) in orders.enumerated(){
            if value.id == id{
                return index
            }
        }
        return -1
    }
}
