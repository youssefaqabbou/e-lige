//
//  BookingDetailView.swift
//  elige
//
//  Created by macbook pro on 6/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct BookingDetailView: View {
    
    @StateObject var vmBookingDetail = BookingDetailViewModel()
    @Binding var showOrderDetail : Bool
    @Binding var order : OrderModel
    @State var showAction : Bool = true
    
    @State var showCancelConfirmation = false
    @State var showAcceptConfirmation = false
    @State var showFinishConfirmation = false
    @State var showNoShowConfirmation = false
    
    
    var body: some View {
        
        VStack(alignment: .leading){
            
            HStack(){
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            showOrderDetail = false
                        }
                    }
                
                Spacer()
                
                Text("Réservation")
                    .textStyle(TitleStyle())
                
                Spacer()
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).hidden()
            }.padding(.bottom, AppConstants.viewSmallMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
            ScrollView(.vertical, showsIndicators: false){
                
                HStack{
                    
                    Spacer()
                    
                    VStack(spacing: 10){
                        
                        WebImage(url: URL(string: vmBookingDetail.order.customer.image.url))
                            .placeholder {
                                Image.placeholderUser.resizable()
                            }
                            .resizable() //
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 100, height: 100)
                            .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                            .padding(.trailing, AppConstants.viewNormalMargin)
                        
                        
                        Text(vmBookingDetail.order.customer.fullname)
                            .font(.system(weight: .regular, size: 24))
                            .foregroundColor(Color.primaryTextColor)
                        
                        Text(vmBookingDetail.order.customer.phone)
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.secondaryTextColor)
                        
                    }
                    
                    Spacer()
                    
                }.padding(.top)
                
                VStack(spacing: 5){
                    
                    Text(order.orderDate.time())
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                    
                    HStack(spacing: 5){
                        
                        Image.iconTime
                            .resizable()
                            .frame(width: 18, height : 18)
                        /*
                        Text("Réserver :")
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(Color.secondaryTextColor)
                        */
                        Text(order.orderDate.toString(format: "HH:mm", isConvertZone: false))
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    HStack(spacing: 5){
                        if(order.orderLocation == ServiceLocation.home.rawValue ){
                            Image.iconBarberDomicile
                                .resizable()
                                .renderingMode(.template)
                                .colorMultiply(Color.gray)
                                .frame(width: 18, height : 18)
                                .foregroundColor(Color.gray)
                        }else{
                            Image.iconBarberShop
                                .resizable()
                                .renderingMode(.template)
                                .colorMultiply(Color.gray)
                                .frame(width: 18, height : 18)
                                .foregroundColor(Color.gray)
                        }
                        
                        Text(order.orderLocation == ServiceLocation.home.rawValue ? "À domicile" : "Salon" )
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                }
                .padding(.vertical, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
                VStack(spacing: 10){
                    ForEach(vmBookingDetail.order.orderLines, id: \.id) { orderLine in
                        OrderLinesItem(orderLine: .constant(orderLine))
                    }
                }
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
            VStack(){
                VStack(spacing: 10){
                    
                    HStack(){
                        
                        Text("Total des prestations").font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(order.subTotal.toPrice()).font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    HStack(){
                        
                        Text("Frais").font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(order.fees.toPrice()).font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    HStack(){
                        
                        Text("Total").font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(order.totalPrice.toPrice()).font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                    }
                }
                .padding(.bottom, AppConstants.viewNormalMargin)
                
                if(showAction){
                    HStack{
                        
                        Spacer()
                        
                        if vmBookingDetail.order.status == OrderStatus.inProgress.rawValue {
                            
                            CustomLoadingButton(isLoading: $vmBookingDetail.isCanceledLoading, text: "Annuler", btnStyle: PrimaryButtonStyle(bgColor: Color.lightGray, fgColor: Color.primaryTextColor, padding: 10), height: 40, action: {
                                withAnimation {
                                    showCancelConfirmation = true
                                }
                            })
                            
                            Spacer(minLength: 20)
                            
                            CustomLoadingButton(isLoading: $vmBookingDetail.isValidateLoading, text: "Confirmer", btnStyle: PrimaryButtonStyle(padding: 10), height: 40, action: {
                                withAnimation {
                                    showAcceptConfirmation = true
                                }
                            })
                            
                        }
                        else if vmBookingDetail.order.status == OrderStatus.validate.rawValue {
                            
                            CustomLoadingButton(isLoading: $vmBookingDetail.isFinishedLoading, text: "Absent", btnStyle: PrimaryButtonStyle(bgColor: Color.lightGray, fgColor: Color.primaryTextColor, padding: 10), height: 40, action: {
                                withAnimation {
                                    showNoShowConfirmation = true
                                }
                            })
                            
                            Spacer(minLength: 20)
                            
                            CustomLoadingButton(isLoading: $vmBookingDetail.isNoShowLoading, text: "Terminer", btnStyle: PrimaryButtonStyle(padding: 10), height: 40, action: {
                                withAnimation {
                                    showFinishConfirmation = true
                                }
                            })
                            
                        }
                        
                        Spacer()
                        
                    }
                    .padding(.bottom, AppConstants.viewNormalMargin)
                }
               
            }
            //.padding(.bottom, AppConstants.viewVeryExtraMargin)
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.topLeft, .topRight], radius: 15)).shadow(color: .tabBarShadowColor, radius: 5, x: 0.0, y: -5))
            
        }
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            //vmCustomerDetail.fetchOrders()
            DebugHelper.debug("onAppear", order.status)
            vmBookingDetail.order = order
        }
        .bottomSheet(isPresented: $showCancelConfirmation, height: 400, topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertCancel, title: LocalizationKeys.title_cancel_order.localized, message:LocalizationKeys.message_cancel_order.localized, positiveAction: {
                vmBookingDetail.updateOrder(status:OrderStatus.canceled.rawValue ,idOrder: order.id) {updatedOrder in
                    //order = updatedOrder
                    withAnimation {
                        showCancelConfirmation = false
                    }
                }
                
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showCancelConfirmation = false
                }
            })
        }
        .bottomSheet(isPresented: $showAcceptConfirmation, height: 400, topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertConfirm, title: LocalizationKeys.title_confirm_order.localized, message: LocalizationKeys.message_confirm_order.localized, positiveAction: {
                vmBookingDetail.updateOrder(status:OrderStatus.validate.rawValue ,idOrder: order.id) { updatedOrder in
                    //order = updatedOrder
                    withAnimation {
                        showAcceptConfirmation = false
                    }
                }
                
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showAcceptConfirmation = false
                }
            })
        }
        .bottomSheet(isPresented: $showFinishConfirmation, height: 400, topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertConfirm, title: LocalizationKeys.title_finish_order.localized, message: LocalizationKeys.message_finish_order.localized, positiveAction: {
                vmBookingDetail.updateOrder(status:OrderStatus.finished.rawValue ,idOrder: order.id) { updatedOrder in
                    withAnimation {
                        showFinishConfirmation = false
                    }
                }
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showFinishConfirmation = false
                }
            })
        }
        .bottomSheet(isPresented: $showNoShowConfirmation, height: 400, topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertCancel, title: LocalizationKeys.title_no_show_order.localized, message: LocalizationKeys.message_no_show_order.localized, positiveAction: {
                vmBookingDetail.updateOrder(status:OrderStatus.noShow.rawValue ,idOrder: order.id) { updatedOrder in
     
                    withAnimation {
                        showNoShowConfirmation = false
                    }
                }
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showNoShowConfirmation = false
                }
            })
        }
    }
}

struct BookingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        BookingDetailView(showOrderDetail: .constant(false), order: .constant(FakeData.getOrderData(id: 1)) )
    }
}
