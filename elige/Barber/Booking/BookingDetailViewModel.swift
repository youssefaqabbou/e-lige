//
//  BookingDetailViewModel.swift
//  elige
//
//  Created by user196417 on 12/21/21.
//

import Foundation

class BookingDetailViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var isValidateLoading = false
    @Published var isCanceledLoading = false
    @Published var isFinishedLoading = false
    @Published var isNoShowLoading = false
    
    @Published var order = OrderModel()
    
    func updateOrder(status: String, idOrder: Int, onSuccess: @escaping ( OrderModel) -> Void) {
        if(status == OrderStatus.canceled.rawValue){
            isCanceledLoading = true
        }else if (status == OrderStatus.validate.rawValue){
            isValidateLoading = true
        }else if (status == OrderStatus.finished.rawValue){
            isFinishedLoading = true
        }else if (status == OrderStatus.noShow.rawValue){
            isNoShowLoading = true
        }
        
        let params = ["status": status] as [String : Any]
        
        apiService.updateOrder(idOrder: idOrder, params: params, onSuccess: { message, order in
            self.order = order
            AppFunctions.showSnackBar(status: .success , message: message)
            onSuccess(order)
            self.isValidateLoading = false
            self.isCanceledLoading = false
            self.isFinishedLoading = false
            self.isNoShowLoading = false
            NotificationHelper.postOrder(order: order)
        }, onFailure: { errorMessage in
            
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            self.isValidateLoading = false
            self.isCanceledLoading = false
            self.isFinishedLoading = false
            self.isNoShowLoading = false
        })
    }
    
}
