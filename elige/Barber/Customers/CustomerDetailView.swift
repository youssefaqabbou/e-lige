//
//  CustomerDetailView.swift
//  elige
//
//  Created by user196417 on 12/6/21.
//

import SwiftUI
import SDWebImageSwiftUI
import Kingfisher

struct CustomerDetailView: View {
    
    @StateObject var vmCustomerDetail = CustomerDetailViewModel()
    
    @Binding var showCustomerDetail : Bool
    
    @State var customer : UserModel
    
    @State var showDeleteConfirmation = false
    @State var showBlockConfirmation = false
    @State var showUnblockConfirmation = false
    
    @State var showOrderDetail = false
    @State var selectedOrder = OrderModel()
    
    @State var isAnimationLoaded = false
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading){
                
                HStack(){
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showCustomerDetail = false
                            }
                        }
                    
                    Spacer()
                    
                    Text("Informations client")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Menu(content: {
                        Button(action: {
                            withAnimation {
                                if vmCustomerDetail.customer.isBlocked {
                                    showUnblockConfirmation = true
                                }else{
                                    showBlockConfirmation = true
                                }
                                
                            }
                            
                        }, label: {
                            
                                Text(vmCustomerDetail.customer.isBlocked ? "Débloquer" :"Bloquer" )
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                           
                        })
                        Button(action: {
                            withAnimation {
                                showDeleteConfirmation = true
                            }
                        }, label: {
                            
                                Text("Supprimer")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                            
                        })
                        
                    }, label: {
                        Image.iconMenuDots
                            .resizable()
                            .frame(width: 25, height: 25)
                    })
                    
                }.padding(.bottom, AppConstants.viewSmallMargin)
                
                //ScrollView(.vertical, showsIndicators: false){
             
                HStack{
                    
                    Spacer()
                    
                   
                    VStack(spacing: 10){
                        KFImage.url(URL(string: vmCustomerDetail.customer.image.url)).placeholder({
                            Image.placeholderUser.resizable()
                        })
                        .fade(duration: 0.25)
                        .resizable() //
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                        /*
                        WebImage(url: URL(string: vmCustomerDetail.customer.image.url))
                            .placeholder {
                                Image.placeholderUser.resizable()
                            }
                            .resizable() //
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 100, height: 100)
                            .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                         */
                        
                        Text(vmCustomerDetail.customer.fullname)
                            .font(.system(weight: .regular, size: 24))
                            .foregroundColor(Color.primaryTextColor)
                        
                        Text(vmCustomerDetail.customer.phone)
                            .font(.system(weight: .regular, size: 12))
                            .foregroundColor(Color.secondaryTextColor)
                        
                    }
                    
                    Spacer()
                    
                }.padding(.top)
                
                //                    HStack{
                //
                //                        Spacer()
                //
                //                        Button(action: {
                //                            withAnimation {
                //                                if vmCustomerDetail.customer.isBlocked {
                //                                    showUnblockConfirmation = true
                //                                }else{
                //                                    showBlockConfirmation = true
                //                                }
                //
                //                            }
                //
                //                        }, label: {
                //
                //                            VStack{
                //
                //                                Image.iconBlock
                //                                             .resizable()
                //                                             .frame(width: 25, height: 25)
                //
                //                                Text(vmCustomerDetail.customer.isBlocked ? "Débloquer" :"Bloquer" )
                //                                    .font(.system(weight: .regular, size: 12))
                //                                    .foregroundColor(Color.secondaryTextColor)
                //                            }
                //
                //                        }).frame(width: 80, height: 80)
                //                            .background(Color.lightGray.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                //
                //                        Spacer()
                //
                //                        Button(action: {
                //                            withAnimation {
                //                                showDeleteConfirmation = true
                //                            }
                //                        }, label: {
                //
                //                            VStack{
                //
                //                                Image.iconDelete
                //                                         .resizable()
                //                                         .frame(width: 25, height: 25)
                //
                //                                Text("Supprimer")
                //                                    .font(.system(weight: .regular, size: 12))
                //                                    .foregroundColor(Color.secondaryTextColor)
                //                            }
                //
                //                        }).frame(width: 80, height: 80)
                //                            .background(Color.lightGray.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                //
                //                        Spacer()
                //
                //                    }.padding(.top, AppConstants.viewExtraMargin)
                
                VStack(alignment: .leading){
                    Text("Réservations passées")
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                    Divider().frame(height: 3, alignment: .center)
                        .background(Color.lightGray)
                }.padding(.vertical, AppConstants.viewExtraMargin)
                
                GeometryReader { geometry in
                    ScrollView(.vertical, showsIndicators: false){
                   
                        LazyVStack(spacing: 20) {
                            ForEach(vmCustomerDetail.customer.orders.indices, id: \.self) {index in   // << here !
                            //ForEach(0..<vmLanding.barberShopList.count, id: \.self){ index in
                                let order = vmCustomerDetail.customer.orders[index]
                                CustomerOrderItemView(order: .constant(order), showName : false, indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY )
                                    .onTapGesture {
                                        withAnimation {
                                            showOrderDetail = true
                                            selectedOrder = order
                                        }
                                    }
                               
                            }
                        }
                    }.padding(.bottom, AppConstants.viewNormalMargin)
                }
                
                /*
                VStack(spacing: 15){
                    ForEach(vmCustomerDetail.customer.orders, id: \.id) { order in
                        CustomerOrderItemView(order: .constant(order), showName : false)
                            .onTapGesture {
                                withAnimation {
                                    showOrderDetail = true
                                    selectedOrder = order
                                }
                            }
                    }
                }*/
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            //.padding(.bottom, AppConstants.viewNormalMargin)
            
            if(showOrderDetail){
                BookingDetailView(showOrderDetail: $showOrderDetail, order: .constant(selectedOrder), showAction : false).transition(.move(edge: .trailing)).animation(.linear, value: showOrderDetail).zIndex(1)
            }
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            vmCustomerDetail.customer = customer
        }
        .bottomSheet(isPresented: $showDeleteConfirmation, height: 400,topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDelete, title:  LocalizationKeys.title_remove_user.localized, message: LocalizationKeys.message_remove_user.localized, positiveAction: {
                withAnimation {
                    showDeleteConfirmation = false
                    vmCustomerDetail.deleteCustomer(customerId: customer.id)
                }
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showDeleteConfirmation = false
                }
            })
        }
        .bottomSheet(isPresented: $showBlockConfirmation, height: 400,topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertBlock, title: LocalizationKeys.title_block_user.localized, message: LocalizationKeys.message_block_user.localized, positiveAction: {
                withAnimation {
                    showBlockConfirmation = false
                    vmCustomerDetail.blockCustomer(customerId: customer.id)
                }
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showBlockConfirmation = false
                }
            })
        }
        
        .bottomSheet(isPresented: $showUnblockConfirmation, height: 400,topBarCornerRadius : 45, showTopIndicator : false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertBlock, title: LocalizationKeys.title_unblock_user.localized, message: LocalizationKeys.message_unblock_user.localized, positiveAction: {
                withAnimation {
                    showUnblockConfirmation = false
                    vmCustomerDetail.unblockCustomer(customerId: customer.id)
                }
            }, isNegative: true,  negativeAction: {
                withAnimation {
                    showUnblockConfirmation = false
                }
            })
        }
    }
}

struct CustomerDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerDetailView(showCustomerDetail: .constant(false), customer: FakeData.getUserData(id: 1))
    }
}
