//
//  CustomerDetailViewModel.swift
//  elige
//
//  Created by user196417 on 12/6/21.
//

import Foundation

class CustomerDetailViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var customer = UserModel()
    
    @Published var customerId: Int = 0
    
    func deleteCustomer(customerId: Int) {
       
        apiService.deleteCustomer(idCustomer: customerId, onSuccess: { message, response in
            
            AppFunctions.showSnackBar(status: .success , message: message)
            
        }, onFailure: { errorMessage in
            
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }
    
    func blockCustomer(customerId: Int) {
        
        apiService.blockCustomer(idCustomer: customerId, onSuccess: { message, response in
            
            AppFunctions.showSnackBar(status: .success , message: message)
            self.customer.isBlocked = true
            self.objectWillChange.send()
        }, onFailure: { errorMessage in
            
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }
    
    func unblockCustomer(customerId: Int) {
        
        apiService.unblockCustomer(idCustomer: customerId, onSuccess: { message, response in
            self.customer.isBlocked = false
            AppFunctions.showSnackBar(status: .success , message: message)
            self.objectWillChange.send()
        }, onFailure: { errorMessage in
            
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }

}
