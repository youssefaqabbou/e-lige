//
//  CustomerListView.swift
//  elige
//
//  Created by macbook pro on 29/11/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct CustomerListView: View {
    @StateObject var vmCustomer = CustomerListViewModel()
    
    var showCustomerDetail : (UserModel) -> () = { user in }
    
    func queryChanged(to value: String) {
        let delay = 0.2
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            vmCustomer.resetPagination()
            vmCustomer.fetchCustomers()
        }
    }
    
    
    var body: some View {
        
        VStack(){
            
            HStack{
                CustomSearchTextField(placeholder: "Recherche", text: $vmCustomer.keyword.onChange(queryChanged))
                    .disableAutocorrection(true)
                
                
                ZStack(){
                    Image.iconSearch
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(Color.secondaryTextColor)
                        .padding(AppConstants.viewExtraMargin)
                        .opacity((vmCustomer.keyword.isEmpty || vmCustomer.loadingState != .loading ) ? 1 : 0)
                    
                    ProgressView()
                        .frame(width: 25, height: 25)
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.primaryColor))
                        .opacity((!vmCustomer.keyword.isEmpty && vmCustomer.loadingState == .loading ) ? 1 : 0)
                }
                
                
            }.background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                .padding(.vertical, AppConstants.viewNormalMargin)
            
            VStack(spacing: 20){
                CollectionLoadingView(loadingState: vmCustomer.loadingState) {
                    ListShimmeringView(count: 15){
                        CustomerItemHolderView()
                    }
                } content: {
                    
                    ScrollView(.vertical, showsIndicators: false){
                        LazyVStack(spacing: 15){
                            ForEach(vmCustomer.customerList, id: \.id) { item in
                                
                                if(!item.items.isEmpty){
                                    VStack(spacing: 10){
                                        HStack(){
                                            
                                            Circle().fill(Color.lightGray)
                                                .frame(width: 40, height: 40, alignment: .center)
                                                .overlay(
                                                    Text(item.header.uppercased())
                                                        .font(.system(weight: .regular, size: 20))
                                                        .foregroundColor(Color.primaryTextColor)
                                                )
                                            
                                            Divider().frame(height: 3)
                                                .frame(maxWidth: .infinity)
                                                .background(Color.lightGray)
                                            
                                        }
                                        
                                        VStack(spacing: 8){
                                            ForEach(item.items, id: \.id) { customer in
                                                CustomerItemView(customer: customer)
                                                    .onTapGesture {
                                                        showCustomerDetail(customer)
                                                    }
                                                    .onAppear {
                                                        vmCustomer.loadMoreIfNeeded(currentItem: item)
                                                    }
                                            }
                                        }.padding(.leading, 45)
                                    }
                                }
                            }
                        }
                        .padding(.bottom, AppConstants.viewNormalMargin)
                    }
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_customer_empty_title.localized, message: LocalizationKeys.list_customer_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_customer_empty_title.localized, message: LocalizationKeys.list_customer_empty_message.localized)
                }
                
            }
            //.padding(.bottom, AppConstants.viewNormalMargin)
            
        }.background(Color.bgViewColor)
            //.padding(.bottom, AppConstants.viewVeryExtraMargin)
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .onAppear {
                vmCustomer.fetchCustomers()
            }
        
    }
}

struct CustomerListView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerListView()
    }
}
