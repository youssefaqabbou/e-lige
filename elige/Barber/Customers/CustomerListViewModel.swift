//
//  CustomerListViewModel.swift
//  elige
//
//  Created by macbook pro on 29/11/2021.
//

import Foundation
import Alamofire
import HCalendar

class CustomerListViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var customerList = [ExpendableUserModel]()
    @Published var userList = [UserModel]()

    @Published var keyword: String = ""
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingMore = false
    //@Published var isLoading = false
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    var searchRequest : DataRequest!
    
    func fetchCustomers(){
        guard !isLoadingMore && canLoadMorePages else {
          return
        }
        
        if(currentPage == 0){
            //isLoading = true
            loadingState = .loading
        }else{
            isLoadingMore = true
        }
        
        let params = ["page": String(currentPage), "size": String(size), "keyword": keyword]
        
        searchRequest = apiService.fetchCustomers(params: params) { customers in
            DebugHelper.debug("onSuccess")
            if  customers.count == 0 {
                self.canLoadMorePages = false
            }
            if( self.currentPage == 0){
                self.userList = customers
            }else{
                self.userList.append(contentsOf: customers)
            }
            
            self.sortCustomerList(self.userList)
            self.currentPage += 1
            self.isLoadingMore = false
            //self.isLoading = false
            if(self.userList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        } onFailure: { errorMessage in
            DebugHelper.debug("onFailure")
            self.canLoadMorePages = false
            self.isLoadingMore = false
            //self.isLoading = false
            if(self.userList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }
    }
    
    
    func sortCustomerList(_ customerList: [UserModel]) {
        DebugHelper.debug(customerList.count)
        self.customerList.removeAll()
        for char in "abcdefghijklmnopqrstuvwxyz" {
            var expendableCustomer = ExpendableUserModel()
            expendableCustomer.header = String(char)
            
            let filtered = customerList.filter { customer in
                return customer.fullname.prefix(1).lowercased() == String(char).lowercased()
            }
            
            expendableCustomer.items = filtered
            
            self.customerList.append(expendableCustomer)
        }
        DebugHelper.debug("after",  self.customerList.count)
    }
    
    func resetPagination(){
        loadingState = .loading
        isLoadingMore = false
        currentPage = 0
        canLoadMorePages = true
        self.userList.removeAll()
        self.customerList.removeAll()
        if(searchRequest != nil){
            searchRequest.cancel()
        }
    }
        
    
    func loadMoreIfNeeded(currentItem item: ExpendableUserModel?) {
        DebugHelper.debug("loadMoreIfNeeded")
        guard let item = item else {
            fetchCustomers()
          return
        }

        let thresholdIndex =  self.customerList.index( self.customerList.endIndex, offsetBy: AppConstants.offsetPagination)
        if  self.customerList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            fetchCustomers()
        }
      }
}
