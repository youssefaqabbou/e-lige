//
//  BarberHomeView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI

struct BarberHomeView: View {
    
    @StateObject var vmBarberHome = BarberHomeViewModel()
    @State var selectedTab = 0
    
    @State var showInfo = false
    @State var showServices = false
    @State var showSchedule = false
    @State var showGallery = false
    @State var showProfile = false
    @State var showReviews = false
    @State var showNotifications = false
    @State var showDisconnectAlert = false
    
    
    @State var showCustomerDetail = false
    @State var selectedCustomer = UserModel()
    
    @State var showOrderDetail = false
    @State var selectedOrder = OrderModel()
    
    @State var showSlider : Bool = false
    @State var selectedMedia : Int = 0
    @State var selectedBarber = BarberShopModel()
    
    var tabs = [TabBarModel(id: BarberTabItems.landing.rawValue, label: "Accueil", icon: Image.iconHomeTabBar, iconSelected: Image.iconHomeTabBar),
                TabBarModel(id: BarberTabItems.orders.rawValue, label: "Réservations", icon: Image.iconOrderTabBar, iconSelected: Image.iconOrderTabBar),
                TabBarModel(id: BarberTabItems.clients.rawValue, label: "Clients", icon: Image.iconCustomerTabBar, iconSelected: Image.iconCustomerTabBar),
                TabBarModel(id: BarberTabItems.settings.rawValue, label: "Paramètres", icon: Image.iconSettingTabBar, iconSelected: Image.iconSettingTabBar)]
    
    
    func containedView() -> AnyView {
        switch selectedTab {
        case BarberTabItems.landing.rawValue: return AnyView(BarberShopLandingView(onEditInfoAction: {
            withAnimation {
                showInfo = true
            }
        }, onEditServiceAction: {
            withAnimation {
                showServices = true
            }
        }, onEditScheduleAction: {
            withAnimation {
                showSchedule = true
            }
        },onEditGalleryAction: {
            withAnimation {
                showGallery = true
            }
        },onShowReviewAction: {
            withAnimation {
                showReviews = true
            }
        }, onNotificationAction: {
            withAnimation {
                showNotifications = true
            }
        },onMediaAction: {barberShop,  index in
            selectedBarber = barberShop
            selectedMedia = index
            withAnimation {
                showSlider = true
            }
        }))
        case BarberTabItems.orders.rawValue: return AnyView(BarberBookingView(showOrderDetail: { order in
            withAnimation {
                showOrderDetail = true
                selectedOrder = order
            }
        } ))
        case BarberTabItems.clients.rawValue: return AnyView(CustomerListView( showCustomerDetail: { customer in
            withAnimation {
                showCustomerDetail = true
                selectedCustomer = customer
            }
        }))
        case BarberTabItems.settings.rawValue: return AnyView(SettingBarberView(updateProfileAction: {
            withAnimation {
                showProfile = true
            }
        }, disconnectAction: {
            withAnimation {
                showDisconnectAlert = true
            }
        }))
        default:
            return AnyView(Color.red)
        }
    }
    
    var body: some View {
        ZStack(alignment: .bottom, content: {
            
            Color.primaryTextLightColor.ignoresSafeArea()
            VStack(spacing: 0){
                containedView()
                CustomTabBar(tabs: tabs, selectedTab: $selectedTab)
                    //.padding(.bottom, 10)
                    .background(Color.primaryTextLightColor)
            }.edgesIgnoringSafeArea(.bottom)
           
            if(showInfo){
                EditBarberShopInfoView(showEditInfo: $showInfo).transition(.move(edge: .trailing)).animation(.linear, value: showInfo).zIndex(1)
            }
            if(showGallery){
                AddMediaBarberShopView(showEditMedia: $showGallery).transition(.move(edge: .trailing)).animation(.linear, value: showInfo).zIndex(2)
            }
            if(showSchedule){
                BarberShopScheduleView(showSchedule: $showSchedule).transition(.move(edge: .trailing)).animation(.linear, value: showSchedule).zIndex(3)
            }
            if(showServices){
                BarberServicesListView(showServices: $showServices).transition(.move(edge: .trailing)).animation(.linear, value: showServices).zIndex(4)
            }
            if(showProfile){
                UpdateProfileView(showProfile: $showProfile).transition(.move(edge: .trailing)).animation(.linear, value: showProfile).zIndex(5)
            }
            if(showCustomerDetail){
                CustomerDetailView(showCustomerDetail: $showCustomerDetail, customer: selectedCustomer).transition(.move(edge: .trailing)).animation(.linear, value: showCustomerDetail).zIndex(6)
            }
            
            if(showOrderDetail){
                BookingDetailView(showOrderDetail: $showOrderDetail, order: .constant(selectedOrder)).transition(.move(edge: .trailing)).animation(.linear, value: showOrderDetail).zIndex(7)
            }
            if(showReviews){
                ListReview(showListReview: $showReviews, barberShop: AppFunctions.decodeBarberShop()).transition(.move(edge: .trailing)).animation(.linear, value: showReviews).zIndex(8)
            }
            else if(showNotifications){
                NotificationView(showNotifications: $showNotifications).transition(.move(edge: .trailing)).animation(.linear, value: showNotifications).zIndex(9)
            }else  if(showSlider){
                BarberShopSliderView(showSlider: $showSlider, barberShop: selectedBarber, selectedIndex: selectedMedia).transition(.move(edge: .trailing)).animation(.linear, value: showSlider).zIndex(1)
            }
           
        })
            .onAppear(perform: {
                vmBarberHome.checkAccount()
            })
            .bottomSheet(isPresented: $showDisconnectAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
                CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDisconnect, title: LocalizationKeys.title_logout.localized, message: LocalizationKeys.message_logout.localized, positiveAction: {
                    
                    UserDefaults.disconnect()
                    
                }, isNegative: true, negativeAction: {
                    withAnimation {
                        showDisconnectAlert = false
                    }
                })
            }
            .bottomSheet(isPresented: $vmBarberHome.showAccountAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false, onTapOutside: false) {
             
                CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertPaymentInfo, title: "Information paiement", message: "Pour commencer à recevoir des réservations, veuillez renseigner quelques informations", positiveAction: {
                    
                    vmBarberHome.getAccountLink { url in
                        withAnimation {
                            vmBarberHome.showAccountAlert = false
                        }
                        vmBarberHome.showSafari = true
                        vmBarberHome.linkStripe = url
                    }
                    
                }, isNegative: true, titleNegative: "Plus tard", negativeAction: {
                    withAnimation {
                        vmBarberHome.showAccountAlert = false
                    }
                })
            }
            .sheet(isPresented: $vmBarberHome.showSafari, onDismiss: {
                DebugHelper.debug("Safari Dismissed")
                
            }) {
                SafariView(url: $vmBarberHome.linkStripe)
            }
            
    }
}

struct BarberHomeView_Previews: PreviewProvider {
    static var previews: some View {
        BarberHomeView()
    }
}
enum BarberTabItems: Int {
    case landing = 0
    case orders = 1
    case clients = 2
    case settings = 3
}
