//
//  BarberHomeViewModel.swift
//  elige
//
//  Created by macbook pro on 5/1/2022.
//

import Foundation

class BarberHomeViewModel: ObservableObject {
    private var apiService: APIService = APIService()
    @Published var showAccountAlert  = false
    
    @Published var barber = AppFunctions.getConnectedUser()
    
    @Published var showSafari = false
    @Published var linkStripe = ""
    
    init() {
        AppFunctions.sendTokenToServer()
    }
    
    func getAccountLink(completion:  @escaping (_ url:String) -> Void) {
        
        let params = [
            "accountId" : barber.accountId,
            "mobile" : "true"
        ]
        DebugHelper.debug(params)
        apiService.completeStripeAccount(params: params) { url in
            DebugHelper.debug(url)
            completion(url)
        } onFailure: { errorMessage in
            DebugHelper.debug(errorMessage)
        }
    }
    
    func checkAccount(){
        apiService.getStripeAccount { completed in
            self.showAccountAlert = false
        } onFailure: { errorMessage in
            self.showAccountAlert = true
        }
    }
}
