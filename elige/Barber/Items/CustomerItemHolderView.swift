//
//  CustomerItemHolderView.swift
//  elige
//
//  Created by macbook pro on 13/1/2022.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct CustomerItemHolderView: View {
    var body: some View {
        
        VStack(spacing: 10){
            HStack(){
                
                Circle().fill(Color.lightGray)
                    .frame(width: 40, height: 40, alignment: .center)
                    
                
                Divider().frame(height: 3)
                    .frame(maxWidth: .infinity)
                    .background(Color.lightGray)
                    
            }
            
            HStack(spacing: 0){
                
                WebImage(url: URL(string: ""))
                    .placeholder {
                        Image.placeholderUser.resizable()
                    }
                    .resizable() //
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 50, height: 50)
                    .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                    .padding(.trailing, AppConstants.viewNormalMargin)
                
                VStack(spacing: 0){
                    HStack(){
                        Text(AppFunctions.randomString(length: 20))
                            .font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 10)
                    }
                    
                    HStack(){
                        Text(AppFunctions.randomString(length: 11))
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(Color.secondaryTextColor)
                        
                        
                        Spacer(minLength: 0)
                    }
                }
                
            }.padding(.leading, 45)
        }.background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
