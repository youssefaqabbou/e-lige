//
//  CustomerItemView.swift
//  elige
//
//  Created by macbook pro on 13/1/2022.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct CustomerItemView: View {
    var customer : UserModel
    var body: some View {
        HStack(spacing: 0){
            
            WebImage(url: URL(string: customer.image.url))
                .placeholder {
                    Image.placeholderUser.resizable()
                }
                .resizable() //
                .aspectRatio(contentMode: .fill)
                .frame(width: 50, height: 50)
                .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                .padding(.trailing, AppConstants.viewNormalMargin)
            
            VStack(spacing: 0){
                HStack(){
                    Text(customer.fullname)
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                    Spacer(minLength: 10)
                }
                
                HStack(){
                    Text(customer.phone)
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    
                    Spacer(minLength: 0)
                }
            }
            
        }
    }
}
