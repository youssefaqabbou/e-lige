//
//  CustomerOrderViewItem.swift
//  elige
//
//  Created by macbook pro on 6/12/2021.
//

import SwiftUI

struct CustomerOrderItemView: View {
    @Binding var order : OrderModel
    @State var showName : Bool = true
    
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    
    var body: some View {
        CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            HStack(spacing: 0){
            VStack(alignment: .leading, spacing: 0){
                
                if(showName){
                    Text( order.customer.fullname)
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                    Spacer()
                }
                
                Text(AppFunctions.getOrderServices(order: order))
                    .font(.system(weight: .regular, size: showName ? 12 : 20))
                    .foregroundColor(showName ? Color.secondaryTextColor : Color.primaryTextColor )
                    .lineLimit(1)
                
                Spacer()
                
                HStack(spacing: 0){
                    Text(order.orderDate.toString(format: "HH:mm", isConvertZone: false))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Text("-")
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Text(order.orderDate.addMinutes(minutes: order.duration).toString(format: "HH:mm", isConvertZone: false))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Spacer()
                }
                
                Spacer()
                
                HStack(){
                    Text(AppFunctions.getOrderStatusLabel(status: order.status))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.primaryTextLightColor)
                        .padding(.vertical, AppConstants.viewTinyMargin)
                        .padding(.horizontal, AppConstants.viewSmallMargin)
                        .background(AppFunctions.getOrderStatusColor(status: order.status))
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                    
                    Spacer(minLength: 10)
                    
                    Text(order.totalPrice.toPrice())
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                }
            }
            
            Spacer(minLength: 10)
            
            VStack{
                
                Text(order.orderDate.time(onlyDay: true))
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(order.orderDate.toString(format: "dd", isConvertZone: false))
                    .font(.system(weight: .regular, size: 34))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(order.orderDate.toString(format: "MMMM",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter())
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
            }.padding(.vertical, AppConstants.viewVerySmallMargin)
            .padding(.horizontal, AppConstants.viewVerySmallMargin)
            .frame(minWidth: 90,  minHeight: 90, alignment: .center)
            .background(Color.lightGray)
                .cornerRadius(10)
            
        }
        }
    }
}

struct CustomerOrderViewItem_Previews: PreviewProvider {
    static var previews: some View {
        CustomerOrderItemView(order: .constant(FakeData.getOrderData(id: 1)))
    }
}
