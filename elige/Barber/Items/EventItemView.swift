//
//  EventItemView.swift
//  elige
//
//  Created by macbook pro on 30/11/2021.
//

import Foundation
import SwiftUI

struct EventItemView: View {
    
    @Binding var order : OrderModel
    
    var body: some View {
        
        if(order.duration <= 20){
            HStack(spacing: 0){
                Divider()
                    .frame(width: 5)
                    .background(Color.primaryColor)
                
                HStack(alignment: .top){
                    Text(order.customer.fullname)
                            .font(.system(weight: .regular, size: 12))
                            .foregroundColor(Color.primaryTextColor)
                            .lineLimit(1)
                    Spacer(minLength: 10)
                    Text(AppFunctions.getOrderStatusLabel(status: order.status))
                        .font(.system(weight: .bold, size: 12))
                        .foregroundColor(AppFunctions.getOrderStatusColor(status: order.status))
                        .lineLimit(1)
                }.padding(AppConstants.viewVerySmallMargin)
            }
            .background(Color(hex: "#F5F5F5"))
            .mask(AppUtils.CustomCorners(corners: .allCorners, radius: 5))
            .padding(.horizontal, AppConstants.viewVerySmallMargin)
            .onAppear {
                DebugHelper.debug("order in event:", order.id, order.status, order.duration)
            }
        }else{
            HStack(spacing: 0){
                Divider()
                    .frame(width: 5)
                    .background(Color.primaryColor)
                VStack(alignment: .leading){
                    
                    HStack(alignment: .top){
                        
                        VStack(alignment: .leading){
                            Text(order.customer.fullname)
                                .font(.system(weight: .regular, size: 15))
                                .foregroundColor(Color.primaryTextColor)
                                .lineLimit(1)
                            
                            Text(AppFunctions.getOrderServices(order: order))
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.secondaryTextColor)
                                .lineLimit(2)
                        }
                        
                        
                        Spacer(minLength: 10)
                        
                        Text(AppFunctions.getOrderStatusLabel(status: order.status))
                            .font(.system(weight: .bold, size: 15))
                            .foregroundColor(AppFunctions.getOrderStatusColor(status: order.status))
                            .lineLimit(1)
                        
                        
                    }
                    Spacer(minLength: 2)
                    HStack(){
                        Spacer()
                        HStack(spacing : 2){
                            let (h,m,_) = AppFunctions.secondsToHoursMinutesSeconds(order.duration * 60)
                            Text("\(String(format: "%02d", h) ):\(String(format: "%02d", m))")
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.secondaryTextColor)
                                .lineLimit(1)
                            Image.iconTime
                                .resizable()
                                .frame(width: 15, height: 15, alignment: .center)
                        }
                    }
                }.padding(AppConstants.viewSmallMargin)
            }
            .background(Color(hex: "#F5F5F5"))
            .mask(AppUtils.CustomCorners(corners: .allCorners, radius: 12))
            .padding(.horizontal, AppConstants.viewVerySmallMargin)
            .onAppear {
                DebugHelper.debug("order in event:", order.id, order.status, order.duration)
            }
        }
        
        
        //.cornerRadius(15)
    }
    
}
