//
//  scheduleBreakViewItem.swift
//  elige
//
//  Created by user196417 on 11/12/21.
//

import SwiftUI

struct scheduleBreakItemView: View {
   
    @State var schedule : ScheduleModel
    
    var onBreakFromAction : () -> () = { }
    var onBreakToAction : () -> () = {}
    var onClearBreakAction : () -> () = {}
    
    let NC = NotificationCenter.default
    @State var attempts: Int = 0
    
    var body: some View {
        
        HStack{
            
            Text(schedule.day.toWeekDay(firstWeekday: 2, local: Locale.init(identifier: "FR")).capitalizingFirstLetter())
                .font(.system(weight: .regular, size: 16))
                .foregroundColor(Color.primaryTextColor)
            
            Spacer()
            
            HStack(){
                if(schedule.breakFrom.isEmpty){
                    Text("DÉBUT")
                        .font(.system(weight: .regular, size: 12))
                        .foregroundColor(Color.primaryTextColor)
                        .onTapGesture {
                            onBreakFromAction()
                        }
                }else{
                    if(schedule.breakFromUpdated){
                        Text(schedule.breakFrom.timeToTimeZone(convertToUtc: false))
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor).onTapGesture {
                                onBreakFromAction()
                            }
                    }else{
                        Text(schedule.breakFrom.timeToTimeZone())
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor).onTapGesture {
                                onBreakFromAction()
                            }
                    }
                }
                
                Text("-")
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.primaryTextColor)
                
                if(schedule.breakTo.isEmpty){
                    Text("FIN")
                        .font(.system(weight: .regular, size: 12))
                        .foregroundColor(Color.primaryTextColor)
                        .onTapGesture {
                            onBreakToAction()
                        }
                }else{
                    if(schedule.breakToUpdated){
                        Text(schedule.breakTo.timeToTimeZone(convertToUtc: false))
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor).onTapGesture {
                                onBreakToAction()
                            }
                    }else{
                        Text(schedule.breakTo.timeToTimeZone())
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor).onTapGesture {
                                onBreakToAction()
                            }
                    }
                }
            }.modifier(Shake(animatableData: CGFloat(attempts)))
            
            Circle().fill(Color.secondaryColor)
                .frame(width: 25, height: 25, alignment: .center)
                .overlay(
                    Image.iconClose
                        .resizable()
                        .renderingMode(.template)
                        .frame(width: 15, height: 15)
                        .colorMultiply(Color.white)
                        .foregroundColor(Color.white)
                )
                .onTapGesture {
                    if(!schedule.breakFrom.isEmpty || !schedule.breakTo.isEmpty || !schedule.isOpen){
                        withAnimation {
                            onClearBreakAction()
                        }
                    }
                }
        }.opacity(schedule.isOpen ? 1 : 0.5)
            .onAppear {
                self.NC.addObserver(forName: NSNotification.breakTimeError, object: nil, queue: nil,
                                       using: self.onTimeError)
            }
    }
    
    func onTimeError(_ notification: Notification) {
        let scheduleError = notification.userInfo!["schedule"] as! ScheduleModel
        if(scheduleError.id == self.schedule.id){
            withAnimation {
                attempts += 1
            }
        }
    }
}

struct scheduleBreakViewItem_Previews: PreviewProvider {
    static var previews: some View {
        scheduleBreakItemView(schedule: ScheduleModel())
    }
}
