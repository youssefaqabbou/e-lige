//
//  scheduleEditViewItem.swift
//  elige
//
//  Created by user196417 on 11/12/21.
//

import SwiftUI

struct ScheduleEditItemView: View {
    @State var schedule : ScheduleModel
    @State var isToggleOn = false
    
    @State var selection: Int = 0
    private let items: [Image] = [Image.iconBarberShop, Image.iconBarberDomicile]
    
    var onScheduleAction : (ScheduleModel) -> () = {schedule in }
    var onStartScheduleAction : () -> () = { }
    var onEndScheduleAction : () -> () = {}
    
    let NC = NotificationCenter.default
    @State var attempts: Int = 0
    
    var body: some View {
        
        HStack(spacing: 0){
            
            Toggle(isOn: $isToggleOn, label: {
                EmptyView()
            }).toggleStyle(CustomToggleStyle(toggleAction: {
                DebugHelper.debug("isToggleOn", isToggleOn)
                schedule.isOpen = isToggleOn
                DebugHelper.debug("isToggleOn", schedule.isOpen)
                onScheduleAction(schedule)
            }))
            
            Spacer()
            
            HStack(spacing: 10){
                
                Text(schedule.day.toWeekDay(weekDayFormat: .short ,firstWeekday: 2, local: Locale.init(identifier: "FR")).capitalizingFirstLetter())
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.primaryTextColor)
                
                Spacer()
                
                if isToggleOn {
                    
                    HStack{
                        
                        if(schedule.openTime.isEmpty){
                            Text("DÉBUT")
                                .font(.system(weight: .regular, size: 16))
                                .foregroundColor(Color.primaryTextColor)
                                .onTapGesture {
                                    onStartScheduleAction()
                                }
                        }else{
                            if(schedule.openTimeUpdated){
                                Text(schedule.openTime.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor).onTapGesture {
                                        onStartScheduleAction()
                                    }
                            }else{
                                Text(schedule.openTime.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor).onTapGesture {
                                        onStartScheduleAction()
                                    }
                            }
                           
                        }
                      
                        Text("-")
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                        
                        if(schedule.closeTime.isEmpty){
                            Text("FIN")
                                .font(.system(weight: .regular, size: 16))
                                .foregroundColor(Color.primaryTextColor).onTapGesture {
                                    onEndScheduleAction()
                                }
                        }else{
                            if(schedule.closeTimeUpdated){
                                Text(schedule.closeTime.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor).onTapGesture {
                                        onEndScheduleAction()
                                    }
                            }else{
                                Text(schedule.closeTime.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor).onTapGesture {
                                        onEndScheduleAction()
                                    }
                            }
                        }
                    }
                    .modifier(Shake(animatableData: CGFloat(attempts)))
                    
                }else{
                    HStack{
                        Spacer()
                        Text("Fermé")
                            .font(.system(weight: .bold, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                    }
                }
            }
            /*
            Spacer()
            
            CustomSegmentedPicker(items: self.items, selection: self.$selection, action: {
                schedule.serviceLocation = selection == 0 ? ServiceLocation.shop.rawValue : ServiceLocation.home.rawValue
                DebugHelper.debug("Location changed", schedule.serviceLocation)
                onScheduleAction(schedule)
            }).frame(width: 80)      
               */
        }
        .onAppear {
            isToggleOn = schedule.isOpen
            //selection = schedule.serviceLocation == ServiceLocation.shop.rawValue ? 0 : 1
            
            self.NC.addObserver(forName: NSNotification.timeError, object: nil, queue: nil,
                                   using: self.onTimeError)
        }
    }
    
    func onTimeError(_ notification: Notification) {
        let scheduleError = notification.userInfo!["schedule"] as! ScheduleModel
        if(scheduleError.id == self.schedule.id){
            withAnimation {
                attempts += 1
            }
        }
    }
}

struct scheduleEditViewItem_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleEditItemView(schedule: ScheduleModel())
    }
}

struct Shake: GeometryEffect {
    var amount: CGFloat = 10
    var shakesPerUnit = 7
    var animatableData: CGFloat

    func effectValue(size: CGSize) -> ProjectionTransform {
        ProjectionTransform(CGAffineTransform(translationX:
            amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
            y: 0))
    }
}
