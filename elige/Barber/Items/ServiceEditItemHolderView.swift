//
//  ServiceEditItemHolderView.swift
//  elige
//
//  Created by macbook pro on 17/1/2022.
//

import Foundation
import SwiftUI

struct ServiceEditItemHolderView: View {
    
    var body: some View {
        HStack(spacing: 0){
           
            VStack(alignment: .leading, spacing: 5){
                
                HStack(){
                    Text(AppFunctions.randomString(length: 20))
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                    Spacer(minLength: 10)
                    
                    Text(AppFunctions.randomString(length: 4))
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                }
                
                Text(AppFunctions.randomString(length: 6))
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.secondaryTextColor)
                
                Text(AppFunctions.randomString(length: 100))
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.thirdTextColor)
                    .lineLimit(2)
            }
            /*
            Spacer(minLength: 10)
            
            Color.lightGray
                .frame(width: 70, height: 70)
                .cornerRadius(12)
                */
        }//.redacted(reason: .placeholder).shimmer()
    }
}
