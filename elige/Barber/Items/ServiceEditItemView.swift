//
//  ServiceEditItemView.swift
//  elige
//
//  Created by macbook pro on 10/11/2021.
//

import SwiftUI

struct ServiceEditItemView: View {
    
    var service : ServiceModel
    var onEditAction : () -> () = {}
    
    var body: some View {
        HStack(spacing: 0){
           
            VStack(alignment: .leading, spacing: 5){
                
                HStack(){
                    Text(service.name)
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                    Spacer(minLength: 10)
                    
                    Text(service.price.toPrice())
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                }
                
                HStack(){
                    ServiceLocationsItem(serviceLocation: .constant(service.serviceLocation))
                                       
                    Spacer(minLength: 4)
                    
                    Text(service.duration.toHourMinute())
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                }
                
                Text(service.description)
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.thirdTextColor)
                    .lineLimit(1)
            }
            /*
            Spacer(minLength: 10)
            
            Color.lightGray
                .frame(width: 70, height: 70)
                .cornerRadius(12)
                .overlay(
                    Image.iconPencil
                        .resizable()
                        .frame(width: 40, height: 40, alignment: .center)
                        .foregroundColor(Color.primaryColor)
                )
                .onTapGesture {
                    onEditAction()
                }
             */
        }.onTapGesture {
            onEditAction()
        }
    }
}

struct ServiceEditItemView_Previews: PreviewProvider {
    static var previews: some View {
        ServiceEditItemView(service: ServiceModel())
    }
}
