//
//  BarberShopLandingView.swift
//  elige
//
//  Created by macbook pro on 30/11/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct BarberShopLandingView: View {
    
    @StateObject var vmLanding = BarberShopLandingViewModel()
    
    @State var scrollViewOffset: CGFloat = 0
    @State var startOffset: CGFloat = 0
    
    let NC = NotificationCenter.default
    
    var onEditInfoAction : () -> () = { }
    var onEditServiceAction : () -> () = {}
    var onEditScheduleAction : () -> () = {}
    var onEditGalleryAction : () -> () = {}
    var onShowReviewAction : () -> () = {}
    var onNotificationAction : () -> () = {}
    var onMediaAction : (BarberShopModel, Int) -> () = { barberShop, index in }
    
    var body: some View {
        ZStack{
            let maxWidth = (UIScreen.main.bounds.width - ((AppConstants.viewVeryExtraMargin * 2) + (15 * 3))  ) / 4
            VStack{
                
                WebImage(url: URL(string: vmLanding.barberShop.image.url))
                    .placeholder {
                        Image.placeholderShop.resizable()
                    }
                    .resizable() //
                    .aspectRatio(contentMode: .fill)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                    .edgesIgnoringSafeArea(.top)
                
                Spacer()
                
            }
            
            ScrollView(.vertical, showsIndicators: false){
                
                VStack(alignment: .leading, spacing: 0){
                    
                    Text("")
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                        .hidden()
                    
                    VStack(alignment: .leading, spacing: 0){
                        
                        VStack(alignment: .leading, spacing: 0){
                            
                            HStack(alignment: .center){
                                
                                Text(vmLanding.barberShop.name)
                                    .font(.system(weight: .regular, size: 22))
                                    .foregroundColor(Color.primaryTextColor)
                                    .lineLimit(3)
                                
                                Spacer()
                                
                                Image.iconPencil
                                    .resizable()
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        onEditInfoAction()
                                    }
                                
                            }.padding(.top, AppConstants.viewNormalMargin)
                            
                            Text(vmLanding.barberShop.address)
                                .font(.system(weight: .regular, size: 15))
                                .lineSpacing(5)
                                .foregroundColor(Color.secondaryTextColor)
                                .padding(.top, 5)
                            
                            
                            HStack{
                                
                                StarsView(rating: CGFloat(vmLanding.barberShop.rating), maxRating: 5, width: 15)
                                    .onTapGesture {
                                        
                                    }
                                
                                Text(vmLanding.barberShop.rating.toRating())
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        
                                    }
                                
                                Text("\(vmLanding.barberShop.countReview) Avis")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                                    .padding(.leading, AppConstants.viewExtraMargin)
                                
                                Spacer(minLength: 5)
                                
                                Text("Voir sur la carte")
                                    .font(.system(weight: .regular, size: 14))
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        vmLanding.openMap()
                                    }
                                
                            }.padding(.vertical, AppConstants.viewSmallMargin)
                            
                            VStack(alignment: .leading, spacing: 10){
                                
                                TagView(tags: $vmLanding.barberShop.tags)
                                    .padding(.bottom, AppConstants.viewSmallMargin)
                                
                                Text("Description")
                                    .font(.system(weight: .bold, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                    
                                
                                Text(vmLanding.barberShop.description)
                                    .font(.system(weight: .regular, size: 14))
                                    .lineSpacing(5)
                                    .foregroundColor(Color.secondaryTextColor)
                                
                            }.padding(.top, AppConstants.viewSmallMargin)
                                .padding(.bottom, AppConstants.viewExtraMargin)
                            
                        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.bottomLeft, .bottomRight], radius: 15)))
                        
                        VStack{
                            
                            HStack(alignment: .center){
                                
                                Text("Prestations")
                                    .font(.system(weight: .bold, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Spacer()
                                
                                Image.iconPencil
                                    .resizable()
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        onEditServiceAction()
                                    }
                            }
                            
                            VStack(spacing: 20){
                                
                                if(vmLanding.services.count > 3){
                                    ForEach(0..<3) { index in
                                        let service = vmLanding.barberShop.services[index]
                                        ServiceDetailItem(service: .constant(service))
                                    }
                                }else{
                                    ForEach(vmLanding.services, id: \.id) { service in
                                        ServiceDetailItem(service: .constant(service))
                                    }
                                }
                                
                            }.padding(.top, AppConstants.viewSmallMargin)
                                .padding(.bottom, AppConstants.viewSmallMargin)
                            /*
                            if(vmLanding.barberShop.countService > 3){
                                Text("Voir Tout")
                                    .font(.system(weight: .regular, size: 14))
                                    .foregroundColor(Color.secondaryTextColor)
                                    .onTapGesture {
                                        onEditServiceAction()
                                    }
                            }
                           */
                            
                        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .padding(.vertical, AppConstants.viewExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                        .padding(.top, AppConstants.viewSmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                        VStack{
                            
                            HStack(alignment: .center){
                                Text("Horaires de travail")
                                    .font(.system(weight: .bold, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Spacer()
                                
                                Image.iconPencil
                                    .resizable()
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        onEditScheduleAction()
                                    }
                            }
                            
                            VStack(spacing: 10){
                                ForEach(vmLanding.schedules, id: \.id) { schedule in
                                    ScheduleItem(schedule: .constant(schedule))
                                }
                            }.padding(.top, AppConstants.viewSmallMargin)
                            
                        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .padding(.vertical, AppConstants.viewExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                        .padding(.top, AppConstants.viewSmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                        VStack{
                            
                            HStack(alignment: .center){
                                
                                Text("Galerie de photos")
                                    .font(.system(weight: .bold, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Spacer()
                                
                                Image.iconPencil
                                    .resizable()
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        onEditGalleryAction()
                                    }
                                
                            }.padding(.bottom, AppConstants.viewSmallMargin)
                            
                            HStack(spacing: 10){
                                
                                
                                if(vmLanding.mediaList.count > 3){
                                    ForEach(0..<3) { index in
                                        let media = vmLanding.mediaList[index]
                                        WebImage(url: URL(string: media.url))
                                            .placeholder {
                                                Rectangle().fill(Color.gray)
                                            }
                                            .resizable() //
                                            .aspectRatio(contentMode: .fill)
                                            .frame(width: maxWidth, height: maxWidth)
                                            .cornerRadius(12)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 12)
                                                    .stroke(Color.primaryColor, lineWidth: 1)
                                            ).onTapGesture {
                                                DebugHelper.debug(index)
                                                onMediaAction(vmLanding.barberShop,  index)
                                               
                                            }
                                    }
                                    
                                    ZStack{
                                        
                                        WebImage(url: URL(string: vmLanding.mediaList[3].url))
                                            .placeholder {
                                                Rectangle().fill(Color.gray)
                                            }
                                            .resizable() //
                                            .aspectRatio(contentMode: .fill)
                                            .frame(width: maxWidth, height: maxWidth)
                                            .cornerRadius(12)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 12)
                                                    .stroke(Color.primaryColor, lineWidth: 1)
                                            )
                                        if(vmLanding.mediaList.count - 4 != 0){
                                            Rectangle().fill(Color.primaryColor.opacity(0.6))
                                                .cornerRadius(12)
                                                .overlay(
                                                    Text("+\(vmLanding.mediaList.count - 4)")
                                                        .font(.system(weight: .bold, size: 18))
                                                        .foregroundColor(Color.primaryTextLightColor)
                                                ).onTapGesture {
                                                    onMediaAction(vmLanding.barberShop, 3)
                                                }
                                        }
                                        
                                    }.frame(width: maxWidth, height: maxWidth)
                                        .cornerRadius(12)
                                        .onTapGesture {
                                            
                                        }
                                }else{
                                    ForEach(vmLanding.mediaList, id: \.id) { media in
                                        WebImage(url: URL(string: media.url))
                                            .placeholder {
                                                Rectangle().fill(Color.gray)
                                                    .cornerRadius(12)
                                            }
                                            .resizable() //
                                            .aspectRatio(contentMode: .fill)
                                            .frame(width: maxWidth, height: maxWidth)
                                            .cornerRadius(12)
                                    }
                                    
                                }
                                
                                Spacer()
                            }
                            
                        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .padding(.vertical, AppConstants.viewExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                            .padding(.top, AppConstants.viewSmallMargin)
                            .background(Color.bgViewSecondaryColor)
                        
                        VStack{
                            
                            HStack{
                                
                                Text("Commentaires")
                                    .font(.system(weight: .bold, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Spacer()
                                
                                StarsView(rating: CGFloat(vmLanding.barberShop.rating), maxRating: 5, width: 15)
                                    .onTapGesture {
                                        
                                    }
                                
                                Text(vmLanding.barberShop.rating.toRating())
                                    .font(.system(weight: .regular, size: 14))
                                    .foregroundColor(Color.primaryColor)
                                    .onTapGesture {
                                        
                                    }
                            }
                            VStack(spacing: 20){
                                if(vmLanding.barberShop.reviews.count > 3){
                                    ForEach(0..<3) { index in
                                        let review = vmLanding.barberShop.reviews[index]
                                        ReviewSmallItemView(review: review)
                                    }
                                }else{
                                    ForEach(vmLanding.barberShop.reviews, id: \.id) { review in
                                        ReviewSmallItemView(review: review)
                                    }
                                }
                            }.padding(.top, AppConstants.viewSmallMargin)
                                .padding(.bottom, AppConstants.viewSmallMargin)
                            
                            if(vmLanding.barberShop.countReview > 3){
                                Text("Voir Tout")
                                    .font(.system(weight: .regular, size: 14))
                                    .foregroundColor(Color.secondaryTextColor)
                                    .onTapGesture {
                                        withAnimation {
                                            onShowReviewAction()
                                        }
                                    }
                            }
                            
                        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .padding(.vertical, AppConstants.viewExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                        .padding(.top, AppConstants.viewSmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                    }.background(Color.bgViewColor)
                    
                }.overlay(
                    
                    GeometryReader{proxy -> Color in
                        
                        DispatchQueue.main.async {
                            
                            if startOffset == 0{
                                self.startOffset = proxy.frame(in: .global).minY
                            }
                            
                            let offset = proxy.frame(in: .global).minY
                            self.scrollViewOffset = offset - startOffset
                            
                        }
                        
                        return Color.clear
                        
                    }.frame(width: 0, height: 0)
                    ,alignment: .top
                )
            }
            .edgesIgnoringSafeArea(.top)
            
            VStack{
                
                HStack{
                    Spacer()
                    
                    BellItem().onTapGesture {
                        onNotificationAction()
                    }.padding(.leading, AppConstants.viewExtraMargin)
                        .padding(.top, getSafe().top == 0 ? 12 : 0)
                        .scaleEffect(-scrollViewOffset > 230 ? 0 : 1)
                        .animation(.linear(duration: 0.2), value: scrollViewOffset)
                        .disabled(-scrollViewOffset > 230 ? true : false)
                   
                }
                
                Spacer()
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
           
            
        }.background(Color.bgViewColor)
            .onAppear {
                vmLanding.getBarberShop()
                
                self.NC.addObserver(forName: NSNotification.shopUpdated, object: nil, queue: nil,
                                    using: self.shopUpdated)
            }
       
    }
    
    func shopUpdated(_ notification: Notification) {
        vmLanding.updateBarberShop()
    }
}

struct BarberShopLandingView_Previews: PreviewProvider {
    static var previews: some View {
        BarberShopLandingView()
    }
}
