//
//  BarberShopLandingViewModel.swift
//  elige
//
//  Created by macbook pro on 2/12/2021.
//

import Foundation
import SwiftUI
import MapKit
import CoreLocation

class BarberShopLandingViewModel : ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var barberShop = BarberShopModel()
    @Published var mediaList = [MediaModel]()
    @Published var services = [ServiceModel]()
    @Published var schedules = [ScheduleModel]()
    
    @Published var isLoadingMedia = true
    
    
    func getBarberShop(){
        let shopId = AppFunctions.getConnectedUser().id
    
        apiService.getBarberShop(idShop: shopId) { barberShop in
            self.barberShop = barberShop
            self.saveBarberShop()
            self.fetchMedias()
        } onFailure: { errorMessage in
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func fetchMedias(){
        isLoadingMedia = true
        apiService.getMedias { medias in
            self.mediaList = medias
            self.barberShop.medias = medias
            self.isLoadingMedia = false
        } onFailure: { errorMessage in
            self.isLoadingMedia = false
            //AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func updateBarberShop(){
        barberShop = AppFunctions.decodeBarberShop()
        services = barberShop.services
        schedules = barberShop.schedules
        fetchMedias()
    }
    
    func saveBarberShop(){
        AppFunctions.encodeBarberShop(barberShop: barberShop)
        services = barberShop.services
        schedules = barberShop.schedules
    }
    
    func openMap(){
        let latitude: CLLocationDegrees = barberShop.latitude
        let longitude: CLLocationDegrees = barberShop.longitude
        let regionDistance:CLLocationDistance = 5000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = barberShop.name
        mapItem.openInMaps(launchOptions: options)
    }
}
