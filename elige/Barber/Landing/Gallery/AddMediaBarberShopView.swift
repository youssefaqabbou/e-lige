//
//  AddMediaBarberShopView.swift
//  elige
//
//  Created by macbook pro on 6/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct AddMediaBarberShopView: View {
    
    @StateObject var vmEditMedia = AddMediaBarberShopViewModel()
    
    @Binding var showEditMedia : Bool
    
    var body: some View {
        VStack(){
            
            HStack(){
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            showEditMedia = false
                        }
                    }
                
                Spacer()
                
                Text("Galerie de photos")
                    .textStyle(TitleStyle())
                
                Spacer()
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .hidden()
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
            ScrollView(){
                VStack{
                    let columns = Array(repeating: GridItem(.flexible(), spacing: 20), count: 3)
                    LazyVGrid(columns: columns) {
                        ForEach(1...vmEditMedia.countMedia, id: \.self){index in
                            EditMediaItemView(medias: $vmEditMedia.mediaList, index: index, w : (UIScreen.main.bounds.width - (AppConstants.viewVeryExtraMargin * 3) - (60)) / 3, onAddAction : { clickedPos in
                                
                                vmEditMedia.isEditing = false
                                vmEditMedia.mediaClikedIndex = index
                                withAnimation {
                                    vmEditMedia.isShowPickerAlert = true
                                }
                                
                            }, onRemoveAction: { clickedPos in
                                vmEditMedia.mediaClikedIndex = clickedPos
                                withAnimation {
                                    vmEditMedia.showDeleteConfirmation = true
                                }
                            }, onEditAction: { clickedPos in
                                vmEditMedia.isEditing = true
                                vmEditMedia.mediaClikedIndex = clickedPos
                                withAnimation {
                                    vmEditMedia.isShowPickerAlert = true
                                }
                            }).padding(.bottom, AppConstants.viewExtraMargin)
                            
                        }
                    }.padding(.leading,20)
                        .padding(.trailing, 30)
                }
            }
            .padding(.top, AppConstants.viewNormalMargin)
        }
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        
        .bottomSheet(isPresented: $vmEditMedia.showDeleteConfirmation, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDelete, title: "Confirmation", message: "Voulez vous supprimer cette photo ?", positiveAction: {
                vmEditMedia.removeImage()
                withAnimation {
                    vmEditMedia.showDeleteConfirmation = false
                }
            }, isNegative: true, negativeAction: {
                withAnimation {
                    vmEditMedia.showDeleteConfirmation = false
                }
            })
        }
        .bottomSheet(isPresented: $vmEditMedia.isShowPickerAlert, height: 450, topBarCornerRadius: 45, showTopIndicator: false) {
            // Content
            AlertMediaPickerView(showingActionSheet: $vmEditMedia.isShowPickerAlert, onGalleryAction :{
                vmEditMedia.isShowSheet = true
                vmEditMedia.isCameraPicker = false
                vmEditMedia.isGalleryPicker = true
            }, onCameraAction: {
                vmEditMedia.isShowSheet = true
                vmEditMedia.isGalleryPicker = false
                vmEditMedia.isCameraPicker = true
            })
            
        }
        .fullScreenCover(isPresented: $vmEditMedia.isShowSheet, onDismiss : {
            vmEditMedia.isShowSheet = false
            vmEditMedia.isGalleryPicker = false
            vmEditMedia.isCameraPicker = false
            onPickerDismiss()
        }, content: {
            if (vmEditMedia.isGalleryPicker) {
                MediaPickerView(sourceType: .photoLibrary, pickedImage: $vmEditMedia.pickedImage, isImagePicked: $vmEditMedia.isImagePicked)
            } else if (vmEditMedia.isCameraPicker) {
                MediaPickerView(sourceType: .camera, pickedImage: $vmEditMedia.pickedImage, isImagePicked: $vmEditMedia.isImagePicked)
            }
            
        })
        .onAppear {
            
        }
        
        
    }
    
    func onPickerDismiss() {
        DebugHelper.debug("onPickerDismiss \(vmEditMedia.mediaClikedIndex)")
        if(vmEditMedia.pickedImage != UIImage()){
            if(vmEditMedia.isEditing){
                vmEditMedia.updateImage { success in
                    
                }
            } else {
                vmEditMedia.uploadImage { success in
                    
                }
            }
        }
    }
}

struct AddMediaBarberShopView_Previews: PreviewProvider {
    static var previews: some View {
        AddMediaBarberShopView(showEditMedia: .constant(false))
    }
}

struct EditMediaItemView: View {
    @Binding var medias : [MediaModel]
    @State var index : Int = 0
    @State var w : CGFloat = 90
    
    var onAddAction : (Int) -> () = {pos in }
    var onRemoveAction : (Int) -> () = {pos in }
    var onEditAction : (Int) -> () = {pos in }
    
    func getMediaByOrder(pos: Int) -> Int {
        for (index, media) in medias.enumerated(){
            if media.order == pos {
                return index
            }
        }
        return -1
    }
    
    var body: some View {
        
        ZStack(alignment: .bottomTrailing){
            
            let order = getMediaByOrder(pos: index)
            if(order == -1){
                WebImage(url: URL(string: ""))
                    .onSuccess { image, data, cacheType in }
                    .resizable()
                    .placeholder {
                        //Rectangle().fill(Color.lightGray)
                        Image.iconPlaceHolder
                            .resizable()
                        
                    }
                    .indicator(.activity)
                    .transition(.fade(duration: 0.5))
                    .cornerRadius(10)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.gray,style: StrokeStyle(lineWidth: 3, dash: [5]))
                    )
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: 30, height: 30, alignment: .center)
                    .overlay(
                        Image(systemName: "plus")
                            .font(.system(weight: .bold, size: 14))
                            .foregroundColor(.white)
                    ).onTapGesture {
                        onAddAction(index)
                    }
                    .offset(x: 15, y : 15)
            }else{
                Button(action: {
                    onEditAction(order)
                }, label: {
                    WebImage(url: URL(string: medias[order].url))
                        .onSuccess { image, data, cacheType in }
                        .resizable()
                        .placeholder {
                            Rectangle().fill(Color.lightGray)
                                .cornerRadius(10)
                        }
                        .indicator(.activity)
                        .transition(.fade(duration: 0.5))
                        .cornerRadius(10)
                })
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.gray,style: StrokeStyle(lineWidth: 3, dash: [5]))
                    )
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: 30, height: 30, alignment: .center)
                    .overlay(
                        Image.iconDelete
                            .resizable()
                            .renderingMode(.template)
                            .frame(width: 20, height: 20, alignment: .center)
                            .colorMultiply(Color.white)
                            .foregroundColor(Color.white)
                    ).onTapGesture {
                        onRemoveAction(order)
                    }.offset(x: 15, y : 15)
            }
        }
        .frame(width: w + 15, height: 140, alignment: .leading)
        
    }
}

struct AlertMediaPickerView : View {
    @Binding var showingActionSheet : Bool
    var onGalleryAction : () -> () = { }
    var onCameraAction : () -> () = { }
    
    var body: some View{
        
        VStack(spacing: 15){
            
            HStack {
                
                Spacer()
                
                Button(action: {
                    self.showingActionSheet = false
                }) {
                    Image.iconClose
                        .resizable()
                        .frame(width: 30, height: 30)
                }.buttonStyle(PressedButtonStyle())
                    .cornerRadius(10)
            }.padding()
                .padding(.top)
            
            VStack(spacing: 20){
                HStack{
                    
                    VStack(alignment: .leading, spacing: 5){
                        Text("Prendre une photo")
                            .font(.system(weight: .regular, size: 22))
                            .foregroundColor(.primaryTextColor)
                        
                        Text("Appareil Photo".uppercased())
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(.primaryColor)
                    }
                    
                    Spacer()
                    
                    Image.iconCamera
                        .resizable()
                        .frame(width: 80, height: 80)
                        .padding(.trailing, 25)
                    
                }.padding()
                    .frame(height: 130)
                    .background(Image.bgPicker.resizable())
                    .overlay(
                        RoundedRectangle(cornerRadius: 12)
                            .stroke(Color.lightGray, lineWidth: 1)
                    )
                    .onTapGesture {
                        onCameraAction()
                        self.showingActionSheet = false
                    }
                
                
                HStack{
                    
                    VStack(alignment: .leading, spacing: 5){
                        
                        Text("Ajouter une image")
                            .font(.system(weight: .regular, size: 22))
                            .foregroundColor(.primaryTextColor)
                        
                        Text("Galerie".uppercased())
                            .font(.system(weight: .regular, size: 18))
                            .foregroundColor(.primaryColor)
                    }
                    
                    Spacer()
                    
                    Image.iconGallery
                        .resizable()
                        .frame(width: 80, height: 80)
                        .padding(.trailing, 25)
                    
                    
                }
                .padding()
                .frame(height: 130)
                .background(Image.bgPicker.resizable())
                .overlay(
                    RoundedRectangle(cornerRadius: 15)
                        .stroke(Color.lightGray, lineWidth: 1)
                )
                .onTapGesture {
                    onGalleryAction()
                    self.showingActionSheet = false
                }.padding(.bottom, 25)
                
            }
            .padding()
            .padding(.bottom)
            .background(Color.bgViewColor)
            
            Spacer()
        }
    }
}

struct MediaPickerView: UIViewControllerRepresentable {
    var sourceType: UIImagePickerController.SourceType
    
    @Binding var pickedImage: UIImage
    @Binding var isImagePicked : Bool
    
    @Environment(\.presentationMode) private var presentationMode
    func makeUIViewController(context: UIViewControllerRepresentableContext<MediaPickerView>) -> UIImagePickerController {
        
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = sourceType
        if(sourceType == .camera){
            imagePicker.cameraCaptureMode = UIImagePickerController.CameraCaptureMode.photo
        }
        imagePicker.delegate = context.coordinator
        
        return imagePicker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<MediaPickerView>) {
        //isReload = true
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    final class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        var parent: MediaPickerView
        
        init(_ parent: MediaPickerView) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            if let image = info[.originalImage] as? UIImage {
                parent.pickedImage = image
                parent.isImagePicked = true
            }else if let image = info[.editedImage] as? UIImage {
                parent.pickedImage = image
                parent.isImagePicked = true
            }
            parent.presentationMode.wrappedValue.dismiss()
            
            
        }
        
    }
    
}
