//
//  AddMediaBarberShopViewModel.swift
//  elige
//
//  Created by macbook pro on 7/12/2021.
//

import Foundation
import UIKit

class AddMediaBarberShopViewModel : ObservableObject {
    
    private var apiService: APIService = APIService()
    @Published var isLoading = false
    @Published var isEditing = false
    
    @Published var countMedia = 9
    @Published var mediaList = [MediaModel]()
    
    @Published var mediaClikedIndex: Int = 0
    
    @Published var isShowPickerAlert: Bool = false
    
    @Published var isGalleryPicker: Bool = false
    @Published var isCameraPicker: Bool = false
    
    @Published var isShowSheet = false
    
    @Published var pickedImage = UIImage()
    @Published var isImagePicked = false
    
    @Published var showDeleteConfirmation = false
    
    
    init(){
        fetchMedias()
    }
    
    func fetchMedias(){
        isLoading = true
        apiService.getMedias { medias in
            self.mediaList = medias
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

       
    }
    
    func uploadImage(onSuccess: @escaping (_ success: Bool) -> Void) {
        
        let params = [
            "order": String(mediaClikedIndex)
        ] as [String : Any]
        
        apiService.uploadImageBarber(image: pickedImage, params: params) { message, media  in
            
            self.pickedImage = UIImage()
            self.isImagePicked = false
            self.mediaList.append(media)
            onSuccess(true)
            self.objectWillChange.send()
            NotificationHelper.postBarberShop(barberShop: BarberShopModel())
        } onFailure: { errorMessage in
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            onSuccess(false)
            self.pickedImage = UIImage()
            self.isImagePicked = false
        }
    }
    
    func updateImage(onSuccess: @escaping (_ success: Bool) -> Void) {
        
        let params = [
            "order": String(mediaClikedIndex + 1)
        ] as [String : Any]
        
        apiService.updateImageBarber(idImage: self.mediaList[self.mediaClikedIndex].id, image: pickedImage, params: params) { message, media  in
            
            self.pickedImage = UIImage()
            self.isImagePicked = false
            self.mediaList[self.mediaClikedIndex] = media
            onSuccess(true)
            self.objectWillChange.send()
            NotificationHelper.postBarberShop(barberShop: BarberShopModel())
        } onFailure: { errorMessage in
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            onSuccess(false)
            self.pickedImage = UIImage()
            self.isImagePicked = false
        }
    }
    
    func removeImage(){
        apiService.removeImageBarber(idImage: self.mediaList[self.mediaClikedIndex].id) { message in
            self.mediaList.remove(at: self.mediaClikedIndex)
            self.objectWillChange.send()
            AppFunctions.showSnackBar(status: .success , message: message)
            NotificationHelper.postBarberShop(barberShop: BarberShopModel())
        } onFailure: { errorMessage in
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
}
