//
//  BarberShopLocationMapView.swift
//  elige
//
//  Created by macbook pro on 6/12/2021.
//

import SwiftUI
import MapKit
struct BarberShopLocationMapView: View {
    
    @StateObject var vmLocationMap = BarberShopLocationMapViewModel()
    
    @Binding var lat : Double
    @Binding var lng : Double
    
    @Binding var showMap : Bool
    
    @State var locationManager = CLLocationManager()
    
    var changeLocation : () -> () = {}
    
    @State var animateLocation = false
    @State var showCenterLocation = false
    @State var showLocationBtn = false
    var body: some View {
        
        VStack(){
            
            HStack(alignment: .center) {
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            showMap = false
                        }
                    }
                
                Spacer()
                
                Text("Localiser ma position")
                    .font(.system(weight: .bold, size: 20))
                    .foregroundColor(.primaryTextColor)
                
                Spacer()
                
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconCheckBlack
                            .resizable()
                            .frame(width: 20, height: 25)
                    ).onTapGesture {
                        vmLocationMap.getCenterMapLocation { latitude, longitude in
                            lat = latitude
                            lng = longitude
                            changeLocation()
                            showCenterLocation = false
                            showLocationBtn = false
                            withAnimation(.linear) {
                                showMap.toggle()
                            }
                        }
                    }
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            
            //Spacer()
            
            ZStack(alignment: .center){
                
                MapUIView(mapView: $vmLocationMap.mapView)
                    .mask(AppUtils.CustomCorners(corners: [.topLeft, .topRight], radius: AppConstants.viewRadius))
                    //.edgesIgnoringSafeArea(.all)
                    
                if(showCenterLocation){
                    Image.iconMarkerMap.resizable()
                        
                        .renderingMode(.template)
                        .colorMultiply(Color.primaryColor)
                        .frame(width: 40, height: 40, alignment: .center)
                        .foregroundColor(Color.primaryColor)
                        .scaleEffect(animateLocation ? 0.8 : 1)
                        .animation(
                            Animation.linear(duration: 1)
                                .repeatForever(autoreverses: false), value: animateLocation
                        )
                        .onAppear {
                            self.animateLocation = true
                            
                        }
                }
                
                VStack(){
                    
                    Spacer()
                    
                    HStack(){
                        
                        Spacer()
                        
                        if(showLocationBtn){
                            Button(action: {
                                
                                if CLLocationManager.locationServicesEnabled() {
                                    
                                    switch CLLocationManager.authorizationStatus() {
                                        case .notDetermined:
                                            DebugHelper.debug("notDetermined")
                                            locationManager.requestAlwaysAuthorization()
                                        case .denied:
                                            DebugHelper.debug("denied")
                                            //locationManager.requestAlwaysAuthorization()
                                            vmLocationMap.permissionDenied = true
                                        case .authorizedAlways, .authorizedWhenInUse:
                                            DebugHelper.debug("authorizedAlways")
                                            vmLocationMap.focusLocation()
                                        @unknown default:
                                            break
                                    }
                                } else {
                                    vmLocationMap.locationDisabled = true
                                }
                            }, label: {
                                CustomLocationView()
                            }).shadow(color: Color.black.opacity(0.3), radius: 5, x: 0, y: 5)
                        }
                    }.padding(AppConstants.viewExtraMargin)
                }
            }
        }
        .onAppear {
            if(lat == 0.0 && lng == 0.0){
                vmLocationMap.setRegion(lat: lat, lng: lng)
            }else{
                vmLocationMap.setRegion(lat: lat, lng: lng)
            }
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.delegate = vmLocationMap
                DebugHelper.debug(locationManager.authorizationStatus.rawValue)
                
                if(locationManager.authorizationStatus == .authorizedWhenInUse || locationManager.authorizationStatus == .authorizedAlways  ){
                    locationManager.requestLocation()
                }
            } else {
                DebugHelper.debug("locationDisabled")
                vmLocationMap.locationDisabled = true
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.6 ) { [self] in
                showCenterLocation = true
                showLocationBtn = true
            }
        }
        .onDisappear {
            showCenterLocation = false
            showLocationBtn = false
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
    }
}
struct BarberShopLocationMapView_Previews: PreviewProvider {
    static var previews: some View {
        BarberShopLocationMapView(lat: .constant(0.0), lng: .constant(0.0), showMap: .constant(false))
    }
}


struct CustomLocationView : View {
    
    @State var animate: CGFloat = 1
    
    var body : some View{
        
        ZStack{
            
            Image.iconMyLocation
                .resizable()
                .renderingMode(.template)
                .colorMultiply(Color.primaryColor)
                .frame(width: 20, height: 20, alignment: .center)
                .foregroundColor(Color.primaryColor)
                .padding(AppConstants.viewNormalMargin)
                .background(Color.white)
                .clipShape(Circle())
                .background(Circle().fill(Color.primaryColor.opacity(0.7))
                                //.stroke(Color.primaryColor)
                                
                                .scaleEffect(animate)
                                .opacity(Double(2 - animate))
                                .animation(
                                    Animation.easeOut(duration: 1)
                                        .repeatForever(autoreverses: false)
                                )
                )
                .onAppear(perform: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        // your code here
                        self.animate = 3
                    }
                    
                }).onDisappear {
                }
            
        }
    }
}
