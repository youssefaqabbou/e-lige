//
//  BarberShopLocationMapViewModel.swift
//  elige
//
//  Created by macbook pro on 6/12/2021.
//

import Foundation
import MapKit
import CoreLocation

class BarberShopLocationMapViewModel : NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var mapView = MKMapView()
    @Published var currentLocation = CLLocationCoordinate2D()
    
    //Region
    @Published var region : MKCoordinateRegion!
    //Alert
    @Published var permissionDenied = false
    @Published var locationDisabled = false
    @Published var isLocationFound = false
    
    func getCenterMapLocation(onSuccess successCallback: ((_ latitude: Double,_ longitude : Double ) -> Void)?){
        DebugHelper.debug(mapView.centerCoordinate)
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        successCallback!(latitude, longitude)
    }
    
    func setRegion(lat : Double, lng :Double){
        
        self.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: lng), latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setRegion(region, animated: true)
        //mapView.setVisibleMapRect(mapView.visibleMapRect, animated: true)
        
    }
    
    func focusLocation(){
        guard let _ = region else {
            return
        }
        mapView.setRegion(region, animated: true)
        mapView.setVisibleMapRect(mapView.visibleMapRect, animated: false)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .denied:
            permissionDenied = true
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse, .authorizedAlways :
            permissionDenied = false
            manager.requestLocation()
            break
        default:
            ()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        currentLocation = location.coordinate
        DebugHelper.debug(currentLocation)
        self.region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        isLocationFound = true
        
    }
}
