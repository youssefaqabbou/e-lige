//
//  EditBarberShopInfoView.swift
//  elige
//
//  Created by macbook pro on 2/12/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct EditBarberShopInfoView: View {
    
    @StateObject var vmEditInfo = EditBarberShopInfoViewModel()
    @Binding var showEditInfo : Bool
    @State var scrollViewOffset: CGFloat = 0
    @State var startOffset: CGFloat = 0
    @State var showMap: Bool = false
    
    var body: some View {
        ZStack{
            
            VStack{
                
                if vmEditInfo.isImagePicked {
                    ZStack(alignment: .center){
                        Image(uiImage: vmEditInfo.pickedImage)
                            .resizable()
                            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                        
                        ProgressView()
                            .frame(width: 25, height: 25)
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.primaryColor))
                            .opacity(vmEditInfo.isUploading ? 1 : 0)
                    }
                }else{
                    WebImage(url: URL(string: vmEditInfo.currentUser.image.url))
                        .placeholder {
                            Image.placeholderShop.resizable()
                        }
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                }
                
                Spacer()
                
            }
            
            ScrollView(.vertical, showsIndicators: false){
                    
                    VStack(alignment: .leading, spacing: 0){
                        
                        Text("")
                            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                            .hidden()
                        
                        VStack(alignment: .leading, spacing: 0){
                            
                            HStack(alignment: .top){
                                
                                Spacer()
                                
                                Circle().fill(Color.primaryColor)
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .overlay(
                                        Image.iconCameraEdit
                                            .resizable()
                                            .frame(width: 35, height: 35, alignment: .center)
                                    )
                                    .padding(.top, getSafe().top == 0 ? 12 : 0)
                                    .scaleEffect(-scrollViewOffset > 230 ? CGFloat.ulpOfOne : 1)
                                    .animation(.linear(duration: 0.2), value: scrollViewOffset)
                                    .offset(y: -42)
                                    .onTapGesture {
                                        hideKeyboard()
                                        vmEditInfo.isShowPickerAlert = true
                                    }.disabled(vmEditInfo.isUploading ? true : false)
                                
                            }.padding(.top, AppConstants.viewNormalMargin)
                            
                            VStack(alignment: .leading, spacing: 20){
                                
                                CustomTextField(placeholder: "Raison sociale", value: $vmEditInfo.shopName, error: vmEditInfo.shopNameError)
                                    .onChange(of: vmEditInfo.shopName, perform: { value in
                                        vmEditInfo.shopNameError = ValidatorHelper.validateSocialReason(socialReason: vmEditInfo.shopName).error
                                    })
                                                               
                                //CustomTextField(placeholder: "Email", value: $vmEditInfo.shopEmail)
                                
                                CustomTextField(placeholder: "Numéro de téléphone", value: $vmEditInfo.shopPhone, error: vmEditInfo.shopPhoneError)
                                    .onChange(of: vmEditInfo.shopPhone, perform: { value in
                                        vmEditInfo.shopPhoneError = ValidatorHelper.validatePhone(phone: vmEditInfo.shopPhone).error
                                    })
                                
                                
                                CustomTextField(placeholder: "Adresse", value: $vmEditInfo.shopAddress)
                                
                                HStack(){
                                    if(vmEditInfo.shopLatitude != 0.0 || vmEditInfo.shopLongitude != 0.0){
                                        Text("\(vmEditInfo.shopLatitude), \(vmEditInfo.shopLongitude)")
                                            .font(.system(weight: .regular, size: 18))
                                            .foregroundColor(Color.primaryTextColor)
                                    }else{
                                        Text("Emplacement").textStyle(PlaceHolderStyle())
                                    }
                                    Spacer(minLength: 0)
                                        
                                }
                                .frame(height: 65 )
                                .padding(.horizontal, AppConstants.viewExtraMargin)
                                .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                                .onTapGesture {
                                    hideKeyboard()
                                    withAnimation {
                                        showMap = true
                                    }
                                    
                                }
                                
                                CustomHintTextEditor(placeholder: "Décrivez votre boutique", text: $vmEditInfo.shopDescription)
                                    .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                                
                                //CustomTextField(placeholder: "", value: )
                                
                            }
                                                
                            Spacer(minLength: UIScreen.main.bounds.height / 3)
                        }
                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                        .padding(.bottom, 80)
                        .background(Color.bgViewColor)
                        
                    }.overlay(
                        
                        GeometryReader{proxy -> Color in
                            
                            DispatchQueue.main.async {
                                
                                if startOffset == 0{
                                    self.startOffset = proxy.frame(in: .global).minY
                                }
                                
                                let offset = proxy.frame(in: .global).minY
                                self.scrollViewOffset = offset - startOffset
                                
                            }
                            
                            return Color.clear
                            
                        }.frame(width: 0, height: 0)
                        ,alignment: .top
                    )
                }
            .onTapGesture {
                hideKeyboard()
            }
            
            VStack{
                
                HStack{
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            hideKeyboard()
                            withAnimation {
                                showEditInfo = false
                            }
                        }
                        .padding(.top, getSafe().top == 0 ? 12 : 0)
                        .scaleEffect(-scrollViewOffset > 230 ? CGFloat.ulpOfOne : 1)
                        .animation(.linear(duration: 0.2), value: scrollViewOffset)
                    
                    Spacer()
                }
                
                Spacer()
                
                CustomLoadingButton(isLoading: $vmEditInfo.isLoading, text: "Enregistrer", action: {
                    if(vmEditInfo.isFormComplete){
                        hideKeyboard()
                        vmEditInfo.updateAccount()
                    }
                })
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewVeryExtraMargin)
            
            if showMap {
                
                BarberShopLocationMapView(lat: $vmEditInfo.shopLatitude, lng: $vmEditInfo.shopLongitude, showMap: $showMap) {
                }.transition(.move(edge: .trailing))
                .zIndex(1)
            }else{
                Color.clear
            }
          
        }
        .onAppear(perform: {
            hideKeyboard()
        })
        
        .bottomSheet(isPresented: $vmEditInfo.isShowPickerAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            // Content
            AlertMediaPickerView(showingActionSheet: $vmEditInfo.isShowPickerAlert, onGalleryAction :{
                
                vmEditInfo.isShowSheet = true
                vmEditInfo.isCameraPicker = false
                vmEditInfo.isGalleryPicker = true
                
            }, onCameraAction: {
                
                vmEditInfo.isShowSheet = true
                vmEditInfo.isGalleryPicker = false
                vmEditInfo.isCameraPicker = true
                
            })
            
        }
        .fullScreenCover(isPresented: $vmEditInfo.isShowSheet, onDismiss : {
            vmEditInfo.isShowSheet = false
            vmEditInfo.isGalleryPicker = false
            vmEditInfo.isCameraPicker = false
            onPickerDismiss()
        }, content: {
            if (vmEditInfo.isGalleryPicker) {
                MediaPickerView(sourceType: .photoLibrary, pickedImage: self.$vmEditInfo.pickedImage, isImagePicked: $vmEditInfo.isImagePicked)
            } else if (vmEditInfo.isCameraPicker) {
                MediaPickerView(sourceType: .camera, pickedImage: self.$vmEditInfo.pickedImage, isImagePicked: $vmEditInfo.isImagePicked)
            }
            
        })
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
    }
    
    func onPickerDismiss() {
        if(vmEditInfo.isImagePicked){
            vmEditInfo.updateImageProfile { success in
            }
        }
       
    }
}

struct EditBarberShopInfoView_Previews: PreviewProvider {
    static var previews: some View {
        EditBarberShopInfoView(showEditInfo: .constant(false))
    }
}
