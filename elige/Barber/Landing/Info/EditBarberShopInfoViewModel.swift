//
//  EditBarberShopInfoViewModel.swift
//  elige
//
//  Created by macbook pro on 2/12/2021.
//

import Foundation
import UIKit

class EditBarberShopInfoViewModel: ObservableObject {
    private var apiService: APIService = APIService()
        
    @Published var isLoading : Bool = false
    @Published var isUploading : Bool = false
    
    @Published var currentUser = UserModel()
    @Published var shopName = ""
    @Published var shopAddress = ""
    @Published var shopEmail = ""
    @Published var shopPhone = ""
    @Published var shopDescription = ""
    
    @Published var shopNameError: String = ""
    @Published var shopAddresseError: String = ""
    @Published var shopEmailError: String = ""
    @Published var shopPhoneError: String = ""
    @Published var shopDescriptionError: String = ""
    
    @Published var shopLatitude = 0.0
    @Published var shopLongitude = 0.0
    
    // Image
    @Published var isShowPickerAlert: Bool = false
    
    @Published var isGalleryPicker: Bool = false
    @Published var isCameraPicker: Bool = false
    
    @Published var isShowSheet = false
    
    @Published var pickedImage = UIImage()
    @Published var isImagePicked = false
    
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
        fillInfo()
        
    }
    
    func fillInfo(){
        
        shopName = currentUser.fullname
        shopAddress = currentUser.address
        shopEmail = currentUser.email
        shopPhone = currentUser.phone
        shopDescription = currentUser.descr
        
        shopLatitude = currentUser.latitude
        shopLongitude = currentUser.longitude
        DebugHelper.debug(shopLatitude, shopLongitude )
    }
    
    var isFormComplete: Bool {
        let (isFullnameValid, shopNameError) = ValidatorHelper.validateSocialReason(socialReason: shopName)
        self.shopNameError = shopNameError
        
        /*let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: shopEmail)
        self.shopEmailError = emailError*/

        let (isPhoneValid, phoneError) = ValidatorHelper.validatePhone(phone: shopPhone)
        self.shopPhoneError = phoneError

        return isFullnameValid /*&& isEmailValid*/ && isPhoneValid
    }
    
    func updateAccount() {
        self.isLoading = true
        let params =  [
            "fullname": shopName,
            "email": shopEmail,
            "address": shopAddress,
            "latitude": String(shopLatitude),
            "longitude": String(shopLongitude),
            "phone": shopPhone,
            "description": shopDescription
        ]
        apiService.updateAccount(idAccount: currentUser.id, params: params) { message in
            self.isLoading = false
            self.updateBarberShop()
            AppFunctions.showSnackBar(status: .success , message: message)
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func updateImageProfile(onSuccess: @escaping (_ success: Bool) -> Void) {
        
        self.isUploading = true
        
        let params = [
            "type": "PROFILE"
        ] as [String : Any]
        
        apiService.uploadImageProfile(image: pickedImage, params: params) { success in
            
            self.isUploading = false
            self.pickedImage = UIImage()
            self.isImagePicked = false
            self.updateBarberShop()
            onSuccess(true)
        } onFailure: { errorMessage in
            self.isUploading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            onSuccess(false)
            self.pickedImage = UIImage()
            self.isImagePicked = false
        }
    }
    
    func updateBarberShop(){
        currentUser = AppFunctions.getConnectedUser()
        var barberShop = AppFunctions.decodeBarberShop()
        barberShop.name = currentUser.fullname
        barberShop.latitude = currentUser.latitude
        barberShop.longitude = currentUser.longitude
        barberShop.email = currentUser.email
        barberShop.phone = currentUser.phone
        barberShop.address = currentUser.address
        barberShop.country = currentUser.country
        barberShop.city = currentUser.city
        barberShop.zipcode = currentUser.zipcode
        barberShop.description = currentUser.descr
        barberShop.image = currentUser.image
        
        AppFunctions.encodeBarberShop(barberShop: barberShop)
    }
}
