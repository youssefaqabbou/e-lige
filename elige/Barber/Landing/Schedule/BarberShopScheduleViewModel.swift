//
//  BarberShopScheduleViewModel.swift
//  elige
//
//  Created by user196417 on 11/23/21.
//

import Foundation
import SwiftUI


class BarberShopScheduleViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    @Published var isLoading = false
    
    @Published var barberShop = BarberShopModel()
    @Published var scheduleList: [ScheduleModel] = []{
        didSet {
            //DebugHelper.debug("scheduleList didSet")
            //bjectWillChange.send()
        }
    }
    
    @Published var selectedSchedule = ScheduleModel()
    @Published var updateStart: Bool = false
    @Published var updateBreak: Bool = false
    @Published var updateBreakFrom: Bool = false
    
    func getScheduleList(){
        barberShop = AppFunctions.decodeBarberShop()
        scheduleList = barberShop.schedules
    }
    
    func checkSchedules() -> Bool {
        var isValid = true
        for schedule in scheduleList{
            if schedule.isOpen {
                if(schedule.openTime.isEmpty || schedule.closeTime.isEmpty  ){
                    self.sendTimeNotification(schedule)
                    isValid = false
                    //return false
                }else{
                    let (oh, om) = AppFunctions.timeToHoursMinutes(schedule.openTime)
                    let openDate = AppFunctions.getTodayDateUTC().changeTo(hour: oh, minute: om,  convertToUtc : !schedule.openTimeUpdated)
                    //let openDate = schedule.openTime.toDate(format: "HH:mm").today
                    let (ch, cm) = AppFunctions.timeToHoursMinutes(schedule.closeTime)
                    let closeDate = AppFunctions.getTodayDateUTC().changeTo(hour: ch, minute: cm,  convertToUtc : !schedule.closeTimeUpdated)
                    //let closeDate = schedule.closeTime.toDate(format: "HH:mm")
                    DebugHelper.debug("openDate", openDate)
                    DebugHelper.debug("closeDate", closeDate)
                    if closeDate.compare(openDate) == .orderedAscending {
                        DebugHelper.debug("First Date is smaller then second date")
                        self.sendTimeNotification(schedule)
                        isValid = false
                    }else{
                        if(!schedule.breakFrom.isEmpty && !schedule.breakTo.isEmpty  ){
                            let (oh, om) = AppFunctions.timeToHoursMinutes(schedule.breakFrom)
                            let breakFromDate = AppFunctions.getTodayDateUTC().changeTo(hour: oh, minute: om , convertToUtc : !schedule.breakFromUpdated)
                            //let openDate = schedule.openTime.toDate(format: "HH:mm").today
                            let (ch, cm) = AppFunctions.timeToHoursMinutes(schedule.breakTo)
                            let breakToDate = AppFunctions.getTodayDateUTC().changeTo(hour: ch, minute: cm, convertToUtc : !schedule.breakToUpdated)
                            
                            DebugHelper.debug("breakFromDate", breakFromDate)
                            DebugHelper.debug("breakToDate", breakToDate)
                            
                            if (breakToDate.compare(breakFromDate) == .orderedAscending || breakToDate.compare(breakFromDate) == .orderedSame ){
                                DebugHelper.debug("breakToDate is smaller than breakFromDate")
                                self.sendBreakTimeNotification(schedule)
                                isValid = false
                            }else if (breakFromDate.compare(openDate) == .orderedSame || breakFromDate.compare(openDate) == .orderedAscending) {
                                self.sendBreakTimeNotification(schedule)
                                self.sendTimeNotification(schedule)
                                isValid = false
                            }else if (closeDate.compare(breakToDate) == .orderedSame  || closeDate.compare(breakToDate) == .orderedAscending) {
                                self.sendBreakTimeNotification(schedule)
                                self.sendTimeNotification(schedule)
                                isValid = false
                            }
                        } else if(!schedule.breakFrom.isEmpty || !schedule.breakTo.isEmpty  ){
                            self.sendBreakTimeNotification(schedule)
                            isValid = false
                        }
                    }
                }
            }
        }
        return isValid
    }
    
    func sendTimeNotification(_ schedule: ScheduleModel){
        NotificationCenter.default.post(name: NSNotification.timeError,
                                        object: nil, userInfo: ["schedule" : schedule])
    }
    func sendBreakTimeNotification(_ schedule: ScheduleModel){
        NotificationCenter.default.post(name: NSNotification.breakTimeError,
                                        object: nil, userInfo: ["schedule" : schedule])
    }
    
    func updateSchedulesRequest(){
        self.isLoading = true
        
        if(!checkSchedules()){
            self.isLoading = false
            return
        }
        
        apiService.updateSchedules(params: getScheduleParams()) { message in
            AppFunctions.showSnackBar(status: .success, message: message)
            self.isLoading = false
            self.barberShop.schedules = self.scheduleList
            AppFunctions.encodeBarberShop(barberShop: self.barberShop)
        } onFailure: { errorMessage in
            AppFunctions.showSnackBar(status: .error, message: errorMessage)
            self.isLoading = false
        }
        
    }
    
    func getScheduleParams() ->  [String : Any] {
        var params: [String: Any] = [
            "schedules": [],
        ]
        for schedule in scheduleList{
            let schedules: [String: Any] = [
                "id": schedule.id,
                "opening": schedule.isOpen ? schedule.openTimeUpdated ? schedule.openTime.timeToUTC() : schedule.openTime : "",
                "closing": schedule.isOpen ? schedule.closeTimeUpdated ? schedule.closeTime.timeToUTC() : schedule.closeTime : "",
                "breakFrom": schedule.isOpen ? schedule.breakFromUpdated ? schedule.breakFrom.timeToUTC() : schedule.breakFrom : "",
                "breakTo": schedule.isOpen ? schedule.breakToUpdated ? schedule.breakTo.timeToUTC() : schedule.breakTo : "",
                "isClosed": !schedule.isOpen,
            ]
            
            // get existing items, or create new array if doesn't exist
            var existingItems = params["schedules"] as? [[String: Any]] ?? [[String: Any]]()
            // append the item
            existingItems.append(schedules)
            // replace back into `data`
            params["schedules"] = existingItems
        }
        
        DebugHelper.debug("params", params.description)
        return params
    }
    
    func updateSchedule(updatedSchedule : ScheduleModel) {
        let indexId = getSchedule(updatedSchedule.id)
        scheduleList[indexId] = updatedSchedule
        DebugHelper.debug("isOpen",  updatedSchedule.isOpen)
        //DebugHelper.debug("Service Location",  updatedSchedule.serviceLocation)
        objectWillChange.send()
    }
    
    func clearBreakSchedule(updatedSchedule : ScheduleModel) {
        let indexId = getSchedule(updatedSchedule.id)
        scheduleList[indexId].breakFrom = ""
        scheduleList[indexId].breakTo = ""
        scheduleList[indexId].haveBreak = false
        objectWillChange.send()
    }
    
    func updateScheduleTime(hours: Int, minutes : Int) {
        let timeSelected =  String(format: "%02d", hours) + ":" + String(format: "%02d", minutes)
        DebugHelper.debug(timeSelected)
        let index = getSchedule(selectedSchedule.id)
        if(index != -1){
            if(!updateBreak ){
                
                if self.updateStart {
                    scheduleList[index].openTimeUpdated = true
                    scheduleList[index].openTime = timeSelected
                }else{
                    scheduleList[index].closeTimeUpdated = true
                    scheduleList[index].closeTime = timeSelected
                }
            }else{
                if self.updateBreakFrom {
                    scheduleList[index].breakFromUpdated = true
                    scheduleList[index].breakFrom = timeSelected
                }else{
                    scheduleList[index].breakToUpdated = true
                    scheduleList[index].breakTo = timeSelected
                }
                if(!scheduleList[index].breakFrom.isEmpty && !scheduleList[index].breakTo.isEmpty){
                    scheduleList[index].haveBreak = true
                }
            }
        }
        
        objectWillChange.send()
    }
    
    func getSelectedTime() -> String {
        if(!updateBreak ){
            if self.updateStart {
                return selectedSchedule.openTime
            }else{
                return selectedSchedule.closeTime
            }
        }else{
            if self.updateBreakFrom {
                return selectedSchedule.breakFrom
            }else{
                return selectedSchedule.breakTo
            }
        }
    }
    
    func getSchedule(_ id : Int) -> Int{
        for (index, value) in scheduleList.enumerated(){
            if value.id == id{
                return index
            }
        }
        return -1
    }
    
}
