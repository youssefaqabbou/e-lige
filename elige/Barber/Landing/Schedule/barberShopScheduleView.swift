//
//  barberShopScheduleView.swift
//  elige
//
//  Created by user196417 on 11/12/21.
//

import SwiftUI

struct BarberShopScheduleView: View {
    
    @StateObject var vmUpdateSchedule = BarberShopScheduleViewModel()
    @Binding var showSchedule : Bool
    //@State var barberShop: BarberShopModel
    @State var showDuration = false
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20){
            
            HStack(){
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    )
                    .onTapGesture {
                        withAnimation {
                            showSchedule = false
                        }
                    }
                
                Spacer()
                
                Text("Horaires")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                
                Spacer()
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .hidden()
                
            }
            
            ScrollView(.vertical, showsIndicators: false){
                
                VStack(alignment: .leading, spacing: 25){
                    
                    Text("Heures disponible")
                        .font(.system(weight: .regular, size: 22))
                        .foregroundColor(Color.primaryTextColor)
                        .padding(.top, 20)
                    
                    VStack(spacing: 10){
                        ForEach(vmUpdateSchedule.scheduleList, id: \.id) { schedule in
                            ScheduleEditItemView(schedule: schedule) { schedul in
                                vmUpdateSchedule.updateSchedule(updatedSchedule: schedul)
                            } onStartScheduleAction: {
                                vmUpdateSchedule.selectedSchedule = schedule
                                vmUpdateSchedule.updateStart = true
                                vmUpdateSchedule.updateBreak = false
                                vmUpdateSchedule.updateBreakFrom = false
                                withAnimation {
                                    showDuration = true
                                }
                                
                            } onEndScheduleAction: {
                                vmUpdateSchedule.selectedSchedule = schedule
                                vmUpdateSchedule.updateStart = false
                                vmUpdateSchedule.updateBreak = false
                                vmUpdateSchedule.updateBreakFrom = false
                                withAnimation {
                                    showDuration = true
                                }
                            }
                        }
                    }
                    
                    Text("Heures de pause")
                        .font(.system(weight: .regular, size: 22))
                        .foregroundColor(Color.primaryTextColor)
                        .padding(.top, 10)
                    
                    VStack(spacing: 20){
                        
                        ForEach(vmUpdateSchedule.scheduleList, id: \.id) { schedule in
                            scheduleBreakItemView(schedule: schedule, onBreakFromAction: {
                                vmUpdateSchedule.selectedSchedule = schedule
                                vmUpdateSchedule.updateStart = false
                                vmUpdateSchedule.updateBreak = true
                                vmUpdateSchedule.updateBreakFrom = true
                                withAnimation {
                                    showDuration = true
                                }
                            }, onBreakToAction: {
                                vmUpdateSchedule.selectedSchedule = schedule
                                vmUpdateSchedule.updateStart = false
                                vmUpdateSchedule.updateBreak = true
                                vmUpdateSchedule.updateBreakFrom = false
                                withAnimation {
                                    showDuration = true
                                }
                            }, onClearBreakAction : {
                                vmUpdateSchedule.clearBreakSchedule(updatedSchedule: schedule)
                            })
                        }
                    }
                }.padding(.bottom, 20)
                   
            }
            
            Spacer()
            
            CustomLoadingButton(isLoading: $vmUpdateSchedule.isLoading, text: "Enregistrer" ) {
                vmUpdateSchedule.updateSchedulesRequest()
            }.padding(.bottom, AppConstants.viewExtraMargin)
        }
        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .bottomSheet(isPresented: $showDuration, height: 400, topBarCornerRadius: 45 , showTopIndicator: false) {
            let (h, m) = AppFunctions.timeToHoursMinutes(vmUpdateSchedule.getSelectedTime())
            ScheduleTimePickerView(showDuration: $showDuration, isStart : vmUpdateSchedule.updateStart ? .constant(true) :  vmUpdateSchedule.updateBreakFrom ? .constant(true) : .constant(false)  ,  hours: h, minutes: m, onChangeTimeAction: { hours, minutes in
                DebugHelper.debug("hours" , hours, "minutes", minutes )
                
                vmUpdateSchedule.updateScheduleTime(hours: hours, minutes: minutes)
                
            })
          
        }.onAppear(){
            vmUpdateSchedule.getScheduleList()
            DebugHelper.debug(vmUpdateSchedule.scheduleList.count)
        }
    }
}

struct BarberShopScheduleView_Previews: PreviewProvider {
    static var previews: some View {
        BarberShopScheduleView(showSchedule: .constant(false))
    }
}

struct ScheduleTimePickerView: View {
    @Binding var showDuration : Bool
    @Binding var isStart : Bool
    @State var hours : Int
    @State var minutes : Int
   
    
    var onChangeTimeAction : (Int, Int) -> () = {hours, minutes in }
    
    var body: some View {
        VStack(alignment: .center, spacing: 10){
            
            Text(isStart ? "Début" : "Fin")
                .textStyle(AlertTitleStyle())
                
            HStack(alignment: .center, spacing: 15){
                
                
                CustomPicker(selection: $hours, numbers: Array(stride(from: 0, through: 23, by: 1)))
                
                Text(":")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .bold, size: 14))
                
                CustomPicker(selection: $minutes, numbers: Array(stride(from: 0, through: 59, by: 1)))
                
            }.padding(.bottom, 30)
           
            Button(action: {
                withAnimation {
                    showDuration = false
                    onChangeTimeAction(hours, minutes)
                    self.hours = 0
                    self.minutes = 0
                }
            }, label: {

                Text("Terminé")
                    .font(.system(weight: .bold, size: 16))

            }).buttonStyle(PrimaryButtonStyle())
            
           
        }.frame(width: UIScreen.main.bounds.width - 40, alignment: .center)
    }
}

struct BasePicker: UIViewRepresentable {
    var selection: Binding<Int>
    let data: [Int]
    
    init(selecting: Binding<Int>, data: [Int]) {
        self.selection = selecting
        self.data = data
    }
    
    func makeCoordinator() -> BasePicker.Coordinator {
        Coordinator(self)
    }
    
    func makeUIView(context: UIViewRepresentableContext<BasePicker>) -> UIPickerView {
        let picker = UIPickerView()
        picker.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        picker.dataSource = context.coordinator
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIView(_ view: UIPickerView, context: UIViewRepresentableContext<BasePicker>) {
        guard let row = data.firstIndex(of: selection.wrappedValue) else { return }
        view.selectRow(row, inComponent: 0, animated: false)
    }
    
    class Coordinator: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
        var parent: BasePicker
        
        init(_ pickerView: BasePicker) {
            parent = pickerView
        }
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
            return 90
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return parent.data.count
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return String(format: "%02d", parent.data[row])
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            parent.selection.wrappedValue = parent.data[row]
        }
    }
}


struct CustomPicker: View {
    @Binding var selection: Int
    let pickerColor: Color = .white
    let width : CGFloat = 90
    let height : CGFloat = 200
    let radius : CGFloat = 15

    let numbers: [Int] // = Array(stride(from: 0, through: 100, by: 1))
    
    var stroke: some View {
        RoundedRectangle(cornerRadius: 16)
            .stroke(lineWidth: 2)
    }
    
    var backgroundColor: some View {
        pickerColor
            //.opacity(0.25)
    }
    
    var body: some View {
        BasePicker(selecting: $selection, data: numbers)
            .frame(width: width, height: height)
            //.overlay(stroke)
            .background(backgroundColor)
            .cornerRadius(radius)
    }
}
