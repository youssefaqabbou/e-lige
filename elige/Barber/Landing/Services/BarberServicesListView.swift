//
//  BarberServicesListView.swift
//  elige
//
//  Created by macbook pro on 10/11/2021.
//

import SwiftUI

struct BarberServicesListView: View {
    @StateObject var vmServices = BarberServicesListViewModel()
    @Binding var showServices : Bool
    @State var showNewService = false
    @State var showEditService = false
    
    @State var selectedService = ServiceModel()
    var body: some View {
        
        ZStack(){
            
            VStack(){
                
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showServices = false
                            }
                        }
                    
                    Spacer()
                    
                    Text("Prestations")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image(systemName: "plus")
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showNewService = true
                            }
                        }
                }
                .padding(.bottom, AppConstants.viewNormalMargin)
                
                
                CollectionLoadingView(loadingState: vmServices.loadingState) {
                    ListShimmeringView(count: 15){
                        ServiceEditItemHolderView()
                    }
                } content: {
                    
                    ScrollView(.vertical, showsIndicators: false){
                        VStack(spacing: 15){
                            ForEach(vmServices.serviceList, id: \.id) { service in
                                ServiceEditItemView(service: service, onEditAction: {
                                    selectedService = service
                                    withAnimation {
                                        showEditService = true
                                    }
                                })
                            }
                        }
                    }
                    .padding(.bottom, AppConstants.viewExtraMargin)
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_service_shop_empty_title.localized, message: LocalizationKeys.list_service_shop_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_service_shop_empty_title.localized, message: LocalizationKeys.list_service_shop_empty_message.localized)
                }
                
            }
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
               
            
            if(showNewService){
                NewServiceView(showNewService: $showNewService, onServiceEdited: { service in
                    vmServices.updateServiceInList(service: service)
                }, onServiceAdded: { service in
                    vmServices.addServiceToList(service: service)
                }, onServiceDeleted: { service in
                    vmServices.removeServiceFromList(service: service)
                }).transition(.move(edge: .trailing)).animation(.linear, value: showServices).zIndex(1)
            }
            if(showEditService){
                NewServiceView(showNewService: $showEditService, isEdit: true, currentService: selectedService, onServiceEdited: { service in
                    vmServices.updateServiceInList(service: service)
                }, onServiceAdded: { service in
                    vmServices.addServiceToList(service: service)
                }, onServiceDeleted: { service in
                    vmServices.removeServiceFromList(service: service)
                }).transition(.move(edge: .trailing)).animation(.linear, value: showServices).zIndex(1)
            }
                
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            vmServices.getServices()
        }
    }
}

struct BarberServicesListView_Previews: PreviewProvider {
    static var previews: some View {
        BarberServicesListView(showServices: .constant(false))
    }
}
