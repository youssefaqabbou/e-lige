//
//  NewServiceView.swift
//  elige
//
//  Created by user196417 on 11/11/21.
//

import SwiftUI

struct NewServiceView: View {
    
    @StateObject var vmAddService = NewServiceViewModel()
    
    @Binding var showNewService: Bool
    @State var isEdit: Bool = false
    @State var currentService: ServiceModel = ServiceModel()
    //@State var isToggleOn = true
    
    var onServiceEdited : (ServiceModel) -> () = {service in }
    var onServiceAdded : (ServiceModel) -> () = {service in }
    var onServiceDeleted : (ServiceModel) -> () = {service in }
    

    @State var titles = ["Salon", "À domicile", "Les deux"]
    
    var body: some View {
        
        ZStack {
            
            VStack(alignment: .leading, spacing: 10) {
                
                HStack() {
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            hideKeyboard()
                            withAnimation {
                                showNewService = false
                            }
                        }
                    
                    Spacer()
                    
                    Text(isEdit ? "Modifier prestation" : "Nouvelle prestation")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).hidden()
                }.padding(.bottom, AppConstants.viewSmallMargin)
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .leading, spacing: 15) {
                        
                        VStack(alignment: .leading, spacing: 5){
                            HStack {
                                Text("Emplacement")
                                    .font(.system(weight: .bold, size: 14))
                                    .foregroundColor(Color.secondaryTextColor)
                                
                                Spacer()
                            }
                            
                            CustomSegmentedView(titles : titles, selected: $vmAddService.selectedServiceLocation,  frame : UIScreen.main
                                                    .bounds.width - ( AppConstants.viewVeryExtraMargin * 2 ) )
                            
                        }
                        
                        VStack(alignment: .leading, spacing: 5){
                            HStack {
                                Text("Nom")
                                    .font(.system(weight: .bold, size: 14))
                                    .foregroundColor(Color.secondaryTextColor)
                                
                                Spacer()
                            }
                            
                            CustomTextField(placeholder: "", value: $vmAddService.name, error: vmAddService.nameError)
                        }
                        
                        
                        VStack(alignment: .leading, spacing: 5){
                            Text("Catégorie")
                                .font(.system(weight: .bold, size: 14))
                                .foregroundColor(Color.secondaryTextColor)
                            
                            HStack() {
                                Text(vmAddService.selectedCategory.id != -1 ? vmAddService.selectedCategory.name : "" )
                                    .foregroundColor(Color.primaryTextColor)
                                    .font(.system(weight: .regular, size: 18))
                                Spacer(minLength: 0)
                                
                            }
                            .frame(height: 65)
                            .padding(.horizontal, AppConstants.viewExtraMargin)
                            .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                            .onTapGesture {
                                hideKeyboard()
                                withAnimation {
                                    vmAddService.showCategories = true
                                    
                                }
                            }
                            
                            CustomErrorText(error: vmAddService.categoryError).padding(.top, AppConstants.viewVerySmallMargin)
                        }
                        
                        VStack(alignment: .leading, spacing: 5){
                            Text("Prix")
                                .font(.system(weight: .bold, size: 14))
                                .foregroundColor(Color.secondaryTextColor)
                            
                            HStack(spacing: 0){
                                CustomTextField(placeholder: "", value: $vmAddService.price, error: vmAddService.priceError, keyboardType: UIKeyboardType.decimalPad)
                                
                                Spacer()
                                
                                Text("€")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                            }.padding(.trailing, AppConstants.viewExtraMargin)
                                .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                            
                        }
                        
                        VStack(alignment: .leading, spacing: 5){
                            
                            Text("Durée")
                                .font(.system(weight: .bold, size: 14))
                                .foregroundColor(Color.secondaryTextColor)
                            
                            HStack() {
                                Text("\(vmAddService.duration)")
                                    .font(.system(weight: .regular, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Spacer(minLength: 0)
                                
                            }
                            .frame(height: 65)
                            .padding(.horizontal, AppConstants.viewExtraMargin)
                            .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                            .onTapGesture {
                                hideKeyboard()
                                withAnimation {
                                    vmAddService.showDuration = true
                                }
                            }
                            
                            CustomErrorText(error: vmAddService.dureeError).padding(.top, AppConstants.viewTinyMargin)
                        }
                        
                        VStack(alignment: .leading, spacing: 5){
                            
                            Text("Description")
                                .font(.system(weight: .bold, size: 14))
                                .foregroundColor(Color.secondaryTextColor)
                            
                            CustomHintTextEditor(placeholder: "", text: $vmAddService.descr)
                                .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                            //.cornerRadius(20, corners: .allCorners)
                            
                            
                        }
                    }
                }
                
                Spacer()
                
                VStack(spacing: 10) {
                    
                    CustomLoadingButton(isLoading: $vmAddService.isLoading, text: "Enregistrer", btnStyle: PrimaryButtonStyle( padding: 15), height: 40) {
                        if(vmAddService.isFormComplete){
                            hideKeyboard()
                            if(isEdit) {
                                vmAddService.updateService(idService: currentService.id){ service in
                                    onServiceEdited(service)
                                    withAnimation {
                                        self.showNewService = false
                                    }
                                }
                            } else {
                                vmAddService.addService { service in
                                    onServiceAdded(service)
                                    withAnimation {
                                        self.showNewService = false
                                    }
                                }
                            }
                        }
                    }.disabled(vmAddService.isDeleting ? true : false)
                        .disabled(vmAddService.isLoading ? true : false)
                    
                    if(isEdit) {
                        CustomLoadingButton(isLoading: $vmAddService.isDeleting, text: "Supprimer", btnStyle: PrimaryButtonStyle(bgColor: Color.red, padding: 15), height: 40) {
                            withAnimation {
                                vmAddService.showDeleteConfirmation = true
                            }
                            
                        }.padding(.top, AppConstants.viewExtraMargin)
                            .disabled(vmAddService.isDeleting ? true : false)
                            .disabled(vmAddService.isLoading ? true : false)
                    }
                }
                .padding(.bottom, AppConstants.viewExtraMargin)
            }
            //.padding(.vertical, AppConstants.viewNormalMargin)
            .onTapGesture {
                hideKeyboard()
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            
            if vmAddService.showCategories {
                CategoryAlertView(closePopup: $vmAddService.showCategories, selectedSpeciality: $vmAddService.selectedCategory)
            }
            
        }
        
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .bottomSheet(isPresented: $vmAddService.showDuration, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            
            VStack(alignment: .center, spacing: 10) {
                
                Text("Durée")
                    .font(.system(weight: .bold, size: 18))
                
                HStack(alignment: .center, spacing: 15) {
                    
                    CustomPicker(selection: $vmAddService.hours, numbers: Array(stride(from: 0, through: 23, by: 1)))
                    
                    Text("Heures")
                        .foregroundColor(Color.primaryTextColor)
                        .font(.system(weight: .bold, size: 14))
                    
                    CustomPicker(selection: $vmAddService.minutes, numbers: Array(stride(from: 0, through: 59, by: 1)))
                   
                    Text("Minutes")
                        .foregroundColor(Color.primaryTextColor)
                        .font(.system(weight: .bold, size: 14))
                    
                }//.padding(.bottom, 30)
                
                Spacer()
                
                Button(action: {
                    withAnimation {
                        vmAddService.updateDuration()
                        vmAddService.showDuration = false
                    }
                }, label: {
                    Text("Valider")
                        .font(.system(weight: .bold, size: 16))
                    
                }).buttonStyle(PrimaryButtonStyle())
                
            }.padding(.horizontal, AppConstants.viewExtraMargin)
                .padding(.bottom, AppConstants.viewVeryExtraMargin)
        }
        .bottomSheet(isPresented: $vmAddService.showDeleteConfirmation, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDelete, title: LocalizationKeys.title_service_remove.localized, message:  LocalizationKeys.message_service_remove.localized , positiveAction: {
                vmAddService.deleteService(idService: currentService.id) { success in
                    withAnimation {
                        vmAddService.showDeleteConfirmation = false
                    }
                    if(success){
                        onServiceDeleted(currentService)
                        withAnimation {
                            self.showNewService = false
                        }
                    }
                }
            }, isNegative: true, negativeAction: {
                withAnimation {
                    vmAddService.showDeleteConfirmation = false
                }
            })
        }
        .onAppear() {
            hideKeyboard()
            if(isEdit) {
                vmAddService.fillInfo(service: currentService)
            }
        }
    }
}

struct NewServiceView_Previews: PreviewProvider {
    static var previews: some View {
        NewServiceView(showNewService: .constant(false))
    }
}

