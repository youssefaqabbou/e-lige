//
//  BarberServicesListViewModel.swift
//  elige
//
//  Created by macbook pro on 10/11/2021.
//

import Foundation

class BarberServicesListViewModel: ObservableObject {
    private var apiService: APIService = APIService()
    @Published var isLoading = false
    
    @Published var barberShop = BarberShopModel()
    @Published var serviceList : [ServiceModel] = []
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    init(){
        barberShop = AppFunctions.decodeBarberShop()
    }
    
    func getServices(){
        
        apiService.getServices(params: nil, onSuccess: { services in
            self.serviceList = services
            
            if(services.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
            
        }, onFailure: { errorMessage in
            self.loadingState = .empty
        })
    }
    
    func updateServiceInList(service: ServiceModel){
        let index = getServiceIndex(serviceId: service.id)
        if index != nil{
            serviceList[index!] = service
            DebugHelper.debug(serviceList[index!].serviceLocation)
        }
        self.barberShop.services = self.serviceList
        AppFunctions.encodeBarberShop(barberShop: self.barberShop)
        objectWillChange.send()
    }
    
    func addServiceToList(service: ServiceModel){
        serviceList.insert(service, at: 0)
        self.barberShop.services = self.serviceList
        AppFunctions.encodeBarberShop(barberShop: self.barberShop)
        objectWillChange.send()
        self.loadingState = .loaded
    }
    
    func removeServiceFromList(service: ServiceModel){
        let index = getServiceIndex(serviceId: service.id)
        if index != nil{
            serviceList.remove(at: index!)
            self.barberShop.services = self.serviceList
            AppFunctions.encodeBarberShop(barberShop: self.barberShop)
            
            if(serviceList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }
    }
    
    func getServiceIndex(serviceId : Int) -> Int? {
        for (index, value) in serviceList.enumerated(){
            if value.id == serviceId {
                return index
            }
        }
        return nil
    }
}
