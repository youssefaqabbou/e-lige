//
//  NewServiceViewModel.swift
//  elige
//
//  Created by user196417 on 11/11/21.
//

import Foundation
import SwiftUI

class NewServiceViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var isLoading = false
    @Published var isDeleting = false
    
    @Published var currentUser = UserModel()
    
    @Published var name: String = ""
    @Published var price: String = ""
    @Published var duration: String = ""
    @Published var descr: String = ""
    @Published var selectedCategory = TagModel()
    
    @Published var nameError: String = ""
    @Published var categoryError: String = ""
    @Published var priceError: String = ""
    @Published var dureeError: String = ""
    @Published var descrError: String = ""
    
    @Published var hours: Int = 0
    @Published var minutes: Int = 0
    
    @Published var showDuration = false
    @Published var showDeleteConfirmation = false
    @Published var showCategories = false
    
    @Published var selectedServiceLocation = 0
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
    }
    
    func fillInfo(service : ServiceModel){
        name = service.name
        price = String(format: "%.2f", service.price)
        descr = service.description
      
        let (h,m,_) = AppFunctions.secondsToHoursMinutesSeconds(service.duration * 60)
        hours = h
        minutes = m
        
        duration = String(format: "%02d", hours)  + ":" + String(format: "%02d", minutes)
        selectedCategory = service.category
        DebugHelper.debug(service.serviceLocation)
        selectedServiceLocation = service.serviceLocation ==  ServiceLocation.shop.rawValue ? 0 : service.serviceLocation ==  ServiceLocation.home.rawValue ? 1 : 2
    }
    
    func updateDuration(){
        duration = String(format: "%02d", hours)  + ":" + String(format: "%02d", minutes)
    }
    
    var isFormComplete: Bool {
        
        let (isNameValid, errorName) = ValidatorHelper.validateServiceName(name : name)
        self.nameError = errorName
        
        let (isCategoryValid, errorCategory) = ValidatorHelper.validateServiceCategory(category: selectedCategory.id)
        self.categoryError = errorCategory
        
        let (isPriceValid, errorPrice) = ValidatorHelper.validateServicePrice(price: price)
        self.priceError = errorPrice
        
        let (isDurationValid, errorDuration) = ValidatorHelper.validateServiceDuration(duration: duration)
        self.dureeError = errorDuration
      
        return isNameValid && isCategoryValid && isPriceValid && isDurationValid
    }
    
    
    func addService(onSuccess: @escaping ( ServiceModel) -> Void) {
        self.isLoading = true
        let params = ["categoryId": String(selectedCategory.id),
                      "name": name,
                      "price": price,
                      "duration": duration,
                      "description": descr,
                      "place": selectedServiceLocation == 0 ? ServiceLocation.shop.rawValue : selectedServiceLocation == 1 ? ServiceLocation.home.rawValue : ServiceLocation.both.rawValue
                      // ["Salon", "À domicile", "Les deux"]
                    ] as [String : Any]
        
        apiService.addService(params: params, onSuccess: { message, service  in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .success , message: message)
            onSuccess(service)
        }, onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })

    }
    
    func updateService(idService: Int, onSuccess: @escaping ( ServiceModel) -> Void) {
        self.isLoading = true
        let params = ["categoryId": String(selectedCategory.id),
                      "name": name,
                      "price": price,
                      "duration": duration,
                      "description": descr,
                      "place": selectedServiceLocation == 0 ? ServiceLocation.shop.rawValue : selectedServiceLocation == 1 ? ServiceLocation.home.rawValue : ServiceLocation.both.rawValue
        ] as [String : Any]
        
        apiService.updateService(idService: idService, params: params, onSuccess: { message, service in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .success , message: message)
            onSuccess(service)
        }, onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }
    
    func deleteService(idService: Int, onSuccess: @escaping ( Bool) -> Void) {
        self.isDeleting = true
        apiService.deleteService(idService: idService, onSuccess: { message, response in
            self.isDeleting = false
            AppFunctions.showSnackBar(status: .success , message: message)
            onSuccess(true)
        }, onFailure: { errorMessage in
            self.isDeleting = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            onSuccess(false)
        })
    }
    
}
