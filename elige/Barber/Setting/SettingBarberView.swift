//
//  SettingBarberView.swift
//  elige
//
//  Created by user196417 on 12/6/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct SettingBarberView: View {
    
    @StateObject var vmSettingBarber = SettingBarberViewModel()
    
    var updateProfileAction : () -> () = {}
    var disconnectAction : () -> () = {}
    
    let NC = NotificationCenter.default
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading, spacing: 15){
                
                HStack{
                    
                    Spacer()
                    
                    Text("Paramètres")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    
                }.padding(.bottom, AppConstants.viewExtraMargin)
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    VStack(alignment: .leading){
                        
                        HStack(spacing: 20){
                            
                            WebImage(url: URL(string: vmSettingBarber.barber.image.url))
                                .placeholder {
                                    Image.placeholderShop.resizable()
                                }
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 80, height: 80)
                                .clipShape(Circle())
                            
                            VStack(alignment: .leading, spacing: 10){
                                
                                Text(vmSettingBarber.barber.fullname)
                                    .font(.system(weight: .regular, size: 20))
                                    .foregroundColor(Color.primaryTextColor)
                                
                                Text(vmSettingBarber.barber.phone)
                                    .font(.system(weight: .regular, size: 14))
                                    .foregroundColor(Color.secondaryTextColor)
                            }
                            
                            Spacer()
                            
                        }.padding(.top, AppConstants.viewExtraMargin)
                        
                        VStack(alignment: .leading, spacing: 30){
                            
                            Divider().frame(width: 200, height: 1, alignment: .center)
                                .background(Color.secondaryTextColor)
                            
                            HStack(spacing: 25){
                                
                                Image.iconProfil
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                
                                Text("Profil")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }.onTapGesture {
                                updateProfileAction()
                            }
                            
                            HStack(spacing: 25){
                                
                                Image.iconEvaluer
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                
                                Text("Évaluer E-lige")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }
                            HStack(spacing: 25){
                                
                                Image.iconApropos
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                
                                Text("À propos")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }
                            
                            
                            HStack(spacing: 25){
                                
                                Image.iconFAQs
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                
                                Text("FAQ")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }
                            
                            HStack(spacing: 25){
                                
                                Image.iconDisconnect
                                    .resizable()
                                    .frame(width: 30, height: 30)
                                
                                Text("Déconnexion")
                                    .font(.system(weight: .bold, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }.onTapGesture {
                                disconnectAction()
                            }
                            
                        }.padding()
                    }
                    
                    CustomLoadingButton(isLoading: $vmSettingBarber.checkingStripe, text: vmSettingBarber.isStripeComplete ? "Modifier votre compte Stripe" : "Compléter votre compte Stripe") {
                        vmSettingBarber.getAccountLink { url in
                            vmSettingBarber.showSafari = true
                            vmSettingBarber.linkStripe = url
                        }
                    }.padding(.horizontal, AppConstants.viewExtraMargin)
                        .padding(.top, AppConstants.viewVeryExtraMargin)
                    
                }
                
            }.padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
        }.background(Color.bgViewColor)
           .sheet(isPresented: $vmSettingBarber.showSafari, onDismiss: {
                vmSettingBarber.checkAccount()
               
            }) {
                SafariView(url: $vmSettingBarber.linkStripe)
            }
            .onAppear {
                vmSettingBarber.checkAccount()
                self.NC.addObserver(forName: NSNotification.profileUpdated, object: nil, queue: nil,
                                    using: self.profileUpdated)
            }
    }
    
    func profileUpdated(_ notification: Notification) {
        vmSettingBarber.refreshProfile()
    }
}

struct SettingBarberView_Previews: PreviewProvider {
    static var previews: some View {
        SettingBarberView()
    }
}


import SafariServices
struct SafariView: UIViewControllerRepresentable {
    
    @Binding var url: String
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<SafariView>) -> SFSafariViewController {
        DebugHelper.debug(url)
        return SFSafariViewController(url: URL(string: url)!)
    }
    
    func updateUIViewController(_ uiViewController: SFSafariViewController, context: UIViewControllerRepresentableContext<SafariView>) {
        
    }
}
