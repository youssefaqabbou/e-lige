//
//  SettingBarberViewModel.swift
//  elige
//
//  Created by user196417 on 12/9/21.
//

import Foundation

class SettingBarberViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var barber = AppFunctions.getConnectedUser()
    
    @Published var showSafari = false
    @Published var linkStripe = ""
    @Published var isStripeComplete : Bool = false
    @Published var checkingStripe = false
    
    func refreshProfile(){
        barber = AppFunctions.getConnectedUser()
    }
    
    func getAccountLink(completion:  @escaping (_ url:String) -> Void) {
        let params = [
            "accountId" : barber.accountId,
            "mobile" : "true"
        ]
        DebugHelper.debug(params)
        apiService.completeStripeAccount(params: params) { url in
            DebugHelper.debug(url)
            completion(url)
        } onFailure: { errorMessage in
            DebugHelper.debug(errorMessage)
        }
    }
    
    func checkAccount(){
        self.checkingStripe = true
        apiService.getStripeAccount { completed in
            
            self.isStripeComplete = true
            self.checkingStripe = false
        } onFailure: { errorMessage in
            self.isStripeComplete = false
            self.checkingStripe = false
        }
    }
    
}
