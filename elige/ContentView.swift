//
//  ContentView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI

struct ContentView: View {
    @StateObject var vmContent = ContentViewModel()
    
    @State var isSplashScreenFinish = false
    @State var isSplashFinishedWithError = false
    @State var isSplashFinishedWithErrorMessage = ""
    //@State var isPresentationViewed = false
    @State var isLogged = false
    @AppStorage(UserDefaults.Keys.showLogin) var showLogin: Bool = true
    @AppStorage(UserDefaults.Keys.isFirstUse) var isFirstUse: Bool = true
    @AppStorage(UserDefaults.Keys.token) var token: String = ""
    @AppStorage(UserDefaults.Keys.validAccount) var isValidateAccount: Bool = false
    
    @AppStorage(UserDefaults.Keys.isBarber) var isBarber: Bool = false
    @AppStorage(UserDefaults.Keys.showLoginDialog) var showLoginAlert: Bool = false
    
    @State var isConnected = true
    
    let NC = NotificationCenter.default
    
    var body: some View {
        ZStack(){
            VStack(spacing: 0) {
                
                if isSplashScreenFinish {
                    
                    if(isFirstUse) {
                        OnboardingView()
                    } else {
                        if(showLogin) {
                            SignInView().transition(.move(edge: .trailing)).animation(.linear, value: showLogin)
                                //.keyboardAdaptive()
                        } else {
                            if(isBarber){
                                BarberHomeView().transition(.move(edge: .trailing)).animation(.linear, value: showLogin)
                            }else{
                                CustomerHomeView().transition(.move(edge: .trailing)).animation(.linear, value: showLogin)
                                //.keyboardAdaptive()
                            }
                        }
                    }
                    
                } else {
                    SplashView(isFinished: $isSplashScreenFinish, withError : $isSplashFinishedWithError, withErrorMessage :$isSplashFinishedWithErrorMessage)
                }
            }
            
            if(!self.isConnected){
                VStack(){
                    Spacer()
                    HStack(){
                        Text("Pas de connexion internet.")
                            .font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextLightColor)
                        
                        Spacer()
                    }
                    .padding( AppConstants.viewNormalMargin)
                    .frame(minHeight: 50)
                    .background(Color.init(hex: "#333333"))
                    .padding(.horizontal, AppConstants.viewNormalMargin)
                }
               
            }
            if( isSplashFinishedWithError ){
                CustomAlertView(showingCustomAlert: $isSplashFinishedWithError, isSuccess: false, title: "Info", message: isSplashFinishedWithErrorMessage.isEmpty ? "Veuillez réessayer ultérieurement" : isSplashFinishedWithErrorMessage, titlePositive: "Ok", positiveAction: {
                    exit(0)
                })
            }
        }.onAppear {
            if !AppFunctions.checkIfUserConnected(){
                UserDefaults.showLogin = true
            }
            self.NC.addObserver(forName: NSNotification.connectivityStatus, object: nil, queue: nil,
                                using: self.onConnectivityUpdated)
        }.bottomSheet(isPresented: $showLoginAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            CustomConfirmationView(title: "Information", message: "Vous devez vous connecter pour accéder à cette fonctionnalité.", positiveAction: {
                withAnimation {
                    UserDefaults.showLoginDialog = false
                    UserDefaults.showLogin = true
                }
               
            }, isNegative: true, negativeAction: {
                withAnimation {
                    UserDefaults.showLoginDialog = false
                }
            })
        }
    }
    
    func onConnectivityUpdated(_ notification: Notification) {
        let connectivityStatus = notification.userInfo!["status"] as! Bool
        self.isConnected = connectivityStatus
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
