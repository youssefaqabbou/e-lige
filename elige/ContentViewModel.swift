//
//  ContentViewModel.swift
//  elige
//
//  Created by macbook pro on 24/1/2022.
//

import Foundation
import Network
import SwiftUI
import CoreLocation
import Combine

class ContentViewModel: NSObject, ObservableObject {
    let monitor = NWPathMonitor()
    let queue = DispatchQueue.global(qos: .background)
    @Published var connected = true
    
 
    override init(){
        super.init()
        
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { path in
            DebugHelper.debug("connection: ", path.status)
            if path.status == .satisfied {
                DebugHelper.debug("We're connected!")
                NotificationHelper.postConnectivityStatus(status: true)
            } else {
                DebugHelper.debug("No connection.")
                NotificationHelper.postConnectivityStatus(status: false)
            }

            DebugHelper.debug(path.isExpensive)
        }
        
        //let queue = DispatchQueue(label: "Monitor")
        //monitor.start(queue: queue)
    }
}
