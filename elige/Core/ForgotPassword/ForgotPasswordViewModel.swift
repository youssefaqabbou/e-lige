//
//  ForgotPasswordViewModel.swift
//  elige
//
//  Created by macbook pro on 9/12/2021.
//

import Foundation
import SwiftUI

class ForgotPasswordViewModel: NSObject, ObservableObject{

    private var apiService: APIService = APIService()
   
    @Published var isLoading: Bool = false
    
    @Published var email: String = ""
    @Published var emailError: String = ""
    
    @Published var toResestPwd: Bool = false
    
    override init() {
        #if DEBUG
            email = "youssef.aqabbou@gmail.com"
        #endif
    }
    
    var isFormComplete: Bool {
        let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: email)
        self.emailError = emailError

      
        return isEmailValid
    }
    
    func forgotPassword(){
        isLoading = true
       
        let params = ["email" : email]
        apiService.forgotPassword(params: params) { response in
            self.toResestPwd = true
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
}
