//
//  ForgotPwView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI

struct ForgotPwView: View {
    
    @StateObject var vmForgotPwd = ForgotPasswordViewModel()
    @Binding var toForgotPwd: Bool
    
    var body: some View {
        
        ZStack{
            VStack(alignment: .center, spacing: 30){
                
                HStack(){

                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                toForgotPwd = false
                            }
                        }
                    /*
                    Image.iconArrowBack
                        .resizable()
                        .frame(width: 20, height: 18)
                        .onTapGesture {
                            withAnimation {
                                toForgotPwd = false
                            }
                        }
                    */
                    Spacer()
                }
                
                
                
                
                Spacer(minLength: 20)
                
                
                Text("Mot de passe oublié")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                
                Text("Entrez l’adresse e-mail associée à votre compte et nous vous enverrons un e-mail avec des instructions pour réinitialiser votre mot de passe")
                    .font(.system(weight: .regular, size: 14, font:  FontStyle.brownStd.rawValue))
                    .foregroundColor(Color.secondaryTextColor)
                    .multilineTextAlignment(.center)
                    
                
                CustomTextField(placeholder: "Email", value: $vmForgotPwd.email, error: vmForgotPwd.emailError)
                    .onChange(of: vmForgotPwd.email, perform: { value in
                        vmForgotPwd.emailError = ValidatorHelper.validateEmail(email: vmForgotPwd.email).error
                    })
                Spacer(minLength: 20)
                
                CustomLoadingButton(isLoading: $vmForgotPwd.isLoading, text: "Envoyer l'instruction") {
                    if(vmForgotPwd.isFormComplete){
                        hideKeyboard()
                        vmForgotPwd.forgotPassword()
                    }
                }
                
                Spacer()
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
                .onTapGesture {
                    hideKeyboard()
                }
            
            if(vmForgotPwd.toResestPwd){
                ResetPwdView(toResetPwd: $vmForgotPwd.toResestPwd, email : vmForgotPwd.email) {
                    withAnimation {
                        self.toForgotPwd = false
                    }
                }.transition(.move(edge: .trailing)).animation(.linear , value: vmForgotPwd.toResestPwd).zIndex(1)
                               
            }
        }.background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                hideKeyboard()
            }
    }
}

struct ForgotPwView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPwView(toForgotPwd: .constant(false))
    }
}
