//
//  LocalisationPermissionView.swift
//  elige
//
//  Created by macbook pro on 4/1/2022.
//

import SwiftUI

struct LocalisationPermissionView: View {
    @State var isLoading = false
    
    var onGpsSettingAction : () -> () = {}
    var onExitAction : () -> () = {}
    
    var body: some View {
        VStack(spacing: 0) {
            
            Spacer()
            
            Text("Réservez votre rendez-vous beauté en ligne près de chez vous à tout moment")
                .fixedSize(horizontal: false, vertical: true)
                .lineLimit(nil)
                .font(.system(weight: .bold, size: 24))
                .foregroundColor(Color.primaryTextColor)
                .multilineTextAlignment(.center)
            
            
            Spacer(minLength: AppConstants.viewVeryExtraMargin)
            
            Image.gpsRequired
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin )
                .padding(.horizontal, AppConstants.viewVeryExtraMargin )
            
            Spacer(minLength: AppConstants.viewVeryExtraMargin)
            
            
            Text("Nous aurons besoin d'accéder à votre position pour vous offrir une meilleure expérience")
                .fixedSize(horizontal: false, vertical: true)
                .lineLimit(nil)
                .font(.system(weight: .regular, size: 20))
                .foregroundColor(Color.secondaryTextColor)
                .multilineTextAlignment(.center)
            
            
            Spacer(minLength: AppConstants.viewVeryExtraMargin)
            
            CustomLoadingButton(isLoading: $isLoading, text: "Oui, j’autorise") {
                onGpsSettingAction()
            }
            .padding(.bottom, AppConstants.viewVeryExtraMargin )
            .padding(.horizontal, AppConstants.viewVeryExtraMargin )
            
            Text("Non, je refuse")
                .font(.system(weight: .regular, size: 16))
                .foregroundColor(Color.secondaryTextColor).onTapGesture {
                    onExitAction()
                }
            
            Spacer(minLength: AppConstants.viewVeryExtraMargin)
            
        } .padding(.horizontal, AppConstants.viewVeryExtraMargin )
            .background(Color.bgViewColor)
    }
}

struct LocalisationPermissionView_Previews: PreviewProvider {
    static var previews: some View {
        LocalisationPermissionView()
    }
}
