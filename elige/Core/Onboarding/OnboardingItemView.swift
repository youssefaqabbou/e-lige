//
//  OnboardingItemView.swift
//  elige
//
//  Created by macbook pro on 2/11/2021.
//

import Foundation
import SwiftUI

struct OnboardingItemView: View {
    @StateObject var onboardingVM = OnboardingItemViewModel()
    
    var title : String = ""
    var subTitle : String = ""
    var image : String = ""
    
    var body: some View {
        VStack(spacing: 0) {
            
            ZStack(alignment: .bottom) {
                Image("img_cloud")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .scaleEffect(onboardingVM.cloudScale ? 1 : 0.5)
                    .opacity(onboardingVM.cloudFade ? 1 : 0)
                    .onAppear(){
                    }
                Image(image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .offset(y: onboardingVM.imageOffset ? 0 : -200)
                    .opacity(onboardingVM.imageFade ? 1 : 0)
                /*
                 1 = 210 , 250
                 2 = 210, 235
                 3 = 210,
                 */
                
            }.padding(.bottom, AppConstants.viewExtraMargin)
            .onAppear(){
                onboardingVM.animateImage()
            }
            
            Text(title)
                .fixedSize(horizontal: false, vertical: true)
                .lineLimit(nil)
                .font(.system(weight: .bold, size: 24))
                .foregroundColor(Color.primaryTextColor)
                .multilineTextAlignment(.center)
                .offset(x: onboardingVM.titleOffset ? 0 : 200)
                .opacity(onboardingVM.titleFade ? 1 : 0)
                .padding(.bottom, AppConstants.viewExtraMargin)
                .onAppear(){
                    onboardingVM.animateTitle()
                }
            
            
            Text(subTitle)
                .fixedSize(horizontal: false, vertical: true)
                .lineLimit(nil)
                .font(.system(weight: .regular, size: 14))
                .foregroundColor(Color.secondaryTextColor)
                .multilineTextAlignment(.center)
                .offset(x: onboardingVM.subtitleOffset ? 0 : -200)
                .opacity(onboardingVM.subtitleFade ? 1 : 0)
                .onAppear(){
                    onboardingVM.animateSubtitle()
                }
            
            Spacer()
             
        }.padding(.horizontal, 25)
    }
}
