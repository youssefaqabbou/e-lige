//
//  OnboardingItemViewModel.swift
//  elige
//
//  Created by macbook pro on 2/11/2021.
//

import Foundation
import SwiftUI

class OnboardingItemViewModel: ObservableObject {
    
    @Published var titleFade = false
    @Published var titleOffset = false
    
    @Published var subtitleFade = false
    @Published var subtitleOffset = false
    
    @Published var cloudScale = false
    @Published var cloudFade = false
    
    @Published var imageFade = false
    @Published var imageOffset = false
    
    func animateTitle(){
        titleOffset = false
        titleFade  = false
        withAnimation(.spring().speed(0.5).delay(0.25)) {
            titleOffset = true
            withAnimation(.spring().speed(1.0)) {
                titleFade  = true
            }
        }
    }
    
    func animateSubtitle(){
        subtitleOffset = false
        subtitleFade  = false
        withAnimation(.spring().speed(0.5).delay(0.25)) {
            subtitleOffset = true
            withAnimation(.spring().speed(1.0)) {
                subtitleFade  = true
            }
        }
    }
    func animateImage(){
        cloudScale = false
        cloudFade  = false
        imageFade = false
        imageOffset = false
        
        withAnimation(.interpolatingSpring(stiffness: 100, damping: 10, initialVelocity: 0.5).speed(0.5)) {
            cloudScale = true
            withAnimation(.spring().speed(0.8)) {
                cloudFade  = true
            }
        }

        withAnimation(.spring().speed(0.5).delay(0.25)) {
            imageOffset = true
            withAnimation(.easeInOut(duration: 1.0)) {
                imageFade  = true
            }
        }
    }
    
}
