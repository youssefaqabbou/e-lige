//
//  OnboardingView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI

struct OnboardingView: View {
    @StateObject var onboardingVM = OnboardingViewModel()
    
    var body: some View {
        VStack() {
            HStack() {
                Spacer(minLength: 20)
                
                Text("Passer")
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.primaryColor)
                    .onTapGesture {
                        UserDefaults.isFirstUse = false
                    }
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
            
            Spacer(minLength: 80)
            
            TabView(selection: $onboardingVM.selection) {
                
                ForEach(0..<onboardingVM.images.count) { i in
                    OnboardingItemView(title: onboardingVM.title[i], subTitle: onboardingVM.subtitle[i], image: onboardingVM.images[i])
                }
            }
            .disabled(true)
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .never))
            .padding(.bottom, 25)
            .onAppear {
                UIPageControl.appearance().currentPageIndicatorTintColor = .clear
                UIPageControl.appearance().pageIndicatorTintColor = .clear
            }

            
            Spacer(minLength: 20)
            
            OnboardingDotView(index: $onboardingVM.selection, maxIndex: onboardingVM.maxIndex)
                .padding(.bottom, 20.0)
            
            ZStack(){
                
                Image.rightArrowIcon
                    .resizable()
                    .frame(width: 30, height: 30, alignment: .center)
                    .opacity(onboardingVM.selection == onboardingVM.maxIndex ? 0 : 1 )
                
                Text("Commencer")
                    .font(.system(weight: .medium, size: 16))
                    .foregroundColor(Color.primaryTextLightColor)
                    .opacity(onboardingVM.selection == onboardingVM.maxIndex ? 1 : 0 )
                
            }
           
            .frame(width:onboardingVM.selection == onboardingVM.maxIndex ? UIScreen.main.bounds.width - 60 : 70, height: onboardingVM.selection == onboardingVM.maxIndex ? 70 : 70, alignment: .center)
            .background(Color.primaryColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: onboardingVM.selection == onboardingVM.maxIndex ? 15 : 35)).shadow(color: Color.shadowColor, radius: 2, x: 0, y: 3))
                .onTapGesture {
                    withAnimation(.spring()) {
                        if(onboardingVM.selection != onboardingVM.maxIndex){
                            onboardingVM.selection += 1
                        }else{
                            UserDefaults.isFirstUse = false
                        }
                    }
                }
            
        }.padding(.vertical, 50)
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}

struct OnboardingDotView: View {
    @Binding var index: Int
    let maxIndex: Int

    var body: some View {
        HStack(spacing: 8) {
            ForEach(0...maxIndex, id: \.self) { index in
                Capsule()
                    .fill( index == self.index ? Color.primaryColor :  Color.secondaryColor)
                    .frame(width: index == self.index ? 25 : 10, height: 10)
                
            }
        }.padding(AppConstants.viewNormalMargin)
    }
}
