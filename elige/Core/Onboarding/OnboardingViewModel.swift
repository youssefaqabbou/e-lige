//
//  OnboardingViewModel.swift
//  elige
//
//  Created by macbook pro on 2/11/2021.
//

import Foundation
import SwiftUI

class OnboardingViewModel: ObservableObject {
    @Published var maxIndex = 2
    @Published var selection = 0
    
    @Published var images = ["img_onboarding_1","img_onboarding_2","img_onboarding_3"]
    
    @Published var title = ["Trouvez votre meilleur salon de coiffure à proximité.",
                            "Vous n'aurez pas besoin de réserver votre après-midi entière.",
                            "Vous avez envie de vivre une expérience beauté incomparable"]
    
    @Published var subtitle = ["Vous recherchez un salon de coiffure? trouvez rapidement le plus proche de chez vous",
                               "Avec Elige vous pouvez choisir le créneau qui vous convient et effectuer votre réservation.",
                               "Nous avons fait une sélection parmi les plus belles adresses des salons de coiffure les plus proches de chez vous."]
    
    @Published var titleFade = false
    @Published var titleOffset = false
    
    @Published var subtitleFade = false
    @Published var subtitleOffset = false
    
    @Published var cloudScale = false
    @Published var cloudFade = false
    
    @Published var imageFade = false
    @Published var imageOffset = false
    
    func animateTitle(){
        titleOffset = false
        titleFade  = false
        withAnimation(.spring().speed(0.5).delay(0.25)) {
            titleOffset = true
            withAnimation(.spring().speed(1.0)) {
                titleFade  = true
            }
        }
    }
    
    func animateSubtitle(){
        subtitleOffset = false
        subtitleFade  = false
        withAnimation(.spring().speed(0.5).delay(0.25)) {
            subtitleOffset = true
            withAnimation(.spring().speed(1.0)) {
                subtitleFade  = true
            }
        }
    }
    func animateImage(){
        cloudScale = false
        cloudFade  = false
        withAnimation(.interpolatingSpring(stiffness: 100, damping: 10, initialVelocity: 0.5).speed(0.5)) {
            cloudScale = true
            withAnimation(.spring().speed(0.8)) {
                cloudFade  = true
            }
        }
        imageFade = false
        imageOffset = false
        
        withAnimation(.spring().speed(0.5).delay(0.25)) {
            imageOffset = true
            withAnimation(.spring().speed(1.0)) {
                imageFade  = true
            }
        }
    }
    
}
