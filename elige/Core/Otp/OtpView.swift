//
//  OtpView.swift
//  elige
//
//  Created by MAC on 1/11/21.
//

import SwiftUI

struct OtpView: View {
    
    @StateObject var vmOtp = OtpViewModel()
    @Binding var toOtp : Bool
    @State var email : String
    @State var isUpdate : Bool = false
    @State var widthPinDots : CGFloat = 90
    var body: some View {
        
        VStack(alignment: .center){
            
            HStack(){
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            toOtp = false
                        }
                    }
                
                Spacer()
            }
            
            Spacer(minLength: 50)
            
            VStack(spacing: 10){
                
                Text(vmOtp.timing)
                    .font(.system(weight: .regular, size: 36))
                    .foregroundColor(Color.primaryTextColor)
                    .padding(.bottom, AppConstants.viewExtraMargin)
                    .onReceive(vmOtp.timer) { _ in
                        vmOtp.updateTimer()
                    }
                
                Text("Saisissez le code de vérification que nous vous avons envoyé")
                    .font(.system(weight: .regular, size: 15))
                    .foregroundColor(Color.secondaryTextColor)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
            }
            
            
            Spacer(minLength: 30)
            
            GeometryReader { reader in
                PinDots(width: reader.frame(in: .local).width,  codes: $vmOtp.code) { width in
                    widthPinDots = width
                }
            }.frame(height: widthPinDots)
            
           
            
            Spacer(minLength: 30)
            
            CustomNumberPad(codes: $vmOtp.code, maxDigits: 4) { code in
                DebugHelper.debug("Code", code)
                if(isUpdate){
                    vmOtp.updateEmail()
                }else{
                    vmOtp.verifyAccount()
                }
            }
            //.background(Color.red)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.horizontal, AppConstants.viewExtraMargin)
            
            
            Spacer(minLength: 20)
            
            Text("Envoyer à nouveau")
                .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                .foregroundColor(Color.primaryTextColor)
                .padding()
                .opacity(vmOtp.timeRemaining > 0 ? 0.5 : 1.0)
                .disabled(vmOtp.timeRemaining > 0 ? true : false)
                .onTapGesture {
                    
                    if(vmOtp.timeRemaining <= 0){
                        if(isUpdate){
                            vmOtp.resendCode()
                        }else{
                            vmOtp.resendCodeForUpdate()
                        }
                       
                        vmOtp.timeRemaining = 300
                    }
                }
            
            Spacer()
            
        }
        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear(){
            vmOtp.email = self.email
        }
    }
}

struct OtpView_Previews: PreviewProvider {
    static var previews: some View {
        OtpView(toOtp: .constant(false), email: "")
    }
}
