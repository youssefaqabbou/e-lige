//
//  OtpViewModel.swift
//  elige
//
//  Created by macbook pro on 9/12/2021.
//

import Foundation
import SwiftUI

class OtpViewModel: NSObject, ObservableObject{

    private var apiService: APIService = APIService()
    
    @Published var isLoading: Bool = false
    
    @Published var email: String = ""
    @Published var code: [String] = []

    
    @Published var alertTitle: String = ""
    @Published var alertMessage: String = ""
    @Published var showingAlert: Bool = false
    
    @Published var timeRemaining = 300
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @Published var timing = ""
    
    
    func updateTimer(){
        if timeRemaining > 0 {
            timeRemaining -= 1
        }
        let (h, m) = AppFunctions.minutesToHoursMinutes(timeRemaining)
        timing = "\(String(format: "%02d", h)): \(String(format: "%02d", m))"
    }
    func verifyAccount(){
        isLoading = true
       
        let params = ["email" : email, "code" : code.joined(separator: "") ] as [String : Any]
        apiService.verifyAccount(params: params) { response in
            UserDefaults.showLogin = false
            self.isLoading = false
        } onFailure: {errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
    
    func resendCode(){
        isLoading = true
       
        let params = ["email" : email ] as [String : Any]
        apiService.resendCode(params: params) { message in
            AppFunctions.showSnackBar(status: .success , message: message)
            self.isLoading = false
        } onFailure: { errorMessage in
            self.alertTitle = errorMessage
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
    
    func updateEmail() {
        self.isLoading = true
       
        let params = ["email" : email, "code" : code.joined(separator: "") ] as [String : Any]
        
        apiService.updateEmail(params: params) { message in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .success , message: message)
            UserDefaults.showLogin = true
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func resendCodeForUpdate(){
        isLoading = true
       
        let params = ["email" : email ] as [String : Any]
        apiService.resendCode(params: params) { message in
            AppFunctions.showSnackBar(status: .success , message: message)
            self.isLoading = false
        } onFailure: { errorMessage in
            self.alertTitle = errorMessage
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
}
