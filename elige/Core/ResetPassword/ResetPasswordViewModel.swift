//
//  ResetPasswordViewModel.swift
//  elige
//
//  Created by macbook pro on 9/12/2021.
//

import Foundation
import SwiftUI

class ResetPasswordViewModel: NSObject, ObservableObject{

    private var apiService: APIService = APIService()
    
    @Published var isLoading: Bool = false
    
    @Published var email: String = ""
    @Published var code: String = ""
    @Published var password: String = ""
    @Published var confirmPassword: String = ""
    
    @Published var codeError: String = ""
    @Published var passwordError: String = ""
    @Published var confirmPasswordError: String = ""
    
    
    override init() {
        #if DEBUG
        code = "1234"
        confirmPassword = "123456aA"
        password = "123456aA"
        #endif
    }
    
    var isFormComplete: Bool {
        
        let (isOldPasswordValid, oldPasswordError) = ValidatorHelper.validatePasswordConfirmation(password: password, passwordConfirm: confirmPassword)
        self.confirmPasswordError = oldPasswordError

        let (isPasswordValid, passwordError) = ValidatorHelper.validatePassword(password: password)
        self.passwordError = passwordError

        return isOldPasswordValid && isPasswordValid
    }
    
    func resetPassword(onSuccess successCallback: (() -> Void)?){
        isLoading = true
       
        let params = ["code" : code, "email" : email, "password": password ]
        apiService.resetPassword(params: params) { response in
            successCallback?()
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
}
