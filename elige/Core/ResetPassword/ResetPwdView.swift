//
//  ResetPwdView.swift
//  elige
//
//  Created by MAC on 1/11/21.
//

import SwiftUI

struct ResetPwdView: View {
    
    @StateObject var vmResetPwd = ResetPasswordViewModel()
    @Binding var toResetPwd : Bool
    @State var email : String
    
    var onPasswordChanged : () -> () = {}
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .center, spacing: 30){
                
                HStack(){
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                toResetPwd = false
                            }
                        }
                    /*
                    Image.iconArrowBack
                        .resizable()
                        .frame(width: 20, height: 18)
                        .onTapGesture {
                            toResetPwd = false
                        }*/
                    Spacer()
                }
                
                Spacer(minLength: 20)
                
                VStack(spacing: 20){
                    
                    Text("Créer un nouveau mot de passe")
                        .font(.system(weight: .regular, size: 24))
                        .foregroundColor(Color.primaryTextColor)
                        .multilineTextAlignment(.center)
                   
                    Text("Votre mot de passe doit être différent du mot de passe utilisé précédemment")
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                        .multilineTextAlignment(.center)
                }
                
                VStack(spacing: 20){
                    
                    CustomTextField(placeholder: "Code", value: $vmResetPwd.code, error: vmResetPwd.codeError)
                    
                    CustomTextField(placeholder: "Nouveau mot de passe", value: $vmResetPwd.password, error: vmResetPwd.passwordError)
                        .onChange(of: vmResetPwd.password, perform: { value in
                            vmResetPwd.passwordError = ValidatorHelper.validatePassword(password: vmResetPwd.password).error
                        })
                    
                    CustomTextField(placeholder: "Confirmer le nouveau mot de passe", value: $vmResetPwd.confirmPassword, error: vmResetPwd.confirmPasswordError)
                        .onChange(of: vmResetPwd.confirmPassword, perform: { value in
                            vmResetPwd.confirmPasswordError = ValidatorHelper.validatePasswordConfirmation(password: vmResetPwd.password, passwordConfirm: vmResetPwd.confirmPassword).error
                        })
                }
                
                Spacer(minLength: 20)
                
                CustomLoadingButton(isLoading: $vmResetPwd.isLoading, text: "Réinitialiser le mot de passe") {
                    if(vmResetPwd.isFormComplete){
                        hideKeyboard()
                        vmResetPwd.resetPassword {
                            onPasswordChanged()
                        }
                    }
                }
               
                Spacer(minLength: 20)
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
                .onTapGesture {
                    hideKeyboard()
                }
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear(){
            hideKeyboard()
            vmResetPwd.email = self.email
        }
    }
}

struct ResetPwdView_Previews: PreviewProvider {
    static var previews: some View {
        ResetPwdView(toResetPwd: .constant(false), email: "")
    }
}
