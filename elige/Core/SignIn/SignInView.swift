//
//  SignInView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI
import GoogleSignIn

struct SignInView: View {
    
    @StateObject var vmSignIn = SignInViewModel()
    @State var showOtpView : Bool = false
    @State var showPhoneView : Bool = false
    
    let NC = NotificationCenter.default
    
    var body: some View {
        
        ZStack{
            VStack(alignment: .leading, spacing: 0){
                
                Image.logoIcon
                    .resizable()
                    .scaledToFit()
                    .frame(height: 160, alignment: .center)
                //.padding()
                
                HStack(spacing: 15){
                        
                    Spacer()
                    
                    Button(action: {
                        
                    }, label: {
                        
                        VStack{
                            
                            Image.icon_google
                                .resizable()
                                .frame(width: 31, height: 31, alignment: .center)
                            Text("Google")
                                .font(.system(weight: .regular, size: 11))
                                .foregroundColor(Color.primaryTextColor)
                            
                        }.padding(.top, AppConstants.viewSmallMargin)
                        .padding(.bottom, 5)
                        .onTapGesture {
                            vmSignIn.isLoading = true
                            //GIDSignIn.sharedInstance().signIn()
                            vmSignIn.signInWithGoogle()
                        }.disabled(vmSignIn.isLoading)
                        
                    }).frame(width: 70, height: 65, alignment: .center)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))

                    Button(action: {
                        
                    }, label: {
                        
                        VStack(alignment: .center){
                            
                            Image.iconApple
                                .resizable()
                                .frame(width: 30, height: 35, alignment: .center)
                            
                            Text("AppleID")
                                .font(.system(weight: .regular, size: 11))
                                .foregroundColor(Color.primaryTextColor)
                            
                        }.onTapGesture {
                            vmSignIn.isLoading = true
                            vmSignIn.signInWithApple()
                        }.disabled(vmSignIn.isLoading)
                            
                    }).frame(width: 70, height: 65, alignment: .center)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))

                    
                    Button(action: {
                        
                    }, label: {
                        
                        VStack{
                            
                            Image.iconFacebook
                                .resizable()
                                .frame(width: 16, height: 31, alignment: .center)
                            
                            Text("Facebook")
                                .font(.system(weight: .regular, size: 11))
                                .foregroundColor(Color.primaryTextColor)
                        }.padding(.top, AppConstants.viewSmallMargin)
                            .padding(.bottom, 5)
                            .onTapGesture {
                                vmSignIn.isLoading = true
                                vmSignIn.signInWithFb()
                            }.disabled(vmSignIn.isLoading)
                        
                    }).frame(width: 70, height: 65, alignment: .center)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                        
                    
                    Spacer()
                    
                }.padding(AppConstants.viewSmallMargin)
                
                Spacer(minLength: 20)
                
                VStack(spacing: 20){
                    
                    CustomTextField(placeholder: "Email", value: $vmSignIn.email, error: vmSignIn.emailError)
                        .onChange(of: vmSignIn.email, perform: { value in
                            vmSignIn.emailError = ValidatorHelper.validateEmail(email: vmSignIn.email).error
                        })
                    CustomTextField(placeholder: "Mot de passe", value: $vmSignIn.password, error: vmSignIn.passwordError, isSecured: true)
                        /*.onChange(of: vmSignIn.password, perform: { value in
                            vmSignIn.passwordError = ValidatorHelper.validatePassword(password: vmSignIn.password).error
                        })*/
                    
                }
                HStack(){
                    Spacer()
                    Text("Mot de passe oublié?")
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.placeHolderTextColor)
                        .underline()
                        .onTapGesture {
                            withAnimation(.linear) {
                                vmSignIn.toForgetPassword = true
                            }
                        }
                    Spacer()
                }.padding(.top, 25)
                
                Spacer(minLength: 20)
                
                VStack(spacing: 15){
                    
                    CustomLoadingButton(isLoading: $vmSignIn.isLoading, text: "Se connecter") {
                        if(vmSignIn.isFormComplete){
                            hideKeyboard()
                            vmSignIn.signIn()
                        }
                    }.padding(.vertical, 10)
                    
                    Text("S'inscrire")
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                        .underline()
                        .padding(.vertical, 10)
                        .onTapGesture {
                            withAnimation() {
                                vmSignIn.toSignUp = true
                            }
                        }
                    
                    Text("Continuer sans vous connecter")
                        .foregroundColor(Color.primaryTextColor)
                        .font(.system(weight: .regular, size: 18))
                        .underline()
                        .onTapGesture {
                            UserDefaults.showLogin = false
                        }
                        .padding(.vertical, 10)
                }
            }.padding(AppConstants.viewVeryExtraMargin)
                .onTapGesture {
                    hideKeyboard()
                }
            if vmSignIn.toForgetPassword {
                ForgotPwView(toForgotPwd: $vmSignIn.toForgetPassword).transition(.move(edge: .trailing)).animation(.linear , value: vmSignIn.toForgetPassword)
                    .zIndex(1)
            }
            if(vmSignIn.toSignUp){
                SignUpView(toSignUp: $vmSignIn.toSignUp).transition(.move(edge: .trailing)).animation(.linear , value: vmSignIn.toSignUp)
                    .zIndex(2)
            }
            if(vmSignIn.toOtp){
                OtpView(toOtp: $vmSignIn.toOtp, email: vmSignIn.email).transition(.move(edge: .trailing)).animation(.linear , value: vmSignIn.toOtp)
                    .zIndex(3)
            }
        }
        .background(Color.bgViewColor)
        .onAppear {
            hideKeyboard()
            self.NC.addObserver(forName: NSNotification.AppleSignIn, object: nil, queue: nil,
                                   using: self.appleSignInChangeStatus)
        }
       
    }
    
    func appleSignInChangeStatus(_ notification: Notification) {
        DebugHelper.debug(notification.userInfo!)
        let success = notification.userInfo!["success"] as! Bool
        if(success){
            
            let identityToken = notification.userInfo!["identityToken"] as! String
            vmSignIn.appleLogin(idToken: identityToken)
        }else{
            vmSignIn.isLoading = false
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
        
    }
}


