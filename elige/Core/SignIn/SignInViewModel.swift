//
//  SignInViewModel.swift
//  elige
//
//  Created by user196417 on 11/2/21.
//

import Foundation
import SwiftUI
import FBSDKLoginKit
import AuthenticationServices
import GoogleSignIn
import FirebaseCore

class SignInViewModel: NSObject, ObservableObject, ASAuthorizationControllerDelegate{
    
    private var apiService: APIService = APIService()
    @State var manager = LoginManager()
    
    @Published var email: String = ""
    @Published var password: String = ""
    
    @Published var emailError: String = ""
    @Published var passwordError: String = ""
    
    @Published var toSignUp: Bool = false
    @Published var toForgetPassword: Bool = false
    
    @Published var isLoading: Bool = false
    
    @Published var toOtp: Bool = false
    
    private lazy var appleSignInCoordinator = AppleDelegate(loginVM: self)
    
    override init() {
        #if DEBUG
            email = "youssef.aqabbou@gmail.com"
            password = "123456aA"
        #endif
    }
    
    var isFormComplete: Bool {
        
        let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: email)
        self.emailError = emailError
        
        let (isPasswordValid, passwordError) = ValidatorHelper.validatePassword(password: password)
        self.passwordError = passwordError
        
        return isEmailValid && isPasswordValid
    }
    
    func signIn(){
        isLoading = true
        
        let params = ["email" : email, "password" : password]
        apiService.signIn(params: params) { response in
            if(response){
                UserDefaults.showLogin = false
                UserDefaults.connectedWithSocial = false
            }else{
                self.toOtp = true
            }
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
        
    }
    
    func googleLogin(idToken: String) {
        isLoading = true
        
        let params = ["id_token": idToken]
        apiService.googleSignIn(params: params) { response in
            if(response){
                UserDefaults.showLogin = false
                UserDefaults.connectedWithSocial = true
            }else{
                self.toOtp = true
            }
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
        
    }
    
    func facebookLogin(accessToken: String) {
        isLoading = true
        
        let params = ["access_token": accessToken]
        apiService.facebookSignIn(params: params) { response in
            if(response){
                UserDefaults.showLogin = false
                UserDefaults.connectedWithSocial = true
            }else{
                self.toOtp = true
            }
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func appleLogin(idToken: String) {
        isLoading = true
        
        let params = ["access_token": idToken]
        apiService.appleSignIn(params: params) { response in
            if(response){
                UserDefaults.showLogin = false
                UserDefaults.connectedWithSocial = true
            }else{
                self.toOtp = true
            }
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func signInWithFb(){
        
        self.isLoading = true
        
        manager.logIn(permissions: ["public_profile", "email"], from: nil){
            
            (result, err) in
            
            if err != nil{
                DebugHelper.debug(err!.localizedDescription)
                self.isLoading = false
                return
            }
            
            if result!.isCancelled {
                DebugHelper.debug(result!.isCancelled)
                self.isLoading = false
                return
            }
            
            let request = GraphRequest(graphPath: "me", parameters: ["fields": "email, name,first_name, last_name"])
            
            request.start { (_, res, _) in
                
                guard let profileData = res as? [String : Any]
                else{
                    self.isLoading = false
                    return
                }
                DebugHelper.debug(profileData)
                if let email = profileData["email"] as? String{
                    
                    let fbAccessToken = AccessToken.current?.tokenString ?? ""
                    DebugHelper.debug(fbAccessToken)
                    
                    self.facebookLogin(accessToken: fbAccessToken)
                }else{
                     //"We are unable to access Facebook account details, please use other sign in methods.")
                    self.isLoading = false
                    AppFunctions.showSnackBar(status: .error, message: "Nous n'avons pas réussi à récupérer votre email. Veuillez utiliser une autre méthode pour s'authentifier.")
                    return
                }
                
            }
        }
    }
    
    func signInWithApple() {
        appleSignInCoordinator.handleAuthorizationAppleIDButtonPress()
    }
    
    func signInWithGoogle(){
        
        guard let presentingViewController = (UIApplication.shared.connectedScenes.first as? UIWindowScene)?.windows.first?.rootViewController else {
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: LocalizationKeys.error_serveur.localized)
            return
        }
        
        guard let clientID = FirebaseApp.app()?.options.clientID else {
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: LocalizationKeys.error_serveur.localized)
            return
        }
        
        DebugHelper.debug(clientID)
        let signInConfig = GIDConfiguration.init(clientID: clientID)
        GIDSignIn.sharedInstance.signIn(
            with: signInConfig,
            presenting: presentingViewController,
            callback: { user, error in
                if let error = error {
                    DebugHelper.debug( "error: \(error.localizedDescription)")
                    self.isLoading = false
                    return
                }
                DebugHelper.debug( user!.authentication.idToken )
                self.googleLogin(idToken: user!.authentication.idToken!)
                // self.checkStatus()
            }
        )
        
    }
    
}
