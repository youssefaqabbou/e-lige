//
//  SignUpView.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI

struct SignUpView: View {
    @StateObject var vmSignUp = SignUpViewModel()
    @Binding var toSignUp: Bool
    
    var body: some View {
        
        ZStack(){
            VStack(alignment: .center, spacing: 10){
                
                HStack(){

                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                toSignUp = false
                            }
                        }
                   
                    Spacer()
                }
              
                Spacer(minLength: 20)
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    VStack(alignment: .center, spacing: 0){
                        
                        Image.logoIcon
                            .resizable()
                            .scaledToFit()
                            .frame(height: 160, alignment: .center)
                        
                        Spacer(minLength: 0)
                        
                        
                        Spacer(minLength: 20)
                        
                        VStack(spacing: 25){
                           
                            if vmSignUp.isBarber {
                                CustomTextField(placeholder: "Raison sociale", value: $vmSignUp.fullname, error: vmSignUp.fullnameError)
                            }else{
                                CustomTextField(placeholder: "Nom complet", value: $vmSignUp.fullname, error: vmSignUp.fullnameError)
                            }
                            
                            CustomTextField(placeholder: "Email", value: $vmSignUp.email, error: vmSignUp.emailError)
                                .onChange(of: vmSignUp.email, perform: { value in
                                    vmSignUp.emailError = ValidatorHelper.validateEmail(email: vmSignUp.email).error
                                })
                            //CustomTextField(placeholder: "Mot de passe", value: $vmSignUp.password, error: vmSignUp.passwordError, isSecured: true)
                            CustomTextField(placeholder: "Mot de passe", value: $vmSignUp.password, error: vmSignUp.passwordError, isSecured: true)
                                .onChange(of: vmSignUp.password, perform: { value in
                                    vmSignUp.passwordError = ValidatorHelper.validatePassword(password: vmSignUp.password).error
                                })
                        }
                        
                        HStack{
                            
                            Text("Êtes-vous un prestataire ?")
                                .font(.system(weight: .regular, size: 14, font:  FontStyle.brownStd.rawValue))
                                .foregroundColor(Color.placeHolderTextColor)
                                .padding(AppConstants.viewSmallMargin)
                            
                            Spacer()
                            
                            Toggle(isOn: $vmSignUp.isBarber, label: {
                                EmptyView()
                            }).toggleStyle(CustomToggleStyle(toggleAction: {
                                
                            }))
                            
                        }.padding(.top, AppConstants.viewExtraMargin)
                            .padding(.bottom, AppConstants.viewExtraMargin)
                        
                        VStack(spacing: 15){
                            
                            CustomLoadingButton(isLoading: $vmSignUp.isLoading, text: "S'inscrire") {
                                if(vmSignUp.isFormComplete){
                                    hideKeyboard()
                                    vmSignUp.signUp()
                                }
                            }
                            
                            Text("Se connecter")
                                .font(.system(weight: .medium, size: 18, font:  FontStyle.brownStd.rawValue))
                                .foregroundColor(Color.primaryTextColor)
                                .underline()
                                .padding()
                                .onTapGesture {
                                    withAnimation {
                                        toSignUp = false
                                    }
                                    
                                }
                            
                        }
                    }
                }
                
                Spacer(minLength: 20)
                
                
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
                .onTapGesture {
                    hideKeyboard()
                }
            
            if(vmSignUp.toOtp){
                OtpView(toOtp: $vmSignUp.toOtp, email : vmSignUp.email).transition(.move(edge: .trailing)).animation(.linear , value: vmSignUp.toOtp)
                    .zIndex(1)
            }
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            hideKeyboard()
        }
       
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView(toSignUp: .constant(false))
    }
}
