//
//  SignUpViewModel.swift
//  elige
//
//  Created by macbook pro on 8/12/2021.
//

import Foundation

class SignUpViewModel: NSObject, ObservableObject{

    private var apiService: APIService = APIService()
   
    @Published var isLoading: Bool = false
    
    @Published var fullname: String = ""
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var isBarber: Bool = false

    @Published var fullnameError: String = ""
    @Published var emailError: String = ""
    @Published var passwordError: String = ""

    @Published var toOtp: Bool = false

    @Published var showingAlert: Bool = false
    @Published var alertMessage: String = ""
    

    
    override init() {
        #if DEBUG
            fullname = "youssef aqabbou"
            email = "youssef.aqabbou@gmail.com"
            password = "123456aA"
        #endif
    }

    var isFormComplete: Bool {
        var isfullnameValid = false
        var fullNameError = ""
        if(isBarber){
            (isfullnameValid, fullNameError) = ValidatorHelper.validateSocialReason(socialReason: fullname)
            self.fullnameError = fullNameError
        }else{
            (isfullnameValid, fullNameError) = ValidatorHelper.validateFullname(fullname: fullname)
            self.fullnameError = fullNameError
        }
        
        let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: email)
        self.emailError = emailError

        let (isPasswordValid, passwordError) = ValidatorHelper.validatePassword(password: password)
        self.passwordError = passwordError

        return isfullnameValid && isEmailValid && isPasswordValid
    }
    
    func signUp(){
        isLoading = true
        /*
         {
           "fullname": "Bernard Salomon",
           "email": "bernard@elige.com",
           "phone": "0661475266",
           "password": "Bern@123",
           "role": "CLIENT"
         }
         */
        let params = ["fullname": fullname ,"email" : email, "password" : password, "role" : isBarber ? "SALON": "CLIENT"]
        apiService.signUp(params: params) { response in
            if(response){
                self.toOtp = true
            }else{
                
            }
            self.isLoading = false
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }

    }
    
}
