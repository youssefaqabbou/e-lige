//
//  SplashView.swift
//  elige
//
//  Created by macbook pro on 2/11/2021.
//

import SwiftUI

struct SplashView: View {
    @Binding var isFinished : Bool
    @Binding var withError : Bool
    @Binding var withErrorMessage : String
    
    var timeSplash = 1.5 // second
    
    var body: some View {
        ZStack {
            Image.logoIcon
                .resizable()
                //.frame(width: 300, height: 85, alignment: .center)
                .aspectRatio(contentMode: .fit)
        }.background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear(){
            DispatchQueue.main.asyncAfter(deadline: .now() + timeSplash) { [self] in
                isFinished = true
            }
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView(isFinished: .constant(false), withError: .constant(false), withErrorMessage: .constant(""))
    }
}
