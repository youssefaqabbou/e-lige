//
//  UpdateEmailView.swift
//  elige
//
//  Created by macbook pro on 17/1/2022.
//

import Foundation
import SwiftUI

struct UpdateEmailView: View {
    
    @StateObject var vmUpdateEmail = UpdateEmailViewModel()
    @Binding var toUpdateEmail: Bool
    
    var body: some View {
        
        ZStack{
            VStack(alignment: .center, spacing: 30){
                
                HStack(){

                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                toUpdateEmail = false
                            }
                        }
               
                    Spacer()
                }
                
                Spacer(minLength: 20)
                
                Text("Modifier votre email")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                
                Text("Entrez la nouvelle adresse e-mail et nous vous enverrons un e-mail avec des instructions pour modifier votre email")
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.secondaryTextColor)
                    .multilineTextAlignment(.center)
                    
                
                CustomTextField(placeholder: "Email", value: $vmUpdateEmail.email, error: vmUpdateEmail.emailError)
                    .onChange(of: vmUpdateEmail.email, perform: { value in
                        vmUpdateEmail.emailError = ValidatorHelper.validateEmail(email: vmUpdateEmail.email).error
                    })
                
                Spacer(minLength: 20)
                
                CustomLoadingButton(isLoading: $vmUpdateEmail.isLoading, text: "Valider") {
                    if(vmUpdateEmail.isFormComplete){
                        hideKeyboard()
                        vmUpdateEmail.resendCodeForUpdate()
                    }
                }
                
                Spacer()
                
            }
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
                .onTapGesture {
                    hideKeyboard()
                }
            
            if(vmUpdateEmail.toOtpView){
                OtpView(toOtp: $vmUpdateEmail.toOtpView, email: vmUpdateEmail.email, isUpdate: true).transition(.move(edge: .trailing)).animation(.linear , value: vmUpdateEmail.toOtpView).zIndex(1)
                               
            }
        }.background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            hideKeyboard()
        }
    }
}

struct UpdateEmailView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateEmailView(toUpdateEmail: .constant(false))
    }
}
