//
//  UpdateEmailViewModel.swift
//  elige
//
//  Created by macbook pro on 17/1/2022.
//

import Foundation
import SwiftUI

class UpdateEmailViewModel: NSObject, ObservableObject{

    private var apiService: APIService = APIService()
   
    @Published var isLoading: Bool = false
    
    @Published var email: String = ""
    @Published var emailError: String = ""
    
    @Published var toOtpView: Bool = false
    
    override init() {
        #if DEBUG
            email = "youssef.aqabbou@gmail.com"
        #endif
    }
    
    var isFormComplete: Bool {
        let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: email)
        self.emailError = emailError

        return isEmailValid
    }
    
    func resendCodeForUpdate(){
        isLoading = true
       
        let params = ["email" : email ] as [String : Any]
        apiService.sendCode(params: params) { message in
            AppFunctions.showSnackBar(status: .success , message: message)
            withAnimation {
                self.toOtpView = true
            }
            self.isLoading = false
        } onFailure: {errorMessage in
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            self.isLoading = false
        }

    }
    

}
