//
//  OrderView.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import SwiftUI
import HCalendar

struct CartView: View {
    @StateObject var vmCart = CartViewModel()
    @Binding var showCart : Bool
    @State var barberShop : BarberShopModel
    @State var showCheckIn = false
    
    
    @State var titles = ["Salon", "À domicile"]
    
    var body: some View {
        ZStack(){
            VStack(spacing: 0){
                
                HStack(){
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showCart = false
                            }
                        }
                    
                    Spacer(minLength: 10)
                    
                    Text("Réservation")
                        .textStyle(TitleStyle())
                        .lineLimit(1)
                    
                    Spacer(minLength: 10)
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                }
                .padding(.bottom, AppConstants.viewSmallMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                //.padding(.top, AppConstants.viewSmallMargin)
                .background(Color.bgViewColor)
                
                ScrollView(showsIndicators: false){
                    
                    VStack(spacing: 10){
                        
                        VStack{
                            HStack(){
                                Text("Choisissez l’emplacement").font(.system(weight: .regular, size: 22))
                                
                                Spacer()
                            }
                            
                            CustomSegmentedView(titles: titles, selected: $vmCart.selectedServiceLocation, frame: UIScreen.main
                                                    .bounds.width - ( AppConstants.viewVeryExtraMargin * 2 )
                                                , onValueChaged: {
                                vmCart.filterServicesByLocation()
                                vmCart.sumTotalPrice()
                            })
                            
                        }.padding(.vertical, AppConstants.viewExtraMargin)
                            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.bottomLeft, .bottomRight], radius: AppConstants.viewExtraMargin)))
                            .padding(.bottom, AppConstants.viewSmallMargin)
                            .background(Color.bgViewSecondaryColor)
                        
                        VStack(alignment: .leading , spacing: 10){
                            if(vmCart.loadingService == .loading){
                                CircularProgressView()
                                    .frame(width: 30, height: 15, alignment: .center)
                            }else if(vmCart.serviceList.isEmpty ){
                                VStack(spacing: 15){
                                    Text("Aucun service trouvé")
                                        .font(.system(weight: .regular, size: 22))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                    Text(vmCart.selectedServiceLocation == 0 ? "On dirait que le salon n’offre pas de service en salon" : "On dirait que le salon n’offre pas de service à domicile" )
                                        .font(.system(weight: .regular, size: 16))
                                        .foregroundColor(Color.primaryTextColor)
                                        .multilineTextAlignment(.center)
                                }
                            }else{
                                HStack(){
                                    Text("Sélectionnez vos prestations").font(.system(weight: .regular, size: 22))
                                    Spacer()
                                }
                                
                                VStack(spacing: 15){
                                    ForEach(vmCart.serviceList, id: \.id) { service in
                                        CartServiceItemView(service: service) { serviceUpdated in
                                            vmCart.updateServiceInList(service: serviceUpdated)
                                        }
                                    }
                                }
                                
                            }
                           
                        }.padding(.vertical, AppConstants.viewExtraMargin)
                            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.bottomLeft, .bottomRight], radius: AppConstants.viewExtraMargin)))
                            .padding(.bottom, AppConstants.viewSmallMargin)
                            .background(Color.bgViewSecondaryColor)
                        
                        VStack(alignment: .leading , spacing: 0){
                            Text("Choisissez la date et l’heure").font(.system(weight: .regular, size: 22))
                                .padding(.bottom, AppConstants.viewSmallMargin)
                                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            
                            HCalendarView(config: vmCart.hCalendarConfig, calendarStyle: vmCart.hCalendarStyle) { selectedDate in
                                vmCart.selectedDateSchedule = AppFunctions.getCurrentScheudle(schedules: barberShop.schedules, currentDay: selectedDate.weekday)
                                DebugHelper.debug("selectedDate", selectedDate)
                                withAnimation(.spring()) {
                                    vmCart.sumTotalPrice()
                                    vmCart.selectedDate = selectedDate
                                    vmCart.getAvailableSchedule(barberId: barberShop.id)
                                }
                            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            
                            if(!vmCart.selectedDateSchedule.isOpen ){
                                HStack(){
                                    Spacer()
                                    VStack(spacing: 15){
                                        Text("Oups !!!")
                                            .font(.system(weight: .regular, size: 22))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Text("Le salon est fermé ce jour là. Veuillez choisir un autre jour.")
                                            .font(.system(weight: .regular, size: 16))
                                            .foregroundColor(Color.primaryTextColor)
                                            .multilineTextAlignment(.center)
                                        
                                    }
                                    Spacer()
                                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                
                            }else if(vmCart.availableScheduleTime.isEmpty ){
                                HStack(){
                                    Spacer()
                                    VStack(spacing: 15){
                                        Text("Oups !!!")
                                            .font(.system(weight: .regular, size: 22))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Text("Tous les créneaux de réservation sont pris.")
                                            .font(.system(weight: .regular, size: 16))
                                            .foregroundColor(Color.primaryTextColor)
                                            .multilineTextAlignment(.center)
                                    }
                                    Spacer()
                                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                
                            }else{
                                ScrollView(.horizontal, showsIndicators: false) {
                                    HStack(spacing: 5){
                                        ForEach(vmCart.availableScheduleTime, id: \.self) { date in
                                            let time = date.toString(format: "HH:mm", isConvertZone: false)
                                            Text(time).font(.system(weight: .regular, size: 16))
                                                .foregroundColor(time == vmCart.selectedTime ? Color.primaryTextLightColor : Color.primaryTextColor)
                                                .padding(4)
                                                .background(time == vmCart.selectedTime ? Color.primaryTextColor : Color.primaryTextLightColor)
                                                .cornerRadius(AppConstants.viewSmallMargin)
                                                .frame(minWidth: 40, idealWidth: 60)
                                                .padding(.leading, date == vmCart.availableScheduleTime.first ? AppConstants.viewExtraMargin : 0)
                                                .padding(.trailing, date == vmCart.availableScheduleTime.last ? AppConstants.viewExtraMargin : 0)
                                                .onTapGesture {
                                                    vmCart.selectedTime = time
                                                }
                                        }
                                    }
                                }.overlay(
                                    HStack(){
                                        LinearGradient(gradient: Gradient(colors: [Color.white.opacity(0), Color.white.opacity(0.5), Color.white.opacity(1)]), startPoint: .trailing, endPoint: .leading)
                                            .frame(width: 20)
                                        Spacer()
                                        
                                        LinearGradient(gradient: Gradient(colors: [Color.white.opacity(0), Color.white.opacity(0.5), Color.white.opacity(1)]), startPoint: .leading, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/)
                                            .frame(width: 20)
                                    }.frame(width:UIScreen.main.bounds.width)
                                ).frame(height : 40)
                            }
                        }.padding(.vertical, AppConstants.viewExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius:  AppConstants.viewExtraMargin)))
                        
                    }
                }
                
                Spacer(minLength: 10)
                
                VStack(spacing: 10){
                    HStack(){
                        Text("Total des prestations").font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(vmCart.totalServices.toPrice()).font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    HStack(){
                        Text("Frais").font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(vmCart.selectedServiceLocation == 0 ? AppConstants.barberFees.toPrice() : AppConstants.homeFees.toPrice())
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    HStack(){
                        Text("Total").font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                        Spacer(minLength: 20)
                        
                        Text(vmCart.totalOrder.toPrice()).font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    Button(action: {
                        vmCart.checkCart {
                            vmCart.validateCart()
                            withAnimation {
                                showCheckIn = true
                            }
                        } onFailure: { message in
                            AppFunctions.showSnackBar(status: .error, message: message)
                        }
                        
                    }, label: {
                        
                        Text("Confirmer la réservation")
                            .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                        
                    }).buttonStyle(PrimaryButtonStyle())
                    
                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    .padding(.bottom, AppConstants.viewVeryExtraMargin)
                    .padding(.top, AppConstants.viewNormalMargin)
                    .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.topLeft, .topRight], radius: 15)).shadow(color: .tabBarShadowColor, radius: 5, x: 0.0, y: -5))
                
            }
            
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            
            if(showCheckIn){
                
                CheckinView(showCheckIn: $showCheckIn, barberShop: barberShop, cart: vmCart.cart, onOrderComplete: {
                    withAnimation {
                        showCart = false
                    }
                }).transition(.move(edge: .trailing)).animation(.linear, value: showCheckIn).zIndex(1)
            }
            
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            //vmCart.serviceList = barberShop.services
            vmCart.selectedDateSchedule = AppFunctions.getCurrentScheudle(schedules: barberShop.schedules, currentDay: vmCart.selectedDate.weekday)
            vmCart.getAvailableSchedule(barberId: barberShop.id)
            vmCart.getServiceByID(salonId: barberShop.id)
        }.background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.bottom)
    }
}

struct OrderView_Previews: PreviewProvider {
    static var previews: some View {
        CartView(showCart: .constant(false), barberShop: BarberShopModel())
    }
}
