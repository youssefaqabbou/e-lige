//
//  CheckinView.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import SwiftUI
import Stripe

struct CheckinView: View {
    @StateObject var vmCheckin = CheckinViewModel()
    @Binding var showCheckIn : Bool
    @State var barberShop : BarberShopModel
    @State var cart : CartModel
    var onOrderComplete : () -> () = {}
    
    var body: some View {
        ZStack(){
            VStack(){
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showCheckIn = false
                            }
                        }
                    
                    Spacer(minLength: 10)
                   
                    Text("Finaliser la réservation")
                        .textStyle(TitleStyle())
                        .lineLimit(1)
                    
                    Spacer(minLength: 10)
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    .padding(.top, AppConstants.viewVeryExtraMargin)
                    .padding(.top, AppConstants.viewNormalMargin)
                
                ScrollView( showsIndicators: false){
                    VStack(spacing: 0){
                        
                        HStack(){
                            
                            VStack(alignment: .leading, spacing: 10){                                
                                Text(vmCheckin.cart.orderDate.time())
                                     .font(.system(weight: .regular, size: 16))
                                     .foregroundColor(Color.secondaryTextColor)
                                
                                HStack(spacing: 5){
                                    Image.iconTime
                                        .resizable()
                                        .frame(width: 16, height : 16)
                                    /*
                                    Text("Réserver :")
                                         .font(.system(weight: .regular, size: 14))
                                         .foregroundColor(Color.secondaryTextColor)
                                    */
                                    Text(vmCheckin.cart.orderDate.toString(format: "HH:mm", isConvertZone: false))
                                         .font(.system(weight: .bold, size: 16))
                                         .foregroundColor(Color.primaryTextColor)
                                         .onAppear {
                                             DebugHelper.debug("cart orderDate",  vmCheckin.cart.orderDate)
                                         }
                                }
                                HStack(spacing: 5){
                                    if(vmCheckin.cart.serviceLocation == ServiceLocation.home.rawValue ){
                                        Image.iconBarberDomicile
                                            .resizable()
                                            .renderingMode(.template)
                                            .colorMultiply(Color.gray)
                                            .frame(width: 18, height : 18)
                                            .foregroundColor(Color.gray)
                                    }else{
                                        Image.iconBarberShop
                                            .resizable()
                                            .renderingMode(.template)
                                            .colorMultiply(Color.gray)
                                            .frame(width: 18, height : 18)
                                            .foregroundColor(Color.gray)
                                    }
                                    
                                    Text(vmCheckin.cart.serviceLocation == ServiceLocation.home.rawValue ? "À domicile" : "Salon" )
                                        .font(.system(weight: .regular, size: 20))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                                
                            }
                            Spacer()
                        }
                        .padding(.top, AppConstants.viewSmallMargin)
                        .padding(.bottom, AppConstants.viewExtraMargin)
                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.bottomLeft, .bottomRight], radius: AppConstants.viewExtraMargin)))
                        .padding(.bottom, AppConstants.viewVerySmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                        
                        VStack(spacing: 10){
                            ForEach(vmCheckin.cart.services, id: \.id) { service in
                                ServiceDetailItem(service: .constant(service))
                            }
                        }
                        .padding(.top, AppConstants.viewSmallMargin)
                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.topLeft, .topRight], radius: AppConstants.viewExtraMargin)))
                        .padding(.top, AppConstants.viewVerySmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                        VStack(spacing: 10){
                            HStack(){
                                Text("Total des prestations").font(.system(weight: .regular, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                Spacer(minLength: 20)
                                
                                Text(vmCheckin.totalServices.toPrice()).font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                            
                            HStack(){
                                Text("Frais").font(.system(weight: .regular, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                                Spacer(minLength: 20)
                                
                                Text(vmCheckin.cart.fees.toPrice()).font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                        
                            HStack(){
                                Text("Total").font(.system(weight: .regular, size: 22))
                                    .foregroundColor(Color.primaryTextColor)
                                Spacer(minLength: 20)
                                
                                Text(vmCheckin.totalOrder.toPrice()).font(.system(weight: .regular, size: 20))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                            
                            
                        }
                        .padding(.top, AppConstants.viewSmallMargin)
                        .padding(.bottom, AppConstants.viewExtraMargin)
                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: AppConstants.viewExtraMargin)))
                        .padding(.bottom, AppConstants.viewVerySmallMargin)
                        .background(Color.bgViewSecondaryColor)
                        
                        VStack(alignment: .center , spacing: 10){
                            
                            HStack(){
                                Text("Mode de paiement :")
                                     .font(.system(weight: .regular, size: 17))
                                     .foregroundColor(Color.secondaryTextColor)
                                     .padding(.bottom, 15)
                                
                                Spacer()
                            }
                          
                            
                            HStack(){
                                
                               
                                if(vmCheckin.cart.selectedCard.paymentId != ""){
                                    Image(AppFunctions.getCardLogo(by: vmCheckin.cart.selectedCard.cartType))
                                        .resizable()
                                        .frame(width: 50, height: 30, alignment: .center)
                                        .scaledToFit()
                                    
                                    VStack(alignment: .leading , spacing: 10){
                                        Text(vmCheckin.cart.selectedCard.name)
                                            .font(.system(weight: .regular, size: 16))
                                            .foregroundColor(Color.primaryTextColor)
                                        Text("**** **** **** \(vmCheckin.cart.selectedCard.last4)")
                                            .font(.system(weight: .regular, size: 12))
                                            .foregroundColor(Color.secondaryTextColor)
                                    }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                    .onTapGesture {
                                        withAnimation {
                                            vmCheckin.showPayment = true
                                        }
                                    }
                                }else if(vmCheckin.isValidCard){
                                    Image(AppFunctions.getCardLogo(from: vmCheckin.paymentMethodCardParams.number!))
                                        .resizable()
                                        .frame(width: 50, height: 30, alignment: .center)
                                        .scaledToFit()
                                    
                                    VStack(alignment: .leading , spacing: 10){
                                        Text(vmCheckin.holderName)
                                            .font(.system(weight: .regular, size: 16))
                                            .foregroundColor(Color.primaryTextColor)
                                        Text("**** **** **** \(vmCheckin.paymentMethodCardParams.last4 ?? "****")")
                                            .font(.system(weight: .regular, size: 12))
                                            .foregroundColor(Color.secondaryTextColor)
                                    }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                    .onTapGesture {
                                        withAnimation {
                                            vmCheckin.showPayment = true
                                        }
                                    }
                                }else{
                                    
                                    Circle().fill(Color.secondaryColor)
                                        .frame(width: 40, height: 40, alignment: .center)
                                        .overlay(
                                            Image(systemName: "creditcard")
                                                .font(.system(weight: .bold, size: 18))
                                                .foregroundColor(Color.primaryTextColor)
                                        )
                                    
                                    
                                    Text("Ajouter une carte")
                                        .font(.system(weight: .regular, size: 16))
                                        .foregroundColor(Color.primaryTextColor)
                                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                        .onTapGesture {
                                            withAnimation {
                                                vmCheckin.showPayment = true
                                            }
                                        }
                                }
                                
                                    
                                Spacer()
                            }
                            .padding(AppConstants.viewExtraMargin)
                            .background(Color.bgViewSecondaryColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                            
                            
                            VStack(){
                                
                                Text(barberShop.name)
                                     .font(.system(weight: .bold, size: 22))
                                     .foregroundColor(Color.primaryTextColor)
                                     .padding(.bottom, AppConstants.viewSmallMargin)
                                
                                Text(barberShop.address)
                                     .font(.system(weight: .regular, size: 13))
                                     .foregroundColor(Color.secondaryTextColor)
                                     .padding(.bottom, 15)
                                
                                Image(uiImage: vmCheckin.snapShotImage)
                                    .resizable()
                                    .frame(height: 100)
                                        .mask(AppUtils.CustomCorners(corners: .allCorners, radius: 15))
                                        .padding(.bottom, 15)
                                
                                /// A voir
                                
                                Text("En finalisant votre commande vous acceptez nos conditions générales de vente et notre politique de confidentialité")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                                    .padding(.bottom, 15)
                                    .multilineTextAlignment(.center)
                                
                            }.padding(.top, AppConstants.viewNormalMargin)
                        }
                        .padding(.top, AppConstants.viewExtraMargin)
                        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                        .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: AppConstants.viewExtraMargin)))
                        .padding(.top, AppConstants.viewVerySmallMargin)
                        .background(Color.bgViewSecondaryColor)
                    }
                }
                .padding(.bottom, 110)
            }
           
            VStack(){
                Spacer()
                
                CustomLoadingButton(isLoading: $vmCheckin.isLoading, text: vmCheckin.isPaymentSuccess ? "Finaliser la réservation" : "Payer", action: {
                    vmCheckin.checkOrder {
                        if(!vmCheckin.isPaymentSuccess){
                            vmCheckin.rePayOrder()
                        }else{
                            vmCheckin.createOrderRequest()
                        }
                        
                    } onFailure: { message in
                        AppFunctions.showSnackBar(status: .error, message: message)
                    }
                })
                .padding(AppConstants.viewVeryExtraMargin)
                .padding(.bottom, AppConstants.viewExtraMargin)
            }
            
            ZStack(){
                if(vmCheckin.showPayment){
                    PaymentView(showPayment: $vmCheckin.showPayment, cart: $cart, cardSelectedAction: { selectedCard in
                        vmCheckin.cart.selectedCard = selectedCard
                        withAnimation {
                            vmCheckin.showPayment = false
                        }
                    }) { methodCardParams, holderName , canSave  in
                        vmCheckin.cart.selectedCard = CardModel()
                        vmCheckin.holderName = holderName
                        vmCheckin.isSaveCard = canSave
                        vmCheckin.isValidCard = true
                        vmCheckin.paymentMethodCardParams = methodCardParams
                        withAnimation {
                            vmCheckin.showPayment = false
                        }
                    }.transition(.move(edge: .trailing)).animation(.linear, value: vmCheckin.showPayment).zIndex(1)
                }
            }
            
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear(){
            vmCheckin.barberShop = barberShop
            vmCheckin.cart = cart
            vmCheckin.calculatePrice()
            vmCheckin.createSnapShot(location: barberShop.coordinate)
        }
        .bottomSheet(isPresented: $vmCheckin.showCompleteAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false, onTapOutside: false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: vmCheckin.showOrderCompleted ?  Image.iconAlertSuccess  : Image.iconAlertFailed , title: vmCheckin.orderCompleteTitle, message: vmCheckin.orderCompleteMessage, titlePositive: vmCheckin.showOrderCompleted ? "Retourner" : "Réessayer" , positiveAction: {
                
                if(vmCheckin.showOrderCompleted && vmCheckin.isPaymentSuccess){
                    onOrderComplete()
                    withAnimation {
                        self.showCheckIn = false
                    }
                }
                withAnimation {
                    vmCheckin.showCompleteAlert = false
                    
                }
            }, isNegative: false)
        }
    }
}

struct CheckinView_Previews: PreviewProvider {
    static var previews: some View {
        CheckinView(showCheckIn: .constant(false), barberShop: BarberShopModel(), cart: CartModel())
    }
}
