//
//  DetailBarberView.swift
//  elige
//
//  Created by user196417 on 11/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailBarberView: View {
    
    @StateObject var vmDetailBarber = DetailBarberViewModel()
    
    @Binding var showDetail : Bool
    @State var barberShop : BarberShopModel
    @State var scrollViewOffset: CGFloat = 0
    @State var startOffset: CGFloat = 0
    
    @State var showCart = false
    @State var showAddReview = false
    @State var showServices = false
    @State var showReviews = false
    @State var showGallery = false
    @State var showBlocked = false
    
    var body: some View {
        
        ZStack{
            let maxWidth = (UIScreen.main.bounds.width - ((AppConstants.viewVeryExtraMargin * 2) + (15 * 3))  ) / 4
            ZStack{
                VStack{
                    
                    WebImage(url: URL(string: barberShop.image.url))
                        .placeholder {
                            Image.placeholderShop.resizable()
                        }
                        .resizable() //
                        .aspectRatio(contentMode: .fill)
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                    
                    Spacer()
                    
                }
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    VStack(alignment: .leading, spacing: 0){
                        
                        Text("")
                            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 3)
                            .hidden()
                        
                        VStack(alignment: .leading, spacing: 0){
                            
                            VStack(alignment: .leading, spacing: 0){
                                
                                HStack(alignment: .top){
                                    VStack(alignment: .leading, spacing: 10){
                                        Text(barberShop.name)
                                            .font(.system(weight: .regular, size: 22))
                                            .foregroundColor(Color.primaryTextColor)
                                            .lineLimit(3)
                                        
                                        if(!barberShop.address.isEmpty){
                                            Text(barberShop.address)
                                                .font(.system(weight: .regular, size: 15))
                                                .lineSpacing(5)
                                                .foregroundColor(Color.secondaryTextColor)
                                               
                                        }
                                        
                                        HStack{
                                            
                                            StarsView(rating: CGFloat(barberShop.rating), maxRating: 5, width: 15)
                                                .onTapGesture {
                                                    withAnimation {
                                                        //showAddReview = true
                                                    }
                                                }
                                            
                                            Text(barberShop.rating.toRating())
                                                .font(.system(weight: .regular, size: 12))
                                                .foregroundColor(Color.primaryColor)
                                                .onTapGesture {
                                                    withAnimation {
                                                        //showAddReview = true
                                                    }
                                                }
                                            
                                            Text("\(barberShop.countReview) Avis")
                                                .font(.system(weight: .regular, size: 12))
                                                .foregroundColor(Color.secondaryTextColor)
                                                .padding(.leading, AppConstants.viewExtraMargin)
                                            
                                        }.padding(.bottom, AppConstants.viewExtraMargin)
                                    }
                                   
                                    Spacer()
                                    
                                    if AppFunctions.checkIfUserConnected() {
                                        
                                        Button(action: {
                                            if barberShop.isFavorite == false{
                                                vmDetailBarber.addFavorite(barberShop: barberShop, onError: {
                                                    barberShop.isFavorite = false
                                                })
                                                barberShop.isFavorite = true
                                            }else{
                                                vmDetailBarber.deleteFavorite(barberShop: barberShop, onError: {
                                                    barberShop.isFavorite = true
                                                })
                                                barberShop.isFavorite = false
                                            }
                                            
                                        }, label: {
                                            
                                            if(vmDetailBarber.isLoading){
                                                CircularProgressView( width: 28, height: 28, lineWidth : 2)
                                                    .frame(width: 28, height: 28)
                                                    .padding(12)
                                            }else{
                                                
                                                if barberShop.isFavorite == true{
                                                   
                                                    Image.iconHeartFav
                                                        .resizable()
                                                        .renderingMode(.template)
                                                        .frame(width: 28, height: 28, alignment: .center)
                                                        .padding(12)
                                                        .colorMultiply(Color.white)
                                                        .foregroundColor(Color.white)
                                                        
                                                }else{
                                                    Image.iconHeartNoFav
                                                        .resizable()
                                                        .frame(width: 28, height: 28, alignment: .center)
                                                        .padding(12)
                                                }
                                                
                                            }
                                            
                                        }).background(Color.btnFavorite.cornerRadius(30))
                                            .padding(.top, getSafe().top == 0 ? 12 : 0)
                                            .scaleEffect(-scrollViewOffset > 200 ? 0 : 1)
                                            .animation(.linear(duration: 0.2), value: scrollViewOffset)
                                            .offset(y: getSafe().top == 0 ? -30 : -40)
                                        
                                    }else{
                                        
                                    }
                                    
                                }.padding(.top, AppConstants.viewNormalMargin)
                                                  
                                TagView(tags: $barberShop.tags)
                                    .padding(.bottom, AppConstants.viewExtraMargin)
                                
                                if(!barberShop.description.isEmpty){
                                    VStack(alignment: .leading, spacing: 10){
                                        Text("Description")
                                            .font(.system(weight: .bold, size: 17))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Text(barberShop.description)
                                            .font(.system(weight: .regular, size: 14))
                                            .lineSpacing(5)
                                            .foregroundColor(Color.secondaryTextColor)
                                      
                                    }.padding(.bottom, AppConstants.viewSmallMargin)
                                }
                                
                                
                            }.padding(.bottom, AppConstants.viewSmallMargin)
                                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.bottomLeft, .bottomRight], radius: AppConstants.viewExtraMargin)))
                                .padding(.bottom, AppConstants.viewSmallMargin)
                                .background(Color.bgViewSecondaryColor)
                            
                            if(!barberShop.services.isEmpty){
                                VStack{
                                    
                                    HStack{
                                        
                                        Text("Prestations")
                                            .font(.system(weight: .bold, size: 17))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Spacer()
                                        
                                    }
                                    
                                    VStack(spacing: 20){
                                        
                                        if(barberShop.services.count > 3){
                                            ForEach(0..<3) { index in
                                                let service = barberShop.services[index]
                                                ServiceDetailItem(service: .constant(service))
                                            }
                                        }else{
                                            ForEach(barberShop.services, id: \.id) { service in
                                                ServiceDetailItem(service: .constant(service))
                                            }
                                        }
                                        
                                    }.padding(.top, AppConstants.viewSmallMargin)
                                        .padding(.bottom, 10)
                                    
                                    
                                    if(barberShop.countService > 3){
                                        Text("Voir Tout")
                                            .font(.system(weight: .regular, size: 14))
                                            .foregroundColor(Color.secondaryTextColor)
                                            .onTapGesture {
                                                withAnimation {
                                                    showServices = true
                                                }
                                            }
                                    }
                                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                .padding(.vertical, AppConstants.viewExtraMargin)
                                .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: AppConstants.viewExtraMargin)))
                                .padding(.bottom, AppConstants.viewSmallMargin)
                                .background(Color.bgViewSecondaryColor)
                            }
                           
                            
                            VStack{
                                
                                HStack{
                                    Text("Horaires de travail")
                                        .font(.system(weight: .bold, size: 17))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                    Spacer()
                                }
                                
                                VStack(spacing: 10){
                                    ForEach(barberShop.schedules, id: \.id) { schedule in
                                        ScheduleItem(schedule: .constant(schedule))
                                    }
                                }.padding(.top, AppConstants.viewSmallMargin)
                                
                            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                            .padding(.vertical, AppConstants.viewExtraMargin)
                            .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: AppConstants.viewExtraMargin)))
                            .padding(.bottom, 10)
                            .background(Color.bgViewSecondaryColor)
                            
                            if(!barberShop.medias.isEmpty){
                                VStack{
                                    
                                    HStack(alignment: .top){
                                        
                                        Text("Galerie")
                                            .font(.system(weight: .bold, size: 17))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Spacer()
                                        
                                        Text("Voir Tout")
                                            .font(.system(weight: .regular, size: 14))
                                            .foregroundColor(Color.secondaryTextColor)
                                            .onTapGesture {
                                                withAnimation {
                                                    showGallery = true
                                                }
                                            }
                                        
                                    }.padding(.bottom, AppConstants.viewSmallMargin)
                                    
                                    HStack(spacing: 10){
                                        
                                        
                                        if(barberShop.medias.count > 3){
                                            ForEach(0..<3) { index in
                                                let media = barberShop.medias[index]
                                                WebImage(url: URL(string: media.url))
                                                    .placeholder {
                                                        Image.placeholderShop
                                                    }
                                                    .resizable() //
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: maxWidth, height: maxWidth)
                                                    .cornerRadius(12)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 12)
                                                            .stroke(Color.primaryColor, lineWidth: 1)
                                                    ).onTapGesture {
                                                        DebugHelper.debug(index)
                                                        withAnimation {
                                                            showGallery = true
                                                        }
                                                    }
                                            }
                                            
                                            ZStack{
                                                
                                                WebImage(url: URL(string: barberShop.medias[3].url))
                                                    .placeholder {
                                                        Image.placeholderShop
                                                    }
                                                    .resizable() //
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: maxWidth, height: maxWidth)
                                                    .cornerRadius(12)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 12)
                                                            .stroke(Color.primaryColor, lineWidth: 1)
                                                    )
                                                    .onTapGesture {
                                                        withAnimation {
                                                            showGallery = true
                                                        }
                                                    }
                                                if(barberShop.countmedia - 4 != 0){
                                                    Rectangle().fill(Color.primaryColor.opacity(0.6))
                                                        .cornerRadius(12)
                                                        .overlay(
                                                            Text("+\(barberShop.countmedia - 4)")
                                                                .font(.system(weight: .bold, size: 18))
                                                                .foregroundColor(Color.primaryTextLightColor)
                                                        ).onTapGesture {
                                                            withAnimation {
                                                                showGallery = true
                                                            }
                                                        }
                                                }
                                                
                                            }.frame(width: maxWidth, height: maxWidth)
                                            .cornerRadius(12)
                                        }else{
                                            ForEach(barberShop.medias, id: \.id) { media in
                                                WebImage(url: URL(string: media.url))
                                                    .placeholder {
                                                        Image.placeholderShop
                                                            .cornerRadius(12)
                                                    }
                                                    .resizable() //
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: maxWidth, height: maxWidth)
                                                    .cornerRadius(12)
                                                    .onTapGesture {
                                                        withAnimation {
                                                            showGallery = true
                                                        }
                                                    }
                                            }
                                            Spacer()
                                        }
                                        
                                       
                                    }
                                    
                                }
                                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                .padding(.vertical, AppConstants.viewExtraMargin)
                                .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: AppConstants.viewExtraMargin)))
                                .padding(.bottom, AppConstants.viewSmallMargin)
                                .background(Color.bgViewSecondaryColor)
                            }
                           
                            if(!barberShop.reviews.isEmpty){
                                VStack{
                                    
                                    HStack{
                                        
                                        Text("Commentaires")
                                            .font(.system(weight: .bold, size: 17))
                                            .foregroundColor(Color.primaryTextColor)
                                        
                                        Spacer()
                                        
                                        StarsView(rating: CGFloat(barberShop.rating), maxRating: 5, width: 15)
                                            .onTapGesture {
                                                withAnimation {
                                                    //showAddReview = true
                                                }
                                            }
                                        
                                        Text(barberShop.rating.toRating())
                                            .font(.system(weight: .regular, size: 14))
                                            .foregroundColor(Color.primaryColor)
                                            .onTapGesture {
                                                withAnimation {
                                                    //showAddReview = true
                                                }
                                            }
                                        
                                        if(barberShop.canReview && !barberShop.isBlocked){
                                            Image.iconAddRatingGray
                                                .resizable()
                                                .frame(width: 25, height: 25)
                                                .onTapGesture {
                                                    withAnimation {
                                                        showAddReview = true
                                                    }
                                                }
                                        }
                                    }
                                    
                                    VStack(spacing: 20){
                                        if(barberShop.reviews.count > 3){
                                            ForEach(0..<3) { index in
                                                let review = barberShop.reviews[index]
                                                ReviewSmallItemView(review: review)
                                            }
                                        }else{
                                            ForEach(barberShop.reviews, id: \.id) { review in
                                                ReviewSmallItemView(review: review)
                                            }
                                        }
                                    }.padding(.top, AppConstants.viewSmallMargin)
                                        .padding(.bottom, AppConstants.viewSmallMargin)
                                    
                                    if(barberShop.countReview > 3){
                                        Text("Voir Tout")
                                            .font(.system(weight: .regular, size: 14))
                                            .foregroundColor(Color.secondaryTextColor)
                                            .onTapGesture {
                                                withAnimation {
                                                    showReviews = true
                                                }
                                            }
                                    }
                                   
                                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                                .padding(.vertical, AppConstants.viewExtraMargin)
                                .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)))
                                .padding(.bottom, AppConstants.viewSmallMargin)
                                .background(Color.bgViewSecondaryColor)
                            }
                            
                        }
                        .padding(.bottom, 80)
                        .background(Color.bgViewColor)
                        
                    }.overlay(
                        
                        GeometryReader{proxy -> Color in
                            
                            DispatchQueue.main.async {
                                
                                if startOffset == 0{
                                    self.startOffset = proxy.frame(in: .global).minY
                                }
                                
                                let offset = proxy.frame(in: .global).minY
                                self.scrollViewOffset = offset - startOffset
                                
                            }
                            
                            return Color.clear
                            
                        }.frame(width: 0, height: 0)
                        ,alignment: .top
                    )
                }
                
                VStack{
                    
                    HStack{
                        
                        Circle().fill(Color.secondaryColor)
                            .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                            .overlay(
                                Image.iconArrowBack
                                    .resizable()
                                    .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                            ).onTapGesture {
                                withAnimation {
                                    showDetail = false
                                }
                            }
                            .padding(.top, getSafe().top == 0 ? 12 : 0)
                            .scaleEffect(-scrollViewOffset > 200 ? 0 : 1)
                            .animation(.linear(duration: 0.2), value: scrollViewOffset)
                        
                        Spacer()
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        
                        if AppFunctions.checkIfUserConnected() {
                            if(barberShop.isBlocked){
                                withAnimation {
                                    showBlocked = true
                                }
                                return
                            }
                            withAnimation {
                                showCart = true
                            }
                        }else{
                            UserDefaults.showLoginDialog = true
                            //UserDefaults.showLogin = true
                            return
                        }
                        
                    }, label: {
                        
                        Text("Réserver maintenant")
                            .font(.system(weight: .regular, size: 16))
                        
                    }).buttonStyle(PrimaryButtonStyle())
                        .padding(.bottom, AppConstants.viewExtraMargin)
                    
                }
                    .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    .padding(.top, AppConstants.viewVeryExtraMargin)
                    .padding(.top, AppConstants.viewNormalMargin)
                    .padding(.bottom, AppConstants.viewExtraMargin)
            }.bottomSheet(isPresented: $showBlocked, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
                CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertBlock,title: LocalizationKeys.title_user_blocked.localized, message: LocalizationKeys.message_user_blocked.localized, titlePositive:  "Fermer" , positiveAction: {
                    withAnimation {
                        showBlocked = false
                    }
                }, isNegative: false)
            }
                
            
            if(showCart){
                CartView(showCart: $showCart, barberShop: barberShop).transition(.move(edge: .trailing)).animation(.linear, value: showCart).zIndex(1)
            }
            
            if(showAddReview){
                AssessmentView(showAssessment: $showAddReview, barberShop: barberShop){
                    barberShop.canReview = false
                }
                .transition(.move(edge: .trailing)).animation(.linear, value: showAddReview).zIndex(1)
            }
            
            if(showServices){
                ListServiceView(showListService: $showServices, barberShop: barberShop ).transition(.move(edge: .trailing)).animation(.linear, value: showServices).zIndex(1)
            }
            
            if(showGallery){
                BarberShopGaleryView(showGallery: $showGallery, barberShop: barberShop).transition(.move(edge: .trailing)).animation(.linear, value: showGallery).zIndex(1)
            }
            
            if(showReviews){
                ListReview(showListReview: $showReviews, barberShop: barberShop).transition(.move(edge: .trailing)).animation(.linear, value: showReviews).zIndex(2)
            }
            
        }
            .background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.all)
            .onAppear(){
                hideKeyboard()
            }
           
    }
}

extension View{
    
    func getSafe() -> UIEdgeInsets{
        
        return UIApplication.shared.windows.first?.safeAreaInsets ?? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}


struct DetailBarberView_Previews: PreviewProvider {
    static var previews: some View {
        DetailBarberView(showDetail: .constant(false), barberShop: FakeData.getBarberShopData(id: 1))
    }
}


struct BlurView :UIViewRepresentable{
    
    var effect : UIBlurEffect.Style
    
    func makeUIView(context: UIViewRepresentableContext<BlurView>) -> UIVisualEffectView {
        
        let blurEffect = UIBlurEffect(style: effect)
        let blurview = UIVisualEffectView(effect: blurEffect)
        
        return blurview
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        
    }
}


