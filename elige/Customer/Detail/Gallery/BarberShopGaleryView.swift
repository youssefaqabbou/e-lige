//
//  BarberShopGalery.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct BarberShopGaleryView: View {
    @StateObject var vmGallery = BarberShopGaleryViewModel()
    @Binding var showGallery : Bool
    @State var barberShop: BarberShopModel

    @State var showSlider : Bool = false
    @State var selectedIndex : Int = 0
    @State var gridLayout: [GridItem] = [GridItem(.adaptive(minimum: 90))]
    
    var body: some View {
        
        ZStack{
            
            VStack{
                
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showGallery = false
                            }
                            
                        }
                    
                    Spacer()
                   
                    Text("Galerie")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                }.padding(.bottom, AppConstants.viewSmallMargin)
                
                Spacer()
                
                CollectionLoadingView(loadingState: vmGallery.loadingState) {
                    VStack(spacing: 20){
                        Spacer()
                    }
                } content: {
                    
                    ScrollView(.vertical, showsIndicators: false){
                        
                            LazyVGrid(columns: gridLayout, alignment: .center, spacing: 10) {
                                
                                ForEach(0..<vmGallery.mediaList.count, id: \.self) { index in
                                    let media = vmGallery.mediaList[index]
                                    WebImage(url: URL(string: media.url))
                                        .placeholder {
                                            Image.placeholderShop
                                        }
                                        .resizable() //
                                        .aspectRatio(contentMode: .fill)
                                        .frame(minWidth: 0, maxWidth: .infinity)
                                        .frame(height: 90)
                                        .cornerRadius(10)
                                        .shadow(color: Color.primary.opacity(0.3), radius: 1)
                                        .onTapGesture {
                                            selectedIndex = index
                                            DebugHelper.debug("selectedIndex", selectedIndex)
                                            withAnimation {
                                                showSlider = true
                                            }
                                        }
                                }
                               
                            }
                    }.padding(.top, AppConstants.viewSmallMargin)
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_photo_empty_title.localized, message: LocalizationKeys.list_photo_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_photo_empty_title.localized, message: LocalizationKeys.list_photo_empty_message.localized)
                }
           
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            
            if(showSlider){
                BarberShopSliderView(showSlider: $showSlider, barberShop: vmGallery.barberShop, selectedIndex: selectedIndex).transition(.move(edge: .trailing)).animation(.linear, value: showSlider).zIndex(1)
            }
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            //barberShop = FakeData.getBarberShopData(id: 1)
            vmGallery.barberShop = barberShop
            vmGallery.fetchMedias()
        }
            
    }
}

struct BarberShopGaleryView_Previews: PreviewProvider {
    static var previews: some View {
        BarberShopGaleryView(showGallery: .constant(false), barberShop: BarberShopModel())
    }
}
