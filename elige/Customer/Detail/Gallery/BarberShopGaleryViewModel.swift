//
//  BarberShopGaleryViewModel.swift
//  elige
//
//  Created by macbook pro on 18/1/2022.
//

import Foundation
import UIKit

class BarberShopGaleryViewModel : ObservableObject {
    
    private var apiService: APIService = APIService()
  
    @Published var barberShop = BarberShopModel()
    @Published var mediaList = [MediaModel]()
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
        
    func fetchMedias(){
        loadingState = .loading
        
        let params: [String: String] = [
            "salonId": String(barberShop.id)
        ]
        apiService.getMediasByShop(params: params) { medias in
            if(medias.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
            self.mediaList = medias
            self.barberShop.medias = medias
        } onFailure: { errorMessage in
            self.loadingState = .empty
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
   
}
