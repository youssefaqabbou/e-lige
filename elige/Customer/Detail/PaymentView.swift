//
//  PaymentView.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import SwiftUI
import Stripe

struct PaymentView: View {
    
    @StateObject var paymentVM = PaymentViewModel()
    
    @Binding var showPayment : Bool
    @Binding var cart : CartModel
    
    @State var showRemoveAlert = false
    @State var isCardAnimLoaded = false
    var animMenu = AnimationFactory.move(yOffset: 280)
    
    var cardSelectedAction : (CardModel) -> () = {card in }
    var cardAddedAction  : (STPPaymentMethodCardParams, String, Bool) -> () = {paymentMethodCardParams, holderName, canSave in }
    
    let NC = NotificationCenter.default
    @State var keyboardOffset : CGFloat = 0
    
    var body: some View {
        ZStack(){
            VStack(){
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showPayment = false
                            }
                        }
                    
                    Spacer()
                   
                    Text("Mode de paiement")
                        .textStyle(TitleStyle())
                        .onTapGesture {
                            hideKeyboard()
                        }
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                }
                
                ScrollView(showsIndicators : false){
                    VStack(alignment: .leading ,spacing: 20){
                        
                        if(paymentVM.loadingCards && paymentVM.listCard.isEmpty || !paymentVM.loadingCards && !paymentVM.listCard.isEmpty){
                            VStack(alignment: .leading){
                                
                                HStack(){
                                    Text(paymentVM.loadingCards ? "Chargement des cartes" : "Choisissez votre carte:" )
                                        .font(.system(weight: .bold, size: 20))
                                        .foregroundColor(.primaryTextColor)
                                        .padding(.top)
                                        .padding(.bottom)
                                    Spacer()
                                    if(paymentVM.loadingCards){
                                        ProgressView()
                                            .frame(width: 25, height: 25)
                                            .progressViewStyle(CircularProgressViewStyle(tint: Color.primaryColor))
                                    }
                                }
                                if(paymentVM.loadingCards){
                                    ListShimmeringView(count: 2){
                                        CardItemHolderView()
                                    }
                                }else{
                                    CustomGrid(data: $paymentVM.listCard, cellWidth: UIScreen.main.bounds.width / 1.3, cellHeight: 180, minSpacing :10 ,animation: animMenu, isScrolled : true) { card in
                                        CardListItemView(card: card) { isChecked in
                                            paymentVM.checkCard(updatedCard: card, isChecked: isChecked)

                                        } removeAction: { cardToRemove in
                                            paymentVM.removedMethodId = cardToRemove.paymentId
                                            showRemoveAlert = true
                                        }
                                    } onLoaded: { collection in
                                        if (!isCardAnimLoaded && !paymentVM.listCard.isEmpty) {
                                            Animator(animation: animMenu).animate(views: collection.visibleCells)
                                            self.isCardAnimLoaded = true
                                        }
                                       
                                      }
                                    .frame(height: 180, alignment: .center)
                                }
                            }
                        }
                        
                        LazyVStack(alignment: .leading){
                            
                            Text("Utilisez une nouvelle carte")
                                .font(.system(weight: .bold, size: 20))
                                .foregroundColor(.primaryTextColor)
                                .padding(.bottom)
                            
                            HStack(spacing: 10) {
                                
                                Image.iconUser
                                    .resizable()
                                   .frame(width: 25, height: 25)
                                   .padding(.leading, AppConstants.viewExtraMargin)
                                
                                CustomHintTextField(placeholder: "Titulaire de la carte", text: $paymentVM.holderName, keyboardType : .alphabet, isFocused: .constant(false))
                                    .background(Color.bgViewSecondaryColor)
                                    .frame(height: 70)
                                
                            }
                            .frame(height: 70)
                            //.padding(.horizontal, AppConstants.viewSmallMargin)
                            .background(Color.bgViewSecondaryColor.clipShape(AppUtils.CustomCorners(corners: [.allCorners], radius: 12)))
                       
                            StripeCardTextField(cardParams: $paymentVM.paymentMethodCardParams, isValid: $paymentVM.isValidCard, borderColor: .clear, cornerRadius: 12)
                                .frame(height: 70)
                                .padding(.top, AppConstants.viewSmallMargin)
                                .padding(.bottom, 15)
                            
                            HStack{
                                
                                Spacer()
                                
                                Button(action: {
                                    paymentVM.isSaveCard.toggle()
                                }, label: {
                                    Text("Enregistrer cette carte")
                                        .font(.system(weight: .medium, size: 14))
                                        .foregroundColor(.primaryTextColor)
                                    
                                    Image(systemName:  paymentVM.isSaveCard ? "checkmark.square" : "square").foregroundColor(.primaryColor)
                                })
                            }
                            .padding(.bottom, AppConstants.viewExtraMargin)
                            
                        }
                        .padding(.top, AppConstants.viewNormalMargin)
                        .disabled(!paymentVM.isFormEnable)
                        
                       
                    }
                }.padding(.bottom, keyboardOffset == 0 ? 90 : keyboardOffset)
               
            }
           
            VStack(){
                Spacer()
                
                Button(action: {
                    hideKeyboard()
                    if(!paymentVM.selectedCard.paymentId.isEmpty){
                        cardSelectedAction(paymentVM.selectedCard)
                    }else if (paymentVM.isValidCard){
                        cardAddedAction(paymentVM.paymentMethodCardParams, paymentVM.holderName, paymentVM.isSaveCard)
                    }
                }, label: {
                    Text("Valider")
                        .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                }).buttonStyle(PrimaryButtonStyle())
                    .padding(.vertical, AppConstants.viewVeryExtraMargin)
            }
            
        }
        .onTapGesture {
            //UIApplication.shared.endEditing()
            //hideKeyboard()
            //paymentVM.isFormEnable = false
            self.hideKeyboard()
        }
        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear(){
            hideKeyboard()
            paymentVM.loadCard()
            self.NC.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil,
                                   using: self.keyboardDidShow)
            self.NC.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil,
                                   using: self.keyboardDidHide)
        }
        .bottomSheet(isPresented: $showRemoveAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDelete, title: "Confirmation", message: "Voulez vous supprimer cette carte ?", positiveAction: {
                paymentVM.removeCard()
                withAnimation {
                    showRemoveAlert = false
                }
            }, isNegative: true, negativeAction: {
                withAnimation {
                    showRemoveAlert = false
                }
            })
        }
    }
    
    func keyboardDidShow(_ notification: Notification) {
        DebugHelper.debug("keyboardDidShow")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            DebugHelper.debug("keyboardDidShow", keyboardSize.height)
            if keyboardOffset == 0 {
                keyboardOffset = keyboardSize.height
            }
        }
    }
    
    func keyboardDidHide(_ notification: Notification) {
        DebugHelper.debug("keyboardDidHide")
        if keyboardOffset != 0 {
            keyboardOffset = 0
        }
    }
}

struct PaymentView_Previews: PreviewProvider {
    static var previews: some View {
       PaymentView(showPayment: .constant(false), cart: .constant(CartModel()))
    }
}

