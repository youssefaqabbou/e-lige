//
//  AssessmentView.swift
//  elige
//
//  Created by user196417 on 11/17/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct AssessmentView: View {
    
    @StateObject var vmAssessment = AssessmentViewModel()
   
    @Binding var showAssessment : Bool
    @State var barberShop : BarberShopModel
    
    var onReviewAdded : () -> () = {}
    
    var body: some View {
        
        ZStack{
            
            VStack(spacing: 15){
                
                HStack{
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showAssessment = false
                            }
                           
                        }
                    Spacer()
                    
                    Text("Évaluation")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                    
                }.padding(.bottom, AppConstants.viewExtraMargin)
               
                VStack{
                    
                    WebImage(url: URL(string: vmAssessment.currentUser.image.url))
                        .placeholder {
                            Rectangle().fill(Color.gray).cornerRadius(12)
                        }
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 90, height: 90)
                        .clipShape(Circle())
                   
                    
                    Text(vmAssessment.currentUser.fullname)
                        .font(.system(weight: .regular, size: 24))
                        .foregroundColor(Color.primaryTextColor)
                    
                }
                
                Spacer()
                
                RatingView(rating: $vmAssessment.rating)
                
                Spacer()
                
                VStack(alignment: .leading){
                    
                    HStack{
                        
                        Text("Commentaire")
                            .font(.system(weight: .bold, size: 15))
                            .foregroundColor(Color.primaryTextColor)
                        
                        Spacer()
                    }
              
                   
                    CustomHintTextEditor(placeholder: "", text: $vmAssessment.review)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                }
                
                Spacer()
                
                CustomLoadingButton(isLoading: $vmAssessment.isLoading, text: "Soumettre") {
                    vmAssessment.addReview { success in
                        var updatedBarberShop = barberShop
                        updatedBarberShop.canReview = false
                        NotificationHelper.postBarberShop(barberShop: updatedBarberShop)
                        onReviewAdded()
                        withAnimation {
                            showAssessment = false
                        }
                    }
                }
                Spacer()
                
            }.padding(AppConstants.viewVeryExtraMargin)
            
        }.padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            .background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                vmAssessment.salonId = barberShop.id
            }.onTapGesture {
                hideKeyboard()
            }
    }
}

struct AssessmentView_Previews: PreviewProvider {
    static var previews: some View {
        AssessmentView(showAssessment: .constant(false), barberShop: BarberShopModel())
    }
}
