//
//  AssessmentViewModel.swift
//  elige
//
//  Created by user196417 on 12/23/21.
//

import Foundation


class AssessmentViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    @Published var isLoading = false
    
    @Published var currentUser = AppFunctions.getConnectedUser()
    
    @Published var salonId: Int = -1
    @Published var review: String = ""
    @Published var rating: Int? = 1
    
    func addReview(onSuccess: @escaping ( Bool) -> Void) {
        
        let params = ["salonId": String(salonId), "review": review, "rating": rating] as [String : Any]
        
        apiService.addReview(params: params, onSuccess: { message, review in
            
            AppFunctions.showSnackBar(status: .success , message: message)
            onSuccess(true)
            
        }, onFailure: { errorMessage in
            onSuccess(false)
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }
    
    
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
    }
    
    func refreshProfile(){
        currentUser = AppFunctions.getConnectedUser()
    }
    
}
