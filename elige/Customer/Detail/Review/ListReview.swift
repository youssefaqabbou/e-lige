//
//  ListReview.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import SwiftUI

struct ListReview: View {
    
    @StateObject var reviewsVM = ListReviewViewModel()
    @Binding var showListReview : Bool
    @State var barberShop : BarberShopModel
    
    @State var isAnimationLoaded = false
    
    var body: some View {
        
        ZStack(){
            
            VStack(){
                
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showListReview = false
                            }
                        }
                    
                    Spacer()
                    
                    Text("Commentaires")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                    
                }.padding(.vertical, AppConstants.viewNormalMargin)
               
                
                CollectionLoadingView(loadingState: reviewsVM.loadingState) {
                    ListShimmeringView(count: 15){
                        ReviewItemHolderView()
                    }
                } content: {
                    GeometryReader { geometry in
                        ScrollView(.vertical, showsIndicators: false){
                            
                            LazyVStack(spacing: 15){
                                ForEach(0..<reviewsVM.reviewList.count, id: \.self){ index in
                                    let review = reviewsVM.reviewList[index]
                                    //ForEach(reviewsVM.reviewList, id: \.id) { review in
                                    ReviewItemView(review: review, indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY)
                                        .onAppear {
                                            reviewsVM.loadMoreIfNeeded(currentItem: review)
                                        }
                                }
                            }.padding(.top, AppConstants.viewNormalMargin)
                        }
                        .padding(.bottom, AppConstants.viewNormalMargin)
                    }
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_comment_empty_title.localized, message: LocalizationKeys.list_comment_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_comment_empty_title.localized, message: LocalizationKeys.list_comment_empty_message.localized)
                }
                
            
              
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
        } .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            .background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                reviewsVM.selectedBarberShop = barberShop
                reviewsVM.getReviews()
            }
    }
}

struct ListReview_Previews: PreviewProvider {
    static var previews: some View {
        ListReview(showListReview: .constant(false), barberShop: BarberShopModel())
    }
}
