//
//  ListReviewViewModel.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import Foundation


class ListReviewViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    @Published var reviewList : [ReviewModel] = []
    @Published var selectedBarberShop = BarberShopModel()
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingMore = false
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    func getReviews(){
        guard !isLoadingMore && canLoadMorePages else {
          return
        }
        
        if(currentPage == 0){
            self.loadingState = .loading
        }else{
            isLoadingMore = true
        }
        let params = ["page" : String(currentPage), "size" : String(size), "salonId" : String(selectedBarberShop.id)]
       
        apiService.getReviews(params: params, onSuccess: { reviews in
            if(reviews.count == 0 && self.currentPage == 0){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
            if reviews.count == 0 {
                self.canLoadMorePages = false
            }
            self.reviewList.append(contentsOf: reviews)
            self.currentPage += 1
            self.isLoadingMore = false
        },  onFailure: { errorMessage in
            self.canLoadMorePages = false
            self.isLoadingMore = false
            if(self.reviewList.count == 0 && self.currentPage == 0){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        })
    }
    
    func resetPagination(){
        isLoadingMore = false
        currentPage = 0
        canLoadMorePages = true
    }
    
    func loadMoreIfNeeded(currentItem item: ReviewModel?) {
        guard let item = item else {
            getReviews()
          return
        }

        let thresholdIndex = reviewList.index(reviewList.endIndex, offsetBy: AppConstants.offsetPagination)
        if reviewList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            getReviews()
        }
      }
}
