//
//  RatingView.swift
//  elige
//
//  Created by user196417 on 11/17/21.
//

import SwiftUI

struct RatingView: View {
    
    @Binding var rating: Int?
    
    public func starType(index: Int) -> Image {
        
        if let rating = self.rating {
            
            return index <= rating ? Image.iconStarColor  : Image.iconStar
        } else {
            return Image.iconStar
        }
    }
    
    var body: some View {
        
        HStack(spacing: 10){
            
            ForEach(1...5, id: \.self){ index in
                
                self.starType(index: index)
                    .resizable()
                    .frame(width: 27, height: 27)
                    .foregroundColor(Color.primaryColor)
                    .padding(AppConstants.viewSmallMargin)
                    .background(Color.secondaryTextColor.opacity(0.1))
                    .frame(width: 50, height: 50)
                    .cornerRadius(15)
                    .onTapGesture {
                        self.rating = index
                    }
            }
        }
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        RatingView(rating: .constant(0))
    }
}
