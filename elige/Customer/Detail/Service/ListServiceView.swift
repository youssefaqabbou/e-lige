//
//  ListServiceView.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import SwiftUI

struct ListServiceView: View {
    
    @StateObject var vmListService = ListServiceViewModel()
    @Binding var showListService : Bool
    @State var barberShop : BarberShopModel
   
    @State var isAnimationLoaded = false
    var body: some View {
        
        ZStack(){
            
            VStack(){
                
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showListService = false
                            }
                        }
                    
                    Spacer()
                    
                    Text("Prestations")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).hidden()
                    
                }.padding(.vertical, AppConstants.viewNormalMargin)
                
                
                CollectionLoadingView(loadingState: vmListService.loadingState) {
                    ListShimmeringView(count: 15){
                        ListServiceItemHolderView()
                    }
                } content: {
                    /*CustomList(data: $vmListService.serviceList, animation: AppConstants.animList) { service in
                        ListServiceItem(service: service)
                            
                        //ListServiceItem(service: service)
                    }onLoaded: { tableView in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            tableView.reloadData()
                        }
                        
                        if (!isAnimationLoaded && !vmListService.serviceList.isEmpty) {
                            Animator(animation: AppConstants.animList).animate(views: tableView.visibleCells)
                            self.isAnimationLoaded = true
                        }
                      }*/
                    
                    GeometryReader { geometry in
                        ScrollView(.vertical, showsIndicators: false){
                            
                            VStack(spacing: 15){
                                ForEach(0..<vmListService.serviceList.count, id: \.self){ index in
                                    let service = vmListService.serviceList[index]
                                //ForEach(vmListService.serviceList, id: \.id) { service in
                                    ServiceItemView(service: service, indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY )
                                }
                            }
                        }
                    }
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_service_empty_title.localized, message: LocalizationKeys.list_service_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_service_empty_title.localized, message: LocalizationKeys.list_service_empty_message.localized)
                }
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
        }
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)        .onAppear {
            vmListService.getServiceByID(salonId: barberShop.id)
        }
    }
}

struct ListServiceView_Previews: PreviewProvider {
    static var previews: some View {
        ListServiceView(showListService: .constant(false), barberShop: BarberShopModel())
    }
}
