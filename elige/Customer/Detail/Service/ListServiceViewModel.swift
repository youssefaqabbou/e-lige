//
//  ListServiceViewModel.swift
//  elige
//
//  Created by user196417 on 12/13/21.
//

import Foundation


class ListServiceViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    @Published var serviceList : [ServiceModel] = []
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    func getServiceByID(salonId: Int){
        //
        self.loadingState = .loading
        
        let params = ["salonId" : String(salonId)]
        
        apiService.getServices(params: params, onSuccess: { services in
            if(services.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
            self.serviceList = services
            
        }, onFailure: { errorMessage in
            self.loadingState = .empty
        })
    }
    
}
