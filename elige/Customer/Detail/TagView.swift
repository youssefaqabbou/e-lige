//
//  TagView.swift
//  elige
//
//  Created by user196417 on 11/16/21.
//

import SwiftUI

struct TagView: View {
    
    
    //var maxLimit: Int
    @Binding var tags : [TagModel]

   
    var body: some View {
        
        VStack(alignment: .leading, spacing: 15){
            
            ForEach(getRow(), id: \.self){ rows in
                
                HStack(alignment:.center, spacing: 8){
                    
                    ForEach(rows){ tag in
                        TagItem(tag: tag)
                    }
                }
            }
        }
    }
    
    
    func getRow() -> [[TagModel]]{
        
        var rows: [[TagModel]] = []
        var currentRow : [TagModel] = []
        
        var totalWidth : CGFloat = 0
        let screenWidth : CGFloat = UIScreen.main.bounds.width - 60
        
        tags.forEach { tag in
            //DebugHelper.debug(tag.name)
            totalWidth += (tag.size + 20 + 8 )
            
            if totalWidth > screenWidth {
                totalWidth = (!currentRow.isEmpty ? (tag.size + 20 + 8) : 0)
                rows.append(currentRow)
                currentRow.removeAll()
                //DebugHelper.debug(tag.name , rows.count )
                currentRow.append(tag)
                
            }else{
                //DebugHelper.debug(tag.name , rows.count )
                currentRow.append(tag)
            }
        }
        if !currentRow.isEmpty{
            rows.append(currentRow)
            //DebugHelper.debug(rows.count)
        }
        //DebugHelper.debug(rows.count)
        return rows
    }
}
/*
struct TagView_Previews: PreviewProvider {
    static var previews: some View {
        TagView( tags: [TagModel]())
    }
}
*/
