//
//  CartViewModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation
import HCalendar
import SwiftUI
import Fakery

class CartViewModel: ObservableObject {
    
    private var apiService = APIService()

    @Published var selectedDate = AppFunctions.getTodayDateUTC()
    @Published var selectedTime = ""
    @Published var scheduleList: [AvailableScheduleModel] = []
    @Published var availableScheduleTime = [Date]()
    @Published var selectedDateSchedule = ScheduleModel()
    //@Published var availableScheduleList: [AvailableScheduleModel] = []
    
    @Published var allServiceList: [ServiceModel] = []
    @Published var serviceList: [ServiceModel] = []
    
    @Published var totalServices: Double = 0.0
    @Published var totalOrder: Double = 0.0
    @Published var totalDuration: Int = 0
    @Published var cart = CartModel()

    @Published var hCalendarConfig = HCalendarConfig()
    @Published var hCalendarStyle = calendarStyle
    
    @Published var selectedServiceLocation = 0
    
    @Published private(set) var loadingService: CollectionLoadingState = .loading
    
    init() {
        hCalendarConfig.showPastWeek = false
        hCalendarConfig.weekdaySymbolFormat = .short
        hCalendarConfig.firstWeekday = 2
        hCalendarConfig.showToday = false
        hCalendarConfig.locale = Locale(identifier: "Fr")

    }
    
    public static var calendarStyle: HCalendarStyle {
        var calendarStyle = HCalendarStyle()

        calendarStyle.background = Color.bgViewColor
        calendarStyle.border = Color.bgViewColor

        var dayStyle = HCalendarDayStyle()
        dayStyle.current = Color.primaryTextColor
        dayStyle.future = Color.primaryTextColor
        dayStyle.past = Color.gray
        dayStyle.selectedBg = Color.primaryTextColor
        dayStyle.selectedFg = Color.primaryTextLightColor

        dayStyle.selectedBg = Color.primaryTextColor
        dayStyle.selectedRadius = 10
        dayStyle.fontSize = 14
        calendarStyle.day = dayStyle

        var weekStyle = HCalendarWeekStyle()
        weekStyle.foreground = Color.primaryTextColor
        weekStyle.font = FontStyle.brownStd.rawValue
        weekStyle.fontSize = 14
        weekStyle.forceUpperCase = true
        calendarStyle.weekSymbol = weekStyle

        var monthStyle = HCalendarMonthStyle()
        monthStyle.foreground = Color.primaryTextColor
        //monthStyle.foreground = Color.primaryTextColor
        monthStyle.font = FontStyle.brownStd.rawValue
        monthStyle.fontSize = 16

        var navStyle = HCalendarNavStyle()
        navStyle.navIconW = 5
        navStyle.navIconH = 8
        navStyle.navImageH = 25
        navStyle.navImageW = 25
        navStyle.navBg = Color.primaryTextColor
        navStyle.navStrokeColor = Color.primaryTextColor
        navStyle.navFg = Color.bgViewColor
        navStyle.navRadius = .infinity

        navStyle.showToday = true
        navStyle.todayStrokeColor = Color.primaryTextColor
        calendarStyle.nav = navStyle

        return calendarStyle
    }

    func getServiceByID(salonId: Int){
        //
        self.loadingService = .loading
        
        let params = ["salonId" : String(salonId)]
        
        apiService.getServices(params: params, onSuccess: { services in
            self.allServiceList = services
            self.loadingService = .loaded
            self.filterServicesByLocation()
        }, onFailure: { errorMessage in
            self.loadingService = .loaded
        })
    }
    
    func filterServicesByLocation(){
        if(selectedServiceLocation == 1 ){
            serviceList = allServiceList.filter { service in
                return service.serviceLocation == ServiceLocation.home.rawValue ||  service.serviceLocation == ServiceLocation.both.rawValue
            }
        }else{
            serviceList = allServiceList.filter { service in
                return service.serviceLocation == ServiceLocation.shop.rawValue ||  service.serviceLocation == ServiceLocation.both.rawValue
            }
        }
       
    }
    
 

    func updateServiceInList(service: ServiceModel) {

        let index = getServiceIndex(serviceId: service.id)

        if index != nil {
            serviceList[index!].isEnabled = serviceList[index!].isEnabled
            serviceList[index!].quantity = service.quantity
            serviceList[index!].totalPrice = Double(service.quantity) * service.price
            sumTotalPrice()
        }
    }

    func sumTotalPrice() {
        var sum = 0.0
        var duration = 0
        for service in serviceList {
            if(service.isEnabled) {
                sum += service.totalPrice
                duration += service.duration
            }
        }
        totalServices = sum
        totalOrder = totalServices + ( self.selectedServiceLocation == 0 ? AppConstants.barberFees  : AppConstants.homeFees)
        totalDuration = duration
        calculateAvailableSchedule()
    }

    func getServiceIndex(serviceId: Int) -> Int? {
        for (index, value) in serviceList.enumerated() {
            if value.id == serviceId {
                return index
            }
        }
        return nil
    }

    func getSelectedServices() -> [ServiceModel] {
        var services = [ServiceModel]()
        for service in serviceList {
            if(service.isEnabled) {
                services.append(service)
            }
        }
        return services
    }

    func checkCart(onSuccess successCallback: (() -> Void)?,
        onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {

        if(selectedTime.isEmpty) {
            failureCallback?(LocalizationKeys.error_time_empty.localized)
            return
        } else if (getSelectedServices().isEmpty) {
            failureCallback?(LocalizationKeys.error_service_empty.localized)
            return
        }
        successCallback?()
    }
    
    func validateCart() {

        cart.fees = self.selectedServiceLocation == 0 ? AppConstants.barberFees : AppConstants.homeFees
        cart.services = getSelectedServices()
        DebugHelper.debug("orderDate before",  cart.orderDate)
        let (h, m) = AppFunctions.timeToHoursMinutes(selectedTime)
        cart.orderDate = selectedDate.changeTo(hour: h, minute: m, convertToUtc: true)
        cart.serviceLocation = selectedServiceLocation == 1 ? ServiceLocation.home.rawValue : ServiceLocation.shop.rawValue
        DebugHelper.debug("orderDate after",  cart.orderDate)
    }
    
    func getAvailableSchedule(barberId : Int){
        self.scheduleList.removeAll()
        let params: [String: String] = [
            "salonId": String(barberId),
            "date": selectedDate.toString(format: "yyyy-MM-dd", isConvertZone: false)
        ]
        
        apiService.getAvailableSchedules(params: params) { availableSchedules in
            self.scheduleList = availableSchedules
            self.calculateAvailableSchedule()
        } onFailure: { errorMessage in
            DebugHelper.debug(errorMessage)
            self.calculateAvailableSchedule()
        }
    }
    
    func calculateAvailableSchedule(){
        selectedTime = ""
        let currentDate = AppFunctions.getTodayDateWithTZ()
        //DebugHelper.debug("currentDate", currentDate)
        availableScheduleTime.removeAll()
        for schedule in scheduleList {
            //DebugHelper.debug("Hello schedule, \(schedule.startAt) , \(schedule.endAt)!")
            getPlagePeriod(openDate: getOpenDate(date: currentDate > schedule.startAt ? currentDate : schedule.startAt), closeDate: getCloseDate(date: schedule.endAt))
        }
    }
    
    
    func getOpenDate(date : Date) -> Date{
        var extendedMinute = 0
        //DebugHelper.debug(date, date.minute)
        var openDate = date //.addMinutes(minutes: restaurantInfo.readyTime)
        //DebugHelper.debug(openDate, openDate.minute)
       
        for index in stride(from: 0, to: 59, by: rangeBy){
            //DebugHelper.debug("from", index , "to", rangeBy + index)
            if(openDate.minute >= index + 1 && openDate.minute < rangeBy + index && openDate.minute != 0){
                extendedMinute = (index + rangeBy) - openDate.minute
                //DebugHelper.debug("In plage minute", (index + rangeBy), "extendedMinute", extendedMinute)
                openDate = openDate.addMinutes(minutes: extendedMinute)
                //DebugHelper.debug("After", openDate, openDate.minute)
                return openDate
            }
        }
        //DebugHelper.debug("After", openDate, openDate.minute)
        return openDate
    }
    
    func getCloseDate(date : Date) -> Date{
        var extendedMinute = 0
        //DebugHelper.debug(date, date.minute)
        var closeDate = date.addMinutes(minutes: rangeBy > totalDuration ? rangeBy  * -1 : totalDuration  * -1)
        //DebugHelper.debug(closeDate, closeDate.minute)
       
        for index in stride(from: 0, to: 59, by: rangeBy){
           // DebugHelper.debug("from", index , "to", rangeBy + index)
            if(closeDate.minute >= index && closeDate.minute < rangeBy + index){
                extendedMinute =  index - closeDate.minute
                //DebugHelper.debug("In plage minute", (index + rangeBy), extendedMinute)
                closeDate = closeDate.addMinutes(minutes: extendedMinute)
                //DebugHelper.debug("After", closeDate, closeDate.minute)
                return closeDate
            }
        }
        closeDate = closeDate.addMinutes(minutes: -rangeBy)
        //DebugHelper.debug("After", closeDate, closeDate.minute)
        return closeDate
    }
    
    private let rangeBy = 15
    func getPlagePeriod(openDate: Date, closeDate: Date){
        
        //DebugHelper.debug("openDate", openDate , "closeDate", closeDate)
        var date = openDate
        if(openDate <= closeDate){
            availableScheduleTime.append(date)
        }
        while date < closeDate {
            date = date.addMinutes(minutes: rangeBy)
            availableScheduleTime.append(date)
        }
        //DebugHelper.debug("available Schedules", availableScheduleTime)
    }
    
}
