//
//  CheckinViewModel.swift
//  elige
//
//  Created by macbook pro on 22/11/2021.
//

import Foundation
import MapKit
import CoreLocation
import SwiftUI
import Stripe

class CheckinViewModel : NSObject, ObservableObject, STPAuthenticationContext {
    
    private var apiService: APIService = APIService()
    @Published var isLoading = false
    
    @Published var totalServices : Double = 0.0
    @Published var totalOrder : Double = 0.0
    private var totalDuration : Int = 0
    @Published var barberShop = BarberShopModel()
    @Published var cart = CartModel()
    
    @Published var snapShotImage : UIImage = UIImage(color: UIColor(Color.bgViewSecondaryColor))!
    
    @Published var showPayment : Bool = false
    @Published var paymentMethodCardParams = STPPaymentMethodCardParams()
    
    @Published var isValidCard = false
    @Published var isSaveCard = false
    @Published var holderName = ""
    
    @Published var isPaymentSuccess = true
    @Published var clientSecret = ""
    
    @Published var showCompleteAlert = false
    @Published var showOrderCompleted = false
    @Published var orderCompleteMessage = ""
    @Published var orderCompleteTitle = ""
    
    func calculatePrice(){
        sumTotalPrice()
    }
    
    func sumTotalPrice() {
        var sum = 0.0
        for service in cart.services {
            sum += service.totalPrice
            totalDuration += service.duration
        }
        totalServices = sum
        totalOrder = totalServices + cart.fees
    }
    
    func createSnapShot(location : CLLocationCoordinate2D){
        let mapSnapshotOptions = MKMapSnapshotter.Options()

        // Set the region of the map that is rendered.
        //let location = CLLocationCoordinate2DMake(37.332077, -122.02962) // Apple HQ
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapSnapshotOptions.region = region

        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale

        // Set the size of the image output.
        mapSnapshotOptions.size = CGSize(width: UIScreen.main.bounds.width - 30, height: 180)

        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true

        let snapshot = MKMapSnapshotter(options: mapSnapshotOptions)
        
        snapshot.start { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                print(error ?? "Unknown error")
                return
            }

            let image = UIGraphicsImageRenderer(size: mapSnapshotOptions.size).image { _ in
                snapshot.image.draw(at: .zero)

                let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
                var pinImage = Image.shopMarkerUIImage
                //let pinImage = UIImage(named: "pinImage")
           
                pinImage = AppFunctions.resizeImage(image: pinImage!, targetSize: CGSize(width: 30, height: 30))
                var point = snapshot.point(for: location)

                //if rect.contains(point) {
                    point.x -= pinView.bounds.height / 2
                    point.y -= pinView.bounds.height / 2
                    pinImage?.draw(at: point)
                //}
            }

            // do whatever you want with this image, e.g.

            DispatchQueue.main.async {
                self.snapShotImage = image
            }
        }
    }
    
    func checkOrder(onSuccess successCallback: (() -> Void)?,
        onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        if(cart.selectedCard.paymentId.isEmpty && !isValidCard ){
            failureCallback?(LocalizationKeys.error_card_empty.localized)
            return
        }
        successCallback?()
    }
    
    func createOrderRequest(){
        self.isLoading = true
       
        apiService.createOrder(params: getOrderParams()) { message, clientSecret , createdOrder in
            if(clientSecret.isEmpty){
                self.isLoading = false
                self.showOrderCompleted = false
                self.orderCompleteMessage = message
                self.orderCompleteTitle = "Réservation Incomplete"
                self.showCompleteAlert = true
            }else{
                // Call Stripe
                self.clientSecret = clientSecret
                self.payOrder(clientSecret: clientSecret) { success, message in
                    if(success){
                        
                        self.showOrderCompleted = true
                        self.isPaymentSuccess = true
                        self.orderCompleteMessage = message
                        self.orderCompleteTitle = "Félicitation"
                        self.showCompleteAlert = true
                    }else{
                        self.showOrderCompleted = false
                        self.isPaymentSuccess = false
                        self.orderCompleteMessage = message
                        self.orderCompleteTitle = "Oups! Désolé"
                        self.showCompleteAlert = true
                    }
                    self.isLoading = false
                }
               
            }
        } onFailure: { errorMessage in
            
            self.showOrderCompleted = false
            self.orderCompleteMessage = errorMessage
            self.orderCompleteTitle = "Oups! Désolé"
            self.isLoading = false
            self.showCompleteAlert = true
        }
    }
    
    func getOrderParams() ->  [String : Any] {
       
        let (h, m) = AppFunctions.minutesToHoursMinutes(totalDuration)
        let duration = "\(String(format: "%02d", h)):\(String(format: "%02d", m))"
        
        /*DebugHelper.debug(cart.orderDate)
        DebugHelper.debug(cart.orderDate.toString(format: "yyyy-MM-dd HH:mm:ss", isConvertZone: false))
        DebugHelper.debug(cart.orderDate.toString(format: "yyyy-MM-dd HH:mm:ss", isConvertZone: true))
        DebugHelper.debug(AppFunctions.convertDateToUTC(currentDate: cart.orderDate))
        */

        var params: [String: Any] = [
            "salonId": String(barberShop.id),
            "date": AppFunctions.convertDateToUTC(currentDate: cart.orderDate).toString(format: "yyyy-MM-dd'T'HH:mm:ssZ", isConvertZone: false),
            "duration": duration,
            "place": cart.serviceLocation,
            "orderLines": [],
        ]
        for service in cart.services{
            let orderLines: [String: Any] = [
                "serviceId": String(service.id),
                "quantity": service.quantity
            ]
            
            // get existing items, or create new array if doesn't exist
            var existingItems = params["orderLines"] as? [[String: Any]] ?? [[String: Any]]()
            // append the item
            existingItems.append(orderLines)
            // replace back into `data`
            params["orderLines"] = existingItems
        }
        
        DebugHelper.debug("Order params", params.description)
        return params
    }
 
    func authenticationPresentingViewController() -> UIViewController {
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({ $0.activationState == .foregroundActive })
            .map({ $0 as? UIWindowScene })
            .compactMap({ $0 })
            .first?.windows
            .filter({ $0.isKeyWindow }).first
        return (keyWindow?.rootViewController)!
    }
    
    func rePayOrder(){
        self.isLoading = true
        self.payOrder(clientSecret: self.clientSecret) { success, message in
            if(success){
                
                self.orderCompleteMessage = message
                self.orderCompleteTitle = "Félicitation"
                self.showOrderCompleted = true
                self.isPaymentSuccess = true
                self.showCompleteAlert = true
            }else{
                
                self.orderCompleteMessage = message
                self.orderCompleteTitle = "Oups! Désolé"
                self.isPaymentSuccess = false
                self.showOrderCompleted = false
                self.showCompleteAlert = true
            }
            self.isLoading = false
        }
    }

    func payOrder(clientSecret: String, completionHandler: @escaping (_ exit: Bool, _ message: String) -> Void) {

        isLoading = true
        if(clientSecret.isEmpty) {
            return;
        } else {
            StripeAPI.defaultPublishableKey = AppConstants.stripePublishableKey
            let billingDetails = STPPaymentMethodBillingDetails()
           
            
           
            let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
            
            if(cart.selectedCard.paymentId.isEmpty){
                billingDetails.name = holderName
                let paymentMethodParams = STPPaymentMethodParams(card: paymentMethodCardParams, billingDetails: billingDetails, metadata: nil)
           
                paymentIntentParams.paymentMethodParams = paymentMethodParams
                if(isSaveCard){
                    paymentIntentParams.setupFutureUsage = STPPaymentIntentSetupFutureUsage.onSession
                    paymentIntentParams.savePaymentMethod = true
                }
            }else{
                billingDetails.name = cart.selectedCard.name
                paymentIntentParams.paymentMethodId = cart.selectedCard.paymentId
            }
            
            let paymentHandler = STPPaymentHandler.shared()
            paymentHandler.confirmPayment(paymentIntentParams, with: self) { [self] status, paymentIntent, error in

                switch (status) {

                case .failed:
                    DebugHelper.debug("Payment failed", error?.localizedDescription)
                    completionHandler(false, LocalizationKeys.message_payment_failed.localized)

                    break
                case .canceled:
                    DebugHelper.debug("Payment canceled", error?.localizedDescription)
                    completionHandler(false, LocalizationKeys.message_payment_canceled.localized)

                    break
                case .succeeded:
                    DebugHelper.debug("Payment succeeded", error?.localizedDescription)
                    completionHandler(true, LocalizationKeys.message_payment_succeeded.localized)

                    break
                @unknown default:
                    completionHandler(false, LocalizationKeys.message_payment_error.localized)

                    break
                }

            }
        }
    }

}
