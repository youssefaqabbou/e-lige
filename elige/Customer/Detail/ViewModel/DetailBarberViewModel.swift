//
//  DetailBarberViewModel.swift
//  elige
//
//  Created by user196417 on 12/13/21.
//

import Foundation


class DetailBarberViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var isLoading: Bool = false
    
    func addFavorite(barberShop: BarberShopModel, onError errorCallback: (() -> Void)?) {
        var currenBarberShop = barberShop
        isLoading = true
        
        let params = ["salonId" : String(barberShop.id)]
        apiService.addToFavorite(params: params) { message in
            //AppFunctions.showSnackBar(status: .success, message: message)
            self.isLoading = false
            currenBarberShop.isFavorite = true
            NotificationHelper.postBarberShop(barberShop: currenBarberShop)
        } onFailure: { errorMessage in
            errorCallback?()
            //AppFunctions.showSnackBar(status: .error , message: errorMessage)
            self.isLoading = false
            currenBarberShop.isFavorite = false
            NotificationHelper.postBarberShop(barberShop: currenBarberShop)
        }

    }
    
    func deleteFavorite(barberShop: BarberShopModel, onError errorCallback: (() -> Void)?){
        var currenBarberShop = barberShop
        isLoading = true
        
        apiService.deleteFavorites(idBarber: barberShop.id, onSuccess: { message in
            //AppFunctions.showSnackBar(status: .success, message: message)
            self.isLoading = false
            currenBarberShop.isFavorite = false
            NotificationHelper.postBarberShop(barberShop: currenBarberShop)
        }, onFailure: { errorMessage in
            errorCallback?()
            //AppFunctions.showSnackBar(status: .error , message: errorMessage)
            self.isLoading = false
            currenBarberShop.isFavorite = true
            NotificationHelper.postBarberShop(barberShop: currenBarberShop)
        })

    }
    
}
