//
//  PaymentViewModel.swift
//  elige
//
//  Created by macbook pro on 23/11/2021.
//

import Foundation
import Stripe

class PaymentViewModel : ObservableObject{
    
    private var apiService: APIService = APIService()
    
    @Published var paymentMethodCardParams: STPPaymentMethodCardParams = STPPaymentMethodCardParams()

    @Published var holderName = ""
    @Published var isValidCard = false
    @Published var isLoading = false
    
    @Published var listCard = [CardModel]()
    @Published var isFormEnable : Bool = true
    @Published var selectedMethodId = ""
    @Published var removedMethodId = ""
    
    @Published var isSaveCard : Bool = false
    @Published var loadingCards : Bool = true
    
    @Published var selectedCard = CardModel()
    
    func loadCard(){
        loadingCards = true
        
        apiService.fetchCards() { list in
            self.listCard = list
            if(!self.listCard.isEmpty){
                self.checkCard(updatedCard: self.listCard[0], isChecked: false)
            }
            self.loadingCards = false
        } onFailure: { message  in
            self.loadingCards = false
        }
    }
    
    
    func checkCard(updatedCard : CardModel, isChecked: Bool) {
        //DebugHelper.debug(updatedExtra)
        selectedMethodId = ""
        selectedCard = CardModel()
        for (index, value) in  listCard.enumerated(){
            listCard[index].isChecked = false
            if value.id == updatedCard.id{
                isFormEnable = isChecked
                
                listCard[index].isChecked = !isChecked
                
                if(listCard[index].isChecked){
                    selectedMethodId = listCard[index].paymentId
                    selectedCard = listCard[index]
                    DebugHelper.debug(selectedMethodId)
                }
            }
        }
    }
    
    func getCard(_ cardId : String) -> Int{
        for (index, value) in  listCard.enumerated(){
            
            if value.paymentId == cardId{
                return index
            }
        }
        return -1
    }
    
    func removeCard() {
        
        apiService.removeCard(pm: removedMethodId, onSuccess: { [self] message in
            let index = getCard(removedMethodId)
            if index != -1 {
                
                if listCard[index].isChecked {
                    selectedMethodId = ""
                    isFormEnable = true
                }
                listCard.remove(at: index)
            }
            AppFunctions.showSnackBar(status: .success, message: message)
        }, onFailure: { message in
            AppFunctions.showSnackBar(status: .error, message: message)
        })
    }
     
}
