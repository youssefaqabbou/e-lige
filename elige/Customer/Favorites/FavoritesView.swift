//
//  FavoriteView.swift
//  elige
//
//  Created by user196417 on 11/8/21.
//

import SwiftUI

struct FavoritesView: View {
    @StateObject var vmFavorites = FavoritesViewModel()
    var barberDetailAction : (BarberShopModel) -> () = {barber in }
    
    let NC = NotificationCenter.default
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading){
                
                HStack(){
                    
                    Spacer()
                    
                    Text("Mes Favoris")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                }.padding(.top, AppConstants.viewSmallMargin)
                    .padding(.bottom, AppConstants.viewExtraMargin)
                
                VStack(spacing: 20){
                    CollectionLoadingView(loadingState: vmFavorites.loadingState) {
                        ListShimmeringView(count: 15){
                            FavoriteItemHolderView()
                        }
                    } content: {
                        GeometryReader { geometry in
                            ScrollView(.vertical, showsIndicators: false){
                                
                                LazyVStack(spacing: 20){
                                    ForEach(0..<vmFavorites.favoriteList.count, id: \.self){ index in
                                        let barberShop = vmFavorites.favoriteList[index]
                                    //ForEach(vmFavorites.favoriteList, id: \.id) { barberShop in
                                      
                                        FavoriteItemView(barberShop: barberShop, indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY , favoriteAction: {
                                            vmFavorites.deleteFavorite(barberShop: barberShop) {
                                                
                                            }
                                        }).onTapGesture {
                                            barberDetailAction(barberShop)
                                        }
                                        .onAppear {
                                            vmFavorites.loadMoreIfNeeded(currentItem: barberShop)
                                        }
                                    }
                                }.padding(.bottom, AppConstants.viewNormalMargin)
                                
                            }.padding(.top, AppConstants.viewNormalMargin)
                        }
                        
                        
                    } empty: {
                        VStack(spacing: 20){
                            Spacer()
                            CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_favoris_empty_title.localized, message: LocalizationKeys.list_favoris_empty_message.localized)
                            Spacer()
                        }
                        
                    } error: {
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_favoris_empty_title.localized, message: LocalizationKeys.list_favoris_empty_message.localized)
                    }
                    
                }
                //.padding(.bottom, AppConstants.viewNormalMargin)
                
            }.padding(.horizontal, AppConstants.viewExtraMargin)
            
        }.background(Color.bgViewColor)
            .edgesIgnoringSafeArea(.bottom)
            .onAppear {
                vmFavorites.checkLocationPermission()
                self.NC.addObserver(forName: NSNotification.shopUpdated, object: nil, queue: nil,
                                    using: self.barberShopUpdated)
            }
    }
    func barberShopUpdated(_ notification: Notification) {
        //vmFavorites.refreshList()
        let barberShop = notification.userInfo!["barberShop"] as! BarberShopModel
        
        vmFavorites.refreshBarberShopList(barberShop: barberShop)
    }
}

struct FavoriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView()
    }
}
