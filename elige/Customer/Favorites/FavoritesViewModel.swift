//
//  FavoritesViewModel.swift
//  elige
//
//  Created by macbook pro on 26/11/2021.
//

import Foundation
import MapKit
import CoreLocation

class FavoritesViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    private var apiService: APIService = APIService()
    
    private let locationManager = CLLocationManager()
    @Published var currentLocation = CLLocationCoordinate2D()
    
    @Published var favoriteList : [BarberShopModel] = []
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingPage = false
    
    private var deleted = 0
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
    }
    
    func getFavorites(){
        guard !isLoadingPage && canLoadMorePages else {
          return
        }
        
        isLoadingPage = true
        
        let params = ["lat" :  UserDefaults.latitude, "lng" :  UserDefaults.longitude, "page": String(currentPage), "size" : String(size), "deleted" : String(deleted)]
       
        apiService.getFavorites(params: params) { favorites in
            if favorites.count == 0 {
                self.canLoadMorePages = false
            }
            self.favoriteList.append(contentsOf: favorites)
            self.currentPage += 1
            self.isLoadingPage = false
            if(self.favoriteList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        } onFailure: { errorMessage in
            self.isLoadingPage = false
            self.canLoadMorePages = false
            if(self.favoriteList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }
    }

    func resetPagination(){
        isLoadingPage = false
        currentPage = 0
        canLoadMorePages = true
        self.loadingState = .loading
    }
    
    func loadMoreIfNeeded(currentItem item: BarberShopModel?) {
        guard let item = item else {
            getFavorites()
          return
        }

        let thresholdIndex = favoriteList.index(favoriteList.endIndex, offsetBy: AppConstants.offsetPagination)
        if favoriteList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            getFavorites()
        }
      }

    func deleteFavorite(barberShop: BarberShopModel, onError errorCallback: (() -> Void)?){
        var currenBarberShop = barberShop
        
        apiService.deleteFavorites(idBarber: barberShop.id, onSuccess: { message in
            let index = self.getBarberShopIndex(id: barberShop.id)
           
            if index != nil{
                self.deleted += 1
                self.favoriteList.remove(at: index!)
                currenBarberShop.isFavorite = false
                NotificationHelper.postBarberShop(barberShop: currenBarberShop)
                if(self.favoriteList.isEmpty){
                    self.loadingState = .empty
                }
            }
            //AppFunctions.showSnackBar(status: .success, message: message)
        }, onFailure: { errorMessage in
            errorCallback?()
            //AppFunctions.showSnackBar(status: .error , message: errorMessage)
        })
    }
    
    func getBarberShopIndex(id : Int) -> Int? {
        for (index, value) in favoriteList.enumerated(){
            if value.id == id {
            return index
           }
        }
        return nil
      }
    
    func refreshBarberShopList(barberShop : BarberShopModel){
        let index = getBarberShopIndex(id: barberShop.id)
        if index != nil {
            self.deleted += 1
            self.favoriteList.remove(at: index!)
            //NotificationHelper.postBarberShop(barberShop: barberShop)
        }
    }
    
    func checkLocationPermission(){
        if(!UserDefaults.latitude.isEmpty && !UserDefaults.longitude.isEmpty){
            getFavorites()
        }
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
               
                break
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.requestLocation()
                break
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
        }

    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        DebugHelper.debug("locationManagerDidChangeAuthorization")
        switch manager.authorizationStatus {
        case .denied:
            break
        case .notDetermined:
            break
        case .authorizedWhenInUse, .authorizedAlways :
            DebugHelper.debug("authorizedWhenInUse && authorizedAlways ")
            locationManager.requestLocation()
            break
        default:
            ()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DebugHelper.debug("didUpdateLocations")
        guard let location = locations.last else {
            return
        }
        currentLocation = location.coordinate
        if(UserDefaults.latitude.isEmpty && UserDefaults.longitude.isEmpty){
            UserDefaults.latitude = String(location.coordinate.latitude)
            UserDefaults.longitude = String(location.coordinate.longitude)
            getFavorites()
        }
        UserDefaults.latitude = String(location.coordinate.latitude)
        UserDefaults.longitude = String(location.coordinate.longitude)
    }
}
