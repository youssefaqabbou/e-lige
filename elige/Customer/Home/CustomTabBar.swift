//
//  TabBarView.swift
//  elige
//
//  Created by user196417 on 11/3/21.
//

import SwiftUI

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerHomeView()
    }
}

struct CustomTabBar: View {
    var tabs: [TabBarModel]
    @Binding var selectedTab: Int
    @State var centerX: CGFloat = 0
    @State var menuRect: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    @Environment(\.verticalSizeClass) var size
    var body: some View {

        VStack(alignment: .leading) {
            HStack(spacing: 0) {

                ForEach(tabs, id: \.id) { tab in

                    GeometryReader { reader in

                        TabBarButton(tab: tab, selectedTab: $selectedTab, centerX: $centerX, rect: reader.frame(in: .global))
                            .onAppear(perform: {
                            if tab.id == tabs.first?.id {
                                centerX = reader.frame(in: .global).midX
                            }
                        })
                        .onChange(of: size) { (_) in
                            if selectedTab == tab.id {
                                centerX = reader.frame(in: .global).midX
                            }
                        }
                    }
                        .frame(width:  UIScreen.main.bounds.width / 4 , height: 25)
                        .padding(.bottom, AppConstants.viewSmallMargin)
                        .padding(.top, AppConstants.viewSmallMargin)
                    if tab.id != tabs.last?.id { Spacer(minLength: 0) }
                }
            }
                //.padding(.horizontal, 25)

            Capsule().fill(Color.primaryColor).frame(width: 10, height: 3, alignment: .leading)
                .offset(x: centerX - 5, y: -10)

        }
        .padding(.bottom, UIDevice.current.hasNotch ? AppConstants.viewSmallMargin : 0)
        .background(Color.bgViewColor.shadow(color: Color.tabBarShadowColor, radius: 2, x: 0, y: -3))
            
        //.ignoresSafeArea(.all, edges: .horizontal)
    }
}

struct TabBarModel {
    var id: Int
    var label: String
    var icon: Image
    var iconSelected: Image
}

enum TabItems: Int {
    case landing = 0
    case nearby = 1
    case orders = 2
    case profile = 3
}

struct TabBarButton: View {

    //@Binding var selected: String
    var tab: TabBarModel
    @Binding var selectedTab: Int
    //var value: String
    @Binding var centerX: CGFloat
    var rect: CGRect

    var body: some View {

        Button(action: {

            if tab.id == TabItems.orders.rawValue || tab.id == TabItems.profile.rawValue{
                    if !AppFunctions.checkIfUserConnected(){
                        //UserDefaults.showLogin = true
                        UserDefaults.showLoginDialog = true
                        return
                    }
            }
            
            withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 0.5, blendDuration: 0.3)) {
                centerX = rect.midX
                //DebugHelper.debug("centerX", centerX)
            }
            withAnimation(.linear(duration: 0.3)) {
                selectedTab = tab.id
            }
            
        }, label: {

                ZStack(alignment: .center) {

                    Text(tab.label)
                        .font(.system(weight: .regular, size: 13))
                        .foregroundColor(Color.primaryTextColor)
                        .offset(y: selectedTab == tab.id ? 0 : -30)
                        .opacity(selectedTab == tab.id ? 1 : 0)

                    tab.icon
                        .resizable()
                        .renderingMode(.template)
                        .frame(width: 30, height: 30)
                        .foregroundColor(Color.secondaryTextColor)
                        .opacity(selectedTab == tab.id ? 0 : 1)

                }.frame(width: UIScreen.main.bounds.width / 4 , height: 30)
            })
    }
}
