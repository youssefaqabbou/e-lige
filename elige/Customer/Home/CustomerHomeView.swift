//
//  HomeView.swift
//  elige
//
//  Created by user196417 on 11/3/21.
//

import SwiftUI

struct CustomerHomeView: View {
    
    @StateObject var homeVM = CustomerHomeViewModel()
    
    @State var selectedTab = 0
    @State var showSearch = false
    @State var showDetailBarber = false
    @State var showSetting = false
    @State var showDetailOrder = false
    @State var showNotifications = false
    
    var tabs = [TabBarModel(id: TabItems.landing.rawValue, label: "Accueil", icon: Image.iconHomeTabBar, iconSelected: Image.iconHomeTabBar),
                TabBarModel(id: TabItems.nearby.rawValue, label: "Carte", icon: Image.iconMapTabBar, iconSelected: Image.iconMapTabBar),
                TabBarModel(id: TabItems.orders.rawValue, label: "Réservations", icon: Image.iconOrderTabBar, iconSelected: Image.iconMapTabBar),
                TabBarModel(id: TabItems.profile.rawValue, label: "Favoris", icon: Image.iconFavorisTabBar , iconSelected: Image.iconFavorisTabBar)]
    
    
    func containedView() -> AnyView {
        switch selectedTab {
        case TabItems.landing.rawValue: return AnyView(LandingView(profileAction: {
            withAnimation {
                showSetting = true
            }
            
        }, searchAction: {
            homeVM.selectedCategory = TagModel()
            withAnimation {
                showSearch = true
            }
        },barberDetailAction: { barberShop in
            homeVM.selectedBarberShop = barberShop
            withAnimation {
                showDetailBarber = true
            }
        }, categoryAction : {  category in
            homeVM.selectedCategory = category
            withAnimation {
                showSearch = true
            }
        }, notificationAction: {
            withAnimation {
                showNotifications = true
            }
        }))
        case TabItems.nearby.rawValue: return AnyView(NearbyBarberView( barberDetailAction: { barberShop in
            homeVM.selectedBarberShop = barberShop
            withAnimation {
                showDetailBarber = true
            }
        }))
        case TabItems.orders.rawValue: return AnyView(ReservationView(detailAction: { order in
            homeVM.selectedOrder = order
            withAnimation {
                showDetailOrder = true
            }
        }))
        case TabItems.profile.rawValue: return AnyView(FavoritesView(barberDetailAction: { barberShop in
            homeVM.selectedBarberShop = barberShop
            withAnimation {
                showDetailBarber = true
            }
        }))
        default:
            return AnyView(Color.red)
        }
    }
    
    var body: some View {
        
        ZStack(alignment: .bottom, content: {
            
            Color.primaryTextLightColor.ignoresSafeArea()
            
            if(homeVM.permissionDenied){
                LocalisationPermissionView( onGpsSettingAction: {
                    homeVM.requestAuthorisation()
                }, onExitAction: {
                    exit(0)
                }).transition(.move(edge: .bottom)).animation(.linear, value: homeVM.permissionDenied).zIndex(6)
            }else{
                VStack(spacing: 0){
                    containedView()
                    CustomTabBar(tabs: tabs, selectedTab: $selectedTab)
                }
                .edgesIgnoringSafeArea(.bottom)
                    
            }
            
            
            if(showSetting){
                SettingView(showSetting: $showSetting).transition(.move(edge: .trailing)).animation(.linear, value: showSetting).zIndex(1)
            }
            if(showSearch){
                SearchView(showSearch: $showSearch, selectedCategory : homeVM.selectedCategory).transition(.move(edge: .trailing)).animation(.linear, value: showSearch).zIndex(2)
            }
            if(showDetailBarber){
                DetailBarberView(showDetail: $showDetailBarber, barberShop: homeVM.selectedBarberShop).transition(.move(edge: .trailing)).animation(.linear, value: showSearch).zIndex(3)
            }
           
            if(showDetailOrder){
                DetailReservationView(showDetailOrder: $showDetailOrder, order: homeVM.selectedOrder).transition(.move(edge: .trailing)).animation(.linear, value: showSearch).zIndex(4)
            }
            
            if(showNotifications){
                NotificationView(showNotifications: $showNotifications).transition(.move(edge: .trailing)).animation(.linear, value: showSearch).zIndex(5)
            }
            
        })
            .onAppear {
                homeVM.checkLocationPermission()
            }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerHomeView()
    }
}
