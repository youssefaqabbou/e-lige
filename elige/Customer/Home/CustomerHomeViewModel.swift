//
//  HomeViewModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation
import CoreLocation
import UIKit
import SwiftUI

class CustomerHomeViewModel: NSObject, ObservableObject , CLLocationManagerDelegate{
    private var apiService: APIService = APIService()
    
    @Published var selectedBarberShop = BarberShopModel()
    @Published var selectedOrder = OrderModel()
    @Published var selectedCategory = TagModel()
    
    private let locationManager = CLLocationManager()
    @Published var currentLocation = CLLocationCoordinate2D()
    @Published var permissionDenied = false
    @Published var locationDisabled = false
    @Published var isLocationFound = false
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
        AppFunctions.sendTokenToServer()
        getSettings()
    }
    
    func checkLocationPermission(){
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
                withAnimation {
                    permissionDenied = true
                }
                   
                DebugHelper.debug("No access")
                case .authorizedAlways, .authorizedWhenInUse:
                    withAnimation {
                        permissionDenied = false
                    }
                    DebugHelper.debug("Access")
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
            withAnimation {
                permissionDenied = true
            }
        }
    }
    
    public func requestAuthorisation(always: Bool = false) {
        DebugHelper.debug("requestAuthorisation")
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .denied:
                    DebugHelper.debug("denied")
                    self.openSettingPhone()
                break
                case .notDetermined, .restricted:
                    
                    if always {
                        self.locationManager.requestAlwaysAuthorization()
                    } else {
                        self.locationManager.requestWhenInUseAuthorization()
                    }
                break
                case .authorizedAlways, .authorizedWhenInUse:
                    withAnimation {
                        permissionDenied = false
                    }
                   
                break
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
            self.openSettingPhone()
        }
        
        
    }
    
    func openSettingPhone(){
        DebugHelper.debug("openSettingPhone")
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        DebugHelper.debug("locationManagerDidChangeAuthorization")
        
        switch manager.authorizationStatus {
        case .denied:
            DebugHelper.debug("denied")
            withAnimation {
                permissionDenied = true
            }
            break
        case .notDetermined:
            DebugHelper.debug("notDetermined")
            withAnimation {
                permissionDenied = true
            }
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse, .authorizedAlways :
            DebugHelper.debug("authorizedWhenInUse && authorizedAlways ")
            locationManager.requestLocation()
            break
        default:
            ()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DebugHelper.debug("didUpdateLocations")
        guard let location = locations.last else {
            return
        }
        currentLocation = location.coordinate
        UserDefaults.latitude = String(location.coordinate.latitude)
        UserDefaults.longitude = String(location.coordinate.longitude)
        
        DebugHelper.debug(currentLocation)
        isLocationFound = true
        permissionDenied = false

    }
    
    func getSettings(){
        apiService.getSettings { success in
        } onFailure: { errorMessage in
        }
    }
}
