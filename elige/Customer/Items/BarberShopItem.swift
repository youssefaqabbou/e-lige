//
//  BarberShopItem.swift
//  elige
//
//  Created by user196417 on 11/4/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct BarberShopItem: View {
    
    @Binding var barberShop : BarberShopModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    var body: some View {
        CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            HStack(spacing: 16){
                
                ZStack(alignment: .topTrailing){
                    WebImage(url: URL(string: barberShop.image.url))
                        .placeholder {
                            Image.placeholderShop.resizable()
                        }
                        .resizable() //
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 70, height: 70)
                        .cornerRadius(12)
                    
                    if(barberShop.isFavorite){
                        ZStack(alignment: .topTrailing){
                            AppUtils.Triangle()
                                .fill(Color.primaryColor)
                                .frame(width: 35, height: 35)
                            
                            Image.iconHeartFav
                                .resizable()
                                .renderingMode(.template)
                                .frame(width: 10, height: 10)
                                .colorMultiply(Color.white)
                                .foregroundColor(Color.white)
                                .padding(6)
                            
                        }.mask(AppUtils.CustomCorners(corners: [.topRight], radius: 12))
                    }
                    
                    
                } .frame(width: 70, height: 70)
                
                
                VStack(alignment: .leading){
                    
                    Text(barberShop.name)
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                    Spacer()
                    
                    HStack{
                        let currentSchedule = AppFunctions.getCurrentScheudle(schedules: barberShop.schedules)
                        VStack(alignment: .leading, spacing: 10){
                            if(currentSchedule.isOpen){
                                Text("OUVERT")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.primaryColor)
                                    .lineLimit(1)
                            }else{
                                Text("FERMÉ")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.red)
                                    .lineLimit(1)
                            }
                           
                            //Spacer()
                            
                            HStack(spacing: 8){
                                
                                Image.iconStarColor
                                    .resizable()
                                    .frame(width: 12, height: 12)
                                    
                                Text(barberShop.rating.toRating())
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.primaryColor)
                                    .lineLimit(1)
                            }
                        }
                        
                        Spacer(minLength: 5)
                        
                        VStack(alignment: .leading, spacing: 10){
                            
                            if(currentSchedule.isOpen){
                                Image.iconTime
                                    .resizable()
                                    .frame(width: 12, height: 12)
                            }else{
                               Spacer()
                            }
                            
                            Image.iconLocation
                                .resizable()
                                .frame(width: 12, height: 12)
                        }
                        
                        VStack(alignment: .leading, spacing: 10){
                           
                            if(currentSchedule.isOpen){
                                Text("\(currentSchedule.openTime.timeToTimeZone()) - \(currentSchedule.closeTime.timeToTimeZone())")
                                    .font(.system(weight: .regular, size: 11))
                                    .foregroundColor(Color.secondaryTextColor)
                                    .lineLimit(1)
                            }else{
                               Spacer()
                            }
                            
                            Text("\(String(format: "%.1f", barberShop.distance)) km")
                                .font(.system(weight: .regular, size: 11))
                                .foregroundColor(Color.secondaryTextColor)
                                .lineLimit(1)
                            
                        }.padding(.trailing, AppConstants.viewSmallMargin)
                    }
                }
            }
        }
    }
}

struct BarberShopItem_Previews: PreviewProvider {
    static var previews: some View {
        BarberShopItem(barberShop: .constant(FakeData.getBarberShopData(id: 2)))
    }
}



