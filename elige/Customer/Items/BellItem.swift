//
//  BellItem.swift
//  elige
//
//  Created by user196417 on 11/4/21.
//

import SwiftUI

struct BellItem: View {
    
    var body: some View {
        
        ZStack{
            
            HStack{
                
                Image.iconBell
                    .resizable()
                    .frame(width: 20, height: 20, alignment: .center)
                
               
                
            }.frame(width: 50, height: 50, alignment: .center)
                .background(Color.secondaryColor)
                .clipShape(Circle())
            /*
            Capsule().fill(Color.red)
                .clipShape(Circle())
                .frame(width: 6, height: 6)
                .zIndex(2)
                .offset(x: 8, y: -8)
           */
        }
    }
}

struct BellItem_Previews: PreviewProvider {
    static var previews: some View {
        BellItem()
    }
}
