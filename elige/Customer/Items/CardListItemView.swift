//
//  CardListItemView.swift
//  Click&Eat
//
//  Created by user196417 on 9/14/21.
//

import SwiftUI

struct CardListItemView: View {
    
    var card : CardModel
    var checkAction : (_ value: Bool) -> () = {_ in }
    var removeAction : (_ card: CardModel) -> () = {_ in }
    
    var body: some View {
            
            VStack(alignment: .leading){
                
                HStack{
                    
                    Text(card.name)
                        .font(.system(weight: .regular, size: 22))
                        .foregroundColor(.primaryTextLightColor)
                        
                    
                    Spacer()
                    
                    if card.isChecked{
                        
                        Image(systemName: "checkmark.circle.fill")
                            .resizable()
                            .clipShape(Circle())
                            .foregroundColor(.primaryTextLightColor)
                            .frame(width: 25, height: 25)
                        
                    }else{
                       
                        Circle()
                            .strokeBorder(Color.primaryTextLightColor,lineWidth: 1)
                            .frame(width: 25, height: 25)
                        
                    }
                    
                }.padding(.bottom)
                
                VStack(alignment: .leading, spacing: -5){
                    
                    HStack{
                        
                        Text("**** **** ****")
                            .font(.system(weight: .bold, size: 25))
                            .foregroundColor(.primaryTextLightColor)
                        
                        Text(card.last4)
                            .font(.system(weight: .bold, size: 20))
                            .foregroundColor(.primaryTextLightColor)
                            .padding(.bottom, AppConstants.viewSmallMargin)
                        
                    }
                    
                    HStack(spacing: 0){
                        
                        Text( String(format: "%02d", card.dateMonth))
                            .font(.system(weight: .regular, size: 14))
                            .foregroundColor(.primaryTextLightColor)
                        Text("/")
                            .font(.system(weight: .regular, size: 14))
                            .foregroundColor(.primaryTextLightColor)
                        Text(String(card.dateYear % 100))
                            .font(.system(weight: .regular, size: 14))
                            .foregroundColor(.primaryTextLightColor)
                        
                        Spacer()
                        
                        Text("***")
                            .font(.system(weight: .bold, size: 22))
                            .foregroundColor(.primaryTextLightColor)
                        
                    }
                    
                }
                
                HStack(alignment: .bottom){
                    
                    Image(AppFunctions.getCardImage(type: card.cartType))
                        .resizable()
                        .frame(width: 50, height: 30, alignment: .center)
                        .scaledToFit()
                    
                    Spacer()
                    
                    Text("Supprimer")
                        .font(.system(weight: .bold, size: 12))
                        .foregroundColor(.primaryTextLightColor)
                        .padding(AppConstants.viewVerySmallMargin)
                        .background(Color.primaryButtonColor)
                        .cornerRadius(10)
                        .onTapGesture {
                            removeAction(card)
                        }
                }
                
            }.padding(AppConstants.viewSmallMargin)
            .padding(.top, 5)
            .padding(.horizontal, AppConstants.viewSmallMargin)
            .frame(width: UIScreen.main.bounds.width / 1.3, height: 180, alignment: .topLeading)
            .background(AppFunctions.getCardColor(type: card.cartType))
            .cornerRadius(15)
            .onTapGesture {
                checkAction(card.isChecked)
            }
        
    }
}

struct CardListItemView_Previews: PreviewProvider {
    static var previews: some View {
        CardListItemView(card: CardModel())
    }
}
