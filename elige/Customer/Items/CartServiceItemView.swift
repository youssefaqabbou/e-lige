//
//  ShopServiceItemView.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import SwiftUI

struct CartServiceItemView: View {
    @State var service : ServiceModel
    @State var isToggleOn = false
    
    var serviceAction : (ServiceModel) -> () = {service in }
    
    var body: some View {
        HStack(spacing: 0){
           
            VStack(alignment: .leading, spacing: 5){
                
                HStack(){
                    Text(service.name)
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                    Spacer(minLength: 10)
                    
                    Text(service.price.toPrice())
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.primaryTextColor)
                }
                HStack(){
                    ServiceLocationsItem(serviceLocation: .constant(service.serviceLocation))
                                       
                    Spacer(minLength: 4)
                    
                    Text(service.duration.toHourMinute())
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                }
                
                
                Text(service.description)
                    .font(.system(weight: .regular, size: 13))
                    .foregroundColor(Color.thirdTextColor)
                    .lineLimit(1)
            }
            
            Spacer(minLength: 10)
            
            VStack(alignment: .trailing , spacing: 0){
                Toggle(isOn: $isToggleOn, label: {
                   EmptyView()
                }).toggleStyle(CustomToggleStyle(toggleAction: {
                    service.isEnabled = isToggleOn
                    serviceAction(service)
                }))
                
                Spacer(minLength: 20)
                /*
                HStack(spacing: 15){
                    Circle().fill(service.quantity > 1 ? Color.primaryTextColor : Color.primaryTextColor.opacity(0.5))
                        .frame(width: 22, height: 22, alignment: .center)
                        .overlay(
                            Image(systemName: "minus")
                                .font(.system(weight: .bold, size: 12))
                                .foregroundColor(Color.primaryTextLightColor)
                        )
                        .onTapGesture {
                            if(service.quantity > 1){
                                service.quantity -= 1
                                serviceAction(service)
                            }
                        }
                    
                    Text("\(service.quantity)").foregroundColor(Color.primaryTextColor)
                        .font(.system(weight: .regular, size: 16))
                        .frame(minWidth: 20)
                    
                    
                    Circle().fill(Color.primaryTextColor)
                        .frame(width: 22, height: 22, alignment: .center)
                        .overlay(
                            Image(systemName: "plus")
                                .font(.system(weight: .bold, size: 12))
                                .foregroundColor(Color.primaryTextLightColor)
                        )
                        .onTapGesture {
                            service.quantity += 1
                            serviceAction(service)
                        }
                }.opacity(isToggleOn ? 0 : 0)
                
                */
            }
           
        }.onAppear(){
            isToggleOn = service.isEnabled
        }
    }
}

struct ShopServiceItemView_Previews: PreviewProvider {
    static var previews: some View {
        CartServiceItemView(service: ServiceModel())
    }
}
