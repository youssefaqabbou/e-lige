//
//  HairStyleItem.swift
//  elige
//
//  Created by user196417 on 11/4/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct CategoryItemView: View {
    @State var category : TagModel
    @State var scale: CGFloat = 1.0
    var onTapAction : () -> () = {}
    var body: some View {
        
        VStack{
            
            VStack(alignment: .center){
                
                WebImage(url: URL(string: category.icon))
                    
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 60, height: 60)
               
            }.frame(width: 65, height: 65, alignment: .center)
                .background(Color.secondaryColor)
                .clipShape(Circle())
                .scaleEffect(scale)
            /*Text(category.name)
                .font(.system(weight: .regular, size: 14))
                .foregroundColor(Color.primaryTextColor)
            */
            
            MarqueeText(
                 text: category.name,
                 font: UIFont.systemFont(ofSize: 13, weight: .regular),
                 leftFade: 10,
                 rightFade: 2,
                 startDelay: 2
                 ).foregroundColor(.primaryTextColor)
                  
           /* Marquee {
                Text(category.name)
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(.primaryColoredTextColor)
                    
            }.frame(width: 85, height: 20, alignment: .center)
            .marqueeDuration(4)
            .marqueeWhenNotFit(true)
            .marqueeIdleAlignment(.center)*/
            
        }
        .onTapGesture(perform: {
            withAnimation(Animation.easeInOut(duration: 0.2).repeatCount(1)) {
                scale = 1.5
            }
            scale = 1.0
            onTapAction()
        })
    }
}

struct CategoryItemView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryItemView(category: TagModel())
    }
}
