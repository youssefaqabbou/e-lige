//
//  FavoriteItem.swift
//  elige
//
//  Created by user196417 on 11/8/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct FavoriteItemView: View {
    
    @State var barberShop : BarberShopModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    var favoriteAction : () -> () = { }
    var body: some View {
        CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            HStack(spacing: 0){
                
                WebImage(url: URL(string: barberShop.image.url))
                    .placeholder {
                        Image.placeholderShop.resizable()
                    }
                    .resizable() //
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 76, height: 83)
                    .cornerRadius(12)
                    .padding(.trailing, 18)
               
                VStack(alignment: .leading, spacing: 0){
                    
                    Text(barberShop.name)
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                   
                    Spacer(minLength: AppConstants.viewTinyMargin)
                    
                    Text(barberShop.address)
                        .font(.system(weight: .regular, size: 12))
                        .foregroundColor(Color.secondaryTextColor)
                        .lineLimit(2)
                        
                    Spacer(minLength: AppConstants.viewTinyMargin)
                  
                    HStack(spacing: 0){
                        HStack(spacing: AppConstants.viewVerySmallMargin){
                            Image.iconStarColor
                                .resizable()
                                .frame(width: 12, height: 12)
                                
                            Text(barberShop.rating.toRating())
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.primaryColor)
                                .lineLimit(1)
                        }
                        
                        Spacer(minLength: AppConstants.viewNormalMargin)
                        
                        HStack(spacing: AppConstants.viewVerySmallMargin){
                            Image.iconLocation
                                .resizable()
                                .frame(width: 15, height: 15)
                            
                            Text("\(String(format: "%.1f", barberShop.distance)) Km")
                                .font(.system(weight: .regular, size: 11))
                                .foregroundColor(Color.secondaryTextColor)
                                .lineLimit(1)
                        }
                    }
                }.padding(.trailing, AppConstants.viewExtraMargin)
                
                VStack{
                    
                    Circle().fill(Color.bgBtnFavorite)
                        .frame(width: 40, height: 40, alignment: .center)
                        .overlay(
                            Image.iconFavoriteRed
                                .resizable()
                                .frame(width: 16, height: 16, alignment: .center)
                        )
                        .onTapGesture {
                            favoriteAction()
                        }
                    
                }//.padding(.trailing, 12)
                
            }.frame(height: 83)
        }
        
    }
}

struct FavoriteItem_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteItemView(barberShop: BarberShopModel())
    }
}
