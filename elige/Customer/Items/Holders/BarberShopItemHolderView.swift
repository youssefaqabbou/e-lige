//
//  BarberShopItemHolderView.swift
//  elige
//
//  Created by user196417 on 1/5/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct BarberShopItemHolderView: View {
    
    var body: some View {
        
        HStack(spacing: 16){
            
            ZStack(alignment: .topTrailing){
                WebImage(url: URL(string: ""))
                    .placeholder {
                        Image.placeholderShop.resizable()
                    }
                    .resizable() //
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .cornerRadius(12)
                
//                ZStack(alignment: .topTrailing){
//                    AppUtils.Triangle()
//                        .fill(Color.primaryColor)
//                        .frame(width: 35, height: 35)
//
//                    Image.iconHeartFav
//                        .resizable()
//                        .renderingMode(.template)
//                        .frame(width: 10, height: 10)
//                        .colorMultiply(Color.white)
//                        .foregroundColor(Color.white)
//                        .padding(6)
//
//                }.mask(AppUtils.CustomCorners(corners: [.topRight], radius: 12))
                
            } .frame(width: 70, height: 70)
            
            VStack(alignment: .leading){
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 20))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
                
                Spacer()
                
                HStack{
                   
                    VStack(alignment: .leading, spacing: 10){
                        
                        Text(AppFunctions.randomString(length: 20))
                            .font(.system(weight: .regular, size: 12))
                            .foregroundColor(Color.primaryTextColor)
                        
                        HStack(spacing: 8){
                            
                            Image.iconStarColor
                                .resizable()
                                .frame(width: 12, height: 12)
                            
                            Text(AppFunctions.randomString(length: 5))
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.primaryTextColor)
                        }
                    }
                    
                    Spacer(minLength: 5)
                    
                    VStack(alignment: .leading, spacing: 10){
                        
                        Image.iconTime
                            .resizable()
                            .frame(width: 12, height: 12)
                    }
                    
                    VStack(alignment: .leading, spacing: 10){
                        
                        Text(AppFunctions.randomString(length: 15))
                            .font(.system(weight: .regular, size: 11))
                            .foregroundColor(Color.secondaryTextColor)
                        
                    }.padding(.trailing, AppConstants.viewSmallMargin)
                }
            }
        }.background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
