//
//  CardItemHolderView.swift
//  Click&Eat
//
//  Created by user196417 on 9/7/21.
//

import SwiftUI

struct CardItemHolderView: View {
    
    
    var body: some View {
        
            
            HStack(spacing: 0){
                
                Circle()
                    .strokeBorder(Color.gray,lineWidth: 1)
                    .frame(width: 25, height: 25)
                
                Spacer(minLength: 5)
                
                Image("ic_visa")
                    .resizable()
                    .frame(width: 50, height: 30, alignment: .center)
                    .scaledToFit()
                
                Spacer(minLength: 5)
                
                VStack(alignment: .leading, spacing: 0){
                    
                    Text(AppFunctions.randomString(length: 12))
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(.primaryTextColor)
                        .padding(.bottom, AppConstants.viewSmallMargin)
                    
                    Text("●●●● ●●●● ●●●● ")
                        .font(.system(weight: .bold, size: 16))
                        .foregroundColor(.primaryTextColor)
                    +
                    Text(AppFunctions.randomString(length: 4))
                        .font(.system(weight: .bold, size: 16))
                        .foregroundColor(.primaryTextColor)
                                
                }
                
                Spacer(minLength: 5)
                
                VStack(alignment: .trailing){
                    
                    Text("Exp")
                        .font(.system(weight: .medium, size: 16))
                        .foregroundColor(.secondaryTextColor)
                        .padding(.bottom, 2)
                    
                    HStack(spacing: 0){
                        Text( AppFunctions.randomString(length: 2))
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(.secondaryTextColor)
                        Text("/")
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(.secondaryTextColor)
                        Text(AppFunctions.randomString(length: 2))
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(.secondaryTextColor)
                    }
                }
            }.padding(AppConstants.viewNormalMargin)
            .background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
            
    }
}

