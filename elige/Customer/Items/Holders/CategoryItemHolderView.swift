//
//  CategoryItemHolderView.swift
//  elige
//
//  Created by user196417 on 1/6/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct CategoryItemHolderView: View {
   
    var body: some View {
        
        VStack{
            
            VStack(alignment: .center){
                
                WebImage(url: URL(string: ""))
                    
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 65, height: 65)
               
            }.frame(width: 65, height: 65, alignment: .center)
                .background(Color.lightGray)
                .clipShape(Circle())
                
            Text(AppFunctions.randomString(length: 10))
                .foregroundColor(.primaryTextColor)
          
        }.background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
