//
//  FavoriteItemHolderView.swift
//  elige
//
//  Created by user196417 on 1/5/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct FavoriteItemHolderView: View {
    
    var body: some View {
        
        HStack(spacing: 0){
            
            WebImage(url: URL(string: ""))
                .placeholder {
                    Image.placeholderShop.resizable()
                }
                .resizable() //
                .aspectRatio(contentMode: .fill)
                .frame(width: 76, height: 83)
                .cornerRadius(12)
                .padding(.trailing, 18)
           
            VStack(alignment: .leading, spacing: 0){
                
                Text(AppFunctions.randomString(length: 12))
                    .font(.system(weight: .regular, size: 20))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
               
                Spacer(minLength: AppConstants.viewTinyMargin)
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 10))
                    .foregroundColor(Color.secondaryTextColor)
                    .lineLimit(2)
                    
                Spacer(minLength: AppConstants.viewTinyMargin)
              
                HStack(spacing: 0){
                    HStack(spacing: AppConstants.viewVerySmallMargin){
                        Image.iconStarColor
                            .resizable()
                            .frame(width: 12, height: 12)
                            
                        Text(AppFunctions.randomString(length: 4))
                            .font(.system(weight: .regular, size: 12))
                            .foregroundColor(Color.primaryTextColor)
                    }
                    
                    Spacer(minLength: AppConstants.viewNormalMargin)
                    
                    HStack(spacing: AppConstants.viewVerySmallMargin){
                        Image.iconLocation
                            .resizable()
                            .frame(width: 15, height: 15)
                        
                        Text(AppFunctions.randomString(length: 3))
                            .font(.system(weight: .regular, size: 10))
                            .foregroundColor(Color.secondaryTextColor)
                    }
                }
            }.padding(.trailing, AppConstants.viewExtraMargin)
            
            VStack{
                
                Circle().fill(Color.bgBtnFavorite)
                    .frame(width: 40, height: 40, alignment: .center)
                    .overlay(
                        Image.iconFavoriteRed
                            .resizable()
                            .frame(width: 16, height: 16, alignment: .center)
                    )
            }//.padding(.trailing, 12)
        }.frame(height: 83)
            .background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
