//
//  ListServiceItemHolderView.swift
//  elige
//
//  Created by user196417 on 1/5/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct ListServiceItemHolderView: View {
    
    var body: some View {
        
        VStack(alignment: .leading){
            
            HStack(alignment: .top){
                
                VStack(alignment: .leading, spacing: 5){
                    
                    Text(AppFunctions.randomString(length: 20))
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(2)
                  
                    Text(AppFunctions.randomString(length: 5))
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                }
                Spacer(minLength: 10)
                
                VStack{
                    
                    Text(AppFunctions.randomString(length: 5))
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                         
                    Text(AppFunctions.randomString(length: 3))
                        .font(.system(weight: .regular, size: 24))
                        .foregroundColor(Color.primaryTextColor)
                    
                }.frame(width: 60, height: 60, alignment: .center)
                .background(Color.lightGray)
                .cornerRadius(10)
                
            }
            
            Text(AppFunctions.randomString(length: 30))
                .font(.system(weight: .regular, size: 14))
                .foregroundColor(Color.thirdTextColor)
            
        }.background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
