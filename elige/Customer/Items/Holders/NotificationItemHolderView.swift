//
//  NotificationItemHolderView.swift
//  elige
//
//  Created by macbook pro on 13/1/2022.
//

import Foundation
import SwiftUI

struct NotificationItemHolderView: View {
    
    var body: some View {
       
        HStack(spacing: 20){
            
            Circle().fill(Color.lightGray)
                .frame(width: 40, height: 40, alignment: .center)
                
                
            VStack(alignment: .leading, spacing: 0){
                
                Text(AppFunctions.randomString(length: 25))
                    .font(.system(weight: .regular, size: 18))
                    .foregroundColor(Color.primaryTextColor)
                    .padding(.bottom, AppConstants.viewVerySmallMargin)
                
                Text(AppFunctions.randomString(length: 50))
                    .font(.system(weight: .regular, size: 15))
                    .foregroundColor(Color.thirdTextColor)
                    .padding(.bottom, AppConstants.viewTinyMargin)
                HStack(spacing: 5){
                    
                    Spacer()
                    
                    Text(AppFunctions.randomString(length: 20))
                        .font(.system(weight: .regular, size: 13))
                        .foregroundColor(Color.primaryTextColor)
                    
                }
            }
        }//.redacted(reason: .placeholder).shimmer()
    }
}
