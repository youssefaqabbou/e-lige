//
//  ReservationItemHolderView.swift
//  elige
//
//  Created by user196417 on 1/5/22.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

struct ReservationItemHolderView: View {
    
    var body: some View {
        
        HStack(spacing: 0){
            
            ZStack(alignment: .bottom){
                
                WebImage(url: URL(string: ""))
                    .placeholder {
                        Image.placeholderShop.resizable()
                    }
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 76, height: 83)
                    .cornerRadius(10)
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.primaryTextLightColor)
                    .frame(width: 93, height: 21, alignment: .center)
                    .background(Color.lightGray)
                    .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                    .zIndex(2)
            }.padding(.trailing, AppConstants.viewNormalMargin)
            
            VStack(alignment: .leading, spacing: AppConstants.viewTinyMargin){
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 20))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
                
                Text(AppFunctions.randomString(length: 15))
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.secondaryTextColor)
                    .lineLimit(1)
                
                Spacer(minLength: AppConstants.viewTinyMargin)
                
                Text(AppFunctions.randomString(length: 12))
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.secondaryTextColor)
                    .lineLimit(1)
                
                Spacer(minLength: AppConstants.viewTinyMargin)
                
                HStack(spacing: AppConstants.viewTinyMargin ){
                    Text(AppFunctions.randomString(length: 10))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    Text(AppFunctions.randomString(length: 2))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    Text(AppFunctions.randomString(length: 15))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Spacer()
                }
                
            }.padding(.trailing, AppConstants.viewNormalMargin)
            
            Spacer(minLength: 10)
            
            VStack{
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 32))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(AppFunctions.randomString(length: 20))
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
            }.frame(width: 83, height: 83, alignment: .center)
            .background(Color.lightGray)
            .cornerRadius(10)
                
        }.frame(height: 83)
            .background(Color.white)
            //.redacted(reason: .placeholder).shimmer()
    }
}
