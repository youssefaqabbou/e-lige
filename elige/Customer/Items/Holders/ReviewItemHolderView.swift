//
//  ReviewItemHolderView.swift
//  elige
//
//  Created by macbook pro on 13/1/2022.
//

import Foundation
import SwiftUI

struct ReviewItemHolderView: View {
    
    var body: some View {
        
        VStack(alignment: .leading){
            
            HStack{
                Circle().fill(Color.lightGray)
                    .frame(width: 40, height: 40, alignment: .center)
                
                Text(AppFunctions.randomString(length: 15))
                    .font(.system(weight: .bold, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
                Spacer()
                
                VStack(alignment: .trailing ,spacing: 10){
                    
                    Text(AppFunctions.randomString(length: 10))
                        .font(.system(weight: .light, size: 12))
                        .foregroundColor(Color.primaryTextColor)
                    
                    Text(AppFunctions.randomString(length: 10))
                        .font(.system(weight: .light, size: 12))
                        .foregroundColor(Color.primaryTextColor)
                }
            }
            
            Text(AppFunctions.randomString(length: 15))
                .font(.system(weight: .light, size: 14))
                .foregroundColor(Color.primaryTextColor)
                
        }.background(Color.white)
            
    }
}
