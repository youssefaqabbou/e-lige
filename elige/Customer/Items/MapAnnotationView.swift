//
//  MapAnnotationView.swift
//  elige
//
//  Created by macbook pro on 4/11/2021.
//

import SwiftUI
import SDWebImageSwiftUI
import Combine
import SDWebImage

struct MapAnnotationView: View {
    @State var barber : BarberShopModel
    @State var barberShopName = ""
    var onImageLoaded: (Bool) -> () = { _ in }

    // Async function
    
    
    @available(iOS 15.0.0, *)
    func asyncLoadImage(url: String) async throws {
        SDImageCache.shared.diskImageExists(withKey: url) { exists in
            if(!exists){
                SDWebImageManager.shared.loadImage(
                        with: URL(string: url),
                        options: .highPriority,
                        progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                            onImageLoaded(true)
                      }
            }else{
                onImageLoaded(false)
            }
        }
    }
    
    func loadImage(url: String) {
        SDImageCache.shared.diskImageExists(withKey: url) { exists in
            if(!exists){
                SDWebImageManager.shared.loadImage(
                        with: URL(string: url),
                        options: .highPriority,
                        progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                            onImageLoaded(true)
                      }
            }else{
                onImageLoaded(false)
            }
        }
    }
    
    
    
    var body: some View {
        VStack(){
            HStack(){
                WebImage(url: URL(string: barber.image.url))// Animation Control, supports dynamic changes
                        .placeholder {
                            Image.placeholderShop.resizable()
                        }
                        .resizable() //
                        .frame(width: 25, height: 25, alignment: .center)
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15))
                        .padding(.trailing, 5)
                VStack(){
                    Text(barber.name).font(.system(weight: .regular, size: 12))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                        .frame(minWidth: 60, maxWidth: 120)
                    HStack(){
                        
                        Text("\(String(format: "%.1f", barber.distance)) km")
                            .font(.system(weight: .regular, size: 10))
                            .foregroundColor(Color.secondaryTextColor)
                            .lineLimit(1)
                        
                        Spacer()
                    }//.frame(maxWidth: 120)
                    
                }
                
            }.padding(AppConstants.viewSmallMargin)
                .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 15)).shadow(color: Color.shadowAnnotation, radius: 5, x: 0, y: 10))
                
            
            Capsule().fill(Color.primaryColor)
                .frame(width: 15, height: 5)
        
        }.frame( height: 120, alignment: .center)
            .onAppear {
                
                if #available(iOS 15.0, *) {
                    DebugHelper.debug("MapAnnotationView onAppear iOS 15.0")
                    Task {
                        try await asyncLoadImage(url: barber.image.url)
                    }
                } else {
                    // Fallback on earlier versions
                    DebugHelper.debug("MapAnnotationView onAppear < iOS 15.0 earlier versions")
                    loadImage(url: barber.image.url)
                }
                /*
                Task {
                    try await asyncGetText(url: barber.image.url)
                }*/
            }
    }
}

struct MapAnnotationView_Previews: PreviewProvider {
    static var previews: some View {
        MapAnnotationView(barber: BarberShopModel())
    }
}
