//
//  NotificationItem.swift
//  elige
//
//  Created by user196417 on 11/9/21.
//

import SwiftUI

struct NotificationItemView: View {
    
    @State var notification : NotificationModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    
    var body: some View {
        CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            HStack(spacing: 20){
            
            Circle().fill(Color.secondaryColor)
                .frame(width: 50, height: 50, alignment: .center)
                .overlay(
                    notification.type == "ORDER" ? Image.iconNotifOrder .resizable()
                        .frame(width: 40, height: 40, alignment: .center) : Image.iconNotifRating.resizable()
                            .frame(width: 35, height: 35, alignment: .center)
                    
                )
                
            VStack(alignment: .leading, spacing: 0){
                
                Text(notification.title)
                    .font(.system(weight: .regular, size: 18))
                    .foregroundColor(Color.primaryTextColor)
                    .padding(.bottom, AppConstants.viewVerySmallMargin)
                    .lineLimit(1)
                
                Text(notification.message)
                    .font(.system(weight: .regular, size: 15))
                    .foregroundColor(Color.thirdTextColor)
                    .padding(.bottom, AppConstants.viewTinyMargin)
                    .lineLimit(2)
                HStack(spacing: 5){
                    
                    Spacer()
                    
                    Text(notification.createdAt.time(since: AppFunctions.getTodayDateWithTZ()))
                        .font(.system(weight: .regular, size: 13))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(1)
                    
                }
            }
        }
        }
    }
}

struct NotificationItem_Previews: PreviewProvider {
    static var previews: some View {
        NotificationItemView(notification: NotificationModel())
    }
}
