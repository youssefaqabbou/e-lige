//
//  OrderLinesItem.swift
//  elige
//
//  Created by user196417 on 12/20/21.
//

import SwiftUI

struct OrderLinesItem: View {
    
    @Binding var orderLine : OrderLinesModel
    
    var body: some View {
        
        HStack(alignment: .top){
            
            VStack(alignment: .leading, spacing: 5){
                
                Text(orderLine.service.name)
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
                
                HStack(){
                    ServiceLocationsItem(serviceLocation: .constant(orderLine.service.serviceLocation))
                                       
                    Spacer(minLength: 4)
                    
                    Text(orderLine.service.duration.toHourMinute())
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                }
                
                Text(orderLine.service.description)
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.thirdTextColor)
                    .lineLimit(1)
            }
            Spacer(minLength: 10)
            
            VStack{
                
                Text(String(orderLine.price.toPrice(currency: "")))
                    .font(.system(weight: .regular, size: 18))
                    .foregroundColor(Color.primaryTextColor)
                     
                Text("€")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                
            }.frame(width: 70, height: 70, alignment: .center)
            .background(Color.lightGray)
            .cornerRadius(10)
        }
    }
}

struct OrderLinesItem_Previews: PreviewProvider {
    static var previews: some View {
        OrderLinesItem(orderLine: .constant(OrderLinesModel()))
    }
}
