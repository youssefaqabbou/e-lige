//
//  ReservationItem.swift
//  elige
//
//  Created by user196417 on 11/5/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ReservationItem: View {
    
    @State var order : OrderModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    var body: some View {
        CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            HStack(spacing: 0){
            
            ZStack(alignment: .bottom){
                
                WebImage(url: URL(string: order.barberShop.image.url))
                    .placeholder {
                        Image.placeholderShop.resizable()
                    }
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 76, height: 83)
                    .cornerRadius(10)
                
                Text(AppFunctions.getOrderStatusLabel(status: order.status))
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.primaryTextLightColor)
                    .frame(width: 93, height: 21, alignment: .center)
                    .background(AppFunctions.getOrderStatusColor(status: order.status))
                    .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                    .zIndex(2)
            }.padding(.trailing, AppConstants.viewNormalMargin)
            
            VStack(alignment: .leading, spacing: AppConstants.viewTinyMargin){
                
                Text(order.barberShop.name)
                    .font(.system(weight: .regular, size: 20))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
                
                Text(AppFunctions.getOrderServices(order: order))
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.secondaryTextColor)
                    .lineLimit(1)
                
                Spacer(minLength: AppConstants.viewTinyMargin)
                
                Text(order.totalPrice.toPrice())
                    .font(.system(weight: .regular, size: 12))
                    .foregroundColor(Color.secondaryTextColor)
                    .lineLimit(1)
                
                Spacer(minLength: AppConstants.viewTinyMargin)
                
                HStack(spacing: AppConstants.viewTinyMargin ){
                    Text(order.orderDate.toString(format: "HH:mm", isConvertZone: false))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    Text("-")
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    Text(order.orderDate.addMinutes(minutes: order.duration).toString(format: "HH:mm", isConvertZone: false))
                        .font(.system(weight: .regular, size: 14))
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Spacer()
                }
                
            }
            .padding(.trailing, AppConstants.viewNormalMargin)
            
            Spacer(minLength: 10)
            
            VStack{
                Text(order.orderDate.time(onlyDay: true))
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(order.orderDate.toString(format: "dd", isConvertZone: false))
                    .font(.system(weight: .regular, size: 32))
                    .foregroundColor(Color.primaryTextColor)
                
                Text(order.orderDate.toString(format: "MMMM",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter())
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                
            }
            .padding(.vertical, AppConstants.viewVerySmallMargin)
            .padding(.horizontal, AppConstants.viewVerySmallMargin)
            .frame(minWidth: 90,  minHeight: 90, alignment: .center)
            .background(Color.lightGray)
            .cornerRadius(10)
                
        }.frame(height: 83)
        }
    }
}

struct ReservationItem_Previews: PreviewProvider {
    static var previews: some View {
        ReservationItem(order: OrderModel())
    }
}
