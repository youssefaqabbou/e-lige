//
//  ListReviewItem.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ReviewItemView: View {
    
    var review : ReviewModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    
    var body: some View {
        
        //CustomAnimateItemView(index: indexItem, type: ItemAnimationType.move, config: AnimationConfig(heigthForItem: heigthForItem, paddingItem: 20)){
            VStack(alignment: .leading){
                
                HStack{
                    WebImage(url: URL(string: review.owner.image.url))// Animation Control, supports dynamic changes
                        .placeholder {
                            Image.placeholderUser.resizable()
                        }
                        .resizable() //
                        .frame(width: 40, height: 40, alignment: .center)
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                    
                    Text(review.owner.fullname)
                        .font(.system(weight: .bold, size: 14))
                        .foregroundColor(Color.primaryTextColor)
                    
                    Spacer()
                    
                    VStack(alignment: .trailing ,spacing: 10){
                        
                        StarsView(rating: CGFloat(review.note), maxRating: 5, width: 15)
                        
                        Text(review.dateCreated.time(since: AppFunctions.getTodayDateWithTZ()))
                            .font(.system(weight: .light, size: 12))
                            .foregroundColor(Color.primaryTextColor)
                    }
                }
                
                Text(review.content)
                    .font(.system(weight: .light, size: 13))
                    .foregroundColor(Color.primaryTextColor)
                
            }.padding(.bottom, AppConstants.viewSmallMargin)
        //}
    }
}

struct ListReviewItem_Previews: PreviewProvider {
    static var previews: some View {
        ReviewItemView(review: FakeData.getReviewData(id: 2))
    }
}
