//
//  ReviewsItem.swift
//  elige
//
//  Created by user196417 on 11/10/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ReviewSmallItemView: View {
    
    @State var review : ReviewModel
    
    var body: some View {
        
        VStack{
            
            HStack(alignment: .center){
                WebImage(url: URL(string: review.owner.image.url))// Animation Control, supports dynamic changes
                        .placeholder {
                            Image.placeholderUser.resizable()
                        }
                        .resizable() //
                        .frame(width: 35, height: 35, alignment: .center)
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
                
                Text(review.owner.fullname)
                    .font(.system(weight: .bold, size: 15))
                    .foregroundColor(Color.primaryTextColor)
                
                Spacer()
                
                StarsView(rating: CGFloat(review.note), maxRating: 5, width: 10)
            }
            
            HStack(alignment: .top){
                
                Text(review.content)
                    .font(.system(weight: .light, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(3)
                
                Spacer()
                
                Text(review.dateCreated.time(since: AppFunctions.getTodayDateWithTZ()))
                    .font(.system(weight: .light, size: 12))
                    .foregroundColor(Color.primaryTextColor)
            }
        }
    }
}

struct ReviewsItem_Previews: PreviewProvider {
    static var previews: some View {
        ReviewSmallItemView(review: ReviewModel())
    }
}
