//
//  ScheduleItem.swift
//  elige
//
//  Created by user196417 on 11/10/21.
//

import SwiftUI

struct ScheduleItem: View {
    @Binding var schedule : ScheduleModel
    var body: some View {
        
        HStack{
            
            Text(schedule.day.toWeekDay(firstWeekday: 2, local: Locale.init(identifier: "FR")).capitalizingFirstLetter())
                .font(.system(weight: .regular, size: 13))
                .foregroundColor(Color.primaryTextColor)
            
            Spacer(minLength: 10)
            
            if(!schedule.isOpen){
                Text("Fermé")
                    .font(.system(weight: .regular, size: 13))
                    .foregroundColor(Color.primaryTextColor)
            }else{
                if(schedule.haveBreak){
                    HStack(){
                        
                        HStack(spacing: 0){
                            if(schedule.openTime.isEmpty){
                                Text("DÉBUT")
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                if(schedule.openTimeUpdated){
                                    Text(schedule.openTime.timeToTimeZone(convertToUtc: false))
                                        .font(.system(weight: .regular, size: 13))
                                        .foregroundColor(Color.primaryTextColor)
                                }else{
                                    Text(schedule.openTime.timeToTimeZone())
                                        .font(.system(weight: .regular, size: 13))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                            }
                            
                            Text(" - ")
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.primaryTextColor)
                            
                            if(schedule.breakFromUpdated){
                                Text(schedule.breakFrom.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                Text(schedule.breakFrom.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                           
                        }.padding(.trailing, AppConstants.viewSmallMargin)
                        
                        HStack(spacing: 0){
                            if(schedule.breakToUpdated){
                                Text(schedule.breakTo.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                Text(schedule.breakTo.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                            Text(" - ")
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.primaryTextColor)
                            
                            if(schedule.closeTime.isEmpty){
                                Text("FIN")
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                if(schedule.closeTimeUpdated){
                                    Text(schedule.closeTime.timeToTimeZone(convertToUtc: false))
                                        .font(.system(weight: .regular, size: 13))
                                        .foregroundColor(Color.primaryTextColor)
                                }else{
                                    Text(schedule.closeTime.timeToTimeZone())
                                        .font(.system(weight: .regular, size: 13))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                            }
                        }
                    }
                    
                }else{
                    HStack(spacing: 0){
                        if(schedule.openTime.isEmpty){
                            Text("DÉBUT")
                                .font(.system(weight: .regular, size: 13))
                                .foregroundColor(Color.primaryTextColor)
                        }else{
                            if(schedule.openTimeUpdated){
                                Text(schedule.openTime.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                Text(schedule.openTime.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                        }
                        Text(" - ")
                            .font(.system(weight: .regular, size: 12))
                            .foregroundColor(Color.primaryTextColor)
                        if(schedule.closeTime.isEmpty){
                            Text("FIN")
                                .font(.system(weight: .regular, size: 13))
                                .foregroundColor(Color.primaryTextColor)
                        }else{
                            if(schedule.closeTimeUpdated){
                                Text(schedule.closeTime.timeToTimeZone(convertToUtc: false))
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                Text(schedule.closeTime.timeToTimeZone())
                                    .font(.system(weight: .regular, size: 13))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                        }
                    }
                }
            }
            /*
            ZStack(){
                if(schedule.serviceLocation == ServiceLocation.shop.rawValue){
                    Image.iconBarberShop
                        .resizable() //
                        .frame(width: 20, height: 20, alignment: .center)
                }else{
                    Image.iconBarberDomicile
                        .resizable() //
                        .frame(width: 20, height: 20, alignment: .center)
                }
            }.padding(8)
            .background(Color.primaryColor).clipShape(Circle())
                .frame(width: 35, height: 35, alignment: .center)*/
        }
    }
}

struct ScheduleItem_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleItem(schedule: .constant(ScheduleModel()))
    }
}

