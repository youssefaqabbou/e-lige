//
//  ServiceDetailItem.swift
//  elige
//
//  Created by MAC on 15/11/21.
//

import SwiftUI

struct ServiceDetailItem: View {
    
    @Binding var service : ServiceModel
    
    var body: some View {
        
        HStack(alignment: .top){
            
            VStack(alignment: .leading, spacing: 5){
                Text(service.name)
                    .font(.system(weight: .regular, size: 16))
                    .foregroundColor(Color.primaryTextColor)
                    .lineLimit(1)
              
                HStack(){
                    
                    ServiceLocationsItem(serviceLocation: .constant(service.serviceLocation))
                    
                    Spacer(minLength: 4)
                    
                    Text(service.duration.toHourMinute())
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.secondaryTextColor)
                }
                
                Text(service.description)
                    .font(.system(weight: .regular, size: 14))
                    .foregroundColor(Color.thirdTextColor)
                    .lineLimit(1)
            }
            Spacer(minLength: 10)
            
            VStack{
                Spacer()
                Text(String(service.price.toPrice(currency: "")))
                    .font(.system(weight: .regular, size: 18))
                    .foregroundColor(Color.primaryTextColor)
                     
                Spacer()
                
                Text("€")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                Spacer()
            }.frame(width: 70, height: 70, alignment: .center)
            .background(Color.lightGray)
            .cornerRadius(10)
        }
    }
}

struct ServiceDetailItem_Previews: PreviewProvider {
    static var previews: some View {
        ServiceDetailItem(service: .constant(ServiceModel()))
    }
}

struct ServiceLocationsItem: View {
    
    @Binding var serviceLocation : String
    
    var body: some View {
        
        ZStack(alignment: .leading){
            
            HStack(){
                Text("Salon")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
                
                Text("À domicile")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
            }.opacity(serviceLocation == ServiceLocation.both.rawValue ? 1 : 0)
            
            Text("À domicile")
                .foregroundColor(Color.primaryTextColor)
                .font(.system(weight: .regular, size: 13))
                .lineLimit(1)
                .padding(.vertical, AppConstants.viewVerySmallMargin)
                .padding(.horizontal, AppConstants.viewSmallMargin)
                .background(Color.secondaryColor)
                .cornerRadius(20)
                .opacity(serviceLocation == ServiceLocation.home.rawValue ? 1 : 0)
            
            
            Text("Salon")
                .foregroundColor(Color.primaryTextColor)
                .font(.system(weight: .regular, size: 13))
                .lineLimit(1)
                .padding(.vertical, AppConstants.viewVerySmallMargin)
                .padding(.horizontal, AppConstants.viewSmallMargin)
                .background(Color.secondaryColor)
                .cornerRadius(20)
                .opacity(serviceLocation == ServiceLocation.shop.rawValue ? 1 : 0)
            
            /*
            if(serviceLocation == ServiceLocation.both.rawValue){
                Text("Salon")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
                
                Text("À domicile")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
              
               
            }else if(serviceLocation == ServiceLocation.home.rawValue){
                Text("À domicile")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
            }else{
                Text("Salon")
                    .foregroundColor(Color.primaryTextColor)
                    .font(.system(weight: .regular, size: 13))
                    .lineLimit(1)
                    .padding(.vertical, AppConstants.viewVerySmallMargin)
                    .padding(.horizontal, AppConstants.viewSmallMargin)
                    .background(Color.secondaryColor)
                    .cornerRadius(20)
            }
            */
        }
        
        
    }
}
