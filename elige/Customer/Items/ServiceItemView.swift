//
//  ListServiceItem.swift
//  elige
//
//  Created by user196417 on 11/19/21.
//

import SwiftUI
import HCalendar

struct ServiceItemView: View {
    
    var service : ServiceModel
    var indexItem = 0
    @State var heigthForItem : CGFloat = 0
    //var returnHeigth : (CGFloat) -> () = { itemHeight in }
    @State var itemHeight : CGFloat = 150
    var body: some View {
        
        VStack(alignment: .leading){
            
            HStack(alignment: .top){
                
                VStack(alignment: .leading, spacing: 5){
                    
                    Text(service.name)
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(Color.primaryTextColor)
                        .lineLimit(2)
                  
                    HStack(){
                        ServiceLocationsItem(serviceLocation: .constant(service.serviceLocation))
                                           
                        Spacer(minLength: 4)
                        
                        Text(service.duration.toHourMinute())
                            .font(.system(weight: .regular, size: 16))
                            .foregroundColor(Color.secondaryTextColor)
                    }
                }
                Spacer(minLength: 10)
                
                VStack{
                    
                    Text(String(service.price))
                        .font(.system(weight: .regular, size: 18))
                        .foregroundColor(Color.primaryTextColor)
                         
                    Text("€")
                        .font(.system(weight: .regular, size: 24))
                        .foregroundColor(Color.primaryTextColor)
                    
                }.frame(width: 60, height: 60, alignment: .center)
                .background(Color.lightGray)
                .cornerRadius(10)
                
            }
            
            Text(service.description)
                .font(.system(weight: .regular, size: 14))
                .foregroundColor(Color.thirdTextColor)
        }
    }
}

struct ListServiceItem_Previews: PreviewProvider {
    static var previews: some View {
        ServiceItemView(service: ServiceModel())
    }
}
