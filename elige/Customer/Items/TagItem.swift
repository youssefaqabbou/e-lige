//
//  TagItem.swift
//  elige
//
//  Created by user196417 on 11/16/21.
//

import SwiftUI

struct TagItem: View {
    
    var tag: TagModel
    @State var start = false
    
    var body: some View {
        
        HStack(spacing: 15){
          
            Text(tag.name)
                .foregroundColor(Color.primaryTextColor)
                .font(.system(weight: .regular, size: 13))
                .lineLimit(1)

        }.padding(.vertical, 8)
        .padding(.horizontal, AppConstants.viewSmallMargin)
        .background(Color.secondaryColor)
        .cornerRadius(7)
        .opacity(start ? 1 : 0)
        .scaleEffect(start ? 1 : 0)
        .onAppear() {
            withAnimation(Animation.easeIn(duration: 0.2).delay(Double(tag.position) * 0.1)){
                self.start = true
            }
        }
    }
}

struct TagItem_Previews: PreviewProvider {
    static var previews: some View {
        TagItem(tag: TagModel())
    }
}
