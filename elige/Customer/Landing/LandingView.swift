//
//  LandingView.swift
//  elige
//
//  Created by user196417 on 11/4/21.
//

import SwiftUI
import SDWebImageSwiftUI
import HCalendar

struct LandingView: View {
    @StateObject var vmLanding = LandingViewModel()
    
    var profileAction : () -> () = {}
    var searchAction : () -> () = {}
    var barberDetailAction : (BarberShopModel) -> () = {barberShop in }
    var categoryAction : (TagModel) -> () = {category in }
    var notificationAction : () -> () = {}
    
    let NC = NotificationCenter.default
    
    var animCategory = AnimationFactory.move(yOffset: 120)
    @State var isCategoryAnimLoaded = false
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading, spacing: 0){
                
                HStack{
                    
                    HStack(spacing: 15){
                        if AppFunctions.checkIfUserConnected(){
                            
                            WebImage(url: URL(string: vmLanding.currentUser.image.url))
                                .placeholder {
                                    Image.placeholderUser.resizable()
                                }
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 60, height: 60)
                                .clipShape(Circle())
                            
                            VStack(alignment: .leading, spacing: 5){
                                
                                Text("Bonjour")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                                
                                Text(vmLanding.currentUser.fullname)
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }
                        }else{
                            WebImage(url: URL(string: vmLanding.currentUser.image.url))
                                .placeholder {
                                    Image.placeholderUser.resizable()
                                }
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 60, height: 60)
                                .clipShape(Circle())
                            
                            VStack(alignment: .leading, spacing: 5){
                                
                                Text("Bonjour")
                                    .font(.system(weight: .regular, size: 12))
                                    .foregroundColor(Color.secondaryTextColor)
                                
                                Text("invité")
                                    .font(.system(weight: .regular, size: 16))
                                    .foregroundColor(Color.primaryTextColor)
                                
                            }
                        }
                        
                    }.onTapGesture(perform: {
                        profileAction()
                    })
                    
                    Spacer(minLength: 20)
                    
                    BellItem().onTapGesture {
                        
                        if AppFunctions.checkIfUserConnected() {
                            notificationAction()
                        }else{
                            UserDefaults.showLoginDialog = true
                            //UserDefaults.showLogin = true
                            return
                        }
                        
                    }
                }
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.top, AppConstants.viewSmallMargin)
                .padding(.bottom, AppConstants.viewNormalMargin)
                
                HStack(spacing: 15){
                    
                    Image.iconSearch
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(Color.secondaryTextColor)
                    
                    Text("Rechercher un coiffeur")
                        .font(.system(weight: .regular, size: 16))
                        .foregroundColor(.secondaryTextColor)
                        
                    Spacer()
                    
                }.padding()
                    .frame(height: 55)
                    .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                    .onTapGesture {
                        searchAction()
                    }
                    .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    .padding(.bottom, AppConstants.viewNormalMargin)
                
                CollectionLoadingView(loadingState: vmLanding.loadingState) {
                    ListShimmeringView(count: 15, isVertical : false){
                        CategoryItemHolderView()
                    }.padding(.bottom, AppConstants.viewNormalMargin)
                        .padding(.horizontal,  AppConstants.viewVeryExtraMargin)
                } content: {
                    CustomGrid(data: $vmLanding.categoryList, cellWidth: 65, cellHeight: 65,minSpacing: AppConstants.viewExtraMargin, animation: animCategory) { category in
                        CategoryItemView(category: category) {
                            categoryAction(category)
                        }
                        //.padding(.leading, category == vmLanding.categoryList.first ? AppConstants.viewVeryExtraMargin : 0)
                        //.padding(.trailing, category == vmLanding.categoryList.last ? AppConstants.viewVeryExtraMargin : 0)
                    } onLoaded: { collection in
                        if (!isCategoryAnimLoaded && !vmLanding.categoryList.isEmpty) {
                            Animator(animation: animCategory).animate(views: collection.visibleCells)
                            self.isCategoryAnimLoaded = true
                        }
                       
                      }
                    .frame(height: 95, alignment: .center)
                    .padding(.leading,  AppConstants.viewVeryExtraMargin)
                } empty: {
                    EmptyView()
                } error: {
                    EmptyView()
                }
                /*
                if(vmLanding.loadingState == .loading){
                    ListShimmeringView(count: 15, isVertical : false){
                        CategoryItemHolderView()
                    }.padding(.bottom, AppConstants.viewNormalMargin)
                        .padding(.horizontal,  AppConstants.viewVeryExtraMargin)
                }else{
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack(spacing: AppConstants.viewExtraMargin){
                            ForEach(vmLanding.categoryList, id: \.id) { category in
                                CategoryItemView(category: category) {
                                    categoryAction(category)
                                }
                                .padding(.leading, category == vmLanding.categoryList.first ? AppConstants.viewVeryExtraMargin : 0)
                                .padding(.trailing, category == vmLanding.categoryList.last ? AppConstants.viewVeryExtraMargin : 0)
                            }
                        }
                    }.padding(.bottom, AppConstants.viewNormalMargin)
                }
                */
                HStack(alignment: .center){
                    
                    Text("Les plus proches")
                        .font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                    
                    Spacer(minLength: 20)
                    
                }
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.bottom, AppConstants.viewNormalMargin)
                .padding(.bottom, AppConstants.viewSmallMargin)
                .padding(.top, AppConstants.viewSmallMargin)
                
                CollectionLoadingView(loadingState: vmLanding.loadingState) {
                    ListShimmeringView(count: 15){
                        BarberShopItemHolderView()
                    }
                } content: {
                    
                    GeometryReader { geometry in
                        ScrollView(.vertical, showsIndicators: false){
                       
                            LazyVStack(spacing: 20) {
                                ForEach(vmLanding.barberShopList.indices, id: \.self) {index in   // << here !
                                //ForEach(0..<vmLanding.barberShopList.count, id: \.self){ index in
                                    let barberShop = vmLanding.barberShopList[index]
                                    BarberShopItem(barberShop: .constant(barberShop), indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY ).onTapGesture {
                                        barberDetailAction(barberShop)
                                    }
                                   
                                }
                            }
                        }.padding(.bottom, AppConstants.viewNormalMargin)
                        
                    }
                   
                } empty: {
                    VStack(spacing: 20){
                        Spacer()
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_shop_empty_title.localized, message: LocalizationKeys.list_shop_empty_message.localized)
                        Spacer()
                    }
                    
                } error: {
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_shop_empty_title.localized, message: LocalizationKeys.list_shop_empty_message.localized)
                } .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                    
            }//.padding(.bottom, AppConstants.viewNormalMargin)
            
        }.background(Color.bgViewColor)
            .onAppear {
                vmLanding.checkLocationPermission()
                self.NC.addObserver(forName: NSNotification.profileUpdated, object: nil, queue: nil,
                                    using: self.profileUpdated)
                self.NC.addObserver(forName: NSNotification.shopUpdated, object: nil, queue: nil,
                                    using: self.barberShopUpdated)
            }
    }
    
    func profileUpdated(_ notification: Notification) {
        vmLanding.refreshProfile()
    }
    func barberShopUpdated(_ notification: Notification) {
        let barberShop = notification.userInfo!["barberShop"] as! BarberShopModel
        DebugHelper.debug(barberShop.name)
        vmLanding.refreshBarberShopList(barberShop: barberShop)
    }
}

struct LandingView_Previews: PreviewProvider {
    static var previews: some View {
        LandingView()
    }
}
