//
//  LandingViewModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation
import CoreLocation
import UIKit

class LandingViewModel: NSObject, ObservableObject , CLLocationManagerDelegate{
    
    private var apiService: APIService = APIService()
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    @Published var currentUser = UserModel()
    @Published var barberShopList : [BarberShopModel] = []
    @Published var categoryList : [TagModel] = []
    
    private let locationManager = CLLocationManager()
    @Published var currentLocation = CLLocationCoordinate2D()
        
    override init() {
        super.init()
        currentUser = AppFunctions.getConnectedUser()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
    }
    
    func refreshProfile(){
        currentUser = AppFunctions.getConnectedUser()
    }
  
    func getBarberShops(){
        self.loadingState = .loading
        let params = ["lat" : UserDefaults.latitude, "lng" : UserDefaults.longitude, "distance": String(AppConstants.distance)]

        apiService.shopCategories(params: params) { shops, categories in
            self.barberShopList = shops
            self.categoryList = categories
            if(shops.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        } onFailure: { errorMessage in
            self.loadingState = .empty
        }
    }
    
    func checkLocationPermission(){
        
        if(!UserDefaults.latitude.isEmpty && !UserDefaults.longitude.isEmpty){
            getBarberShops()
        }
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
               
                break
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.requestLocation()
                break
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
        }

    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        DebugHelper.debug("locationManagerDidChangeAuthorization")
        switch manager.authorizationStatus {
        case .denied:
            break
        case .notDetermined:
            break
        case .authorizedWhenInUse, .authorizedAlways :
            DebugHelper.debug("authorizedWhenInUse && authorizedAlways ")
            locationManager.requestLocation()
            break
        default:
            ()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DebugHelper.debug("didUpdateLocations")
        guard let location = locations.last else {
            return
        }

        currentLocation = location.coordinate
        if(UserDefaults.latitude.isEmpty && UserDefaults.longitude.isEmpty){
            UserDefaults.latitude = String(location.coordinate.latitude)
            UserDefaults.longitude = String(location.coordinate.longitude)
            
            DebugHelper.debug("locationManager", "new Request")
            getBarberShops()
        }
        UserDefaults.latitude = String(location.coordinate.latitude)
        UserDefaults.longitude = String(location.coordinate.longitude) 
        //getBarberShops()
    }
    
    func refreshBarberShopList(barberShop : BarberShopModel){
        DebugHelper.debug("isFavorite", barberShop.isFavorite)
        let index = getBarberShopIndex(id: barberShop.id)

        if index != nil {
            DebugHelper.debug("list isFavorite", barberShopList[index!].isFavorite)
            barberShopList[index!].isFavorite = barberShop.isFavorite
            barberShopList[index!].canReview = barberShop.canReview
            self.objectWillChange.send()
        }
    }
    
    func getBarberShopIndex(id: Int) -> Int? {
        for (index, value) in barberShopList.enumerated() {
            if value.id == id {
                return index
            }
        }
        return nil
    }
}
