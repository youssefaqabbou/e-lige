//
//  NearbyBarberView.swift
//  elige
//
//  Created by macbook pro on 4/11/2021.
//

import SwiftUI
import MapKit
import SDWebImageSwiftUI

struct NearbyBarberView: View {
    
    @StateObject var vmNearby = NearbyBarberViewModel()
    @State var selectedBarberShop = BarberShopModel()
    
    var barberDetailAction : (BarberShopModel) -> () = {barber in }
    
    let NC = NotificationCenter.default
    
    var body: some View {
        ZStack(){
            MapUIView(mapView: $vmNearby.mapView, annotationSelect: { annotation in
                
                if let mapAnnotation = annotation as? CustomMapAnnotation {
                    if let barberShop = mapAnnotation.mapModel as? BarberShopModel {
                        DebugHelper.debug("annotationSelect", barberShop.name, barberShop.isFavorite)
                        selectedBarberShop = barberShop
                        withAnimation {
                            vmNearby.showMapDetail = true
                        }
                    }
                }
            })
            .ignoresSafeArea(.all)
            .disabled(vmNearby.showMapDetail ? true : false)

        }.onAppear {
            vmNearby.checkLocationPermission()
            self.NC.addObserver(forName: NSNotification.shopUpdated, object: nil, queue: nil,
                                using: self.barberShopUpdated)
        }
        .bottomSheet(isPresented: $vmNearby.showMapDetail, height: 400, topBarCornerRadius: 45, contentBackgroundColor: Color.bgViewColor) {
            VStack(){
                WebImage(url: URL(string: selectedBarberShop.image.url))// Animation Control, supports dynamic changes
                        .placeholder {
                            Image.placeholderShop.resizable()
                        }
                        .resizable() //
                        .frame(width: 80, height: 80, alignment: .center)
                        .clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: .infinity))
            
                VStack(spacing: 0){
                    Text(selectedBarberShop.name).font(.system(weight: .regular, size: 20))
                        .foregroundColor(Color.primaryTextColor)
                    
                    Text(selectedBarberShop.address).font(.system(weight: .regular, size: 12))
                        .foregroundColor(Color.secondaryTextColor).padding(.top, 5)
                    
                    StarsView(rating: 3.8, maxRating: 5, width: 15)
                        .padding(.top, AppConstants.viewNormalMargin)
                    
                }.padding(.vertical, 25)
                   
                Button(action: {
                    withAnimation {
                        vmNearby.showMapDetail = false
                        barberDetailAction(selectedBarberShop)
                    }
                   
                }, label: {
                    
                    Text("Détails")
                        .font(.system(weight: .regular, size: 16, font:  FontStyle.brownStd.rawValue))
                    
                }).buttonStyle(PrimaryButtonStyle())
                
            }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                .padding(.vertical, 50)
        }

    }
    
    func barberShopUpdated(_ notification: Notification) {
        let barberShop = notification.userInfo!["barberShop"] as! BarberShopModel
        DebugHelper.debug(barberShop.name)
        vmNearby.refreshBarberShopList(barberShop: barberShop)
    }
}

struct NearbyBarberView_Previews: PreviewProvider {
    static var previews: some View {
        NearbyBarberView()
    }
}
