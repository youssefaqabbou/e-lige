//
//  NearbyBarberViewModel.swift
//  elige
//
//  Created by macbook pro on 4/11/2021.
//

import Foundation
import MapKit
import CoreLocation
import Alamofire

class NearbyBarberViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    private var apiService: APIService = APIService()
    
    private let locationManager = CLLocationManager()
    @Published var mapView = MKMapView()
    @Published var currentLocation = CLLocationCoordinate2D()
    
    //Region
    @Published var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 51.514134, longitude: -0.104236),
        span: MKCoordinateSpan(latitudeDelta: 0.075, longitudeDelta: 0.075))
   
    @Published var barberShopList : [BarberShopModel] = []
    @Published var showMapDetail = false
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
    }
    
    func getBarberShops(){
        let params = ["lat" :  UserDefaults.latitude, "lng" :  UserDefaults.longitude, "distance": String(AppConstants.distance)]

        apiService.getShops(params: params, onSuccess: { shops in
            DebugHelper.debug( shops.count)
            self.barberShopList = shops
            DebugHelper.debug( self.barberShopList.count)
            self.addStoresToMap()
        }, onFailure: { errorMessage in
            
        })
    }
    
    func addStoresToMap()  {
        mapView.removeAnnotations(mapView.annotations)
        
        for barber in self.barberShopList {
            let annotation = CustomMapAnnotation(coordinate: barber.coordinate)
            annotation.mapModel = barber as AnyObject
            mapView.addAnnotation(annotation)
        }
        if(!self.barberShopList.isEmpty){
            self.fitMapViewToAnnotaionList()
        }
    }
    
    func fitMapViewToAnnotaionList() -> Void {
        let mapEdgePadding = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        var zoomRect:MKMapRect = MKMapRect.null

        for index in 0..<mapView.annotations.count {
            let annotation = mapView.annotations[index]
            if (annotation is CustomMapAnnotation) {
                let aPoint:MKMapPoint = MKMapPoint(annotation.coordinate)
                let rect:MKMapRect = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)

                if zoomRect.isNull {
                    zoomRect = rect
                } else {
                    zoomRect = zoomRect.union(rect)
                }
            }
        }

        mapView.setVisibleMapRect(zoomRect, edgePadding: mapEdgePadding, animated: true)
    }
    
    func checkLocationPermission(){
        if(!UserDefaults.latitude.isEmpty && !UserDefaults.longitude.isEmpty){
            getBarberShops()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
               
                break
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.requestLocation()
                break
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
        }

    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        DebugHelper.debug("locationManagerDidChangeAuthorization")
        switch manager.authorizationStatus {
        case .denied:
            break
        case .notDetermined:
            break
        case .authorizedWhenInUse, .authorizedAlways :
            DebugHelper.debug("authorizedWhenInUse && authorizedAlways ")
            locationManager.requestLocation()
            break
        default:
            ()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DebugHelper.debug("didUpdateLocations")
        guard let location = locations.last else {
            return
        }
        currentLocation = location.coordinate
        
        if(UserDefaults.latitude.isEmpty && UserDefaults.longitude.isEmpty){
            UserDefaults.latitude = String(location.coordinate.latitude)
            UserDefaults.longitude = String(location.coordinate.longitude)
            getBarberShops()
        }
        UserDefaults.latitude = String(location.coordinate.latitude)
        UserDefaults.longitude = String(location.coordinate.longitude)
       
    }
    
    func refreshBarberShopList(barberShop : BarberShopModel){
        let index = getBarberShopIndex(id: barberShop.id)
        if index != nil {
            barberShopList[index!].isFavorite = barberShop.isFavorite
            addStoresToMap()
        }
    }
    
    func getBarberShopIndex(id: Int) -> Int? {
        for (index, value) in barberShopList.enumerated() {
            if value.id == id {
                return index
            }
        }
        return nil
    }
}
