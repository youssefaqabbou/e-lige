//
//  NotificationView.swift
//  elige
//
//  Created by user196417 on 11/9/21.
//

import SwiftUI

struct NotificationView: View {
    
    @StateObject var vmNotifications = NotificationViewModel()
    @Binding var showNotifications : Bool
    
    var body: some View {
        
        VStack(alignment: .leading){
            
            HStack(){
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            showNotifications = false
                        }
                    }
                
                Spacer()
                
                Text("Notifications")
                    .font(.system(weight: .regular, size: 24))
                    .foregroundColor(Color.primaryTextColor)
                
                Spacer()
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .hidden()
            }
            .padding(.vertical, AppConstants.viewNormalMargin)
           
            CollectionLoadingView(loadingState: vmNotifications.loadingState) {
                ListShimmeringView(count: 15){
                    NotificationItemHolderView()
                }
            } content: {
                GeometryReader { geometry in
                    ScrollView(.vertical, showsIndicators: false){
                        
                        LazyVStack(spacing: 15){
                            ForEach(0..<vmNotifications.notificationList.count, id: \.self){ index in
                                let notification = vmNotifications.notificationList[index]
                            //ForEach(vmNotifications.notificationList, id: \.id) { notification in
                                NotificationItemView(notification: notification, indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY)
                                .onAppear {
                                    vmNotifications.loadMoreIfNeeded(currentItem: notification)
                                }
                            }
                        }
                    }//.padding(.top, AppConstants.viewNormalMargin)
                    //.padding(.bottom, AppConstants.viewExtraMargin)
                }
                
            } empty: {
                VStack(spacing: 20){
                    Spacer()
                    CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_notification_empty_title.localized, message: LocalizationKeys.list_notification_empty_message.localized)
                    Spacer()
                }
                
            } error: {
                CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_notification_empty_title.localized, message: LocalizationKeys.list_notification_empty_message.localized)
            }
            
        }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            vmNotifications.fetchNotifications()
        }
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView(showNotifications: .constant(false))
    }
}
