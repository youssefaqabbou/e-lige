//
//  NotificationViewModel.swift
//  elige
//
//  Created by user196417 on 1/13/22.
//

import Foundation


class NotificationViewModel: NSObject, ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var notificationList : [NotificationModel] = []
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingMore = false
    
    //@Published var isLoading : Bool = true
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    func fetchNotifications(){
        guard !isLoadingMore && canLoadMorePages else {
          return
        }
        
        if(currentPage == 0){
            //isLoading = true
        }else{
            isLoadingMore = true
        }
        
        let params = ["page": String(currentPage), "size" : String(size)]
       
        apiService.getNotifications(params: params) { notifs in
            if notifs.count == 0 {
                self.canLoadMorePages = false
            }
            self.notificationList.append(contentsOf: notifs)
            self.currentPage += 1
            self.isLoadingMore = false
            //self.isLoading = false
            if(self.notificationList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        } onFailure: { errorMessage in
            self.isLoadingMore = false
            self.canLoadMorePages = false
            //self.isLoading = false
            if(self.notificationList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }
    }
    
    func resetPagination(){
        isLoadingMore = false
        currentPage = 0
        canLoadMorePages = true
    }
    
    func loadMoreIfNeeded(currentItem item: NotificationModel?) {
        guard let item = item else {
            fetchNotifications()
          return
        }

        let thresholdIndex = notificationList.index(notificationList.endIndex, offsetBy: AppConstants.offsetPagination)
        if notificationList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            fetchNotifications()
        }
      }
}
