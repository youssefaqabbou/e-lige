//
//  DetailReservationView.swift
//  elige
//
//  Created by user196417 on 11/25/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailReservationView: View {
    
    
    @StateObject var vmDetailReservation = DetailReservationViewModel()
    @Binding var showDetailOrder : Bool
    @State var order : OrderModel
    
    
    var body: some View {
        VStack{
            
            HStack{
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .overlay(
                        Image.iconArrowBack
                            .resizable()
                            .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                    ).onTapGesture {
                        withAnimation {
                            showDetailOrder = false
                        }
                    }
                
                Spacer(minLength: 10)
                
                Text("Détails réservation")
                    .textStyle(TitleStyle())
                    .lineLimit(1)
                
                Spacer(minLength: 10)
                
                Circle().fill(Color.secondaryColor)
                    .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                    .hidden()
            }
            
            ScrollView(.vertical, showsIndicators: false){
                
                VStack{
                    
                    VStack(spacing: 10){
                        
                        Text("État de votre réservation")
                            .font(.system(weight: .regular, size: 14))
                            .foregroundColor(Color.secondaryTextColor)
                        
                        Text(AppFunctions.getReservationStatusLabel(status: order.status, name: vmDetailReservation.currentUser.fullname))
                            .font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                        
                        Text(AppFunctions.getReservationStatusLabel(status: order.status))
                            .font(.system(weight: .regular, size: 20))
                            .foregroundColor(Color.primaryTextColor)
                            .multilineTextAlignment(.center)
                        
                    }.padding(AppConstants.viewExtraMargin)
                    
                    VStack(spacing: 15){
                        
                        WebImage(url: URL(string: order.barberShop.image.url))
                            .placeholder {
                                Image.placeholderShop.resizable()
                            }.resizable()
                            .frame(height: 150)
                            .cornerRadius(15)
                        
                        Text(order.barberShop.name)
                            .font(.system(weight: .regular, size: 24))
                            .foregroundColor(Color.primaryTextColor)

                        
                        VStack(spacing: 5){
                            
                            Text(order.orderDate.time())
                                 .font(.system(weight: .regular, size: 15))
                                 .foregroundColor(Color.secondaryTextColor)
                            
                            HStack(spacing: 5){
                                
                                Image.iconTime
                                    .resizable()
                                    .frame(width: 16, height : 16)
                                /*
                                Text("Réserver :")
                                     .font(.system(weight: .regular, size: 14))
                                     .foregroundColor(Color.secondaryTextColor)
                                */
                                Text(order.orderDate.toString(format: "HH:mm", isConvertZone: false))
                                     .font(.system(weight: .bold, size: 14))
                                     .foregroundColor(Color.primaryTextColor)
                            }
                            
                            HStack(spacing: 5){
                                if(order.orderLocation == ServiceLocation.home.rawValue ){
                                    Image.iconBarberDomicile
                                        .resizable()
                                        .renderingMode(.template)
                                        .colorMultiply(Color.gray)
                                        .frame(width: 18, height : 18)
                                        .foregroundColor(Color.gray)
                                }else{
                                    Image.iconBarberShop
                                        .resizable()
                                        .renderingMode(.template)
                                        .colorMultiply(Color.gray)
                                        .frame(width: 18, height : 18)
                                        .foregroundColor(Color.gray)
                                }
                                
                                Text(order.orderLocation == ServiceLocation.home.rawValue ? "À domicile" : "Salon" )
                                    .font(.system(weight: .regular, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                            }
                            
                        }
                        Text(order.barberShop.address)
                             .font(.system(weight: .regular, size: 14))
                             .foregroundColor(Color.secondaryTextColor)
                             .padding(.top, AppConstants.viewNormalMargin)
                        
                        VStack(spacing: 10){
                            Image(uiImage: vmDetailReservation.snapShotImage)
                                .resizable()
                                .frame(height: 100)
                                    .mask(AppUtils.CustomCorners(corners: .allCorners, radius: 15))
                                    .cornerRadius(15)
                            
                            Text("Afficher sur la carte")
                                .font(.system(weight: .regular, size: 14))
                                .foregroundColor(Color.primaryColor)
                                .onTapGesture {
                                    vmDetailReservation.openMap(barberShop: order.barberShop)
                                }
                                //.underline()
                        }
                        
                        Text("Récapitulatif de votre réservation")
                             .font(.system(weight: .regular, size: 16))
                             .foregroundColor(Color.secondaryTextColor)
                             .padding(.top, AppConstants.viewSmallMargin)
                        
                        VStack{
                            VStack(spacing: 10){
                                
                                ForEach(order.orderLines, id: \.id) { orderLine in
                                    OrderLinesItem(orderLine: .constant(orderLine))
                                }
                                
                            }.padding(.bottom, AppConstants.viewNormalMargin)
                            
                            VStack(spacing: 10){
                                
                                HStack(){
                                    
                                    Text("Total des prestations").font(.system(weight: .regular, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    Spacer(minLength: 20)
                                    
                                    Text(order.subTotal.toPrice()).font(.system(weight: .regular, size: 16))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                                
                                HStack(){
                                    
                                    Text("Frais").font(.system(weight: .regular, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    Spacer(minLength: 20)
                                    
                                    Text(order.fees.toPrice()).font(.system(weight: .regular, size: 16))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                            
                                HStack(){
                                    
                                    Text("Total").font(.system(weight: .regular, size: 22))
                                        .foregroundColor(Color.primaryTextColor)
                                    Spacer(minLength: 20)
                                    
                                    Text(order.totalPrice.toPrice()).font(.system(weight: .regular, size: 20))
                                        .foregroundColor(Color.primaryTextColor)
                                }
                            }
                        }
                        /*
                        HStack{
                            
                            Text("Mode de paiement :")
                                 .font(.system(weight: .regular, size: 16))
                                 .foregroundColor(Color.secondaryTextColor)
                            
                            Text("MasterCard")
                                 .font(.system(weight: .regular, size: 16))
                                 .foregroundColor(Color.primaryColor)
                        }.padding(.bottom, AppConstants.viewExtraMargin)
                         */
                    }
                }
            }
            .padding(.bottom, AppConstants.viewExtraMargin)
        }
        .padding(.horizontal, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewVeryExtraMargin)
        .padding(.top, AppConstants.viewNormalMargin)
        .padding(.bottom, AppConstants.viewExtraMargin)
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            vmDetailReservation.createSnapShot(location: order.barberShop.coordinate)
        }
    }
}

struct DetailReservationView_Previews: PreviewProvider {
    static var previews: some View {
        DetailReservationView(showDetailOrder: .constant(false) ,order: OrderModel())
    }
}
