//
//  ReservationView.swift
//  elige
//
//  Created by user196417 on 11/5/21.
//

import SwiftUI

struct ReservationView: View {
    
    @StateObject var reservationVM = ReservationViewModel()
    
    
    var detailAction : (OrderModel) -> () = { order in }
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading){
                
                HStack(){
                   
                    Spacer()
                    
                    Text("Mes réservations")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                }.padding(.top, AppConstants.viewSmallMargin)
                    .padding(.bottom, AppConstants.viewExtraMargin)
                
                VStack(spacing: 20){
                    CollectionLoadingView(loadingState: reservationVM.loadingState) {
                        ListShimmeringView(count: 15){
                            ReservationItemHolderView()
                        }
                    } content: {
                        GeometryReader { geometry in
                            ScrollView(.vertical, showsIndicators: false){
                                
                                LazyVStack(spacing: 20){
                                    
                                   // if(reservationVM.loadingState){
                                   //     ListShimmeringView(count: 15){
                                   //         ReservationItemHolderView()
                                   //     }
                                   // }else{
                                    ForEach(0..<reservationVM.orderList.count, id: \.self){ index in
                                        let order = reservationVM.orderList[index]
                                        //ForEach(reservationVM.orderList, id: \.id) { order in
                                        
                                            ReservationItem(order: order , indexItem : index, heigthForItem: geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY )
                                                .onTapGesture {
                                                    detailAction(order)
                                                }.onAppear {
                                                    reservationVM.loadMoreIfNeeded(currentItem: order)
                                                }
                                        }
                                   // }
                                }
                                .padding(.bottom, AppConstants.viewNormalMargin)
                            }.padding(.top, AppConstants.viewNormalMargin)
                        }
                        
                    } empty: {
                        VStack(spacing: 20){
                            Spacer()
                            CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_order_empty_title.localized, message: LocalizationKeys.list_order_empty_message.localized)
                            Spacer()
                        }
                        
                    } error: {
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_order_empty_title.localized, message: LocalizationKeys.list_order_empty_message.localized)
                    }
                    
                }
                //.padding(.bottom, AppConstants.viewNormalMargin)
                
//                ScrollView(.vertical, showsIndicators: false){
//
//                    LazyVStack(spacing: 20){
//
//                        if(reservationVM.isLoading){
//                            ListShimmeringView(count: 15){
//                                ReservationItemHolderView()
//                            }
//                        }else{
//
//                            ForEach(reservationVM.orderList, id: \.id) { order in
//
//                                ReservationItem(order: order)
//                                    .onTapGesture {
//                                        detailAction(order)
//                                    }.onAppear {
//                                        reservationVM.loadMoreIfNeeded(currentItem: order)
//                                    }
//                            }
//                        }
//                    }
//                }
//                .padding(.bottom, AppConstants.viewNormalMargin)
                
            }
            .padding(.horizontal, AppConstants.viewExtraMargin)
            
        }.background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.bottom)
        .onAppear {
            reservationVM.resetPagination()
            reservationVM.featchOrders()
        }
    }
}

struct ReservationView_Previews: PreviewProvider {
    static var previews: some View {
        ReservationView()
    }
}
