//
//  DetailReservationViewModel.swift
//  elige
//
//  Created by user196417 on 11/25/21.
//

import Foundation
import MapKit
import CoreLocation
import SwiftUI


class DetailReservationViewModel : ObservableObject{
    

    private var apiService: APIService = APIService()
    
    @Published var order = [OrderModel]()
    @Published var currentUser = UserModel()
    
    @Published var snapShotImage : UIImage = UIImage(color: UIColor(Color.bgViewSecondaryColor))!
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
    }
  
   
    func createSnapShot(location : CLLocationCoordinate2D){
        let mapSnapshotOptions = MKMapSnapshotter.Options()

        // Set the region of the map that is rendered.
        //let location = CLLocationCoordinate2DMake(37.332077, -122.02962) // Apple HQ
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapSnapshotOptions.region = region

        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale

        // Set the size of the image output.
        mapSnapshotOptions.size = CGSize(width: UIScreen.main.bounds.width - 30, height: 180)

        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true

        let snapshot = MKMapSnapshotter(options: mapSnapshotOptions)
        
        snapshot.start { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                print(error ?? "Unknown error")
                return
            }

            let image = UIGraphicsImageRenderer(size: mapSnapshotOptions.size).image { _ in
                snapshot.image.draw(at: .zero)

                let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
                var pinImage = Image.shopMarkerUIImage 
                //let pinImage = UIImage(named: "pinImage")
           
                pinImage = AppFunctions.resizeImage(image: pinImage!, targetSize: CGSize(width: 25, height: 50))
                var point = snapshot.point(for: location)

                //if rect.contains(point) {
                    point.x -= pinView.bounds.height / 2
                    point.y -= pinView.bounds.height / 2
                    //point.x += pinView.centerOffset.x
                    //point.y += pinView.centerOffset.y
                    DebugHelper.debug(pinView.bounds.width / 2)
                    DebugHelper.debug(pinView.bounds.height / 2)
                    DebugHelper.debug(pinView.centerOffset.x)
                    DebugHelper.debug(pinView.centerOffset.y)
                    DebugHelper.debug(point)
                    pinImage?.draw(at: point)
                //}
            }

            // do whatever you want with this image, e.g.

            DispatchQueue.main.async {
                self.snapShotImage = image
            }
        }
    }
    
    
    func openMap(barberShop : BarberShopModel){
        let latitude: CLLocationDegrees = barberShop.latitude
        let longitude: CLLocationDegrees = barberShop.longitude
        let regionDistance:CLLocationDistance = 5000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = barberShop.name
        mapItem.openInMaps(launchOptions: options)
    }
}
