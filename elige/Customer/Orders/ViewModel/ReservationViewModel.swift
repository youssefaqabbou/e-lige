//
//  ReservationViewModel.swift
//  elige
//
//  Created by user196417 on 11/25/21.
//

import Foundation
import MapKit
import CoreLocation

class ReservationViewModel : ObservableObject{
    
    private var apiService: APIService = APIService()
    
    @Published var orderList = [OrderModel]()
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingMore = false
    
    //@Published var isLoading = false
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    func featchOrders(){
        
        guard !isLoadingMore && canLoadMorePages else {
          return
        }
        
        if(currentPage == 0){
            //isLoading = true
        }else{
            isLoadingMore = true
        }
        
        let params = ["page": String(currentPage), "size" : String(size)]
        apiService.fetchOrders(params: params, onSuccess: { orders in
            if orders.count == 0 {
                self.canLoadMorePages = false
            }
            self.orderList.append(contentsOf: orders)
            self.currentPage += 1
            self.isLoadingMore = false
            //self.isLoading = false
            if(self.orderList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }, onFailure: { errorMessage in
            self.isLoadingMore = false
            self.canLoadMorePages = false
            //self.isLoading = false
            if(self.orderList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        })
    }
    func resetPagination(){
        isLoadingMore = false
        currentPage = 0
        canLoadMorePages = true
    }
    
    func loadMoreIfNeeded(currentItem item: OrderModel?) {
        guard let item = item else {
            featchOrders()
          return
        }

        let thresholdIndex = orderList.index(orderList.endIndex, offsetBy: AppConstants.offsetPagination)
        if orderList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            featchOrders()
        }
      }
}
