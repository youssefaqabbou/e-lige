//
//  CategoryAlertView.swift
//  elige
//
//  Created by user196417 on 12/17/21.
//

import SwiftUI

struct CategoryAlertView: View {
    
    @StateObject var vmCategory = CategoryAlertViewModel()
    
    @Binding var closePopup: Bool
    @Binding var selectedSpeciality : TagModel

    
    var body: some View {
        
        ZStack{
            
            Color.black.opacity(0.4)
                //.edgesIgnoringSafeArea(.vertical)
                .onTapGesture(perform: {
                    withAnimation(.easeInOut){
                        closePopup.toggle()
                    }
                }).edgesIgnoringSafeArea(.vertical)
            
            VStack {
                
                HStack(alignment: .center){
                    
                    Spacer()
                    Text("Catégories").textStyle(AlertTitleStyle())
        
                    Spacer()
                    Image.iconClose.resizable()
                        .frame(width: 30, height: 30).onTapGesture(perform: {
                            withAnimation(.easeInOut){
                                closePopup.toggle()
                            }
                    })
                }.padding(.top, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 0) {
                     ForEach(vmCategory.originalData, id: \.self) { speciality in
                        Button(action: {
                            
                            withAnimation() {
                                vmCategory.checkSpeciality(speciality: speciality)
                            }
                        }, label: {
                            HStack{
                                Text(speciality.name)
                                    .font(.system(weight:.regular, size: 17))
                                    .foregroundColor(Color.primaryTextColor)
                                Spacer()
                                
                                Circle()
                                    .fill(speciality.isSelected ? Color.primaryColor : Color.bgViewColor)
                                    .clipShape(Circle())
                                    .padding(2)
                                    .background(Color.bgViewColor)
                                    .clipShape(Circle())
                                    .padding(2)
                                    .background(Color.primaryColor)
                                    .clipShape(Circle())
                                    .frame(width: 18, height: 18)
                            }.padding(.all, 5)
                            .padding(.vertical, 8)
                            
                        })
                        Divider()
                     }
                  }
                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
                Button(action : {
                    selectedSpeciality = vmCategory.getSelectSpeciality()
                    withAnimation(){
                        closePopup.toggle()
                        
                    }
                }){
                    Text("Sélectionner")
                        .foregroundColor(.white)
                        .frame(minWidth: 20, maxWidth: .infinity)
                        .font(.system(weight:.regular, size: 16))
                        
                }
                .buttonStyle(PrimaryButtonStyle())
                .padding(.horizontal, 50)
                .padding(.vertical, 8)
                
            }.background(Color.white)
            .cornerRadius(AppConstants.viewRadius)
            .frame(width: 340, height: 500, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .onAppear(perform: {
                DebugHelper.debug("SpecialitiesAlertView",  "onAppear")
                if(selectedSpeciality.id != -1){
                    vmCategory.checkSpeciality(speciality: selectedSpeciality)
                }
            })
        }.onAppear {
           vmCategory.getCategories()
        }
    }
}

struct CategoryAlertView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryAlertView(closePopup: .constant(false),selectedSpeciality: .constant(TagModel()))
    }
}
