//
//  CategoryAlertViewModel.swift
//  elige
//
//  Created by user196417 on 12/17/21.
//

import Foundation

class CategoryAlertViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var originalData: [TagModel] = AppFunctions.getSavedCategories()
    
    func getCategories(){
        if(originalData.isEmpty){
            apiService.getCategories(){ categories in
                self.originalData = categories
            } onFailure: { errorMessage in
                
            }
        }
        
    }
    
    func checkSpeciality(speciality: TagModel ) {
        
        for (indexOrigin, origin) in originalData.enumerated(){
                originalData[indexOrigin].isSelected = false
                if origin.id == speciality.id {
                    originalData[indexOrigin].isSelected = true
                    //break
                }
        }
        objectWillChange.send()
    }
    
    func getSelectSpeciality() -> TagModel {
        let selectedSpeciality = TagModel()
        
        for (_, speciality) in originalData.enumerated(){
            if speciality.isSelected{
               return speciality
            }
        }
        
        return selectedSpeciality
    }
    
}



