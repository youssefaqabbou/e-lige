//
//  FilterView.swift
//  elige
//
//  Created by macbook pro on 10/12/2021.
//

import SwiftUI
import HCalendar

struct FilterView: View {
    
    @StateObject var vmFilter =  FilterViewModel()
    
    @Binding var showFilter: Bool
    @Binding var selectedCategories : [TagModel]
    @Binding var selectedServiceLocation: Int
    
    var onFilterAction : ([TagModel], Float, Int) -> () = {categories, distance, location in}
    var onResetAction : () -> () = {}
    var onCategoryAction : () -> () = {}
    
    @State var titles = ["Salon", "À domicile", "Les deux"]
    

    
    var body: some View {
        ZStack(alignment: .bottom){
            
            VStack(){
                HStack(){
                    Image.iconClose.resizable()
                        .padding(AppConstants.viewSmallMargin)
                        .frame(width: 50, height: 50)
                        .hidden()
                    
                    Spacer()
                    
                    Text("Filtre")
                        .textStyle(AlertTitleStyle())
                        .padding(.vertical, AppConstants.viewExtraMargin)
                    
                    Spacer()
                    
                    Image.iconClose.resizable()
                        .padding(AppConstants.viewSmallMargin)
                        .frame(width: 50, height: 50)
                        .onTapGesture {
                            withAnimation {
                                showFilter = false
                            }
                        }
                }
                
                HStack(){
                    Text(selectedCategories.isEmpty ? "Catégories" : AppFunctions.getCategoriesText(categories: selectedCategories)).textStyle(AlertTitleStyle())
                        .lineLimit(1)
                    
                    Spacer()
                    
                    Image.iconArrowBack.resizable().frame(width: 10, height: 10, alignment: .center).rotationEffect(Angle.degrees(-90))
                    
                }.onTapGesture {
                    onCategoryAction()
                }
                .padding()
                .background(Color.white.clipShape(AppUtils.CustomCorners(corners: [.allCorners], radius: 10)))
                .overlay(AppUtils.CustomCorners(corners: [.allCorners], radius: 10).stroke(Color.lightGray, lineWidth: 1))
                
                HStack(){
                    Text("Emplacement").textStyle(AlertTitleStyle())
                    Spacer()
                }.padding(.top, AppConstants.viewExtraMargin)
                
                CustomSegmentedView(titles : titles, selected: $vmFilter.selectedServiceLocation,  frame : UIScreen.main
                                        .bounds.width - ( AppConstants.viewVeryExtraMargin * 2 ) )
                
                
                HStack(){
                    Text("Périmètre").textStyle(AlertTitleStyle())
                    Spacer()
                }
                .padding(.top, AppConstants.viewExtraMargin)
                
                HStack(){
                    Spacer()
                    Text("\( Int(vmFilter.radius)) Km").textStyle(AlertTitleStyle())
                   
                }
                
                CustomSlider(value: $vmFilter.radius,
                             in: vmFilter.minValue...vmFilter.maxValue,
                             step : vmFilter.stepValue,
                                 minimumValueLabel: Text(""),
                                 maximumValueLabel: Text(""),
                                 onEditingChanged: { started in
                               //print("started custom slider: \(started)")
                    }, track: {
                      Capsule()
                        .foregroundColor(Color.gray.opacity(0.20))
                        .frame(width: UIScreen.main.bounds.width - (AppConstants.viewVeryExtraMargin * 2), height: 3)
                    }, fill: {
                      Capsule()
                            .foregroundColor(Color.primaryColor)
                    }, thumb: {
                      Circle()
                        .foregroundColor(Color.primaryColor)
                        .shadow(radius: vmFilter.thumbRadius / 1)
                    }, thumbSize: CGSize(width: vmFilter.thumbRadius, height: vmFilter.thumbRadius))
                
                HStack(spacing: 20){
                    
                    Button(action: {
                        vmFilter.radius = AppConstants.distance
                        withAnimation {
                            onResetAction()
                            showFilter = false
                        }
                    }) {
                        Text("Réinitialiser")
                    }.buttonStyle(PrimaryButtonStyle(bgColor: Color.lightGray, fgColor: Color.primaryTextColor, padding: 15))
                    
                    Button(action: {
                        withAnimation {
                            onFilterAction(selectedCategories, vmFilter.radius, vmFilter.selectedServiceLocation)
                            showFilter = false
                        }
                    }) {
                        Text("Appliquer")
                    }.buttonStyle(PrimaryButtonStyle(padding: 15))
                }.padding(.vertical, AppConstants.viewVeryExtraMargin)
                .padding(.horizontal, AppConstants.viewSmallMargin)
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.bottom, edges == 0 ? 15 : 0)
            
            
        }.onAppear {
            DebugHelper.debug("FilterView appear")
        }
    }
   
}

var edges = UIApplication.shared.windows.first?.safeAreaInsets.bottom

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView(showFilter: .constant(false), selectedCategories: .constant([TagModel]()), selectedServiceLocation: .constant(2))
    }
}
