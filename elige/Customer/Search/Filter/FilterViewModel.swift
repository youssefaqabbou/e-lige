//
//  FilterViewModel.swift
//  elige
//
//  Created by macbook pro on 10/12/2021.
//

import Foundation
import SwiftUI

class FilterViewModel: ObservableObject {
    
    var maxValue : Float = 200
    var minValue : Float = 1
    var thumbRadius: CGFloat = 25
    var stepValue : Float = 5
    
    @Published var radius: Float  = AppConstants.distance
    @Published var selectedServiceLocation = 2

}

