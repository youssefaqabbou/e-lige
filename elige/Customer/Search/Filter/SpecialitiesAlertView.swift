//
//  SpecialitiesAlertView.swift
//  Homeat
//
//  Created by macbook pro on 10/5/2021.
//

import Foundation
import SwiftUI

struct SpecialitiesAlertView: View {
    
    @StateObject var specialitiesVM = SpecialitiesAlertViewModel()
    
    @Binding var closePopup: Bool
    @Binding var selectedSpecialites : [TagModel]

    
    var body: some View {
        
        ZStack{
            
            Color.black.opacity(0.4)
                //.edgesIgnoringSafeArea(.vertical)
                .onTapGesture(perform: {
                    withAnimation(.easeInOut){
                        closePopup.toggle()
                    }
                }).edgesIgnoringSafeArea(.vertical)
            
            VStack {
                
                HStack(alignment: .center){
                    
                    Spacer()
                    Text("Catégories").textStyle(AlertTitleStyle())
        
                    Spacer()
                    Image.iconClose.resizable()
                        .frame(width: 30, height: 30).onTapGesture(perform: {
                            withAnimation(.easeInOut){
                                closePopup.toggle()
                            }
                    })
                }.padding(.top, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 0) {
                     ForEach(specialitiesVM.originalData, id: \.self) { speciality in
                        Button(action: {
                            
                            withAnimation() {
                                specialitiesVM.checkSpeciality(name: speciality.name)
                            }
                        }, label: {
                            HStack{
                                Text(speciality.name)
                                    .font(.system(weight:.regular, size: 17))
                                    .foregroundColor(Color.primaryTextColor)
                                Spacer()
                               if(speciality.isSelected){
                                   Rectangle().fill(Color.primaryColor)
                                       .padding(.all, 4)
                                       .border(Color.primaryColor)
                                       .frame(width: 18, height: 18)
                               }else{
                                   Rectangle().fill(Color.bgViewColor)
                                       .border(Color.primaryColor)
                                       .frame(width: 18, height: 18)
                               }
                            }.padding(.all, 5)
                            .padding(.vertical, 8)
                            
                        })
                        Divider()
                     }
                  }
                }.padding(.horizontal, AppConstants.viewVeryExtraMargin)
                
                Button(action : {
                    selectedSpecialites = specialitiesVM.getSelectSpeciality()
                    withAnimation(){
                        closePopup.toggle()
                    }
                }){
                    Text("Sélectionner")
                        .foregroundColor(.white)
                        .frame(minWidth: 20, maxWidth: .infinity)
                        .font(.system(weight:.regular, size: 16))
                        
                }
                .buttonStyle(PrimaryButtonStyle())
                .padding(.horizontal, 50)
                .padding(.vertical, 8)
                
            }.background(Color.white)
            .cornerRadius(AppConstants.viewRadius)
            .frame(width: 340, height: 500, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .onAppear(perform: {
                DebugHelper.debug("SpecialitiesAlertView",  "onAppear")
                if(!selectedSpecialites.isEmpty){
                    specialitiesVM.checkSpecialities(specialities: selectedSpecialites)
                }
            })
        }
    }
}

struct SpecialitiesAlertView_Previews: PreviewProvider {
    //@State static var selected = SpecialtyModel.selectedSpecialitiesData
    
    static var previews: some View {
        
        SpecialitiesAlertView(closePopup: .constant(false),selectedSpecialites: .constant([TagModel]()))
    }
}
