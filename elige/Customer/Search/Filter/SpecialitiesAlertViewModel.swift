//
//  SpecialitiesAlertViewModel.swift
//  Homeat
//
//  Created by macbook pro on 10/5/2021.
//

import Foundation
import Combine

class SpecialitiesAlertViewModel: ObservableObject {
    
    @Published var originalData: [TagModel] = AppFunctions.getSavedCategories()
    
   // @Published var selectedSpecialities: [SpecialtyModel] = SpecialtyModel.specialitiesData
    
    
    func checkSpeciality(name: String) {
        
        for (index, value) in originalData.enumerated(){
            if value.name == name {
                originalData[index].isSelected.toggle()
            }
        }
        objectWillChange.send()
    }
    
    func checkSpecialities(specialities: [TagModel] ) {
        
        for (indexOrigin, origin) in originalData.enumerated(){
            for (_, category) in specialities.enumerated(){
                if origin.id == category.id {
                    originalData[indexOrigin].isSelected = true
                    break
                }
            }
        }
        objectWillChange.send()
    }
    
    func getSelectSpeciality() -> [TagModel] {
        var selectedSpecialities = [TagModel]()
        
        for (_, value) in originalData.enumerated(){
            if value.isSelected{
                selectedSpecialities.append(value)
            }
        }
        
        return selectedSpecialities
    }
    
}
