//
//  SearchView.swift
//  elige
//
//  Created by user196417 on 11/4/21.
//

import SwiftUI

struct SearchView: View {
    
    @StateObject var vmSearch = SearchViewModel()
    @Binding var showSearch: Bool
    @State var selectedCategory = TagModel()
    
    let NC = NotificationCenter.default
    
    func queryChanged(to value: String) {
        let delay = 0.2
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            DebugHelper.debug("queryChanged", value)
            vmSearch.resetPagination()
            vmSearch.checkLocationPermission()
        }
    }
    
    var body: some View {
        
        ZStack {
            
            VStack(alignment: .leading, spacing: 0) {
                
                HStack() {
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showSearch = false
                            }
                        }
                    Spacer()
                    
                    Text("Recherche")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconFilter
                                .resizable()
                                .frame(width: 18, height: 18)
                        ).onTapGesture {
                            hideKeyboard()
                            withAnimation {
                                vmSearch.showFilter = true
                            }
                        }
                }
                .padding(.bottom, AppConstants.viewExtraMargin)
                
                HStack {
                    
                    CustomSearchTextField(placeholder: "Rechercher un coiffeur", text: $vmSearch.query.onChange(queryChanged))
                        .disableAutocorrection(true)
                    
                    ZStack(){
                        Image.iconSearch
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(Color.secondaryTextColor)
                            .padding(AppConstants.viewExtraMargin)
                            .opacity((vmSearch.query.isEmpty || vmSearch.loadingState != .loading ) ? 1 : 0)
                        
                        ProgressView()
                            .frame(width: 25, height: 25)
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.primaryColor))
                            .opacity((!vmSearch.query.isEmpty && vmSearch.loadingState == .loading ) ? 1 : 0)
                    }
                   
                }.background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                    .padding(.bottom, AppConstants.viewExtraMargin)
                
                Text("Résultat de la recherche")
                    .font(.system(weight: .regular, size: 20))
                    .foregroundColor(Color.primaryTextColor)
                    .padding(.bottom, AppConstants.viewExtraMargin)
                
                VStack(spacing: 20){
                    CollectionLoadingView(loadingState: vmSearch.loadingState) {
                        ListShimmeringView(count: 15,  content: {
                            BarberShopItemHolderView()
                        })
                    } content: {
                        GeometryReader { geometry in
                            ScrollView(.vertical, showsIndicators: false){
                                
                                LazyVStack(spacing: 20) {
                                    ForEach(0..<vmSearch.barberShopList.count, id: \.self){ index in
                                        let barberShop = vmSearch.barberShopList[index]
                                    //ForEach(vmSearch.barberShopList, id: \.id) { barberShop in
                                        BarberShopItem(barberShop: .constant(barberShop), indexItem : index, heigthForItem: (geometry.frame(in: .global).maxY - geometry.frame(in: .global).minY)).onTapGesture {
                                            vmSearch.selectedBarberShop = barberShop
                                            withAnimation {
                                                vmSearch.showDetailBarber = true
                                            }
                                        }.onAppear {
                                            vmSearch.loadMoreIfNeeded(currentItem: barberShop)
                                        }
                                    }
                                }
                            }.padding(.top, AppConstants.viewNormalMargin)
                        }
                    } empty: {
                        VStack(spacing: 20){
                            Spacer()
                            CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_shop_empty_title.localized, message: LocalizationKeys.list_shop_empty_message.localized)
                            Spacer()
                        }
                        
                    } error: {
                        CustomEmptyView(showIcon: false ,title: LocalizationKeys.list_shop_empty_title.localized, message: LocalizationKeys.list_shop_empty_message.localized)
                    }
                    
                }
                .padding(.bottom, AppConstants.viewNormalMargin)
              
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            
            .bottomSheet(isPresented: $vmSearch.showFilter, height: 520, topBarCornerRadius: 45, showTopIndicator: false, content: {
                FilterView(showFilter: $vmSearch.showFilter, selectedCategories: $vmSearch.categories, selectedServiceLocation : $vmSearch.selectedServiceLocation, onFilterAction: { selectedCategories, distance, selectedServiceLocation in
                    //vmSearch.categories = selectedCategories
                    
                    vmSearch.distance = distance
                    vmSearch.selectedServiceLocation = selectedServiceLocation
                    vmSearch.checkLocationPermission()
                }, onResetAction: {
                    vmSearch.resetFilter()
                    vmSearch.checkLocationPermission()
                }, onCategoryAction: {
                    withAnimation {
                        vmSearch.isShowCategories = true
                    }
                    
                })
                
            })
            
            if(vmSearch.showDetailBarber) {
                DetailBarberView(showDetail: $vmSearch.showDetailBarber, barberShop: vmSearch.selectedBarberShop).transition(.move(edge: .trailing)).animation(.linear, value: showSearch).zIndex(3)
            }
            
            if(vmSearch.isShowCategories) {
                SpecialitiesAlertView(closePopup: $vmSearch.isShowCategories, selectedSpecialites: $vmSearch.categories)
            }
        }
        .onTapGesture(perform: {
            hideKeyboard()
        })
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear() {
            if(selectedCategory.id > 0) {
                vmSearch.categories.append(selectedCategory)
            }
            vmSearch.checkLocationPermission()
            self.NC.addObserver(forName: NSNotification.shopUpdated, object: nil, queue: nil,
                                using: self.barberShopUpdated)
        }
    }
    func barberShopUpdated(_ notification: Notification) {
        let barberShop = notification.userInfo!["barberShop"] as! BarberShopModel
        DebugHelper.debug(barberShop.name)
        vmSearch.refreshBarberShopList(barberShop: barberShop)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(showSearch: .constant(false))
    }
}

