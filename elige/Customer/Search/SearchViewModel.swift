//
//  SearchViewModel.swift
//  elige
//
//  Created by macbook pro on 26/11/2021.
//

import Foundation
import SwiftUI
import CoreLocation
import UIKit
import Alamofire

class SearchViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    private var apiService: APIService = APIService()
    @Published var barberShopList : [BarberShopModel] = []
    @Published var query = ""
    
    @Published var showDetailBarber = false
    @Published var showFilter = false
    
    @Published var selectedBarberShop = BarberShopModel()
    
    @Published var categories = [TagModel]()
    @Published var distance : Float = AppConstants.distance
    
    @Published var isShowCategories: Bool  = false
    
    
    private let locationManager = CLLocationManager()
    @Published var currentLocation = CLLocationCoordinate2D()
    
    private var currentPage = 0
    private var size = 15
    private var canLoadMorePages = true
    @Published var isLoadingPage = false
    
    @Published private(set) var loadingState: CollectionLoadingState = .loading
    
    @Published var selectedServiceLocation = 2
    var searchRequest : DataRequest!
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
    }
    
    func resetFilter(){
        categories = [TagModel]()
        distance = AppConstants.distance
        selectedServiceLocation = 2
    }
    
    func searchBarberShop(){
        
        guard !isLoadingPage && canLoadMorePages else {
            return
        }
        
        isLoadingPage = true
        
        let params = ["keyword" : query,
                      "categoryId" : AppFunctions.getCategoriesId(categories: categories),
                      "page" : String(currentPage),
                      "size" :  String(size),
                      "lat" : UserDefaults.latitude,
                      "lng" : UserDefaults.longitude,
                      "distance" : String(distance),
                      "place": selectedServiceLocation == 0 ? ServiceLocation.shop.rawValue : selectedServiceLocation == 1 ? ServiceLocation.home.rawValue : ServiceLocation.both.rawValue
        
        ]
        
        searchRequest = apiService.fetchBarberShop(params: params) { barberShops in
            DebugHelper.debug(barberShops.count)
            if barberShops.count == 0 {
                self.canLoadMorePages = false
            }
            if( self.currentPage == 0){
                self.barberShopList = barberShops
                DebugHelper.debug("removeAll", self.barberShopList.count)
            }else{
                self.barberShopList.append(contentsOf: barberShops)
                DebugHelper.debug( "append" , self.barberShopList.count)
            }
            
           
            self.currentPage += 1
            self.isLoadingPage = false
            
            if(self.barberShopList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        } onFailure: { errorMessage in
            self.isLoadingPage = false
            self.canLoadMorePages = false
            
            if(self.barberShopList.isEmpty){
                self.loadingState = .empty
            }else{
                self.loadingState = .loaded
            }
        }
    }
    
    
    func resetPagination(){
        loadingState = .loading
        isLoadingPage = false
        currentPage = 0
        canLoadMorePages = true
        self.barberShopList.removeAll()
        if(searchRequest != nil){
            searchRequest.cancel()
        }
    }
    
    func loadMoreIfNeeded(currentItem item: BarberShopModel?) {
        guard let item = item else {
            searchBarberShop()
            return
        }
        
        let thresholdIndex = barberShopList.index(barberShopList.endIndex, offsetBy: AppConstants.offsetPagination)
        if barberShopList.firstIndex(where: { $0.id == item.id }) == thresholdIndex {
            searchBarberShop()
        }
    }
    
    func checkLocationPermission(){
        DebugHelper.debug("checkLocationPermission")
        resetPagination()
        if(!UserDefaults.latitude.isEmpty && !UserDefaults.longitude.isEmpty){
            searchBarberShop()
            return
        }
        DebugHelper.debug("checkLocationPermission" ,"latitude empty")
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
               
                break
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.requestLocation()
                break
                @unknown default:
                    break
            }
        } else {
            print("Location services are not enabled")
        }

    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        DebugHelper.debug("locationManagerDidChangeAuthorization")
        switch manager.authorizationStatus {
        case .denied:
            break
        case .notDetermined:
            break
        case .authorizedWhenInUse, .authorizedAlways :
            DebugHelper.debug("authorizedWhenInUse && authorizedAlways ")
            locationManager.requestLocation()
            break
        default:
            ()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DebugHelper.debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DebugHelper.debug("didUpdateLocations")
        guard let location = locations.last else {
            return
        }
        currentLocation = location.coordinate
        if(UserDefaults.latitude.isEmpty && UserDefaults.longitude.isEmpty){
            UserDefaults.latitude = String(location.coordinate.latitude)
            UserDefaults.longitude = String(location.coordinate.longitude)
            self.searchBarberShop()
        }
        UserDefaults.latitude = String(location.coordinate.latitude)
        UserDefaults.longitude = String(location.coordinate.longitude)
       
    }
    
    func refreshBarberShopList(barberShop : BarberShopModel){
        let index = getBarberShopIndex(id: barberShop.id)
        if index != nil {
            barberShopList[index!].isFavorite = barberShop.isFavorite
            self.objectWillChange.send()
        }
    }
    
    func getBarberShopIndex(id: Int) -> Int? {
        for (index, value) in barberShopList.enumerated() {
            if value.id == id {
                return index
            }
        }
        return nil
    }
    
}
