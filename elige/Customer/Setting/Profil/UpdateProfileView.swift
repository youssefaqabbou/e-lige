//
//  UpdateProfilView.swift
//  elige
//
//  Created by user196417 on 11/5/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct UpdateProfileView: View {
    
    @StateObject var vmUpdateProfil = UpdateProfileViewModel()
    
    @Binding var showProfile : Bool
    
    let NC = NotificationCenter.default
    @State var showMap: Bool = false
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading, spacing: 0){
                
                HStack(){
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showProfile = false
                            }
                        }
                    
                    Spacer()
                   
                    Text("Profil")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).hidden()
                }.padding(.bottom, AppConstants.viewSmallMargin)
                
                ScrollView(.vertical, showsIndicators: false){
                    
                    HStack{
                        
                        Spacer()
                        
                        VStack{
                            
                            ZStack(alignment: .bottomTrailing){
                                if vmUpdateProfil.isImagePicked {
                                    ZStack(alignment: .center){
                                        Image(uiImage: vmUpdateProfil.pickedImage)
                                            .resizable()
                                            .frame(width: 100, height: 100)
                                            .clipShape(Circle())
                                        
                                        ProgressView()
                                            .frame(width: 25, height: 25)
                                            .progressViewStyle(CircularProgressViewStyle(tint: Color.primaryColor))
                                            .opacity(vmUpdateProfil.isUploading ? 1 : 0)
                                    }
                                }else{
                                    WebImage(url: URL(string: vmUpdateProfil.currentUser.image.url))
                                        .placeholder {
                                            Image.placeholderUser.resizable()
                                        }
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: 100, height: 100)
                                        .clipShape(Circle())
                                        .onAppear {
                                            DebugHelper.debug("onAppear", vmUpdateProfil.currentUser.image.url)
                                        }
                                }
                                
                                Button(action: {
                                    vmUpdateProfil.isShowPickerAlert = true
                                }) {
                                    Image.iconCameraEdit
                                        .resizable()
                                        .frame(width: 30, height: 30, alignment: .center)
                                    
                                }.background(Color.primaryColor)
                                    .clipShape(Circle())
                                    .padding(2)
                                    .background(Color.white)
                                    .clipShape(Circle())
                                    .disabled(vmUpdateProfil.isUploading ? true : false)
                            }
                            
                            Text(vmUpdateProfil.currentUser.fullname)
                                .font(.system(weight: .regular, size: 24))
                                .foregroundColor(Color.primaryTextColor)
                            
                            Text(vmUpdateProfil.currentUser.phone)
                                .font(.system(weight: .regular, size: 12))
                                .foregroundColor(Color.secondaryTextColor)
                        }
                        
                        Spacer()
                        
                    }
                    
                    VStack(alignment: .leading, spacing: 15){
                    
                        HStack(){
                            
                            Text(vmUpdateProfil.email)
                                .font(.system(weight: .regular, size: 18))
                                .foregroundColor(Color.primaryTextColor)
                                .lineLimit(1)
                            
                            Spacer(minLength: 10)
                            
                            if(!UserDefaults.connectedWithSocial){
                                Image.iconPencil
                                    .resizable()
                                    .frame(width: 30, height: 30, alignment: .center)
                                    .foregroundColor(Color.primaryTextColor)
                                    .onTapGesture {
                                        withAnimation {
                                            vmUpdateProfil.toUpdateEmail = true
                                        }
                                    }
                            }
                           
                        }
                        .padding(.horizontal, AppConstants.viewExtraMargin)
                        .frame(height: 65)
                            .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                        
                        CustomTextField(placeholder: vmUpdateProfil.currentUser.role == "SALON" ? "Raison sociale" : "Nom complet", value: $vmUpdateProfil.fullname, error: vmUpdateProfil.fullnameError)
                            .onChange(of: vmUpdateProfil.fullname, perform: { value in
                                if(vmUpdateProfil.currentUser.role == "SALON"){
                                    vmUpdateProfil.fullnameError = ValidatorHelper.validateSocialReason(socialReason: vmUpdateProfil.fullname).error
                                }else{
                                    vmUpdateProfil.fullnameError = ValidatorHelper.validateFullname(fullname: vmUpdateProfil.fullname).error
                                }
                               
                            })
                        
                        /*
                        CustomTextField(placeholder: "Email", value: $vmUpdateProfil.email, error: vmUpdateProfil.emailError)
                            .onChange(of: vmUpdateProfil.email, perform: { value in
                                vmUpdateProfil.emailError = ValidatorHelper.validateEmail(email: vmUpdateProfil.email).error
                            })
                        */
                        CustomTextField(placeholder: "Numéro de téléphone", value: $vmUpdateProfil.phone, error: vmUpdateProfil.phoneError)
                            .onChange(of: vmUpdateProfil.phone, perform: { value in
                                vmUpdateProfil.phoneError = ValidatorHelper.validatePhone(phone: vmUpdateProfil.phone).error
                            })
                        
                        CustomTextField(placeholder: "Adresse", value: $vmUpdateProfil.customerAddress, error: vmUpdateProfil.customerAddressError)
                            .onChange(of: vmUpdateProfil.customerAddress, perform: { value in
                                //vmUpdateProfil.phoneError = ValidatorHelper.validatePhone(phone: vmUpdateProfil.phone).error
                            })
                        
                        HStack(){
                            if(vmUpdateProfil.customerLatitude != 0.0 || vmUpdateProfil.customerLongitude != 0.0){
                                Text("\(vmUpdateProfil.customerLatitude), \(vmUpdateProfil.customerLongitude)")
                                    .font(.system(weight: .regular, size: 18))
                                    .foregroundColor(Color.primaryTextColor)
                            }else{
                                Text("Emplacement").textStyle(PlaceHolderStyle())
                            }
                            Spacer(minLength: 0)
                                
                        }
                        .frame(height: 65 )
                        .padding(.horizontal, AppConstants.viewExtraMargin)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                        .onTapGesture {
                            hideKeyboard()
                            withAnimation {
                                showMap = true
                            }
                            
                        }
                        
                        CustomLoadingButton(isLoading: $vmUpdateProfil.isLoading, text: "Enregistrer") {
                            if(vmUpdateProfil.isFormComplete){
                                vmUpdateProfil.updateAccount()
                            }
                        }.padding(.top, AppConstants.viewExtraMargin)
                        
                    }.padding(.top, AppConstants.viewVeryExtraMargin)
                    
                    if(!UserDefaults.connectedWithSocial){
                        Divider().frame(height: 3, alignment: .center)
                            .background(Color.secondaryTextColor.opacity(0.3))
                            .padding(AppConstants.viewVeryExtraMargin)
                        
                        VStack(spacing: 15){
                            
                            CustomTextField(placeholder: "Ancien mot de passe", value: $vmUpdateProfil.oldPassword, error: vmUpdateProfil.oldPasswordError, isSecured: true)
                                .onChange(of: vmUpdateProfil.oldPassword, perform: { value in
                                    vmUpdateProfil.oldPasswordError = ValidatorHelper.validatePassword(password: vmUpdateProfil.oldPassword).error
                                })
                            
                            CustomTextField(placeholder: "Nouveau mot de passe", value: $vmUpdateProfil.password, error: vmUpdateProfil.passwordError, isSecured: true)
                                .onChange(of: vmUpdateProfil.password, perform: { value in
                                    vmUpdateProfil.passwordError = ValidatorHelper.validatePassword(password: vmUpdateProfil.password).error
                                })
                            
                            CustomTextField(placeholder: "Confirmation mot de passe", value: $vmUpdateProfil.confirmPassword, error: vmUpdateProfil.confirmPasswordError, isSecured: true)
                                .onChange(of: vmUpdateProfil.confirmPassword, perform: { value in
                                    vmUpdateProfil.confirmPasswordError = ValidatorHelper.validatePasswordConfirmation(password: vmUpdateProfil.password, passwordConfirm:  vmUpdateProfil.confirmPassword).error
                                })
                            
                        }.padding(.bottom, AppConstants.viewExtraMargin)
                        
                        CustomLoadingButton(isLoading: $vmUpdateProfil.isLoadingPwd, text: "Enregistrer") {
                            hideKeyboard()
                            if(vmUpdateProfil.isFormPasswordComplete){
                                vmUpdateProfil.updatePassword()
                            }
                        }.padding(.bottom, 30)
                    }
                    
                    
                }
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            .onTapGesture {
                hideKeyboard()
            }
            
            if(vmUpdateProfil.toUpdateEmail){
                UpdateEmailView(toUpdateEmail: $vmUpdateProfil.toUpdateEmail).transition(.move(edge: .trailing)).animation(.linear , value: vmUpdateProfil.toUpdateEmail)
                    .zIndex(1)
            }
            
            if showMap {
                
                BarberShopLocationMapView(lat: $vmUpdateProfil.customerLatitude, lng: $vmUpdateProfil.customerLongitude, showMap: $showMap) {
                }.transition(.move(edge: .trailing))
                .zIndex(2)
            }else{
                Color.clear
            }
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .bottomSheet(isPresented: $vmUpdateProfil.isShowPickerAlert, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            // Content
            AlertMediaPickerView(showingActionSheet: $vmUpdateProfil.isShowPickerAlert, onGalleryAction :{
                
                vmUpdateProfil.isShowSheet = true
                vmUpdateProfil.isCameraPicker = false
                vmUpdateProfil.isGalleryPicker = true
                
            }, onCameraAction: {
                
                vmUpdateProfil.isShowSheet = true
                vmUpdateProfil.isGalleryPicker = false
                vmUpdateProfil.isCameraPicker = true
                
            })
            
        }
        .fullScreenCover(isPresented: $vmUpdateProfil.isShowSheet, onDismiss : {
            vmUpdateProfil.isShowSheet = false
            vmUpdateProfil.isGalleryPicker = false
            vmUpdateProfil.isCameraPicker = false
            onPickerDismiss()
        }, content: {
            if (vmUpdateProfil.isGalleryPicker) {
                MediaPickerView(sourceType: .photoLibrary, pickedImage: self.$vmUpdateProfil.pickedImage, isImagePicked: $vmUpdateProfil.isImagePicked)
            } else if (vmUpdateProfil.isCameraPicker) {
                MediaPickerView(sourceType: .camera, pickedImage: self.$vmUpdateProfil.pickedImage, isImagePicked: $vmUpdateProfil.isImagePicked)
            }
            
        })
        .onAppear {
            hideKeyboard()
            self.NC.addObserver(forName: NSNotification.profileUpdated, object: nil, queue: nil,
                                   using: self.profileUpdated)
        }
    }
    
    func onPickerDismiss() {
        if(vmUpdateProfil.isImagePicked){
            vmUpdateProfil.updateImageProfile { success in
                vmUpdateProfil.isImagePicked = false
            }
        }
        //updateBarVM.uploadPhoto(image: self.pickedImage, order: updateBarVM.mediaClikedIndex+1)
        //vmUpdateProfil.isImagePicked = false
    }
    
    func profileUpdated(_ notification: Notification) {
        vmUpdateProfil.refreshProfile()
    }
}

struct UpdateProfileView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateProfileView(showProfile: .constant(false))
    }
}
