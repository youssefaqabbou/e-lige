//
//  UpdateProfileViewModel.swift
//  elige
//
//  Created by user196417 on 11/8/21.
//

import Foundation
import UIKit
import Fakery


class UpdateProfileViewModel: ObservableObject {
    
    private var apiService: APIService = APIService()
    
    @Published var isShowPickerAlert: Bool = false
    
    @Published var isGalleryPicker: Bool = false
    @Published var isCameraPicker: Bool = false
    
    @Published var isShowSheet = false
    
    @Published var pickedImage = UIImage()
    @Published var isImagePicked = false
    
    @Published var currentUser = UserModel()
    @Published var fullname: String = ""
    @Published var email: String = ""
    @Published var phone: String = ""
    @Published var password: String = ""
    @Published var oldPassword: String = ""
    @Published var confirmPassword: String = ""
    
    @Published var fullnameError: String = ""
    @Published var emailError: String = ""
    @Published var phoneError: String = ""
    @Published var passwordError: String = ""
    @Published var oldPasswordError: String = ""
    @Published var confirmPasswordError: String = ""
    
    @Published var isLoading : Bool = false
    @Published var isLoadingPwd : Bool = false
    @Published var isUploading : Bool = false
    
    @Published var toUpdateEmail : Bool = false
    
    @Published var customerAddress = ""
    @Published var customerAddressError: String = ""
    
    @Published var customerLatitude = 0.0
    @Published var customerLongitude = 0.0
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
        fullname = currentUser.fullname
        email = currentUser.email
        phone = currentUser.phone
        customerAddress = currentUser.address
        customerLatitude = currentUser.latitude
        customerLongitude = currentUser.longitude
    }
    
    func refreshProfile(){
        currentUser = AppFunctions.getConnectedUser()
        fullname = currentUser.fullname
        email = currentUser.email
        phone = currentUser.phone
        customerAddress = currentUser.address
        customerLatitude = currentUser.latitude
        customerLongitude = currentUser.longitude
    }
    
    
    var isFormComplete: Bool {
        
        var isFullnameValid = false
        var fullNameError = ""
        if(currentUser.role == "SALON"){
            (isFullnameValid, fullNameError) = ValidatorHelper.validateSocialReason(socialReason: fullname)
            self.fullnameError = fullNameError
        }else{
            (isFullnameValid, fullNameError) = ValidatorHelper.validateFullname(fullname: fullname)
            self.fullnameError = fullNameError
        }
        
        let (isEmailValid, emailError) = ValidatorHelper.validateEmail(email: email)
        self.emailError = emailError

        let (isPhoneValid, phoneError) = ValidatorHelper.validatePhone(phone: phone)
        self.phoneError = phoneError

        return isFullnameValid && isEmailValid && isPhoneValid
    }
    
    var isFormPasswordComplete: Bool {
        let (isOldPasswordValid, oldPasswordError) =  ValidatorHelper.validatePassword(password: oldPassword)
        self.oldPasswordError = oldPasswordError
        
        let (isPasswordValid, passwordError) =  ValidatorHelper.validatePassword(password: password)
        self.passwordError = passwordError
        
        let (isConfirmPasswordValid, confirmPasswordError) = ValidatorHelper.validatePasswordConfirmation(password: password, passwordConfirm: confirmPassword)
        self.confirmPasswordError = confirmPasswordError


        return isOldPasswordValid && isPasswordValid && isConfirmPasswordValid
    }
    
    
    func updateAccount() {
        self.isLoading = true
       
        let params = ["fullname": fullname,
                      "email": email,
                      "phone": phone,
                      "address": customerAddress,
                      "latitude": String(customerLatitude),
                      "longitude": String(customerLongitude)]
        apiService.updateAccount(idAccount: currentUser.id, params: params) { message in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .success , message: message)
        } onFailure: { errorMessage in
            self.isLoading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func updatePassword() {
        
        self.isLoadingPwd = true
        let params = ["oldPassword": oldPassword, "newPassword" : password]
        
        apiService.changePassword(params: params) { message in
            self.isLoadingPwd = false
            AppFunctions.showSnackBar(status: .success , message: message)
        } onFailure: { errorMessage in
            self.isLoadingPwd = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
        }
    }
    
    func updateImageProfile(onSuccess: @escaping (_ success: Bool) -> Void) {
        
        self.isUploading = true
        
        let params = [
            "type": "PROFILE"
        ] as [String : Any]
        
        apiService.uploadImageProfile(image: pickedImage, params: params) { success in
            
            self.isUploading = false
            self.pickedImage = UIImage()
            self.isImagePicked = false
            onSuccess(true)
        } onFailure: { errorMessage in
            self.isUploading = false
            AppFunctions.showSnackBar(status: .error , message: errorMessage)
            onSuccess(false)
            self.pickedImage = UIImage()
            self.isImagePicked = false
        }
    }
    
}

