//
//  SettingView.swift
//  elige
//
//  Created by user196417 on 11/24/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct SettingView: View {
    
    @StateObject var vmSetting = SettingViewModel()
    
    @Binding var showSetting : Bool
    @State var showProfile = false
    
    let NC = NotificationCenter.default
    
    var body: some View {
        
        ZStack{
            
            VStack(alignment: .leading, spacing: 15){
                
                HStack{
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .overlay(
                            Image.iconArrowBack
                                .resizable()
                                .frame(width: AppConstants.backIconWidth, height: AppConstants.backIconHeight)
                        ).onTapGesture {
                            withAnimation {
                                showSetting = false
                            }
                        }
                    
                    Spacer()
                    
                    Text("Paramètres")
                        .textStyle(TitleStyle())
                    
                    Spacer()
                    
                    Circle().fill(Color.secondaryColor)
                        .frame(width: AppConstants.backCircleWidth, height: AppConstants.backCircleWidth, alignment: .center)
                        .hidden()
                    
                }
                
                ScrollView(.vertical, showsIndicators: false){
                    VStack(alignment: .leading){
                        
                        if AppFunctions.checkIfUserConnected(){
                            HStack(spacing: 20){
                                
                                WebImage(url: URL(string: vmSetting.currentUser.image.url))
                                    .placeholder {
                                        Image.placeholderUser.resizable()
                                    }
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: 70, height: 70)
                                    .clipShape(Circle())
                                
                                VStack(alignment: .leading, spacing: 5){
                                    
                                    Text(vmSetting.currentUser.fullname)
                                        .font(.system(weight: .regular, size: 20))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                    Text(vmSetting.currentUser.phone)
                                        .font(.system(weight: .regular, size: 14))
                                        .foregroundColor(Color.secondaryTextColor)
                                }
                                
                                Spacer()
                                
                            }.padding(.top, AppConstants.viewSmallMargin)
                        }else{
                            HStack(spacing: 20){
                                
                                WebImage(url: URL(string: vmSetting.currentUser.image.url))
                                    .placeholder {
                                        Image.placeholderUser.resizable()
                                    }
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: 70, height: 70)
                                    .clipShape(Circle())
                                
                                VStack(alignment: .leading, spacing: 5){
                                    
                                    Text("invité")
                                        .font(.system(weight: .regular, size: 20))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                    
                                }
                                
                                Spacer()
                                
                            }.padding(.top, AppConstants.viewSmallMargin)
                        }
                        
                        if AppFunctions.checkIfUserConnected(){
                            VStack(alignment: .leading, spacing: 30){
                                
                                Divider().frame(width: 200, height: 1, alignment: .center)
                                    .background(Color.secondaryTextColor)
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconProfil
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("Profil")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }.onTapGesture {
                                    withAnimation {
                                        showProfile = true
                                    }
                                }
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconEvaluer
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("Évaluer E-lige")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconApropos
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("À propos")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconFAQs
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("FAQ")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                                Button(action: {
                                    withAnimation {
                                        vmSetting.showDeleteConfirmation = true
                                    }
                                }, label: {
                                    
                                    HStack(spacing: 25){
                                        
                                        Image.iconDisconnect
                                            .resizable()
                                            .frame(width: 30, height: 30)
                                        
                                        Text("Déconnexion")
                                            .font(.system(weight: .bold, size: 18))
                                            .foregroundColor(Color.primaryTextColor)
                                    }
                                })
                            }.padding()
                        }else{
                            VStack(alignment: .leading, spacing: 30){
                                
                                Divider().frame(width: 200, height: 1, alignment: .center)
                                    .background(Color.secondaryTextColor)
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconEvaluer
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("Évaluer E-lige")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconApropos
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("À propos")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                                
                                HStack(spacing: 25){
                                    
                                    Image.iconFAQs
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                    
                                    Text("FAQ")
                                        .font(.system(weight: .bold, size: 18))
                                        .foregroundColor(Color.primaryTextColor)
                                    
                                }
                                
                            }.padding()
                        }
                    }
                }
            }
            .padding(.horizontal, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewVeryExtraMargin)
            .padding(.top, AppConstants.viewNormalMargin)
            .padding(.bottom, AppConstants.viewExtraMargin)
            
            if(showProfile){
                UpdateProfileView(showProfile: $showProfile).transition(.move(edge: .trailing)).animation(.linear, value: showProfile).zIndex(1)
            }
            
        }
        .background(Color.bgViewColor)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            self.NC.addObserver(forName: NSNotification.profileUpdated, object: nil, queue: nil,
                                using: self.profileUpdated)
        }
    
        .bottomSheet(isPresented: $vmSetting.showDeleteConfirmation, height: 400, topBarCornerRadius: 45, showTopIndicator: false) {
            CustomConfirmationView(isCustomIcon: true, customIcon: Image.iconAlertDisconnect, title: LocalizationKeys.title_logout.localized, message: LocalizationKeys.message_logout.localized, positiveAction: {
                
                UserDefaults.disconnect()
                
            }, isNegative: true, negativeAction: {
                withAnimation {
                    vmSetting.showDeleteConfirmation = false
                }
            })
        }
    }
    
    func profileUpdated(_ notification: Notification) {
        vmSetting.refreshProfile()
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        SettingView(showSetting: .constant(false))
    }
}
