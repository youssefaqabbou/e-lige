//
//  SettingViewModel.swift
//  elige
//
//  Created by user196417 on 12/9/21.
//

import Foundation

class SettingViewModel: ObservableObject {
    
    @Published var currentUser = AppFunctions.getConnectedUser()
    
    @Published var showDeleteConfirmation = false
    
    init(){
        currentUser = AppFunctions.getConnectedUser()
    }
    
    func refreshProfile(){
        currentUser = AppFunctions.getConnectedUser()
    }
}
