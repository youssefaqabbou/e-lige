//
//  AppleDelegate.swift
//  Homeat
//
//  Created by macbook pro on 23/6/2021.
//

import Foundation
import AuthenticationServices
import HCalendar

struct AppleUser: Codable {
    let userId: String
    let firstName: String
    let lastName: String
    
    init?(credentials: ASAuthorizationAppleIDCredential) {
        guard
            let firstName = credentials.fullName?.givenName,
            let lastName = credentials.fullName?.familyName
        else { return nil }
        
        self.userId = credentials.user
        self.firstName = firstName
        self.lastName = lastName
    }
}


class AppleDelegate: NSObject, ASAuthorizationControllerDelegate {
    
    var loginViewModel: SignInViewModel
        
        init(loginVM: SignInViewModel) {
            self.loginViewModel = loginVM
        }
    
    // Shows Sign in with Apple UI
        func handleAuthorizationAppleIDButtonPress() {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
    
    
    func authorizationController(controller: ASAuthorizationController,
                                 didCompleteWithAuthorization authorization: ASAuthorization) {
        DebugHelper.debug("authorizationController")
        switch authorization.credential {
        
        case let appleIdCredentials as ASAuthorizationAppleIDCredential:
            DebugHelper.debug("ASAuthorizationAppleIDCredential")
            DebugHelper.debug("Token", String(data: appleIdCredentials.identityToken!, encoding: .utf8))
            if let appleUser = AppleUser(credentials: appleIdCredentials),
               let appleUserData = try? JSONEncoder().encode(appleUser) {
                UserDefaults.standard.setValue(appleUserData, forKey: appleUser.userId)
                
                DebugHelper.debug("Saved apple user", appleUser)
               
                NotificationHelper.postAppleResult(status: true, identityToken: String(data: appleIdCredentials.identityToken!, encoding: .utf8)!)
                
            } else {
                guard
                    let appleUserData = UserDefaults.standard.data(forKey: appleIdCredentials.user),
                    let appleUser = try? JSONDecoder().decode(AppleUser.self, from: appleUserData)
                else {
                    DebugHelper.debug("Apple Problem")
                    NotificationHelper.postAppleResult(status: false, identityToken: "")
                    return
                }
                
                DebugHelper.debug("logged apple user", appleUser)
                NotificationHelper.postAppleResult(status: true, identityToken: String(data: appleIdCredentials.identityToken!, encoding: .utf8)!)
            }
            
            break
            
        case let passwordCredential as ASPasswordCredential:
            DebugHelper.debug("\n ** ASPasswordCredential ** \n" , passwordCredential)
            //signinWithUserNamePassword(credential: passwordCredential)
            break
            
        default:
            DebugHelper.debug("\n ** default ** \n")
            NotificationCenter.default.post(name: NSNotification.AppleSignIn, object: nil, userInfo: ["success" :  false])
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        DebugHelper.debug("ASAuthorizationControllerDelegate" , error)
        NotificationCenter.default.post(name: NSNotification.AppleSignIn, object: nil, userInfo: ["success" :  false])
    }
    
}

