//
//  BarberModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation

struct BarberModel: Codable, Identifiable {
    var id : Int = -1
    var firstName :String = ""
    var lastName : String = ""
    var email : String = ""
    var phone : String = ""
    var image : MediaModel = MediaModel()
}
