//
//  BarberModel.swift
//  elige
//
//  Created by macbook pro on 4/11/2021.
//

import Foundation
import MapKit

struct BarberShopModel: Codable, Identifiable {
    var id : Int = -1
    var name :String = ""
    var address : String = ""
    var zipcode : String = ""
    var city : String = ""
    var country : String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var phone : String = ""
    var description = ""
    var email: String = ""
    var image : MediaModel = MediaModel()
    var rating : Float = 0.0
    var distance : Float = 0.0
    var isFavorite : Bool = false
    var isBlocked : Bool = false
    var canReview : Bool = false
    
    var countService : Int = 0
    var services : [ServiceModel] = [ServiceModel]()
    var countReview : Int = 0
    var reviews : [ReviewModel] = [ReviewModel]()
    var countmedia : Int = 0
    var medias : [MediaModel] = [MediaModel]()
    
    var tags : [TagModel] = [TagModel]()
    var schedules : [ScheduleModel] = [ScheduleModel]()
    
    var owner = UserModel()
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(){
        
    }

}
