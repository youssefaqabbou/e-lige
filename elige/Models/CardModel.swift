//
//  CardModel.swift
//  elige
//
//  Created by macbook pro on 29/11/2021.
//

import Foundation

struct CardModel: Codable, Identifiable{
    
    var id = UUID()
    var isChecked: Bool = false
    var cartType: String = ""
    var name: String = ""
    var dateMonth : Int = -1
    var dateYear : Int = -1
    var last4 : String = ""
    var code: String = ""
    var paymentId  : String = ""
    
}
