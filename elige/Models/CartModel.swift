//
//  CartModel.swift
//  elige
//
//  Created by macbook pro on 22/11/2021.
//

import Foundation

struct CartModel: Identifiable {
    var id : Int = -1
    
    var orderDate : Date = AppFunctions.getTodayDateWithTZ()
    var services = [ServiceModel]()
    var selectedCard = CardModel()
    var fees : Double = AppConstants.barberFees
    var serviceLocation : String = ServiceLocation.shop.rawValue
}
