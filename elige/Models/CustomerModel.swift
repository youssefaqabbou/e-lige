//
//  Customer.swift
//  elige
//
//  Created by macbook pro on 29/11/2021.
//

import Foundation


struct CustomerModel: Identifiable {
    var id : Int = -1
    var firstName : String = ""
    var lastName : String = ""
    var image : MediaModel = MediaModel()
    var phone : String = ""
}


struct ExpendableCustomerModel: Identifiable {
    let id : UUID = UUID()
    var header : String = ""
    var items : [CustomerModel] =  [CustomerModel]()
}
