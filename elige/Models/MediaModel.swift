//
//  MediaModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation

class MediaModel: NSObject, Codable, Identifiable {
    
    var id: Int = -1
    var type: String = ""
    var originalName: String = ""
    var size: String = ""
    var url: String = ""
    var order: Int = -1
    var isUploading = false
    
    override init() {
        
    }
    
    init(id: Int, type: String, originalName: String, size: String, url: String, order: Int) {
        
        self.id = id
        self.type = type
        self.originalName = originalName
        self.size = size
        self.url = url
        self.order = order
    }
    
}
