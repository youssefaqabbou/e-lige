//
//  NotificationModel.swift
//  elige
//
//  Created by user196417 on 1/13/22.
//

import Foundation


struct NotificationModel: Codable, Identifiable {
    
    var id : Int = -1
    var type : String = ""
    var title : String = ""
    var message : String = ""
    var createdAt : Date = Date()
    
}
