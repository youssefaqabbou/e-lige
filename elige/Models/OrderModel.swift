//
//  OrderModel.swift
//  elige
//
//  Created by macbook pro on 22/11/2021.
//

import Foundation


struct OrderModel: Codable, Identifiable, Equatable{
    
    static func == (lhs: OrderModel, rhs: OrderModel) -> Bool {
        return lhs.id == rhs.id ? true : false
    }
    
    var id : Int = -1
    var number : String = ""
    var status : String = OrderStatus.inProgress.rawValue
    var totalPrice: Double = 0
    var subTotal: Double = 0
    var note : String = ""
    var currency : String = ""
    var fees: Double = 0
    //var start : String = ""
    var duration : Int = 0
    var barberShop = BarberShopModel()
    var customer = UserModel()
    
    var paymentIntent : String = ""
    var paymentStatus = PaymentStatus.pending.rawValue

    var createdDate: Date = Date()
    var orderDate: Date = Date()
    //var orderTime: Int = 0
   
    var orderLines = [OrderLinesModel]()

    var paymentCard = CardModel()
    var orderLocation : String = ServiceLocation.shop.rawValue
}

struct OrderLinesModel: Codable, Identifiable, Equatable{
    
    var id : Int = -1
    var quantity : Int = 0
    var price : Double = 0
    
    var service = ServiceModel()
}

enum OrderStatus: String {
    case canceled = "cancelled"
    case noShow = "no_show"
    case validate = "validated"
    case finished = "finished"
    case inProgress = "in_progress"
}

enum PaymentStatus: Int {
    case pending = 0
    case succeded = 1
    case canceled = 2
    case failed = 3
}
