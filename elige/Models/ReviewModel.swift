//
//  ReviewModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation

class ReviewModel: NSObject, Codable, Identifiable {
    
    var id : Int = -1
    var content :String = ""
    var dateCreated : Date = Date()
    var note : Int = 0
    var owner : UserModel = UserModel()
    
    override init() {
        
    }
    
    init(id: Int, content: String, dateCreated: Date, note: Int, owner: UserModel) {
        
        self.id = id
        self.content = content
        self.dateCreated = dateCreated
        self.note = note
        self.owner = owner
        
    }
    
}


