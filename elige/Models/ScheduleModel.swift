//
//  ScheduleModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation

class ScheduleModel: NSObject, Codable, Identifiable {
    
    var id : Int = -1
    var day : Int = -1
    var openTime : String = ""
    var closeTime : String = ""
    var isOpen : Bool = false
    var haveBreak : Bool = false
    //var breakTime : ScheduleBreakModel = ScheduleBreakModel()
    var breakFrom : String = ""
    var breakTo : String = ""
    var wrongAttempt : Int = 0
    
    var openTimeUpdated : Bool = false
    var closeTimeUpdated : Bool = false
    var breakFromUpdated : Bool = false
    var breakToUpdated : Bool = false
    
    override init() {
        
    }
    
}


class AvailableScheduleModel: NSObject, Codable, Identifiable {
    
    var id : UUID = UUID()
    var startAt : Date = Date()
    var endAt : Date = Date()
    var duration : Int  = 0
    
    override init() {
        super.init()
    }
}
