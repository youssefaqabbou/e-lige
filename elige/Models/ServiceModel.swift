//
//  ServiceModel.swift
//  elige
//
//  Created by macbook pro on 10/11/2021.
//

import Foundation

class ServiceModel:  Codable, Identifiable, Equatable{
    
    static func == (lhs: ServiceModel, rhs: ServiceModel) -> Bool {
        return lhs.id == rhs.id ? true : false
    }
    var id : Int = -1
    var name: String = ""
    var price : Double = 0.0
    var duration : Int = 0
    var description = ""
    //var image : MediaModel = MediaModel()
    var category = TagModel()
    var isEnabled : Bool = false
    var quantity: Int = 1
    var totalPrice : Double = 0.0
    var serviceLocation : String = ServiceLocation.shop.rawValue
}
                


enum ServiceLocation: String {
    case shop = "SALON"
    case home = "HOME"
    case both = "BOTH"
}
