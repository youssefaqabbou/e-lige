//
//  TagModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation
import SwiftUI

class TagModel : NSObject, Codable, Identifiable{
    
    var id : Int = -1
    var name : String = ""
    var descr : String = ""
    var icon : String = ""
    var order : Int = 0
    var size : CGFloat = 0
    var position : Int = 0
    var isSelected : Bool = false
    
    override init() {
        
    }
    
    init(id: Int, name: String, descr: String, icon: String, order: Int, size: CGFloat, position: Int) {
        
        self.id = id
        self.name = name
        self.descr = descr
        self.icon = icon
        self.order = order
        self.size = size
        self.position = position
        
    }
    
}


