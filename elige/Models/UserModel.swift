//
//  UserModel.swift
//  elige
//
//  Created by macbook pro on 12/11/2021.
//

import Foundation

class UserModel: NSObject, Codable, Identifiable {
    
    var id : Int = -1
    var fullname :String = ""
    var address : String = ""
    var zipcode : String = ""
    var city : String = ""
    var country : String = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var phone : String = ""
    var descr : String = ""
    var email: String = ""
    var image : MediaModel = MediaModel()
    var role: String = ""
    var isVerified: Bool = true
    var isBlocked: Bool = false
    var orders: [OrderModel] =  [OrderModel]()
    var accountId: String = ""
    
    override init() {
        
    }
    
    init(id: Int, fullname: String, address: String, zipcode: String, city: String, country: String, lat: Double, lng: Double, phone: String, descr: String, email: String, image: MediaModel, role: String, isVerified: Bool) {
        
        self.id = id
        self.fullname = fullname
        self.address = address
        self.zipcode = zipcode
        self.city = city
        self.country = country
        self.latitude = lat
        self.longitude = lng
        self.phone = phone
        self.descr = descr
        self.email = email
        self.image = MediaModel()
        self.role = role
        self.isVerified = isVerified
    }
    
}

struct ExpendableUserModel: Identifiable {
    let id : UUID = UUID()
    var header : String = ""
    var items : [UserModel] =  [UserModel]()
}
