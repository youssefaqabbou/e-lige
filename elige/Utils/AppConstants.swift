//
//  AppConstants.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import Foundation

import SwiftUI

struct AppConstants {
    
    static var stripePublishableKey : String = "pk_test_51JwOCvFh9KpAF0Jn1abkHFFb6TWTlSLOh6KCxBk1KpSu3hqxanMFqVpd7aCCy2WGKYQiG00vqnnLdKyYSb1G3c4T00bcB4F8av"
    
    static var barberFees : Double = 9.0
    static var homeFees : Double = 9.0
    
    static var phoneIndicator = "+212"
    static var distance : Float = 150
    
    // TODO: fixed Blur
    static var blurValue : CGFloat = 20
    
    /// Animation
    static var animList = AnimationFactory.move(yOffset: UIScreen.main.bounds.height / 2, duration : 0.7)
    
    /// Pagination
    static var offsetPagination = -2
    
    /// Dimensions

    // View
    static var viewRadius : CGFloat = 45
    static var viewTinyMargin : CGFloat = 2
    static var viewVerySmallMargin : CGFloat = 5
    static var viewSmallMargin : CGFloat = 10
    static var viewNormalMargin : CGFloat = 15
    static var viewExtraMargin : CGFloat = 20
    static var viewVeryExtraMargin : CGFloat = 30

    // icons
    static var backCircleWidth : CGFloat = 50
    static var backIconWidth : CGFloat = 18
    static var backIconHeight : CGFloat = 15

    // Button
    static var buttonRadius : CGFloat = 15
    static var buttonMargin : CGFloat = 5
    
    // Alert
    static var shadowRadiusValue : CGFloat = 25
    static var alertRadius : CGFloat = 25
    
   
    
}
