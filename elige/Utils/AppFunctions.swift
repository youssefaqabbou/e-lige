//
//  AppFunctions.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import Foundation
import Alamofire
import SwiftUI
import TTGSnackbar
import SwiftyJSON
import Stripe

enum ResultStatus {
    case success
    case info
    case error
}


class AppFunctions {
    
    
    static func getHeader() -> HTTPHeaders {
        var headers: HTTPHeaders = ["Content-Type": "application/json"]
        if(UserDefaults.token != "") {
            headers.add(name: "Authorization", value: "bearer \(UserDefaults.token)")
        }
        
        return headers
    }
    
    static func sendTokenToServer(){
        DebugHelper.debug("sendTokenToServer")
        if(checkIfCanSendToken()){
            DebugHelper.debug("send Token To Server")
            APIService().setDeviceToken(idAccount: getConnectedUser().id)
        }
    }
    static func checkIfCanSendToken()-> Bool{
        if(!UserDefaults.token.isEmpty && !UserDefaults.deviceToken.isEmpty && !UserDefaults.deviceToken.elementsEqual(UserDefaults.deviceTokenSent)){
            if( getConnectedUser().id != -1){
                return true
            }
        }
        DebugHelper.debug("false")
        return false
    }
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func getCardImage(type: String) -> String {
        
        switch(type) {
        case "visa":
            return "ic_visa_card"
        case "mastercard":
            return "ic_mastercard_card"
        case "maestro":
            return "ic_maestro_card"
        default:
            return "ic_noCard"
        }
    }
    
    static func getCardLogo(by type: String) -> String {
        switch(type) {
        case "visa":
            return "ic_visa"
        case "mastercard":
            return "ic_mastercard"
        case "maestro":
            return "ic_maestro"
        default:
            return "ic_noCard"
        }
    }
    
    static func getCardLogo(from number: String) -> String {
        let brandName = STPCardValidator.brand(forNumber: number)
        switch brandName {
        case STPCardBrand.visa:
            return "ic_visa"
        case STPCardBrand.mastercard:
            return "ic_mastercard"
        default:
            return "ic_noCard"
        }
    }
    
    static func getCardColor(type: String) -> Color {
        
        switch(type) {
        case "visa":
            return Color(hex: "#09549A")
        case "mastercard":
            return Color(hex: "#F7371A")
        case "maestro":
            return Color(hex: "#0D63C5")
        default:
            return Color(hex: "#232323")
        }
    }
    
    static func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map { _ in letters.randomElement()! })
    }
    
    static func getReservationStatusLabel(status: String, name: String) -> String {
        switch status {
        case OrderStatus.inProgress.rawValue:
            return "Merci \(name),"
        case OrderStatus.canceled.rawValue:
            return "Navré \(name),"
        case OrderStatus.validate.rawValue:
            return "Félicitations \(name),"
        case OrderStatus.finished.rawValue:
            return "Félicitations \(name),"
        case OrderStatus.noShow.rawValue:
            return "\(name),"
        default:
            return "Merci \(name),"
        }
    }
    
    static func getReservationStatusLabel(status: String) -> String {
        switch status {
        case OrderStatus.inProgress.rawValue:
            return "votre réservation a bien été enregistrée"
        case OrderStatus.canceled.rawValue:
            return "votre réservation a été annulée"
        case OrderStatus.validate.rawValue:
            return "votre réservation a bien été confirmée"
        case OrderStatus.finished.rawValue:
            return "le service a bien été terminé"
        case OrderStatus.noShow.rawValue:
            return "vous avez manqué votre réservation"
        default:
            return "votre réservation a bien été enregistrée"
        }
    }
    
    static func getOrderStatusLabel(status: String) -> String {
        switch status {
        case OrderStatus.inProgress.rawValue:
            return "En cours"
        case OrderStatus.canceled.rawValue:
            return "Annulée"
        case OrderStatus.validate.rawValue:
            return "Confirmée"
        case OrderStatus.finished.rawValue:
            return "Terminée"
        case OrderStatus.noShow.rawValue:
            return "Absent"
        default:
            return "En cours de traitement"
        }
    }
    
    static func getOrderStatusColor(status: String) -> Color {
        switch status {
        case OrderStatus.inProgress.rawValue:
            return Color(hex: "#EEBB2A")
        case OrderStatus.canceled.rawValue:
            return Color(hex: "#C70039")
        case OrderStatus.validate.rawValue:
            return Color(hex: "#3AA26E")
        case OrderStatus.finished.rawValue:
            return Color(hex: "#235A8C")
        case OrderStatus.noShow.rawValue:
            return Color(hex: "#892e2e")
        default:
            return Color(hex: "#EEBB2A")
        }
    }
    
    static func secondsToHoursMinutesSeconds(_ seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func minutesToHoursMinutes(_ minutes: Int) -> (Int, Int) {
        return ((minutes % 3600) / 60, (minutes % 3600) % 60)
    }
    
    static func getOrderServices(order: OrderModel) -> String {
        var servicesNames = ""
        for item in order.orderLines {
            servicesNames.append(item.service.name)
            if(order.orderLines.last != item) {
                servicesNames.append(" • ")
            }
        }
        return servicesNames
    }
    
    static func decodeBarberShop() -> BarberShopModel {
        do {
            let barberShopJson = UserDefaults.barberShopJson
            let barberShopData = barberShopJson.data(using: .utf8)!
            let jsonDecoder = JSONDecoder()
            return try jsonDecoder.decode(BarberShopModel.self, from: barberShopData)
        } catch {
            DebugHelper.debug("catch")
        }
        return BarberShopModel()
    }
    
    static func getConnectedUser() -> UserModel {
        
        if let data = UserDefaults.user.data(using: .utf8) {
            
            if let json = try? JSON(data: data) {
                
                let user: UserModel = JsonHelper.getUser(item: json.dictionary!)
                
                return user
            }
        }
        return UserModel()
    }
    
    static func checkIfUserConnected()-> Bool{
        let user = getConnectedUser()
        if(!UserDefaults.token.isEmpty && user.id != -1){
            //DebugHelper.debug("User connected")
          return true
        }
        //DebugHelper.debug("User not connected")
        return false
      }

    
    static func encodeBarberShop(barberShop: BarberShopModel) {
        do {
            let jsonData = try JSONEncoder().encode(barberShop)
            let shopJson = String(data: jsonData, encoding: String.Encoding.utf8)
            // DebugHelper.debug("cartJson String : " + cartJson!)
            UserDefaults.barberShopJson = shopJson!
            
            NotificationHelper.postBarberShop(barberShop: barberShop)
            //NotificationCenter.default.post(name: NSNotification.shopUpdated, object: nil, userInfo: ["barberShop": barberShop])
        } catch {
        }
    }
    
    static func updateConnectedUser(user: UserModel) {
        do {
            let jsonData = try JSONEncoder().encode(user)
            let userJson = JSON(jsonData).rawString([.jsonSerialization: JSONSerialization.WritingOptions.prettyPrinted, .encoding: String.Encoding.utf8])
            //DebugHelper.debug(userJson)
            UserDefaults.user = userJson!
            
            NotificationCenter.default.post(name: NSNotification.profileUpdated,
                                            object: nil, userInfo: ["user": user])
        } catch {
        }
    }
    
    static func getSavedCategories() -> [TagModel] {
        
        if let data = UserDefaults.categories.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                let categories = JsonHelper.getCategories(items: json.array!)
                return categories
            }
        }
        return [TagModel]()
    }
    
    static func getFullName(customer: CustomerModel) -> String {
        return customer.firstName + " " + customer.lastName
    }
    
    static func getUrlWithParams(url: String, parameters: [String: String]?) -> String {
        var urlComps = URLComponents(string: url)!
        if(parameters != nil) {
            var queryItems = [URLQueryItem]()
            for (param, value) in parameters! {
                let queryItem = URLQueryItem(name: param, value: value)
                queryItems.append(queryItem)
            }
            urlComps.queryItems = queryItems
        }
        let stringUrl: String = urlComps.url!.absoluteString
        return String(stringUrl)
    }
    
   
    static func showSnackBar(status: ResultStatus, message: String, buttonText: String = "Ok") {
        
        let snackbar = TTGSnackbar(message: message, duration: .long)
        switch status {
        case .success:
            snackbar.backgroundColor = UIColor(Color.snackBarSuccess)
        case .info:
            snackbar.backgroundColor = UIColor(Color.snackBarInfo)
        case .error:
            snackbar.backgroundColor = UIColor(Color.snackBarError)
            
        }
        snackbar.messageTextFont = UIFont.init(name: FontNameManager.BrownStd.regular, size: 14)!
        snackbar.actionText = buttonText
        snackbar.actionTextColor = UIColor.white
        snackbar.actionTextNumberOfLines = 2
        snackbar.separateViewBackgroundColor = UIColor.white
        snackbar.contentInset = UIEdgeInsets.init(top: 10, left: 4, bottom: 10, right: 4)
        snackbar.leftMargin = 12
        snackbar.rightMargin = 12
        snackbar.bottomMargin = 12
        snackbar.actionBlock = { (snackbar) in
            NSLog("Button clicked !")
            snackbar.dismiss()
        }
        snackbar.show()
    }
    
    static func getCategoriesId(categories: [TagModel]) -> String {
        var ids = ""
        for category in categories {
            ids.append(String(category.id))
            if(categories.last != category) {
                ids.append(";")
            }
        }
        return ids
    }
    
    static func getCategoriesText(categories: [TagModel]) -> String {
        //DebugHelper.debug("categories", categories.count)
        var categoriesText = ""
        for category in categories {
            categoriesText.append(category.name)
            if(categories.last != category) {
                categoriesText.append(" ; ")
            }
        }
        return categoriesText
    }
    
    static func timeToHoursMinutes(_ time: String) -> (Int, Int) {
        if(time.isEmpty) {
            return (0, 0)
        }
        let components = time.split { $0 == ":" } .map { (x) -> Int in return Int(String(x))! }
        if(components.count != 2) {
            return (0, 0)
        }
        let hours = components[0]
        let minutes = components[1]
        //DebugHelper.debug(hours, minutes)
        return (hours, minutes)
    }
    
    static func getTagSize(text: String, size: CGFloat) -> CGFloat{
            let font = UIFont.systemFont(ofSize: size)
            let attributes = [NSAttributedString.Key.font: font]
            
            let size = (text as NSString).size(withAttributes: attributes)
            return size.width
    }
    
    static func getCurrentScheudle(schedules : [ScheduleModel], currentDay : Int = AppFunctions.getTodayDateWithTZ().weekday) -> ScheduleModel{
        //let currentDay = AppFunctions.getTodayDateWithTZ().weekday
        //DebugHelper.debug(currentDay)
        for schedule in schedules{
            if(currentDay == schedule.day){
                return schedule
            }
        }
        return ScheduleModel()
    }
    
    static func getTodayDateWithTZ() -> Date {
        let currentDate = Date()
        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        let epochDate = currentDate.timeIntervalSince1970
        let timezoneEpochOffset = (epochDate + Double(timezoneOffset))
        return Date(timeIntervalSince1970: timezoneEpochOffset)
    }
    
    
    static func getTodayDateUTC() -> Date {
        let currentDate = Date()
        let epochDate = currentDate.timeIntervalSince1970
        let timezoneEpochOffset = (epochDate)
        return Date(timeIntervalSince1970: timezoneEpochOffset)
    }
    
    static func convertDateToUTC(currentDate : Date) -> Date {

        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        DebugHelper.debug("timezoneOffset", timezoneOffset)
        let epochDate = currentDate.timeIntervalSince1970
        DebugHelper.debug("epochDate", epochDate)
        let timezoneEpochOffset = (epochDate - Double(timezoneOffset))
        DebugHelper.debug("timezoneEpochOffset", timezoneEpochOffset)
        return Date(timeIntervalSince1970: timezoneEpochOffset)
    }
    
    static func convertDateToCurrent(currentDate : Date) -> Date {

        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        //DebugHelper.debug("timezoneOffset", timezoneOffset)
        let epochDate = currentDate.timeIntervalSince1970
        //DebugHelper.debug("epochDate", epochDate)
        let timezoneEpochOffset = (epochDate + Double(timezoneOffset))
        //DebugHelper.debug("timezoneEpochOffset", timezoneEpochOffset)
        return Date(timeIntervalSince1970: timezoneEpochOffset)
    }
    
    
}

public enum WeekdaySymbolsEnum {
    case normal // -> ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    case short // -> ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    case veryShort // -> ["S", "M", "T", "W", "T", "F", "S"]
}

