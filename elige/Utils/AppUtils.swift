//
//  AppUtils.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import Foundation
import SwiftUI


class AppUtils {
 
    struct CustomCorners: Shape {

        var corners: UIRectCorner
        var radius: CGFloat
        
        func path(in rect: CGRect) -> Path {
            
            let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            
            return Path(path.cgPath)
        }
        
    }
    
    struct Triangle: Shape {
        func path(in rect: CGRect) -> Path {
            var path = Path()

            path.move(to: CGPoint(x: rect.minX , y: rect.minY))
            path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.maxX , y: rect.maxY))

            return path
        }
    }
    
    struct MovePath {
        
        var p1: CGPoint
        var p2: CGPoint
        
        func path() -> Path {
            let l1 = CGPoint(x: p1.x + ((p2.x - p1.x) / 1.2), y: p2.y * 2)
            
            //let c1 = CGPoint(x: l1.x, y: (l1.y + p2.y)/2)
            
            let c2 = CGPoint(x: p2.x, y: l1.y / 2)
            
            var result = Path()
            result.move(to: p1)
            result.addLine(to: l1)
            //result.addQuadCurve(to: p2, control: c2)
            result.addCurve(to: p2, control1: c2, control2: c2)
            return result
        }
    }

}
