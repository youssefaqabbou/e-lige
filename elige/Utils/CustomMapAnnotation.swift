//
//  MapAnnotation.swift
//  Homeat
//
//  Created by macbook pro on 6/5/2021.
//

import Foundation



import Foundation
import MapKit

class CustomMapAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    var mapModel: AnyObject!
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
}
