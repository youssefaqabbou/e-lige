//
//  FakeData.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import Foundation
import Fakery

struct FakeData {
    
    static func getServicesDatas(count: Int) -> [ServiceModel] {
        var serviceList : [ServiceModel] = []
        for n in 1...count {
            serviceList.append(getServicesData(id: n))
        }
        return serviceList
    }
    
    static func getServicesData(id : Int) -> ServiceModel{
        let faker = Faker(locale: "fr")
        
        let service = ServiceModel()
        service.id = id
        service.name = faker.lorem.words(amount: 15)
        service.price = faker.number.randomDouble(min: 30, max: 120)
        //service.quantity = faker.number.randomInt(min: 0, max: 2)
        service.description = faker.lorem.words(amount: faker.number.randomInt(min: 15, max:200))
        service.duration = faker.number.randomInt(min: 15, max: 120)
        return service
    }
    
    static func getBarberShopDatas(count: Int) -> [BarberShopModel] {
        var barberList : [BarberShopModel] = []
        for n in 1...count {
            barberList.append(getBarberShopData(id: n))
        }
        return barberList
    }
    
    static func getBarberShopData(id : Int) -> BarberShopModel{
        let faker = Faker(locale: "fr")
        var barberShop = BarberShopModel()
        barberShop.id = id
        barberShop.name = faker.lorem.words(amount: Int.random(in: 1..<8))
        barberShop.image = getMediaData(id: id, type : "Shop")

        barberShop.description = faker.lorem.words(amount: Int.random(in: 0...50))
        //barberShop.benefits = faker.lorem.words(amount: Int.random(in: 0...30))
        
        barberShop.services = getServicesDatas(count: Int.random(in: 5...20))
        barberShop.medias = getMediaDatas(count: Int.random(in: 2...20), type : "barber")
        barberShop.reviews = getReviewDatas(count: Int.random(in: 2...10))
        barberShop.schedules = getSheduleDatas()
        barberShop.tags = getTagsDatas(count: Int.random(in: 2...20))
        
        
        barberShop.rating = faker.number.randomFloat(min: 0, max: 5)
        barberShop.countReview = faker.number.randomInt(min: 0, max: 200)
        
        let address = faker.address.secondaryAddress()  + ", " + faker.address.streetName()   + ". " + faker.address.postcode() + ". " +   faker.address.city() + ". " +  faker.address.country()
        barberShop.address = address
        barberShop.longitude = faker.address.longitude()
        barberShop.latitude = faker.address.latitude()
        
        return barberShop
    }
    
    static func getMediaDatas(count: Int, type : String)-> [MediaModel] {
        var mediaList : [MediaModel] = []
        for n in 1...count {
            mediaList.append(getMediaData(id : n, type : type))
        }
        return mediaList
    }
    
    static func getMediaData(id : Int, type: String) -> MediaModel{
        var media = MediaModel()
        media.id = id
        media.url = "https://loremflickr.com/g/\(Int.random(in: 255..<512))/\(Int.random(in: 255..<512))/\(type)"
        
        return media
    }
    
    static func getReviewDatas(count: Int)-> [ReviewModel] {
        var reviewList : [ReviewModel] = []
        for n in 1...count {
            reviewList.append(getReviewData(id : n))
        }
        return reviewList
    }
    
    static func getReviewData(id : Int) -> ReviewModel{
        let faker = Faker(locale: "fr")
        var review = ReviewModel()
        review.id = id
        review.content = faker.lorem.paragraphs(amount: Int.random(in: 1...2))
        review.note = faker.number.randomInt(min: 0, max: 5)
        review.dateCreated = faker.date.forward(Int.random(in: 0...120))
        review.owner = getUserData(id: id)
        return review
    }
    
    static func getSheduleDatas() -> [ScheduleModel] {
        let faker = Faker(locale: "fr")
        var scheduleList : [ScheduleModel] = []
        for n in 1...7 {
            var schedule = ScheduleModel()
            schedule.id = n
            schedule.day = n
            schedule.openTime = faker.date.forward(0).toString(format: "HH:mm", isConvertZone: false)
            schedule.closeTime = faker.date.forward(0).toString(format: "HH:mm", isConvertZone: false)
            
            schedule.isOpen = faker.number.randomBool()
            
            schedule.haveBreak = faker.number.randomBool()
            
            schedule.breakFrom = faker.date.forward(0).toString(format: "HH:mm", isConvertZone: false)
            schedule.breakTo = faker.date.forward(0).toString(format: "HH:mm", isConvertZone: false)
           /*
            var breakTime = ScheduleBreakModel()
            breakTime.id = n
            breakTime.startTime = faker.date.forward(0)
            breakTime.endTime = faker.date.forward(0)
            
            schedule.breakTime = breakTime*/
            scheduleList.append(schedule)
        }
        return scheduleList
    }
    
    static func getUserData(id : Int) -> UserModel{
        let faker = Faker(locale: "fr")
        let user = UserModel()
        user.id = id
        user.fullname = faker.name.firstName()
        user.image = getMediaData(id: id, type: "Man")
        user.phone = faker.phoneNumber.phoneNumber()
        return user
    }
    
    static func getTagsDatas(count: Int)-> [TagModel] {
        let faker = Faker(locale: "fr")
        
        var tagList : [TagModel] = []
        for n in 1...count {
            var tag = TagModel()
            tag.name = faker.lorem.word()
            tag.id = n
            tag.size = getTagSize(text : tag.name, size: 13)
            tag.position = n
            tagList.append(tag)
        }
        return tagList
    }
    
    static func getTagSize(text: String, size: CGFloat) -> CGFloat{
            let font = UIFont.systemFont(ofSize: size)
            let attributes = [NSAttributedString.Key.font: font]
            
            let size = (text as NSString).size(withAttributes: attributes)
            return size.width
    }
    
    static func getCardDatas(count: Int)-> [CardModel] {
        let faker = Faker(locale: "fr")
        
        var cardList : [CardModel] = []
        for n in 1...count {
            var card = CardModel()
            card.last4 = String(faker.number.randomInt(min: 1000, max: 9999))
            card.name = faker.name.firstName()
            card.paymentId = "pm_card_authenticationRequired"
            card.cartType = "Visa"
            card.dateMonth = faker.number.randomInt(min: 1, max: 12)
            card.dateYear = faker.number.randomInt(min: 2022, max: 2025)
            cardList.append(card)
        }
        return cardList
    }
    
    static func getOrderDatas(count: Int)-> [OrderModel] {
        var reviewList : [OrderModel] = []
        for n in 1...count {
            reviewList.append(getOrderData(id : n))
        }
        return reviewList
    }
    
    static func getOrderData(id : Int) -> OrderModel{
        let faker = Faker(locale: "fr")
        var order = OrderModel()
        order.id = id
        
        order.totalPrice = faker.number.randomDouble(min: 10, max: 50)
        order.barberShop = getBarberShopData(id: id)
       // order.services = getServicesDatas(count: faker.number.randomInt(min: 1, max: 10))
        order.orderDate = faker.date.backward(days: faker.number.randomInt(min: 1, max: 10))
        order.duration = faker.number.randomInt(min: 15, max: 30)
        //order.status = faker.number.randomInt(min: 0, max: 4)
        //order.customer = getCustomerData(id: id)
        return order
    }
    
    static func getCustomerDatas(count: Int)-> [CustomerModel] {
        var customerList : [CustomerModel] = []
        for n in 1...count {
            customerList.append(getCustomerData(id : n))
        }
        return customerList
    }
    
    static func getCustomerData(id : Int) -> CustomerModel{
        let faker = Faker(locale: "fr")
        var customer = CustomerModel()
        customer.id = id
        customer.firstName = faker.name.firstName()
        customer.lastName = faker.name.lastName()
        customer.image = getMediaData(id: 1, type : "Man")
        customer.phone = faker.phoneNumber.phoneNumber()
        return customer
    }
    
}
