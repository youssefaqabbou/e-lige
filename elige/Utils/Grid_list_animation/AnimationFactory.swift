//
//  AnimationFactory.swift
//  AnimationFactory
//
//  Created by macbook pro on 16/8/2021.
//

import UIKit

typealias AnimationView = (UIView, Int) -> Void

public enum CellAnimationType: String {
    case alpha
    case wave
    case leftToRight
    case topToBottom
    case bounce
    case rightToLeft
    case rotate
    case linear
    case zoom
    case cardDrop
    case dragFromRight
}
enum AnimationFactory {
   
    static func fade(duration: TimeInterval = 0.5, delay: Double = 0.1, startAlpha: CGFloat = 0) -> AnimationView {
        return { view, indexPath in
            view.alpha = startAlpha

            UIView.animate(
                withDuration: duration,
                delay: delay * Double(indexPath),
                animations: {
                    view.alpha = 1
                })
        }
    }

    static func move(fromTop: Bool = true, yOffset: CGFloat, duration: TimeInterval = 0.5, delay: Double = 0.1, withBounce: Bool = false, withScale: Bool = false, startAlpha: CGFloat = 0) -> AnimationView {
        return { view, indexPath  in
            
            if(fromTop) {
                if(withScale){
                    view.transform = CGAffineTransform(translationX: 0, y: yOffset).scaledBy(x: 0, y: 0)
                }else{
                    view.transform = CGAffineTransform(translationX: 0, y: yOffset)
                }
                
            } else {
                if(withScale){
                    view.transform = CGAffineTransform(translationX: 0, y: -yOffset).scaledBy(x: 0, y: 0)
                }else{
                    view.transform = CGAffineTransform(translationX: 0, y: -yOffset)
                }
            }
           
            view.alpha = startAlpha

            if(withBounce) {
                UIView.animate(
                    withDuration: duration,
                    delay: delay * Double(indexPath),
                    usingSpringWithDamping: 0.4,
                    initialSpringVelocity: 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                        if(withScale){
                            view.transform = CGAffineTransform(translationX: 0, y: 0).scaledBy(x: 1, y: 1)
                        }else{
                            view.transform = CGAffineTransform(translationX: 0, y: 0)
                        }
                        
                        view.alpha = 1
                    })
            } else {
                                
                UIView.animate(
                    withDuration: duration,
                    delay: delay * Double(indexPath),
                    options: [.curveEaseInOut],
                    animations: {
                        if(withScale){
                            view.transform = CGAffineTransform(translationX: 0, y: 0).scaledBy(x: 1, y: 1)
                        }else{
                            view.transform = CGAffineTransform(translationX: 0, y: 0)
                        }
                        view.alpha = 1
                    })
            }

        }
    }

    static func slide(fromLeft: Bool = true, duration: TimeInterval = 0.5, delay: Double = 0.1, withBounce: Bool = false, withScale: Bool = false, startAlpha: CGFloat = 1) -> AnimationView {
        return { view, indexPath in
            
            view.alpha = startAlpha
            if(fromLeft) {
                if(withScale){
                    ///tableView.bounds.width change to cell.contentView.frame.width
                    view.transform = CGAffineTransform(translationX: view.frame.width, y: 0).scaledBy(x: 0, y: 0)
                }else{
                    view.transform = CGAffineTransform(translationX: view.frame.width, y: 0)
                }
            } else {
                if(withScale){
                    view.transform = CGAffineTransform(translationX: -view.frame.width, y: 0).scaledBy(x: 0, y: 0)
                }else{
                    view.transform = CGAffineTransform(translationX: -view.frame.width, y: 0)
                }
            }

            if(withBounce) {
                UIView.animate(
                    withDuration: duration,
                    delay: delay * Double(indexPath),
                    usingSpringWithDamping: 0.4,
                    initialSpringVelocity: 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                        if(withScale){
                            view.transform = CGAffineTransform(translationX: 0, y: 0).scaledBy(x: 1, y: 1)
                        }else{
                            view.transform = CGAffineTransform(translationX: 0, y: 0)
                        }
                    })
            } else {
                UIView.animate(
                    withDuration: duration,
                    delay: delay * Double(indexPath),
                    options: [.curveEaseInOut],
                    animations: {
                        if(withScale){
                            view.transform = CGAffineTransform(translationX: 0, y: 0).scaledBy(x: 1, y: 1)
                        }else{
                            view.transform = CGAffineTransform(translationX: 0, y: 0)
                        }
                    })
            }

        }
    }

    static func linear(duration: TimeInterval = 0.5, delay: Double = 0.1) -> AnimationView {

        return { view, indexPath in
            view.transform = CGAffineTransform(translationX: view.frame.width, y: view.frame.height)
            UIView.animate(
                withDuration: duration,
                delay: delay * Double(indexPath),
                options: [.curveLinear],
                animations: {
                    view.transform = CGAffineTransform(translationX: 0, y: 0)
                })
        }
    }

    /// todo update to Grid
    static func wave(isHorizontal : Bool = false, duration: TimeInterval = 4, delay: Double = 0.1) -> AnimationView {

        return { view, indexPath in
            if(isHorizontal){
                view.transform = CGAffineTransform(translationX: view.frame.width * 10, y: 0)
            }else{
                view.transform = CGAffineTransform(translationX: view.frame.width, y: 0)
            }
            
            UIView.animate(
                withDuration: duration,
                delay: delay * Double(indexPath),
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 0.1,
                options: [.curveEaseIn],
                animations: {
                    if(isHorizontal){
                        view.transform = CGAffineTransform(translationX: 0, y: 0)
                    }else{
                        view.transform = CGAffineTransform(translationX: view.frame.width, y: view.frame.height)
                    }
                    
                })
        }
    }

    static func zoom(scaleX: CGFloat = 0, scaleY: CGFloat = 0, duration: TimeInterval = 0.5, delay: Double = 0.1) -> AnimationView {

        return { view, indexPath in
            view.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            UIView.animate(
                withDuration: duration,
                delay: delay * Double(indexPath),
                //options: [.curveEaseInOut],
                animations: {
                    view.transform = CGAffineTransform(scaleX: 1, y: 1)
                })
        }
    }

    static func dragFromRight(duration: TimeInterval = 0.5, delay: Double = 0.1) -> AnimationView {
        return { view, indexPath in
            view.center.x += 200
            UIView.animate(withDuration: duration, delay: delay * Double(indexPath), animations: {
                view.center.x -= 200
                })
        }
    }

    static func cardDrop(duration: TimeInterval = 1, delay: Double = 0.1) -> AnimationView {
        return { view, indexPath in
            //let view = cell.contentView
            view.layer.transform = TipInCellAnimatorStartTransform
            view.layer.opacity = 0.8
            UIView.animate(withDuration: duration,
                delay: delay * Double(indexPath),
                animations: {
                    view.layer.transform = CATransform3DIdentity
                    view.layer.opacity = 1
                })
        }
    }

    static func rotate(angle: CGFloat = -45, duration: TimeInterval = 2, delay: Double = 0.1, withBounce: Bool = false, startAlpha: CGFloat = 0) -> AnimationView {
        
        return { view, indexPath in
            view.transform = CGAffineTransform(rotationAngle: angle)
            view.alpha = startAlpha
            
            if(withBounce){
                UIView.animate(withDuration: duration,
                    delay: delay * Double(indexPath),
                    usingSpringWithDamping: 0.4,
                    initialSpringVelocity: 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                    view.transform = CGAffineTransform(rotationAngle: 0.0)
                    view.alpha = 1
                    })
            }else{
                UIView.animate(withDuration: duration,
                    delay: delay * Double(indexPath),
                    options: [.curveEaseInOut],
                    animations: {
                    view.transform = CGAffineTransform(rotationAngle: 0.0)
                    view.alpha = 1
                    })
            }
            
        }
    }
    
    static func testCombine(angle: CGFloat = -45, duration: TimeInterval = 5, delay: Double = 0.1, withBounce: Bool = false, startAlpha: CGFloat = 1) -> AnimationView {
        
        return { view, indexPath in
            //let scaleTrans = CGAffineTransform(scaleX: 0, y: 0).translatedBy(x: 100, y: 0)
            //let rotateTrans = CGAffineTransform(rotationAngle: 20 );
            //cell.transform = rotateTrans.concatenating(scaleTrans)
            view.transform = CGAffineTransform(translationX: 0, y: 80).scaledBy(x: 0, y: 0)
            
           
            view.alpha = startAlpha
            
            if(withBounce){
                UIView.animate(withDuration: duration,
                    delay: delay * Double(indexPath),
                    usingSpringWithDamping: 0.4,
                    initialSpringVelocity: 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                    view.transform = CGAffineTransform(rotationAngle: 0.0)
                    view.alpha = 1
                    })
            }else{
                UIView.animate(withDuration: duration,
                    delay: delay * Double(indexPath),
                               usingSpringWithDamping: 0.4,
                               initialSpringVelocity: 0.1,
                    options: [.curveEaseInOut],
                    animations: {
                    view.transform = CGAffineTransform(translationX: 0, y: 0).scaledBy(x: 1, y: 1)
                    view.alpha = 1
                    })
            }
            
        }
    }
    
    private static let TipInCellAnimatorStartTransform: CATransform3D = {
        let rotationDegrees: CGFloat = -15.0
        let rotationRadians: CGFloat = rotationDegrees * (CGFloat(Double.pi) / 180.0)
        let offset = CGPoint(x: -20, y: -20)
        var startTransform = CATransform3DIdentity
        startTransform = CATransform3DRotate(CATransform3DIdentity,
            rotationRadians, 0.0, 0.0, 1.0)
        startTransform = CATransform3DTranslate(startTransform, offset.x, offset.y, 0.0)

        return startTransform
    }()
}
