//
//  Animator.swift
//  Animator
//
//  Created by macbook pro on 16/8/2021.
//

import UIKit
final class Animator {
    private var hasAnimatedAllCells = false
    private let animation: AnimationView

    init(animation: @escaping AnimationView) {
        self.animation = animation
    }

    func animate(view: UIView, at indexPath: Int) {
        guard !hasAnimatedAllCells else {
            return
        }
        animation(view, indexPath)
    }
    
    func animate(views: [UIView], initialAlpha: CGFloat = 0.0,
                                delay: Double = 0) {
            guard views.count > 0 else {
                return
            }
           
            let dispatchGroup = DispatchGroup()
            for _ in 1...views.count { dispatchGroup.enter() }
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                for (index, view) in views.enumerated() {
                    //animationBlock(view, index, dispatchGroup)
                    self.animate(view: view, at: index)
                }
            }
            
            dispatchGroup.notify(queue: .main) {
                //completion?()
            }
        }
}
extension UICollectionView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleItems.last else {
            return false
        }

        return lastIndexPath == indexPath
    }
}

extension UITableView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }

        return lastIndexPath == indexPath
    }
}
