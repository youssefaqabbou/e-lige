//
//  CustomGrid.swift
//  CustomGrid
//
//  Created by macbook pro on 18/8/2021.
//

import Foundation
import UIKit
import SwiftUI

private class UICollectionCell<Content: View>: UICollectionViewCell {
    var host: UIHostingController<Content>?

    func setup(with view: Content) {
        if host == nil {
            let controller = UIHostingController(rootView: view)
            host = controller

            guard let content = controller.view else { return }
            content.backgroundColor = UIColor.clear
            contentView.addSubview(content)
            content.translatesAutoresizingMaskIntoConstraints = false
            content.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            content.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            
        } else {
            host?.rootView = view
        }

        setNeedsLayout()
    }
}

struct CustomGrid<Data, Row: View>: UIViewRepresentable {

    @Binding var data: [Data]
    var cellWidth : CGFloat
    var cellHeight : CGFloat
    var minSpacing : CGFloat = 5
    var animation: AnimationView
    var isScrolled : Bool = false
    let content: (Data) -> Row
    var onLoaded : (_ value: UICollectionView) -> () = {_ in }
    
    func makeUIView(context: Context) -> UICollectionView {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: cellWidth , height: cellHeight)
        flowLayout.minimumLineSpacing = minSpacing
        flowLayout.minimumInteritemSpacing = 1.0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        
        
        collectionView.delegate = context.coordinator
        collectionView.dataSource = context.coordinator
        collectionView.allowsMultipleSelection = false
        collectionView.allowsMultipleSelectionDuringEditing = false
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UICollectionCell<Row>.self, forCellWithReuseIdentifier: "Cell")

        return collectionView
    }

    func updateUIView(_ uiView: UICollectionView, context: Context) {
        context.coordinator.data = data
        uiView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            onLoaded(uiView)
        }
    }

    func makeCoordinator() -> UICollectionViewCoordinator {
        UICollectionViewCoordinator(data, content,isScrolled,  animation)
    }

    class UICollectionViewCoordinator: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
        var content: (Data) -> Row
        var data: [Data]
        var isScrolled : Bool
        let animation: AnimationView
        var shownIndexes = [IndexPath]()

        init(_ data: [Data], _ content: @escaping (Data) -> Row, _ isScrolled : Bool, _ animation: @escaping AnimationView) {
            self.content = content
            self.data = data
            self.isScrolled = isScrolled
            self.animation = animation
        }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            data.count
        }

        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            guard let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? UICollectionCell<Row> else {
                return UICollectionViewCell()
            }
            let data = self.data[indexPath.row]
            let view = content(data)
            collectionViewCell.setup(with: view)
            if(isScrolled){
                if !shownIndexes.contains(indexPath) {
                    shownIndexes.append(indexPath)
                    let animator = Animator(animation: animation)
                    animator.animate(view: collectionViewCell.contentView , at: indexPath.row)
                }
            }
            
            return collectionViewCell
        }

        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        /*
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let dimension = (collectionView.frame.width - 20) / 3
            return .init(width: dimension, height: dimension)
        }

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 1
        }

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 1
        }

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
                .init(top: 5, left: 5, bottom: 5, right: 5)
        }*/
    }
}
