//
//  CustomList.swift
//  CustomList
//
//  Created by macbook pro on 17/8/2021.
//

import Foundation
import SwiftUI
import UIKit

private class UITableCell<Content: View>: UITableViewCell {
    var host: UIHostingController<Content>?

    func setup(with view: Content) {
        if host == nil {
            let controller = UIHostingController(rootView: view)
            host = controller
            guard let content = controller.view else { return }
            
            contentView.addSubview(content)
            
            content.translatesAutoresizingMaskIntoConstraints = false
            content.backgroundColor = UIColor.clear
            content.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
            content.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
            content.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
            content.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        } else {
            //DebugHelper.error("reused cell")
            host?.rootView = view
        }

        setNeedsLayout()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
}

struct CustomList<Data, Row: View>: UIViewRepresentable {
    @Binding var data: [Data]
    var animation : AnimationView
    var isScrolled : Bool = false
    let content: (Data) -> Row
    var onLoaded : (_ value: UITableView) -> () = {_ in }
    
    func makeUIView(context: Context) -> UITableView {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = context.coordinator
        tableView.dataSource = context.coordinator
        tableView.allowsSelection = false
        tableView.allowsMultipleSelection = false
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.backgroundColor = UIColor.clear
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.register(UITableCell<Row>.self, forCellReuseIdentifier: "Cell")
        DebugHelper.error("CustomList makeUIView")
        return tableView
    }
    
    func updateUIView(_ uiView: UITableView, context: Context) {
        //DebugHelper.error("CustomList updateUIView", uiView.visibleCells.count)
        context.coordinator.data = data
        uiView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            //DebugHelper.error("CustomList updateUIView reloadData", uiView.visibleCells.count)
            onLoaded(uiView)
        }
       
    }
    
    func makeCoordinator() -> UITableViewCoordinator {
        UITableViewCoordinator(data, content , isScrolled, animation)
    }

    class UITableViewCoordinator: NSObject, UITableViewDataSource, UITableViewDelegate {

        var content: (Data) -> Row
        var data: [Data]
        var isScrolled : Bool
        let animation : AnimationView
        var shownIndexes = [IndexPath]()
        
        init(_ data: [Data], _ content: @escaping (Data) -> Row,_ isScrolled : Bool,  _ animation: @escaping AnimationView) {
            self.content = content
            self.data = data
            self.isScrolled = isScrolled
            self.animation = animation
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            data.count
        }
        
       
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return section == 0 ? 0.0 : 0
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return nil
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            return nil
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return nil
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 0
        }
        func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
            return nil
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension;
        }
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension;
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? UITableCell<Row> else {
                return UITableViewCell()
            }
            let data = self.data[indexPath.row]
            let view = content(data)
            tableViewCell.setup(with: view)
            tableViewCell.backgroundColor = UIColor.clear
            
            if(isScrolled){
                if !shownIndexes.contains(indexPath) {
                    shownIndexes.append(indexPath)
                    tableViewCell.contentView.alpha = 0
                    let animator = Animator(animation: animation)
                    animator.animate(view: tableViewCell.contentView, at: indexPath.row)
                }
            }
            tableViewCell.setNeedsUpdateConstraints()
            tableViewCell.updateConstraintsIfNeeded()
            return tableViewCell
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            DebugHelper.error("willDisplay", indexPath.row)
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
        }
    }
}
