//
//  AnimationHelper.swift
//  elige
//
//  Created by macbook pro on 2/2/2022.
//

import Foundation
import SwiftUI

struct AnimationConfig {
    var duration : TimeInterval  = 0.5
    var delay : Double = 0.1
   
    var fromTop : Bool = false
    var yOffset : CGFloat = UIScreen.main.bounds.height
    
    var fromLeft : Bool = false
    var xOffset : CGFloat = UIScreen.main.bounds.width
    
    var withBounce : Bool = false
    
    var withScale : Bool = false
    var startScale : CGFloat = 0
    
    var withAlpha : Bool = true
    var startAlpha : CGFloat = 0
    
    var angle : Double = -45
    
    var heigthForItem : CGFloat = 0
    var paddingItem : CGFloat = 0
}

public enum ItemAnimationType: String {
    case alpha
    case slide
    case move
    case zoom
    case rotate
    
    case wave
    case bounce
    case linear

    case cardDrop
    case dragFromRight
}

public struct CustomAnimateItemView<ItemContent : View> : View {

    @State var animateItem = false
    var indexItem = 0
    private let makeItemContent: () -> ItemContent
    
    var animType : ItemAnimationType = ItemAnimationType.move
    var animConfig : AnimationConfig = AnimationConfig()
   
    public init(index: Int, @ViewBuilder itemContent: @escaping () -> ItemContent ) {
        indexItem = index
        makeItemContent = itemContent
    }
    
    init(index: Int, type : ItemAnimationType, config : AnimationConfig , @ViewBuilder itemContent: @escaping () -> ItemContent ) {
        indexItem = index
        animType = type
        animConfig = config
        makeItemContent = itemContent
    }
   
    
    @State var isAnimate = true
    @State var itemHeight : CGFloat = 40
    
    public var body: some View {
       

            makeItemContent()
                .background(GeometryReader {geometry -> Color in
                    DispatchQueue.main.async {
                        // update on next cycle with calculated height of ZStack !!!
                        itemHeight = geometry.size.height
                        
                        //let minY =  geometry.frame(in: .global).minY
                        //let maxX =  geometry.frame(in: .global).maxX
                        //let maxY =  geometry.frame(in: .global).maxY
                        
                        //DebugHelper.debug("screen heigth For Items",  animConfig.heigthForItem )
                        //DebugHelper.debug("indexItem", indexItem, "itemHeight" , itemHeight, "minY", minY, "animateItem", animateItem)
                        
                        //DebugHelper.debug("screen height",  UIScreen.main.bounds.height )
                        //DebugHelper.debug("screen heigth For Items",  animConfig.heigthForItem )
                       
                        //DebugHelper.debug("item total height", indexItem,   ((geometry.size.height + animConfig.paddingItem) * CGFloat(indexItem)))
                        
                        //isAnimate = minY < animConfig.heigthForItem
                        isAnimate = (animConfig.heigthForItem + (UIApplication.shared.windows.first?.safeAreaInsets.bottom)!) > ((geometry.size.height + animConfig.paddingItem) * CGFloat(indexItem))
                        //DebugHelper.debug("indexItem", indexItem, "item isAnimate",  isAnimate)
                   
                        //listItems[self.indexItem] = geometry.size.height
                        withAnimation(getCellAnimation(indexItem: indexItem, animType: animType, animConfig: animConfig)){
                           animateItem = true
                        }
                        
                    }
                    return Color.clear
                })
                .animateItem(isAnimate: isAnimate, animate: animateItem, animationType: animType, config: animConfig)
        
    }
}

func getCellAnimation(indexItem: Int, animType : ItemAnimationType, animConfig : AnimationConfig) -> Animation{
    switch animType {
        case ItemAnimationType.alpha:
            return .linear(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
        case ItemAnimationType.move:
            if(animConfig.withBounce){
                return .interactiveSpring(response: 0.1, dampingFraction: 0.4, blendDuration: 0).delay(Double(indexItem) * animConfig.delay)
            }
            return .easeInOut(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
        case ItemAnimationType.slide:
            if(animConfig.withBounce){
                return .interactiveSpring(response: 0.1, dampingFraction: 0.4, blendDuration: 0).delay(Double(indexItem) * animConfig.delay)
            }
            return .easeInOut(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
        case ItemAnimationType.zoom:
            if(animConfig.withBounce){
                return .interpolatingSpring(mass: 1.0,
                                            stiffness: 100.0,
                                            damping: 10,
                                            initialVelocity: 0).delay(Double(indexItem) * animConfig.delay)
            }
            return .easeInOut(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
        case ItemAnimationType.rotate:
            if(animConfig.withBounce){
                return .interactiveSpring(response: 0.1, dampingFraction: 0.4, blendDuration: 0).delay(Double(indexItem) * animConfig.delay)
            }
            return .easeInOut(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
        default:
            return .linear(duration: animConfig.duration).delay(Double(indexItem) * animConfig.delay)
    }
}


struct AnnimationEffect: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false

    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        content
            .offset(y: isAnimate ? animateItem ? 0 : UIScreen.main.bounds.height  : 0)
            .opacity( isAnimate ? animateItem ? 1 : 0.1 : 1)
            .scaleEffect( isAnimate ? animateItem ? 1 : 0.5 : 1)
    }
}

struct MoveAnimation: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false
    var animConfig : AnimationConfig
    
    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        
        if(animConfig.fromTop){
            content
                .offset(y: isAnimate ? animateItem ? 0 : -animConfig.yOffset  : 0)
                .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
                .scaleEffect( isAnimate ? animConfig.withScale ? animateItem ? 1 : animConfig.startScale : 1 : 1)
        }else{
            content
                .offset(y: isAnimate ? animateItem ? 0 : animConfig.yOffset  : 0)
                .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
                .scaleEffect( isAnimate ? animConfig.withScale ? animateItem ? 1 : animConfig.startScale : 1 : 1)
        }
    }
}

struct SlideAnimation: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false
    var animConfig : AnimationConfig
    
    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        
        if(animConfig.fromLeft){
            content
                .offset(x: isAnimate ? animateItem ? 0 : -animConfig.xOffset  : 0)
                .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
                .scaleEffect( isAnimate ? animConfig.withScale ? animateItem ? 1 : animConfig.startScale : 1 : 1)
        }else{
            content
                .offset(x: isAnimate ? animateItem ? 0 : animConfig.xOffset  : 0)
                .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
                .scaleEffect( isAnimate ? animConfig.withScale ? animateItem ? 1 : animConfig.startScale : 1 : 1)
        }
    }
}
    
struct FadeAnimation: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false
    var animConfig : AnimationConfig
    
    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        content
            .opacity( isAnimate ? animateItem ? 1 : animConfig.startAlpha : 1)
    }
}

struct ZoomAnimation: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false
    var animConfig : AnimationConfig
    
    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        content
            .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
            .scaleEffect(x: isAnimate ?  animateItem ? 1 : animConfig.startScale : 1, y: isAnimate ?  animateItem ? 1 : animConfig.startScale : 1)
    }
}

struct RotateAnimation: AnimatableModifier {
    var isAnimate: Bool = false
    var animateItem: Bool = false
    var animConfig : AnimationConfig
    
    var animatableData: Bool {
        get {
            animateItem
        } set {
            animateItem = newValue
        }
    }

    func body(content: Content) -> some View {
        content
            .opacity( isAnimate ? animConfig.withAlpha ? animateItem ? 1 : animConfig.startAlpha : 1 : 1)
            .scaleEffect( isAnimate ? animConfig.withScale ? animateItem ? 1 : animConfig.startScale : 1 : 1)
            .rotationEffect(isAnimate ? animateItem ? .degrees(0) : .degrees(animConfig.angle) : .degrees(0), anchor: .topLeading)
    }
}
 
extension View {
    func animateItem(isAnimate: Bool , animate: Bool, animationType : ItemAnimationType, config : AnimationConfig) -> some View {
        ZStack(){
            switch animationType {
            case ItemAnimationType.alpha:
                modifier(FadeAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            case ItemAnimationType.move:
                 modifier(MoveAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            case ItemAnimationType.slide:
                 modifier(SlideAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            case ItemAnimationType.zoom:
                 modifier(ZoomAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            case ItemAnimationType.rotate:
                 modifier(RotateAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            default:
                 modifier(FadeAnimation(isAnimate: isAnimate, animateItem: animate, animConfig : config))
            }
        }
        
    }
}
