//
//  ColorsHelper.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import SwiftUI

extension Color {
    
    static var primaryColor = Color(hex: "#EEBB2A")
    static var secondaryColor = Color(hex: "#EEBB2A") //FAEABF
    
    static var primaryTextColor = Color(hex: "#333333")
    static var secondaryTextColor = Color(hex: "#666666")
    static var thirdTextColor = Color(hex: "#B8B8B8")
    
    static var primaryTextLightColor = Color(hex: "#FFFFFF")
    
    
    static var primaryButtonColor = Color(hex: "#EEBB2A")
    
    static var editTextBgColor = Color(hex: "#EAEAEA") // F9F9FB
    static var placeHolderTextColor = Color(hex: "#666666")
  
    static var shadowColor = Color(hex: "#000000").opacity(0.29)
    static var tabBarShadowColor = Color(hex: "#DDDDDD").opacity(0.8)
    static var errorColor = Color(hex: "#333333")
    
    static var bgViewColor = Color(hex: "#FFFFFF")
    static var bgViewSecondaryColor = Color(hex: "#F7F7F7")
    
    static var shadowAlert = Color(hex: "#4DEEBB2A")
    
    static var toggleStrockColor = Color(hex: "#707070")
    static var shadowAnnotation = Color(hex: "#3D8F8F8F")
    static var lightGray = Color(hex: "#EDEDED")
    static var bgFavorite = Color(hex: "#FFF0F1")

    static var snackBarSuccess = Color(hex: "#3AA26E")
    static var snackBarInfo = Color(hex: "#3AA26E")
    static var snackBarError = Color(hex: "#C70039")
    
    static var btnFavorite = Color(hex: "#FC4050")
    static var bgBtnFavorite = Color(hex: "#FFF0F1")
    
}

extension Color {
    
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }

        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
}
