//
//  ComponentHelper.swift
//  Homeat
//
//  Created by macbook pro on 4/5/2021.
//

import Foundation
import SwiftUI

extension Binding {
    func onChange(_ handler: @escaping (Value) -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler(newValue)
            }
        )
    }
}

private extension ButtonStyle {
    var foregroundColor: Color { .white }
    var padding: CGFloat { 10 }
    var cornerRadius: CGFloat { 10 }
    var pressedColorOpacity: Double { 0.7 }
}

extension Text {
    func textStyle<Style: ViewModifier>(_ style: Style) -> some View {
        ModifiedContent(content: self, modifier: style)
    }
}

struct TitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            //.lineSpacing(8)
            .font(.system(weight: .bold, size: 22))
            .foregroundColor(Color.primaryTextColor)
    }
}

struct AlertTitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(weight: .regular, size: 20))
            .foregroundColor(Color.primaryTextColor)
    }
}

struct ContentStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.body)
            .lineSpacing(4)
            .foregroundColor(.secondary)
    }
}

struct PlaceHolderStyle: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .font(.system(weight: .regular, size: 16))
            .foregroundColor(Color.placeHolderTextColor)
    }
}

struct PrimaryButtonStyle: ButtonStyle {
    var bgColor : Color = Color.primaryButtonColor
    
    var fgColor : Color = Color.primaryTextLightColor
    var shdowColor : Color = Color.shadowColor

    var raduis : CGFloat = AppConstants.buttonRadius
    var padding : CGFloat = 20
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(.system(weight: .regular, size: 16))
            .padding(padding)
            .frame(minWidth: 0, maxWidth: .infinity)
            .foregroundColor(fgColor)
            .background(bgColor)
            .cornerRadius(raduis)
            .opacity(configuration.isPressed ? pressedColorOpacity : 1)
            .shadow(color: Color.shadowColor, radius: 2, x: 0, y: 3)
    }
}

struct OutlineStyle: ButtonStyle {
    var fgColor : Color = Color.primaryColor
    var bgColor : Color = Color.primaryColor
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(.system(weight: .regular, size: 16))
            .padding(AppConstants.viewNormalMargin)
            .frame(minWidth: 0, maxWidth: .infinity)
            .foregroundColor(fgColor)
            .background(RoundedRectangle(cornerRadius: AppConstants.buttonRadius).stroke(bgColor))
            .opacity(configuration.isPressed ? pressedColorOpacity : 1)
    }
}

struct PressedButtonStyle: ButtonStyle {
  func makeBody(configuration: Self.Configuration) -> some View {
    configuration.label
      .opacity(configuration.isPressed ? 0.7 : 1)
  }
}

struct CustomToggleStyle: ToggleStyle {
    var width: CGFloat = 40
    var height: CGFloat = 22
    var circlePadding: CGFloat = 2
    var toggleAction : () -> () = {}

    func makeBody(configuration: Configuration) -> some View {
        HStack {
            //configuration.label
            //Spacer()
            GeometryReader { geometry in
                
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: .infinity)
                        .strokeBorder(configuration.isOn ? Color.primaryColor : Color.toggleStrockColor, lineWidth: 1)
                        .background(RoundedRectangle(cornerRadius: .infinity).fill(configuration.isOn ? Color.primaryColor : Color.clear))
                        .frame(width: width, height: height, alignment: .center)
                    
                    Circle()
                        .fill(configuration.isOn ? Color.toggleStrockColor : Color.toggleStrockColor)
                        .frame(width: (height - (circlePadding * 2)), height: (height - (circlePadding * 2)), alignment: .center)
                        .offset(x: configuration.isOn ? geometry.frame(in: .local).maxX - (height - circlePadding) : geometry.frame(in: .local).minX + circlePadding, y: 0)
                        .animation(Animation.linear(duration: 0.1), value: configuration.isOn)
                    
                }.onTapGesture {
                        //DebugHelper.debug(geometry.frame(in: .local).minX, geometry.frame(in: .local).maxY)
                        configuration.isOn.toggle()
                        toggleAction()
                    }
            }.frame(width: width, height: height, alignment: .center)
            

        }
    }
}




// Minimal modifier.
struct Moving: AnimatableModifier {
    var time : CGFloat  // Normalized from 0...1.
    let path : Path
    let start: CGPoint  // Could derive from path.

    var animatableData: CGFloat {
        get { time }
        set { time = newValue }
    }

    func body(content: Content) -> some View {
        content
        .position(
            path.trimmedPath(from: 0, to: time).currentPoint ?? start
        )
    }
}

