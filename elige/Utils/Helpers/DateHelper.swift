//
//  DateHelper.swift
//  Click&Eat
//
//  Created by user196402 on 8/2/21.
//

import Foundation

extension Date {

    func toString(format: String = "yyyy-MM-dd", locale : Locale = .current, isConvertZone : Bool = false) -> String {
        //DebugHelper.debug("toString", self)
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        formatter.locale = locale
        //DebugHelper.debug("before TZ", formatter.string(from: self))
        if(isConvertZone){
            formatter.timeZone = .current
            //DebugHelper.debug("isConvertZone" ,formatter.timeZone,  formatter.string(from: self))
        }else{
            formatter.timeZone = TimeZone(abbreviation: "UTC")
            //DebugHelper.debug("isConvertZone" ,formatter.timeZone,  formatter.string(from: self))
        }
        //DebugHelper.debug("after TZ" ,formatter.string(from: self))
        return formatter.string(from: self)
    }
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
            calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func isDateInYesterday() -> Bool{
        let calendar = Calendar.current
        if calendar.isDateInYesterday(self) { return true }
        return false
    }
    
    func isDateInToday() -> Bool{
        let calendar = Calendar.current
        if calendar.isDateInToday(self) { return true }
        
        return false
    }
    
    func isDateInTomorrow() -> Bool{
        let calendar = Calendar.current
        if calendar.isDateInTomorrow(self) { return true }
        return false
    }
    
    func isInSameYear(as date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
       
    func isInSameMonth(as date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    
    func isInSameWeek(as date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }
    
    func timeIn24HourFormat(isConvertZone : Bool = false) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.dateFormat = "HH:mm"
        if(isConvertZone){
            formatter.timeZone = .current
        }
        return formatter.string(from: self)
    }
    
    var weekday: Int {
           return Calendar.current.component(.weekday, from: self) - 1
    }
    
    var hour : Int{
        var calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
        
        if let timeZone = TimeZone(identifier: "UTC") {
           calendar.timeZone = timeZone
        }
        return calendar.component(.hour, from: self)
    }
    
    var minute : Int{
        var calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
        
        if let timeZone = TimeZone(identifier: "UTC") {
           calendar.timeZone = timeZone
        }
        return calendar.component(.minute, from: self)
    }
    
    func addDays(days: Int) -> Date {
        //DebugHelper.debug(self)
        let endDate = Calendar.current.date(byAdding: .day, value: days, to: self)
        return endDate ?? Date()
    }
    
    func time(since fromDate: Date) -> String {
        let earliest = self < fromDate ? self  : fromDate
       // DebugHelper.debug("earliest", earliest)
        let latest = (earliest == self) ? fromDate : self
        //DebugHelper.debug("fromDate", fromDate)
        //DebugHelper.debug("latest", latest)
    
        let components:DateComponents = Calendar.current.dateComponents([.minute,.hour,.day,.weekOfYear,.month,.year,.second], from: earliest, to: latest)
        let year = components.year  ?? 0
        let month = components.month  ?? 0
        let week = components.weekOfYear  ?? 0
        let day = components.day ?? 0
        let hours = components.hour ?? 0
        let minutes = components.minute ?? 0
        let seconds = components.second ?? 0
        

        if (day >= 1 || week >= 1 || month >= 1 || year >= 1 ) {
            
            if(self.isInSameYear(as: fromDate)){
                return  self.toString(format: "dd MMM 'à' HH:mm",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
            }
            return  self.toString(format: "dd MMM yyyy 'à' HH:mm",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
        }
        else if hours >= 12{
            if(self.isDateInToday()){
                return LocalizationKeys.todayAt(time: self.toString(format: "HH:mm", isConvertZone: false))
            } else if(self.isDateInYesterday()){
                return LocalizationKeys.yesterdayAt(time: self.toString(format: "HH:mm", isConvertZone: false))
            }
            return  self.toString(format: "dd MMM 'à' HH:mm",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
        }
        else if (hours >= 2) {
            return LocalizationKeys.hourAgo(hour: hours)
        } else if (hours >= 1){
            return LocalizationKeys.label_one_hour_ago.localized
        } else if (minutes >= 2) {
            return LocalizationKeys.minuteAgo(min: minutes)
        } else if (minutes >= 1){
            return LocalizationKeys.label_one_minute_ago.localized
        } else if (seconds >= 3) {
            return LocalizationKeys.secondAgo(sec: seconds)
        } else {
            return LocalizationKeys.label_now.localized
        }
        
    }
    
    public func changeTo(hour: Int, minute: Int, convertToUtc : Bool = false ) -> Date {
        var calendar = Calendar.current // or e.g.
        if(convertToUtc){
            if let timeZone = TimeZone(identifier: "UTC") {
                calendar.timeZone = timeZone
            }
        }
        return calendar.date(bySettingHour: hour, minute: minute, second: 0, of: self)!
    }
    
    func time(onlyDay: Bool = false) -> String {

        if(self.isDateInToday()){
            return "Aujourd'hui"
        } else if(self.isDateInYesterday()){
            return "Hier"
        }else if(self.isDateInTomorrow()){
            return "Demain"
        }else if(self.isInSameYear(as: AppFunctions.getTodayDateWithTZ())){
            if(onlyDay){
                return  self.toString(format: "EEEE",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
            }
            return  self.toString(format: "EEEE dd MMMM",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
        }else{
            if(onlyDay){
                return  self.toString(format: "EEEE",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
            }
            return  self.toString(format: "EEEE dd MMMM yyyy",locale: Locale.init(identifier: "FR"), isConvertZone: false).capitalizingFirstLetter()
        }
    }
}
