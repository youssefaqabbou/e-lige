//
//  ExtensionHelper.swift
//  Click&Eat
//
//  Created by user196417 on 7/15/21.
//

import Foundation
import UIKit
import SwiftUI


extension Int {
    
    func toWeekDay(weekDayFormat: WeekdaySymbolsEnum = .normal, firstWeekday : Int = 1, local : Locale = Locale.current) -> String{
        let fmt = DateFormatter()
        
        fmt.locale = local
        var symbols : [String] = fmt.weekdaySymbols
        switch weekDayFormat {
        case .short:
            symbols  = fmt.shortWeekdaySymbols
        case .veryShort:
            symbols = fmt.veryShortWeekdaySymbols
        default:
            symbols  = fmt.weekdaySymbols
        }
        symbols = Array(symbols[firstWeekday-1..<symbols.count]) + symbols[0..<firstWeekday-1]
        return symbols[self - 1]
    }
    
    func toHourMinute() -> String  {
        let (h,m,_) = AppFunctions.secondsToHoursMinutesSeconds(self * 60)
        return ("\(String(format: "%02d", h) ):\(String(format: "%02d", m))")
    }
    
}

extension Double {
    
    func toPrice(currency: String = " €") -> String {
        return String(format: "%.2f", self) + currency
    }
}

extension Float {
    
    func toRating() -> String {
        return String(format: "%.2f", self)
    }
}



extension String {
    
    func localized() -> String {
        let localizedString = NSLocalizedString(self, comment: "")
        return localizedString
    }
    
    func toDateUTC(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        if(dateFormatter.date(from: self) == nil) {
            return self.toDateFormat()
        }
        //DebugHelper.debug("after Convert" , dateFormatter.date(from: self))
        return dateFormatter.date(from: self)!
    }

    func toDate(format: String = "yyyy-MM-dd" , convertToCurrent : Bool = false) -> Date {
        let dateFormatter = DateFormatter()
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = format
        //dateFormatter.locale = .current
        //dateFormatter.timeStyle = DateFormatter.Style.none
        //DebugHelper.debug("before TZ" , dateFormatter.date(from: self))
        if(convertToCurrent){
            dateFormatter.locale = .current
            dateFormatter.timeZone = TimeZone.current

            //DebugHelper.debug("isConvertZone" ,dateFormatter.timeZone,  dateFormatter.date(from: self))
        }else{
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            //DebugHelper.debug("isConvertZone false" , dateFormatter.date(from: self))
        }
        
        
        if(dateFormatter.date(from: self) == nil) {
            return self.toDateFormat()
        }
        //DebugHelper.debug("toDate", self)
        //DebugHelper.debug("after TZ" , dateFormatter.date(from: self))
        return dateFormatter.date(from: self)!
    }

    func toDateFormat() -> Date {
         //DebugHelper.debug("toDateFormat", self)
        let date = self.components(separatedBy: " ").first

        let day = Int((date?.components(separatedBy: "/")[0])!) ?? 00
        let month = Int((date?.components(separatedBy: "/")[1])!) ?? 00
        let year = Int((date?.components(separatedBy: "/")[2])!) ?? 00

        let time = self.components(separatedBy: " ").last

        let hour = Int((time?.components(separatedBy: ":")[0])!) ?? 00
        let minute = Int((time?.components(separatedBy: ":")[1])!) ?? 00
        let second = Int((time?.components(separatedBy: ":")[2])!) ?? 00


        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        dateComponents.timeZone = TimeZone(abbreviation: "UTC")

        // let notifyTime = Calendar.current.dateComponents(in: TimeZone.current, from: sessionTime)

        // Create date from components
        let userCalendar = Calendar(identifier: .gregorian) // since the components above (like year 1980) are for Gregorian
        let dateTime = userCalendar.date(from: dateComponents) ?? Date()

        //DebugHelper.debug("toDateFormat", dateTime)
        //let dateTime = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: Date())!

        return dateTime
    }
 
    func toMediaUrl() -> String {
        return baseUrl + self.replacingOccurrences(of: "public/", with: "")
    }

    func toPhoneIndicator(indicator: String = AppConstants.phoneIndicator) -> String {
        return indicator + self
    }

    func toPhone(indicator: String = AppConstants.phoneIndicator) -> String {
        if(self.contains(indicator)) {
            return self.replacingOccurrences(of: indicator, with: "")
        }
        return self
    }
    
    func toPrice(currency: String = " €") -> String {
        return self + currency
    }
    
    func toApiUrl() -> String {
        return apiBaseUrl + self
    }
    
    func toTime() -> Int {
        let components = self.split { $0 == ":" } .map { (x) -> Int in return Int(String(x))! }

        if(components.isEmpty){
            return 0
        }
        let hours = components[0]
        let minutes = components[1]
        
        let timeInMinutes = hours * 60 + minutes
        return timeInMinutes
    }
    
}


public extension UIImage {
      convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
      }
    }


extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

extension View {
    // Requires the origin and size of the view. These can be gotten
    // from the view using a GeometryReader.
    func snapshot() -> UIImage {

        let controller = UIHostingController(rootView: self)
        let targetSize = controller.view.intrinsicContentSize
        let window = UIWindow(frame: CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height))
    
        controller.view.frame = window.frame
        controller.view.backgroundColor = .clear
  
        window.addSubview(controller.view)
        window.makeKeyAndVisible()

        return controller.view.renderedImage
    }
}


extension UIView {
  var renderedImage: UIImage {

    // The size of the image we want to create, based on the size of the
    // current view.
    let rect = self.bounds

    // Start an image context, using the rect from above to set the size.
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
    let context: CGContext = UIGraphicsGetCurrentContext()!

    // Render the current view into the image context.
    self.layer.render(in: context)

    // Extract an image from the context.
    let capturedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!

    UIGraphicsEndImageContext()
    return capturedImage
  }
    
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func timeToTimeZone(convertToUtc : Bool = true) -> String {
        //DebugHelper.debug(self)
        var today = AppFunctions.getTodayDateUTC()
        //DebugHelper.debug(today)
        let (h, m) = AppFunctions.timeToHoursMinutes(self)
        today = today.changeTo(hour: h, minute: m)
        //DebugHelper.debug(today)
        return  today.toString(format: "HH:mm", isConvertZone: convertToUtc)
    }
    
    func timeToUTC() -> String {
        DebugHelper.debug(self)
        var today = AppFunctions.getTodayDateWithTZ()
        DebugHelper.debug(today)
        let (h, m) = AppFunctions.timeToHoursMinutes(self)
        today = today.changeTo(hour: h, minute: m,  convertToUtc : false )
        DebugHelper.debug(today)
        let time = today.toString(format: "HH:mm", isConvertZone: false)
        DebugHelper.debug(time)
        return time
    }
}



