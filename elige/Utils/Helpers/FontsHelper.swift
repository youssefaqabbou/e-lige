//
//  FontsHelper.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import SwiftUI

struct FontNameManager {
    struct BrownStd {
        static let bold = "BrownStd-Bold"
        static let light = "BrownStd-Light"
        static let thin = "BrownStd-Thin"
        static let regular = "BrownStd-Regular"
        
    }
}
public enum FontStyle : String{
    case brownStd = "BrownStd"
}
extension Font {
    
    /// Create a font with the large title text style.
    public static var largeTitle: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .largeTitle).pointSize)
    }
    
    /// Create a font with the title text style.
    public static var title: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .title1).pointSize)
    }
    
    /// Create a font with the headline text style.
    public static var headline: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .headline).pointSize)
    }
    
    /// Create a font with the subheadline text style.
    public static var subheadline: Font {
        return Font.custom(FontNameManager.BrownStd.light, size: UIFont.preferredFont(forTextStyle: .subheadline).pointSize)
    }
    
    /// Create a font with the body text style.
    public static var body: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .body).pointSize)
    }
    
    /// Create a font with the callout text style.
    public static var callout: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .callout).pointSize)
    }
    
    /// Create a font with the footnote text style.
    public static var footnote: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .footnote).pointSize)
    }
    
    /// Create a font with the caption text style.
    public static var caption: Font {
        return Font.custom(FontNameManager.BrownStd.regular, size: UIFont.preferredFont(forTextStyle: .caption1).pointSize)
    }
    
    public static func system(weight: Font.Weight = .regular, size: CGFloat, font: String  = FontStyle.brownStd.rawValue) -> Font {
        
        var fonts = FontNameManager.BrownStd.regular
        switch font{
            
        case FontStyle.brownStd.rawValue :
            switch weight {
            case .bold: fonts = FontNameManager.BrownStd.bold
                break
            case .light: fonts = FontNameManager.BrownStd.light
            case .thin: fonts =  FontNameManager.BrownStd.thin
            case .regular: fonts = FontNameManager.BrownStd.regular
            default: break
            }
            
        default: break
            
        }
        return Font.custom(fonts, size: size)
    }
}
