//
//  ImageHelper.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import SwiftUI

extension Image{
 
    static var smallLogoIcon = Image("logo")
    // TODO:  Change marker map icon
    static var shopMarkerUIImage = UIImage(named: "icon_shop_pin")
    static var userMarkerIcon = Image("icon_user_marker")
    static var iconMarkerMap = Image("icon_location")
    
    static var logoIcon = Image("logo")
    static var iconEyeOff = Image("icon_eye_off")
    static var iconEyeOn = Image("icon_eye_on")

    static var iconFacebook = Image("icon_fb")
    static var iconApple = Image("icon_apple")
    static var icon_google = Image("icon_google")
    
    static var rightArrowIcon = Image("icon_right_arrow")
    static var deleteLeftIcon = Image("icon_delete_left")
    static var iconLocation = Image("icon_location")
    static var iconTime = Image("icon_time")
    static var iconBell = Image("icon_bell")
    static var iconCameraEdit = Image("icon_camera_edit")
    static var iconStar = Image("icon_star")
    static var iconStarColor = Image("icon_starColor")
    
    static var iconFavoriteRed = Image("icon_heart")
    static var iconGallery = Image("icon_gallery")
    static var iconClose = Image("icon_close")
    static var iconCheck = Image("icon_check")
    
    static var imgUser = Image("img_user_profile")
    static var imgBarberShop = Image("img_barberShop_1")
    
    static var iconHeartNoFav = Image("icon_heart_noFav")
    static var iconHeartFav = Image("icon_heart_fav")
    
    
    static var iconAddRating = Image("icon_add_rating")
    static var iconAddRatingGray = Image("icon_add_ratingGray")
    
    static var iconBarberShop = Image("icon_barberShop")
    static var iconBarberDomicile = Image("icon_barberDomicile")
    
    static var iconProfil = Image("icon_profil")
    static var iconEvaluer = Image("icon_evaluer")
    static var iconApropos = Image("icon_apropos")
    static var iconFAQs = Image("icon_faqs")
    static var iconDisconnect = Image("icon_disconnect")
    static var iconAlertDisconnect = Image("icon_alert_disconnect")
    
    static var iconDelete = Image("icon_close")
    static var iconAlertBlock = Image("icon_alert_block")
    static var iconAlertDelete = Image("icon_alert_delete")
    
    
    static var bgPicker = Image("bg_picker")

    
    static var iconCancel = Image("icon_cancel")
    static var iconAlertConfirm = Image("icon_alert_confirm")
    
    static var iconArrowBack = Image("icon_backward")
    static var iconPencil = Image("icon_pencil")
    static var iconCamera = Image("icon_camera")
    
    static var iconFilter = Image("icon_filter")
    
    static var placeholderShop = Image("placeholder_shop")
    static var placeholderUser = Image("placeholder_user")
    static var iconCheckBlack = Image("icon_check_black")
    static var iconMyLocation = Image("icon_myLocation")
    
    static var gpsRequired = Image("img_gps_required")
    
    static var iconNotifOrder = Image("ic_notif_order")
    static var iconNotifRating = Image("ic_notif_rating")
    static var iconSearch = Image("ic_search")
    
    static var iconMapTabBar = Image("icon_map_tabbar")
    static var iconHomeTabBar = Image("icon_home_tabbar")
    static var iconOrderTabBar = Image("icon_order_tabbar")
    static var iconFavorisTabBar = Image("icon_favoris_tabbar")
    
    static var iconSettingTabBar = Image("icon_setting_tabbar")
    static var iconCustomerTabBar = Image("icon_customer_tabbar")
    
    static var iconUser = Image("icon_user")
    static var iconPlaceHolder = Image("ic_placeHolder_img")
    static var iconAlertPaymentInfo = Image("icon_alert_payment_info")
    static var iconMenuDots = Image("ic_menu_dots")
    static var iconAlertCancel = Image("icon_alert_cancel")
    
    static var iconAlertFailed = Image("icon_alert_failed")
    static var iconAlertSuccess = Image("icon_alert_success")
    
}

