//
//  JsonHelper.swift
//  Click&Eat
//
//  Created by user196402 on 7/13/21.
//

import Foundation
import SwiftyJSON
import SwiftUI

enum JsonKeys: String {
    case statusCode = "statusCode"
    case error = "error"
    case message = "message"
    
    case clientSecret = "clientSecret"
    
    case accessToken = "accessToken"
    case refreshToken = "refreshToken"
    
    //user
    case id = "id"
    case fullname = "fullname"
    case address = "address"
    case zipcode = "zipcode"
    case city = "city"
    case country = "country"
    case latitude = "latitude"
    case longitude = "longitude"
    case phone = "phone"
    case description = "description"
    case email = "email"
    case profileImage = "profileImage"
    case image = "image"
    case role = "role"
    case isVerified = "isVerified"
    case blocked = "blocked"
    
    //media
    case type = "type"
    case originalName = "originalName"
    case size = "size"
    case url = "url"
    case order = "order"
    
    //schedule
    case day = "day"
    case opening = "opening"
    case closing = "closing"
    case breakFrom = "breakFrom"
    case breakTo = "breakTo"
    case place = "place"
    case isClosed = "isClosed"
    
    //category
    case name = "name"
    case icon = "icon"
    
    //service
    case price = "price"
    case duration = "duration"
    case category = "category"
    
    //reviews
    case review = "review"
    case rating = "rating"
    case createdAt = "createdAt"
    case user = "user"
    
    // BarberShop
    case ratingAvg = "ratingAvg"
    case reviewsCount = "reviewsCount"
    case servicesCount = "servicesCount"
    case imagesCount = "imagesCount"
    case distance = "distance"
    case isFavorite = "isFavorite"
    case isBlocked = "isBlocked"
    case canBeReviewed = "canBeReviewed"
    
    case schedules = "schedules"
    case reviews = "reviews"
    case services = "services"
    case images = "images"
    case tags = "tags"
    case media = "media"
    
    //Order
    case orders = "orders"
    case number = "number"
    case fees = "fees"
    case start = "start"
    case currency = "currency"
    case note = "note"
    case orderStatus = "orderStatus"
    case paymentIntent = "paymentIntent"
    case paymentStatus = "paymentStatus"
    case createdDate = "date"
    case subTotal = "subTotal"
    case total = "total"
    case orderLines = "orderLines"
    case quantity = "quantity"
    
    //Notification
    case title = "title"
}

class JsonHelper {
    
    static func getBarberShops(items: [JSON]) -> [BarberShopModel] {
        
        var barberShopList: [BarberShopModel] = []
        
        for item in items {
            let barberShop = getBarberShop(item: item.dictionaryValue)
            if(barberShop.id != -1){
                barberShopList.append(barberShop)
            }
        }
        
        return barberShopList
    }
    
    static func getBarberShop(item: [String: JSON]) -> BarberShopModel {
        var barberShop = BarberShopModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            barberShop.id = id
        }
        if let fullname = item[JsonKeys.fullname.rawValue]?.string {
            barberShop.name = fullname
        }
        if let address = item[JsonKeys.address.rawValue]?.string {
            barberShop.address = address
        }
        if let zipcode = item[JsonKeys.zipcode.rawValue]?.string {
            barberShop.zipcode = zipcode
        }
        if let city = item[JsonKeys.city.rawValue]?.string {
            barberShop.city = city
        }
        if let country = item[JsonKeys.country.rawValue]?.string {
            barberShop.country = country
        }
        if let lat = item[JsonKeys.latitude.rawValue]?.string {
            if(!lat.isEmpty){
                barberShop.latitude = Double(lat)!
            }
            
        }
        if let lng = item[JsonKeys.longitude.rawValue]?.string {
            if(!lng.isEmpty){
                barberShop.longitude = Double(lng)!
            }
        }
        if let phone = item[JsonKeys.phone.rawValue]?.string {
            barberShop.phone = phone
        }
        if let descr = item[JsonKeys.description.rawValue]?.string {
            barberShop.description = descr
        }
        if let email = item[JsonKeys.email.rawValue]?.string {
            barberShop.email = email
        }
        
        if let image = item[JsonKeys.profileImage.rawValue]?.string{
            let media = MediaModel()
            media.url = image.toMediaUrl()
            barberShop.image = media
        }else if let image = item[JsonKeys.image.rawValue]?[JsonKeys.url.rawValue].string{
            let media = MediaModel()
            media.url = image
            barberShop.image = media
        }
        
        if let distance = item[JsonKeys.distance.rawValue]?.float {
            barberShop.distance = distance
        }
        
        if let ratingAvg = item[JsonKeys.ratingAvg.rawValue]?.float {
            barberShop.rating = ratingAvg
        }
        
        if let reviewsCount = item[JsonKeys.reviewsCount.rawValue]?.int {
            barberShop.countReview = reviewsCount
        }
        
        if let imagesCount = item[JsonKeys.imagesCount.rawValue]?.int {
            barberShop.countmedia = imagesCount
        }
        
        if let servicesCount = item[JsonKeys.servicesCount.rawValue]?.int {
            barberShop.countService = servicesCount
        }
        
        if let isFavorite = item[JsonKeys.isFavorite.rawValue]?.bool {
            barberShop.isFavorite = isFavorite
        }
        
        if let services = item[JsonKeys.services.rawValue]?.array {
            barberShop.services = getServices(items: services)
        }
        
        if let reviews = item[JsonKeys.reviews.rawValue]?.array {
            barberShop.reviews = getReviews(items: reviews)
        }
        
        if let schedules = item[JsonKeys.schedules.rawValue]?.array {
            barberShop.schedules = getSchedules(items: schedules)
        }
        
        if let images = item[JsonKeys.images.rawValue]?.array {
            barberShop.medias = getMedias(items: images)
        }
        if let tags = item[JsonKeys.tags.rawValue]?.array {
            barberShop.tags = getCategories(items: tags)
        }
        
        if let isBlocked = item[JsonKeys.isBlocked.rawValue]?.bool {
            barberShop.isBlocked = isBlocked
        }
        
        if let canReview = item[JsonKeys.canBeReviewed.rawValue]?.bool {
            barberShop.canReview = canReview
        }
        
        
        return barberShop
    }
    
    static func getReviews(items: [JSON]) -> [ReviewModel] {
        
        var reviewList: [ReviewModel] = []
        
        for item in items {
            reviewList.append(getReview(item: item.dictionaryValue))
        }
        
        return reviewList
    }
    
    static func getReview(item: [String: JSON]) -> ReviewModel {
        
        let review = ReviewModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            review.id = id
        }
        if let content = item[JsonKeys.review.rawValue]?.string {
            review.content = content
        }
        if let note = item[JsonKeys.rating.rawValue]?.int {
            review.note = note
        }
        if let dateCreated = item[JsonKeys.createdAt.rawValue]?.string {
            review.dateCreated = AppFunctions.convertDateToCurrent(currentDate: dateCreated.toDateUTC())
            //DebugHelper.debug(review.dateCreated)
        }
        if let userObject = item[JsonKeys.user.rawValue]?.dictionary {
            review.owner = getUser(item: userObject)
        }
        
        return review
    }
    
    static func getServices(items: [JSON]) -> [ServiceModel] {
        
        var serviceList: [ServiceModel] = []
        
        for item in items {
            let service = getService(item: item.dictionaryValue)
            if(service.id != -1){
                serviceList.append(service)
            }
        }
        
        return serviceList
    }
    
    static func getService(item: [String: JSON]) -> ServiceModel {
        
        let service = ServiceModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            service.id = id
        }
        if let name = item[JsonKeys.name.rawValue]?.string {
            service.name = name
        }
        if let price = item[JsonKeys.price.rawValue]?.double {
            service.price = price
        }
        if let duration = item[JsonKeys.duration.rawValue]?.string {
            service.duration = duration.toTime()
        }
        if let descr = item[JsonKeys.description.rawValue]?.string {
            service.description = descr
        }
        
        if let categoryObject = item[JsonKeys.category.rawValue]?.dictionary {
            service.category = getCategory(index: 0, item: categoryObject)
        }
        if let serviceLocation = item[JsonKeys.place.rawValue]?.string {
            service.serviceLocation = serviceLocation
        }
        
        return service
    }
    
    static func getCategories(items: [JSON]) -> [TagModel] {
        
        var categoryList: [TagModel] = []
        
        for index in 0..<items.count {
            //print("\(index) times 5 is \(index * 5)")
            categoryList.append(getCategory(index: index, item: items[index].dictionaryValue))
        }
        //for item in items {
        //    categoryList.append(getCategory(item: item.dictionaryValue))
        //}
        
        return categoryList
    }
    
    static func getCategory(index: Int, item: [String: JSON]) -> TagModel {
        
        let category = TagModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            category.id = id
        }
        if let name = item[JsonKeys.name.rawValue]?.string {
            category.name = name
        }
        if let icon = item[JsonKeys.icon.rawValue]?.string {
            category.icon = icon.toMediaUrl()
        }
        if let descr = item[JsonKeys.description.rawValue]?.string {
            category.descr = descr
        }
        if let order = item[JsonKeys.order.rawValue]?.int {
            category.order = order
        }
        category.size = AppFunctions.getTagSize(text : category.name, size: 13)
        category.position = index
        return category
    }
    
    static func getSchedules(items: [JSON]) -> [ScheduleModel] {
        
        var scheduleList: [ScheduleModel] = []
        
        for item in items {
            scheduleList.append(getSchedule(item: item.dictionaryValue))
        }
        scheduleList.sort {
            $0.day < $1.day
        }
        return scheduleList
    }
    
    static func getSchedule(item: [String: JSON]) -> ScheduleModel {
        
        let schedule = ScheduleModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            schedule.id = id
        }
        if let day = item[JsonKeys.day.rawValue]?.int {
            schedule.day = day
        }
        if let opening = item[JsonKeys.opening.rawValue]?.string {
            schedule.openTime = opening
        }
        if let closing = item[JsonKeys.closing.rawValue]?.string {
            schedule.closeTime = closing
        }
        if let breakFrom = item[JsonKeys.breakFrom.rawValue]?.string {
            schedule.breakFrom = breakFrom
        }
        if let breakTo = item[JsonKeys.breakTo.rawValue]?.string {
            schedule.breakTo = breakTo
        }
        
        if let isClosed = item[JsonKeys.isClosed.rawValue]?.bool {
            schedule.isOpen = !isClosed
        }
        if(!schedule.breakFrom.isEmpty && !schedule.breakTo.isEmpty){
            schedule.haveBreak = true
        }
       
        return schedule
    }
    
    static func getMedias(items: [JSON]) -> [MediaModel] {
        
        var mediaList: [MediaModel] = []
        
        for item in items {
            mediaList.append(getMedia(item: item.dictionaryValue))
        }
        
        return mediaList
    }
    
    static func getMedia(item: [String: JSON]) -> MediaModel {
        
        let media = MediaModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            media.id = id
        }
        if let type = item[JsonKeys.type.rawValue]?.string {
            media.type = type
        }
        if let originalName = item[JsonKeys.originalName.rawValue]?.string {
            media.originalName = originalName
        }
        if let size = item[JsonKeys.size.rawValue]?.string {
            media.size = size
        }
        if let url = item[JsonKeys.url.rawValue]?.string {
            media.url = url.toMediaUrl()
        }
        if let order = item[JsonKeys.order.rawValue]?.int {
            media.order = order
        }
        
        return media
    }
   
    static func getUsers(items: [JSON]) -> [UserModel] {
        var userList: [UserModel] = []
        for item in items {
            userList.append(getUser(item: item.dictionaryValue))
        }
        return userList
    }
    
    static func getUser(item: [String: JSON]) -> UserModel {

        let user = UserModel()
        if let id = item[JsonKeys.id.rawValue]?.int {
            user.id = id
        }
        if let fullname = item[JsonKeys.fullname.rawValue]?.string {
            user.fullname = fullname
        }
        if let address = item[JsonKeys.address.rawValue]?.string {
            user.address = address
        }
        if let zipcode = item[JsonKeys.zipcode.rawValue]?.string {
            user.zipcode = zipcode
        }
        if let city = item[JsonKeys.city.rawValue]?.string {
            user.city = city
        }
        if let country = item[JsonKeys.country.rawValue]?.string {
            user.country = country
        }
        if let lat = item[JsonKeys.latitude.rawValue]?.string {
            if(!lat.isEmpty){
                user.latitude = Double(lat)!
            }
        }else if let lat = item[JsonKeys.latitude.rawValue]?.double {
            user.latitude = lat
        }
        if let lng = item[JsonKeys.longitude.rawValue]?.string {
            if(!lng.isEmpty){
                user.longitude = Double(lng)!
            }
        }else if let lng = item[JsonKeys.longitude.rawValue]?.double {
            user.longitude = lng
        }
        if let phone = item[JsonKeys.phone.rawValue]?.string {
            user.phone = phone
        }
        if let descr = item[JsonKeys.description.rawValue]?.string {
            user.descr = descr
        }else if let descr = item["descr"]?.string {
            user.descr = descr
        }
        if let email = item[JsonKeys.email.rawValue]?.string {
            user.email = email
        }
        
        if let role = item[JsonKeys.role.rawValue]?.string {
            user.role = role
        }
        if let isVerified = item[JsonKeys.isVerified.rawValue]?.bool {
            user.isVerified = isVerified
        }
        if let blocked = item[JsonKeys.blocked.rawValue]?.bool {
            user.isBlocked = blocked
        }
        if let image = item[JsonKeys.profileImage.rawValue]?.string{
            let media = MediaModel()
            media.url = image.toMediaUrl()
            user.image = media
        }else if let image = item[JsonKeys.image.rawValue]?[JsonKeys.url.rawValue].string{
            let media = MediaModel()
            media.url = image
            user.image = media
        }
        
        if let orders = item[JsonKeys.orders.rawValue]?.array {
            user.orders = getOrders(items: orders)
        }
        
        if let stripeId = item["stripeId"]?.string {
            user.accountId = stripeId
        }else if let stripeId = item["accountId"]?.string {
            user.accountId = stripeId
        }
        return user
    }
    
    static func getOrders(items: [JSON]) -> [OrderModel] {
        
        var orderList: [OrderModel] = []
        
        for item in items {
            orderList.append(getOrder(item: item.dictionaryValue))
        }
        
        return orderList
    }
    
    static func getOrder(item: [String: JSON]) -> OrderModel {
        
        var order = OrderModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            order.id = id
        }
        if let number = item[JsonKeys.number.rawValue]?.string {
            order.number = number
        }
        /*if let start = item[JsonKeys.start.rawValue]?.string {
            order.start = start
        }*/
        if let subTotal = item[JsonKeys.subTotal.rawValue]?.double {
            order.subTotal = subTotal
        }
        if let total = item[JsonKeys.total.rawValue]?.double {
            order.totalPrice = total
        }
        if let fees = item[JsonKeys.fees.rawValue]?.double {
            order.fees = fees
        }
        if let currency = item[JsonKeys.currency.rawValue]?.string {
            order.currency = currency
        }
        if let note = item[JsonKeys.note.rawValue]?.string {
            order.note = note
        }
        if let orderDate = item["startAt"]?.string {
            
            order.orderDate = AppFunctions.convertDateToCurrent(currentDate: orderDate.toDateUTC())
        }
        if let duration = item[JsonKeys.duration.rawValue]?.string {
            order.duration = duration.toTime()
        }
        
        if let orderStatus = item[JsonKeys.orderStatus.rawValue]?.string {
            order.status = orderStatus
        }
        if let paymentStatus = item[JsonKeys.paymentStatus.rawValue]?.int {
            order.paymentStatus = paymentStatus
        }
        if let paymentIntent = item[JsonKeys.paymentIntent.rawValue]?.string {
            order.paymentIntent = paymentIntent
        }
        if let user = item[JsonKeys.user.rawValue]?.dictionary {
            order.customer = getUser(item: user)
        }
        if let barber = item[JsonKeys.user.rawValue]?.dictionary {
            order.barberShop = getBarberShop(item: barber)
        }
        if let orderLines = item[JsonKeys.orderLines.rawValue]?.array {
            order.orderLines = getOrderLines(items: orderLines)
        }
        
        if let serviceLocation = item[JsonKeys.place.rawValue]?.string {
            order.orderLocation = serviceLocation
        }
        return order
    }
    
    static func getOrderLines(items: [JSON]) -> [OrderLinesModel] {
        
        var orderLinesList: [OrderLinesModel] = []
        
        for item in items {
            orderLinesList.append(getOrderLine(item: item.dictionaryValue))
        }
        
        return orderLinesList
    }
    
    static func getOrderLine(item: [String: JSON]) -> OrderLinesModel {
        
        var orderLine = OrderLinesModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            orderLine.id = id
        }
        if let quantity = item[JsonKeys.quantity.rawValue]?.int {
            orderLine.quantity = quantity
        }
        if let price = item[JsonKeys.price.rawValue]?.double {
            orderLine.price = price
        }
        
        let service = ServiceModel()
        if let serviceId = item["serviceId"]?.int {
            service.id = serviceId
            if let duration = item[JsonKeys.duration.rawValue]?.string {
                service.duration = duration.toTime()
            }
            if let description = item[JsonKeys.description.rawValue]?.string {
                service.description = description
            }
            if let serviceLocation = item[JsonKeys.place.rawValue]?.string {
                service.serviceLocation = serviceLocation
            }
            if let serviceName = item["name"]?.string {
                service.name = serviceName
                orderLine.service = service
            }
        }
        return orderLine
    }

    static func getCards(items: [JSON]) -> [CardModel]{
        
        var cards: [CardModel] = []
        
        for item in items {
            cards.append(getCard(item: item.dictionaryValue))
        }
        
        return cards
    }
    
    static func getCard(item: [String: JSON]) -> CardModel {
       
        var cardModel = CardModel()
        
        if let paymentId = item["paymentMethodId"]?.string{
            cardModel.paymentId = paymentId
        }
        if let brand = item["brand"]?.string{
            cardModel.cartType = brand
        }
        if let dateMonth = item["exp_month"]?.int{
            cardModel.dateMonth = dateMonth
        }
        if let dateYear = item["exp_year"]?.int{
            cardModel.dateYear = dateYear
        }
        if let last4 = item["last4"]?.string{
            cardModel.last4 = last4
        }
        if let name = item["holderName"]?.string{
            cardModel.name = name
        }
        
        return cardModel
    }
    
    static func getAvailableSchedules(items: [JSON]) -> [AvailableScheduleModel] {
        
        var scheduleList: [AvailableScheduleModel] = []
        
        for item in items {
            scheduleList.append(getAvailableSchedule(item: item.dictionaryValue))
        }
      
        return scheduleList
    }
    
    static func getAvailableSchedule(item: [String: JSON]) -> AvailableScheduleModel {
        
        let schedule = AvailableScheduleModel()
      
        if let startAt = item["startAt"]?.string {
            let startDate = AppFunctions.convertDateToCurrent(currentDate: startAt.toDateUTC())
            //DebugHelper.debug(startDate)
            schedule.startAt = startDate
        }
        if let endAt = item["endAt"]?.string {
            let endDate = AppFunctions.convertDateToCurrent(currentDate: endAt.toDateUTC())
            //DebugHelper.debug(endDate)
            schedule.endAt = endDate
        }
      
        return schedule
    }
    
    static func getNotifs(items: [JSON]) -> [NotificationModel] {
        
        var notifList: [NotificationModel] = []
        
        for item in items {
            notifList.append(getNotif(item: item.dictionaryValue))
        }
        
        return notifList
    }
    
    static func getNotif(item: [String: JSON]) -> NotificationModel {
        
        var notif = NotificationModel()
        
        if let id = item[JsonKeys.id.rawValue]?.int {
            notif.id = id
        }
        if let type = item[JsonKeys.type.rawValue]?.string {
            notif.type = type
        }
        if let title = item[JsonKeys.title.rawValue]?.string {
            notif.title = title
        }
        if let message = item[JsonKeys.message.rawValue]?.string {
            notif.message = message
        }
        if let createdAt = item[JsonKeys.createdAt.rawValue]?.string {
            notif.createdAt = AppFunctions.convertDateToCurrent(currentDate: createdAt.toDateUTC())
            //DebugHelper.debug(notif.createdAt)
        }
        
        return notif
    }
}


