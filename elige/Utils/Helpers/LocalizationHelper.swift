//
//  Translation.swift
//  Homeat
//
//  Created by macbook pro on 4/5/2021.
//

import Foundation
import SwiftUI

enum LocalizationKeys: String {
    //error
    case error_social_reason_empty = "error_social_reason_empty"
    case error_fullname_empty = "error_fullname_empty"
    case error_email_empty = "error_email_empty"
    case error_email_validation = "error_email_validation"
    case error_phone_empty = "error_phone_empty"
    case error_phone_validation = "error_phone_validation"
    case error_password_empty = "error_password_empty"
    case error_password_validation = "error_password_validation"
    case error_password_confirmation_empty = "error_password_confirmation_empty"
    case error_password_confirmation_validation = "error_password_confirmation_validation"
    
    case error_code_empty = "error_code_empty"
    case error_code_validation = "error_code_validation"
    
    case label_now = "label_now"
    case label_one_minute_ago = "label_one_minute_ago"
    case label_one_hour_ago = "label_one_hour_ago"
    
    case label_second_ago = "label_second_ago"
    case label_minute_ago = "label_minute_ago"
    case label_hour_ago = "label_hour_ago"
    case label_today_at = "label_today_at"
    case label_yesterday_at = "label_yesterday_at"
    
    case error_service_name_empty = "error_service_name_empty"
    case error_service_category_empty = "error_service_category_empty"
    case error_service_price_empty = "error_service_price_empty"
    case error_service_price_validation = "error_service_price_validation"
    case error_service_duration_empty = "error_service_duration_empty"
    case error_service_duration_validation = "error_service_duration_validation"
    
    case error_service_empty = "error_service_empty"
    case error_time_empty = "error_time_empty";
    case error_card_empty = "error_card_empty";

    case message_payment_succeeded = "message_payment_succeeded";
    case message_payment_failed = "message_payment_failed"
    case message_payment_canceled = "message_payment_canceled"
    case message_payment_error = "message_payment_error"

    case message_stripe_account_completed = "message_stripe_account_completed";
    case message_stripe_account_uncompleted = "message_stripe_account_uncompleted"


    case title_confirm_order = "title_confirm_order";
    case message_confirm_order = "message_confirm_order";

    case title_cancel_order = "title_cancel_order";
    case message_cancel_order = "message_cancel_order";

    case title_finish_order = "title_finish_order";
    case message_finish_order = "message_finish_order";

    case title_no_show_order = "title_no_show_order";
    case message_no_show_order = "message_no_show_order";

    case title_user_blocked = "title_user_blocked";
    case message_user_blocked = "message_user_blocked";

    case title_user_unblocked = "title_user_unblocked";
    case message_user_unblocked = "message_user_unblocked";

    case title_service_remove = "title_service_remove";
    case message_service_remove = "message_service_remove";

    case title_block_user = "title_block_user";
    case message_block_user = "message_block_user";

    case title_unblock_user = "title_unblock_user";
    case message_unblock_user = "message_unblock_user";

    case title_remove_user = "title_remove_user";
    case message_remove_user = "message_remove_user";


    case list_service_shop_empty_title = "list_service_shop_empty_title";
    case list_service_shop_empty_message = "list_service_shop_empty_message";

    case list_customer_empty_title = "list_customer_empty_title";
    case list_customer_empty_message = "list_customer_empty_message";

    case list_order_empty_title = "list_order_empty_title";
    case list_order_empty_message = "list_order_empty_message";

    case list_order_shop_empty_title = "list_order_shop_empty_title";
    case list_order_shop_empty_message = "list_order_shop_empty_message";
    
    case list_service_empty_title = "list_service_empty_title";
    case list_service_empty_message = "list_service_empty_message";

    case list_comment_empty_title = "list_comment_empty_title";
    case list_comment_empty_message = "list_comment_empty_message";

    case list_photo_empty_title = "list_photo_empty_title";
    case list_photo_empty_message = "list_photo_empty_message";

    case list_notification_empty_title = "list_notification_empty_title";
    case list_notification_empty_message = "list_notification_empty_message";

    case list_favoris_empty_title = "list_favoris_empty_title";
    case list_favoris_empty_message = "list_favoris_empty_message";

    case list_shop_empty_title = "list_shop_empty_title";
    case list_shop_empty_message = "list_shop_empty_message";

    case title_logout = "title_logout";
    case message_logout = "message_logout";
    
    case error_serveur = "error_serveur";
    
    
    var localized: String {
        return self.rawValue.localized()
    }

    static func welcome(name: String) -> String {

        return String.localizedStringWithFormat(NSLocalizedString("Welcome %@", comment: ""), name)
    }
    
    static func secondAgo(sec: Int) -> String {

        return String.localizedStringWithFormat(label_second_ago.localized, sec)
    }
    static func minuteAgo(min: Int) -> String {

        return String.localizedStringWithFormat(label_minute_ago.localized, min)
    }
    static func hourAgo(hour: Int) -> String {

        return String.localizedStringWithFormat(label_hour_ago.localized, hour)
    }
    static func todayAt(time: String) -> String {

        return String.localizedStringWithFormat(label_today_at.localized, time)
    }
    static func yesterdayAt(time: String) -> String {

        return String.localizedStringWithFormat(label_yesterday_at.localized, time)
    }
    
}

