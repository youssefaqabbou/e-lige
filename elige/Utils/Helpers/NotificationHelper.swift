//
//  NotificationHelper.swift
//  elige
//
//  Created by macbook pro on 12/1/2022.
//

import Foundation
class NotificationHelper {
    static func postBarberShop (barberShop: BarberShopModel){
        NotificationCenter.default.post(name: NSNotification.shopUpdated,
                                        object: nil, userInfo: ["barberShop": barberShop])
    }
    
    static func postOrder (order: OrderModel){
        NotificationCenter.default.post(name: NSNotification.orderUpdated,
                                        object: nil, userInfo: ["order": order])
    }
    
    static func postConnectivityStatus (status: Bool){
        NotificationCenter.default.post(name: NSNotification.connectivityStatus,
                                        object: nil, userInfo: ["status": status])
    }
    
    static func postAppleResult (status: Bool, identityToken : String){
        NotificationCenter.default.post(name: NSNotification.AppleSignIn,
                                        object: nil, userInfo: ["success": status, "identityToken" : identityToken])
    }
    
}

extension NSNotification {
    static let GoogleSignIn = NSNotification.Name.init("GoogleSignIn")
    static let AppleSignIn = NSNotification.Name.init("AppleSignIn")
    
    static let shopUpdated = NSNotification.Name.init("BarberShopUpdated")
    static let profileUpdated = NSNotification.Name.init("profileUpdated")
    static let timeError = NSNotification.Name.init("timeError")
    static let breakTimeError = NSNotification.Name.init("breakTimeError")
    
    static let connectivityStatus = NSNotification.Name.init("Connectivity")
    static let orderUpdated = NSNotification.Name.init("orderUpdated")
    
}

