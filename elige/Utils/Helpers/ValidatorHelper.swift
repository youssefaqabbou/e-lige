//
//  ValidatorHelper.swift
//  Homeat
//
//  Created by macbook pro on 7/6/2021.
//

import Foundation


class ValidatorHelper {
    
    static let emailRegex = NSPredicate(format: "SELF MATCHES %@", "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\\]?)$")
    
    static let passwordRegex = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$")
    
    static let phoneRegex = NSPredicate(format: "SELF MATCHES %@", "^\\d{10}$")
    
    static let codeRegex = NSPredicate(format: "SELF MATCHES %@", "^\\d{4,4}$")
    
    static func validateEmail(email: String ) -> (success: Bool, error :String){
        
        if (email.isEmpty) {
            let errorMsg = LocalizationKeys.error_email_empty.localized
            return (false, errorMsg)
        } else if (!emailRegex.evaluate(with: email)) {
            let errorMsg = LocalizationKeys.error_email_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    
    static func validatePassword(password : String ) -> (success: Bool, error :String){
        
        if (password.isEmpty) {
            let errorMsg = LocalizationKeys.error_password_empty.localized
            return (false, errorMsg)
        }else if (!passwordRegex.evaluate(with: password)) {
            let errorMsg =  LocalizationKeys.error_password_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg = ""
            return (true, errorMsg)
        }
    }
    
    static func validatePasswordConfirmation(password: String, passwordConfirm : String) -> (success: Bool, error :String) {
        if (passwordConfirm.isEmpty) {
            let errorMsg = LocalizationKeys.error_password_confirmation_empty.localized
            return (false, errorMsg)
        } else if (password != passwordConfirm) {
            let errorMsg =  LocalizationKeys.error_password_confirmation_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg = ""
            return (true, errorMsg)
        }
    }
    
    static func validateFullname(fullname: String) -> (success: Bool, error :String){
        if (fullname.isEmpty) {
            let errorMsg = LocalizationKeys.error_fullname_empty.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    
    static func validateSocialReason(socialReason: String) -> (success: Bool, error :String){
        if (socialReason.isEmpty) {
            let errorMsg = LocalizationKeys.error_social_reason_empty.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    
    static func validatePhone(phone: String) -> (success: Bool, error :String) {
       
        if (phone.isEmpty) {
            let errorMsg = LocalizationKeys.error_phone_empty.localized
            return (false, errorMsg)
        } else if (!phoneRegex.evaluate(with: phone)) {
            let errorMsg = LocalizationKeys.error_phone_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg = ""
            return (true, errorMsg)
        }
    }
    
    static func validateCode(code: String) -> (success: Bool, error :String) {
       
        if (code.isEmpty) {
            let errorMsg = LocalizationKeys.error_code_empty.localized
            return (false, errorMsg)
        } else if (!codeRegex.evaluate(with: code)) {
            let errorMsg = LocalizationKeys.error_code_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg = ""
            return (true, errorMsg)
        }
    }
    
    static func validateNumber(num : String) -> (success: Bool, error :String){
        return (true, "")
    }
    
    static func validateServiceName(name: String) -> (success: Bool, error :String){
        if (name.isEmpty) {
            let errorMsg = LocalizationKeys.error_service_name_empty.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    
    static func validateServicePrice(price: String) -> (success: Bool, error :String){
        if (price.isEmpty) {
            let errorMsg = LocalizationKeys.error_service_price_empty.localized
            return (false, errorMsg)
        } else if(Double(price) == nil ){
            let errorMsg = LocalizationKeys.error_service_price_validation.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    static func validateServiceDuration(duration: String) -> (success: Bool, error :String){
        DebugHelper.debug("duration", duration)
        if (duration.isEmpty || duration == "0:0") {
            let errorMsg = LocalizationKeys.error_service_duration_empty.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
    
    static func validateServiceCategory(category: Int) -> (success: Bool, error :String){
        if (category == -1 ) {
            let errorMsg = LocalizationKeys.error_service_category_empty.localized
            return (false, errorMsg)
        } else {
            let errorMsg  = ""
            return (true, errorMsg)
        }
    }
}
