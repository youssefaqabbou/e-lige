//
//  ViewHelper.swift
//  Homeat
//
//  Created by macbook pro on 4/6/2021.
//

import Foundation
import SwiftUI

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( AppUtils.CustomCorners(corners: corners, radius: radius) )
    }
    
    func getSafeArea()->UIEdgeInsets{
        return UIApplication.shared.windows.first?.safeAreaInsets ?? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func hideKeyboard() {
        //let resign = #selector(UIResponder.resignFirstResponder)
        //UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
        UIApplication.shared.windows.first{$0.isKeyWindow }?.endEditing(false)
    }
    /*
    func keyboardAdaptive() -> some View {
         ModifiedContent(content: self, modifier: KeyboardAdaptive())
     }
     */
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
