//
//  CustomRadioButtonField.swift
//  Click&Eat
//
//  Created by user196402 on 8/2/21.
//

import Foundation
import SwiftUI

struct RadioButtonField: View {
    let id: Int
    let label: String
    let size: CGFloat
    let color: Color
    let bgColor: Color
    let textSize: CGFloat
    let isMarked:Bool
    let isRtl :  Bool
    let callback: (Int)->()
    
    
    init(
        id: Int,
        label:String,
        size: CGFloat = 20,
        color: Color = Color.black,
        bgColor: Color = Color.black,
        textSize: CGFloat = 14,
        isMarked: Bool = false,
        isRtl :  Bool = false,
        callback: @escaping (Int)->()
        ) {
        self.id = id
        self.label = label
        self.size = size
        self.color = color
        self.bgColor = bgColor
        self.textSize = textSize
        self.isMarked = isMarked
        self.isRtl = isRtl
        self.callback = callback
    }
    
    var body: some View {
        
        Button(action:{
            
            self.callback(self.id)
            
        }) {
            
            HStack(alignment: .center) {
                
                if isRtl {
                    
                    if self.isMarked{
                        
                        Image(systemName: "checkmark.circle.fill")
                            .resizable()
                            .clipShape(Circle())
                            .foregroundColor(self.bgColor)
                            .frame(width: 25, height: 25)
                        
                    }else{
                       
                        Circle()
                            .strokeBorder(Color.primaryColor,lineWidth: 1)
                            .frame(width: 25, height: 25)
                        
                    }
                   
                    Text(label)
                        .font(.system(weight: self.isMarked ? .bold : .medium, size: textSize))
                        .padding(.leading, AppConstants.viewSmallMargin)
                    
                    Spacer()
                   
                }else{
                    
                    Spacer()
                    
                    Text(label)
                        .font(.system(weight: self.isMarked ? .bold : .medium, size: textSize))
                        .padding(.trailing, AppConstants.viewSmallMargin)
                    
                    if self.isMarked{
                        Image(systemName: "checkmark.circle.fill")
                            .resizable()
                            .clipShape(Circle())
                            .foregroundColor(self.bgColor)
                            .frame(width: 25, height: 25)
                        
                    }else{
                        Circle()
                            .strokeBorder(Color.primaryColor,lineWidth: 1)
                            .frame(width: 25, height: 25)
                        
                    }
                    
                }
                
            }.foregroundColor(self.color)
        }
        .foregroundColor(Color.white)
    }
}
