//
//  BottomSheet.swift
//  elige
//
//  Created by macbook pro on 8/11/2021.
//

import Foundation
import SwiftUI

public struct BottomSheet<Content: View>: View {
    
    private var dragToDismissThreshold: CGFloat { height * 0.2 }
    private var grayBackgroundOpacity: Double { isPresented ? (0.4 - Double(draggedOffset)/600) : 0 }
    
    @State private var draggedOffset: CGFloat = 0
    @State private var previousDragValue: DragGesture.Value?

    @Binding var isPresented: Bool
    private let height: CGFloat
    private let topBarHeight: CGFloat
    private let topBarCornerRadius: CGFloat
    private let content: Content
    private let contentBackgroundColor: Color
    private let topBarBackgroundColor: Color
    private let showTopIndicator: Bool
    private let onTapOutside: Bool
    private let onDrag: Bool
    
    public init(
        isPresented: Binding<Bool>,
        height: CGFloat,
        topBarHeight: CGFloat = 30,
        topBarCornerRadius: CGFloat? = nil,
        topBarBackgroundColor: Color = Color(.systemBackground),
        contentBackgroundColor: Color = Color(.systemBackground),
        showTopIndicator: Bool,
        onTapOutside : Bool,
        onDrag : Bool,
        @ViewBuilder content: () -> Content
    ) {
        self.topBarBackgroundColor = topBarBackgroundColor
        self.contentBackgroundColor = contentBackgroundColor
        self._isPresented = isPresented
        self.height = height
        self.topBarHeight = topBarHeight
        if let topBarCornerRadius = topBarCornerRadius {
            self.topBarCornerRadius = topBarCornerRadius
        } else {
            self.topBarCornerRadius = topBarHeight / 3
        }
        self.showTopIndicator = showTopIndicator
        self.onTapOutside = onTapOutside
        self.onDrag =  onDrag
        self.content = content()
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack {
                self.fullScreenLightGrayOverlay()
                VStack(spacing: 0) {
                    self.topBar(geometry: geometry)
                    VStack(spacing: -8) {
                        Spacer()
                        self.content.padding(.bottom, geometry.safeAreaInsets.bottom)
                        Spacer()
                    }
                }
                .frame(height: self.height - min(self.draggedOffset*2, 0))
                .background(self.contentBackgroundColor)
                .cornerRadius(self.topBarCornerRadius, corners: [.topLeft, .topRight])
                //.animation(.interactiveSpring())
                .offset(y: self.isPresented ? (geometry.size.height/2 - self.height/2 + geometry.safeAreaInsets.bottom + self.draggedOffset) : (geometry.size.height/2 + self.height/2 + geometry.safeAreaInsets.bottom))
            }
        }
    }
    
    fileprivate func fullScreenLightGrayOverlay() -> some View {
        Color
            .black
            .opacity(grayBackgroundOpacity)
            .edgesIgnoringSafeArea(.all)
            //.animation(.interactiveSpring())
            .onTapGesture {
                if(onTapOutside){
                    withAnimation(.linear) {
                        self.isPresented = false
                    }
                }
                
            }
    }
    
    fileprivate func topBar(geometry: GeometryProxy) -> some View {
        ZStack {
            RoundedRectangle(cornerRadius: 6)
                .fill(Color.secondary)
                .frame(width: 40, height: 6)
                .opacity(showTopIndicator ? 1 : 0)
        }
        .frame(width: geometry.size.width, height: topBarHeight)
        .background(topBarBackgroundColor)
        .gesture(
            DragGesture()
                .onChanged({ (value) in
                    if(onDrag){
                        let offsetY = value.translation.height
                        withAnimation(.interactiveSpring()) {
                            self.draggedOffset = offsetY
                        }
                        
                        
                        if let previousValue = self.previousDragValue {
                            let previousOffsetY = previousValue.translation.height
                            let timeDiff = Double(value.time.timeIntervalSince(previousValue.time))
                            let heightDiff = Double(offsetY - previousOffsetY)
                            let velocityY = heightDiff / timeDiff
                            if velocityY > 1400 {
                                withAnimation(.interactiveSpring()) {
                                    self.isPresented = false
                                }
                                return
                            }
                        }
                        self.previousDragValue = value
                    }
                    
                })
                .onEnded({ (value) in
                    if(onDrag){
                        let offsetY = value.translation.height
                        if offsetY > self.dragToDismissThreshold {
                            withAnimation(.interactiveSpring()) {
                                self.isPresented = false
                            }
                        }
                        withAnimation(.interactiveSpring()) {
                            self.draggedOffset = 0
                        }
                    }
                })
        )
    }
}

public extension View {
    func bottomSheet<Content: View>(
        isPresented: Binding<Bool>,
        height: CGFloat,
        topBarHeight: CGFloat = 30,
        topBarCornerRadius: CGFloat? = nil,
        contentBackgroundColor: Color = Color(.systemBackground),
        topBarBackgroundColor: Color = Color(.systemBackground),
        showTopIndicator: Bool = true,
        onTapOutside: Bool = true,
        onDrag : Bool = false,
        @ViewBuilder content: @escaping () -> Content
    ) -> some View {
        ZStack {
            self
            BottomSheet(isPresented: isPresented,
                        height: height,
                        topBarHeight: topBarHeight,
                        topBarCornerRadius: topBarCornerRadius,
                        topBarBackgroundColor: topBarBackgroundColor,
                        contentBackgroundColor: contentBackgroundColor,
                        showTopIndicator: showTopIndicator,
                        onTapOutside : onTapOutside,
                        onDrag : onDrag,
                        content: content)
        }
    }
}
