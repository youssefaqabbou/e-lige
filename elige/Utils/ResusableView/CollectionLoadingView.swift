//
//  CollectionLoadingView.swift
//  CollectionLoadingView
//
//  Created by macbook pro on 23/8/2021.
//

import Foundation
import SwiftUI

public enum CollectionLoadingState {
    case loading, loaded, empty, error
}

public struct CollectionLoadingView<PlaceHolderContent : View,  Content: View, EmptyView: View, ErrorView: View>: View {

    private let fade = AnyTransition.opacity.animation(Animation.linear(duration: 0.5))
    private let state: CollectionLoadingState

    private let makePlaceHolder: () -> PlaceHolderContent
    private let makeContent: () -> Content
    private let makeEmpty: () -> EmptyView
    private let makeError: () -> ErrorView

    public init(loadingState: CollectionLoadingState,
        @ViewBuilder placeHolder: @escaping () -> PlaceHolderContent,
        @ViewBuilder content: @escaping () -> Content,
        @ViewBuilder empty: @escaping () -> EmptyView,
        @ViewBuilder error: @escaping () -> ErrorView) {
        state = loadingState
        makePlaceHolder = placeHolder
        makeContent = content
        makeEmpty = empty
        makeError = error
    }
    
    public var body: some View {
        
        ZStack(){
            makePlaceHolder()
                .disabled(true).opacity(state == .loading ? 1 : 0 )
            
            makeContent().opacity(state == .loaded ? 1 : 0 )
            
            makeEmpty()
                .disabled(true).opacity(state == .empty ? 1 : 0 )
            
            makeError()
                .disabled(true).opacity(state == .error ? 1 : 0 )
        }
    }
}


