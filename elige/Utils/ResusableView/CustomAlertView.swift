//
//  CustomAlertView.swift
//  Homeat
//
//  Created by macbook pro on 24/5/2021.
//

import SwiftUI

struct CustomAlertView: View {
    
    @Binding var showingCustomAlert: Bool
    
    var isCustomIcon = false
    var customIcon = Image.smallLogoIcon
    
    var isSuccess = true
    var title: String
    var message: String
    
    var titlePositive = "Oui"
    var positiveAction = { }
    
    var isNegative = false
    var titleNegative = "Non"
    var negativeAction = { }
    
    var body: some View {
        Group {
            ZStack {
                if showingCustomAlert {
                    VStack() {
                        if(isCustomIcon) {
                            customIcon.resizable().frame(width: 150, height: 80, alignment: .center)
                                .padding(.top, AppConstants.viewExtraMargin)
                        } else {
                            //                            if(isSuccess) {
                            //                                Image.successIcon.resizable().frame(width: 100, height: 100, alignment: .center)
                            //                                    .padding(.top, 20)
                            //                            } else {
                            //                                Image.errorIcon.resizable().frame(width: 100, height: 100, alignment: .center)
                            //                                    .padding(.top, 20)
                            //                            }
                        }
                        
                        Text(title)
                            .font(.system(weight: .medium, size: 22))
                            .foregroundColor(isSuccess ? Color.primaryColor : Color.primaryColor)
                            .padding(.top, AppConstants.viewExtraMargin)
                            .lineLimit(2)
                        
                        Text(message)
                            .font(.system(weight: .medium, size: 16))
                            .foregroundColor(Color.primaryTextColor)
                            .padding(.top, AppConstants.viewExtraMargin)
                            .lineLimit(5)
                        
                        HStack(spacing: 20) {
                            if(isNegative) {
                                Button(action: {
                                    negativeAction()
                                }, label: {
                                    Text(titleNegative).textCase(.uppercase)
                                }).buttonStyle(OutlineStyle())
                            }
                            
                            Button(action: {
                                positiveAction()
                            }, label: {
                                Text(titlePositive).textCase(.uppercase)
                            }).buttonStyle(PrimaryButtonStyle())
                        }.padding(.top, AppConstants.viewExtraMargin)
                    }
                    .padding(.all, 25)
                    .frame(maxWidth: .infinity)
                    .background(Color.bgViewColor.clipShape(AppUtils.CustomCorners(corners: [.allCorners], radius: AppConstants.alertRadius)).shadow(color: .shadowAlert, radius: AppConstants.shadowRadiusValue, x: 0.0, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/))
                    .padding(.all, 15)
                }
            } .animation(.default, value: showingCustomAlert)
        }
        
        
    }
}

struct CustomAlertView_Previews: PreviewProvider {
    static var previews: some View {
        CustomAlertView(showingCustomAlert: .constant(false), isCustomIcon: true, isSuccess: false, title: LocalizationKeys.error_phone_empty.localized, message: "", positiveAction: { })
        
    }
}
