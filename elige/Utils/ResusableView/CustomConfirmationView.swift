//
//  ConfirmationViewItem.swift
//  elige
//
//  Created by user196417 on 12/6/21.
//

import SwiftUI

struct CustomConfirmationView: View {
    
    var isCustomIcon = false
    var customIcon = Image.smallLogoIcon
    
    var isSuccess = true
    var title: String
    var message: String
    
    var titlePositive = "Oui"
    var positiveAction = { }
    
    var isNegative = false
    var titleNegative = "Non"
    var negativeAction = { }
    
    var body: some View {
        
        VStack(spacing: 10){
            if(isCustomIcon) {
                customIcon.resizable().frame(width: 120, height: 120, alignment: .center)
                    //.padding(.top, AppConstants.viewExtraMargin)
            }else{
                Image.smallLogoIcon.resizable().frame(width: 130, height: 90, alignment: .center)
                    //.padding(.top, AppConstants.viewExtraMargin)
            }
           
            Text(title)
                .font(.system(weight: .regular, size: 24))
                .foregroundColor(Color.primaryTextColor)
                .multilineTextAlignment(.center)
                //.padding(.top, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewExtraMargin)
            Text(message)
                .font(.system(weight: .regular, size: 16))
                .foregroundColor(Color.secondaryTextColor)
                .multilineTextAlignment(.center)
                .padding(.horizontal, AppConstants.viewExtraMargin)
            
            HStack(spacing: 40){
                
                if(isNegative){
                    Button(action: {
                        negativeAction()
                    }, label: {
                        Text(titleNegative)
                            .font(.system(weight: .regular, size: 18, font:  FontStyle.brownStd.rawValue))
                        
                    }).buttonStyle(PrimaryButtonStyle(bgColor: Color.lightGray, fgColor: Color.primaryTextColor))
                }
                
                Button(action: {
                    positiveAction()
                }, label: {
                    Text(titlePositive)
                        .font(.system(weight: .regular, size: 18, font:  FontStyle.brownStd.rawValue))
                    
                }).buttonStyle(PrimaryButtonStyle())
                
               
            }.padding(.horizontal, AppConstants.viewExtraMargin)
                .padding(.top, AppConstants.viewExtraMargin)
        }
        
        
    }
}

struct ConfirmationViewItem_Previews: PreviewProvider {
    static var previews: some View {
        CustomConfirmationView(title: "", message: "")
    }
}
