//
//  CustomEmptyView.swift
//  Homeat
//
//  Created by macbook pro on 7/6/2021.
//

import Foundation
import SwiftUI

struct CustomEmptyView: View {
    
    var showIcon = false
    var icon = Image.smallLogoIcon
    
    var title : String
    var message : String
    
    var isShowButton = false
    var titleButton = "Oui"
    var buttonAction = {}
    
    var body: some View {
         
        VStack(alignment: .center){
        
            if(showIcon){
                icon.resizable().frame(width: 200, height: 150, alignment: .center)
                    .padding(.top, AppConstants.viewExtraMargin)
                    .opacity(0.8)
            }
            
            Text(title)
                .font(.system(weight: .medium, size: 24))
                .foregroundColor(Color.primaryTextColor)
                .multilineTextAlignment(.center)
                .padding(.top, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewExtraMargin)
                //.lineLimit(2)
            
            Text(message)
                .font(.system(weight: .regular, size: 16))
                .foregroundColor(Color.secondaryTextColor.opacity(0.5))
                .multilineTextAlignment(.center)
                .padding(.top, AppConstants.viewExtraMargin)
                .padding(.horizontal, AppConstants.viewExtraMargin)
                //.lineLimit(3)
            
            
            if(isShowButton){
                Button(action: {
                    buttonAction()
                }, label: {
                    Text(titleButton)
                })//.buttonStyle(NegativeButtonStyle())
            }
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
        
    }
}
