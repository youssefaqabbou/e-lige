//
//  CustomLoadingButton.swift
//  Click&Eat
//
//  Created by macbook pro on 23/8/2021.
//

import Foundation
import SwiftUI

struct CustomLoadingButton: View {
    
    @Binding var isLoading: Bool
    let text: String
    var btnStyle : PrimaryButtonStyle = PrimaryButtonStyle()
    var height : CGFloat = 60
    let action: () -> Void
    
    var body: some View {
        
        Button(action: {
            action()
        }, label: {
            HStack {
                
                if (isLoading) {
                    CircularProgressView()
                        .frame(width: 30, height: 15, alignment: .center)
                } else {
                    Text(text).font(.system(weight: .regular, size: 16))
                }

            }.padding(AppConstants.buttonMargin)
            
        })
        .frame(height: height)
        .frame(minHeight: height)
        .buttonStyle(btnStyle)
        .disabled(isLoading)

    }
}


struct CustomLoadingButtonV<ContentView>: View where ContentView: View {
    
    @Binding var isLoading: Bool
    let content: () -> ContentView
    let action: () -> Void
    let btnStyle : PrimaryButtonStyle = PrimaryButtonStyle()
    
    var body: some View {
        
        Button(action: {
            action()
        }, label: {
            HStack {
               
                if (isLoading) {
                    CircularProgressView()
                        .frame(width: 30, height: 15, alignment: .center)
                } else {
                    content()
                }

            }.padding(AppConstants.buttonMargin)
            
        }).buttonStyle(btnStyle)
        .disabled(isLoading)

    }
}


struct CircularProgressView: View {
    
    @State private var isCircleRotating = true
    @State private var animateStart = true
    @State private var animateEnd = false
    var width : CGFloat = 30
    var height : CGFloat = 30
    var lineWidth : CGFloat = 4
    var body: some View {
        
        ZStack {
            
            Circle()
                .stroke(lineWidth: lineWidth)
                .fill(Color.clear)
                .frame(width: width, height: height)
            
            Circle()
                .trim(from: animateStart ? 1/3 : 1/9, to: animateEnd ? 2/5 : 1)
                .stroke(lineWidth: lineWidth)
                .rotationEffect(.degrees(isCircleRotating ? 360 : 0))
                .frame(width: width, height: height)
                .foregroundColor(Color.white)
                .onAppear() {
                    withAnimation(Animation.linear(duration: 1)
                                    .repeatForever(autoreverses: false)) {
                        self.isCircleRotating.toggle()
                    }
                    withAnimation(Animation.linear(duration: 1)
                                    .delay(0.5)
                                    .repeatForever(autoreverses: true)) {
                        self.animateStart.toggle()
                    }
                    withAnimation(Animation.linear(duration: 1)
                                    .delay(1)
                                    .repeatForever(autoreverses: true)) {
                        self.animateEnd.toggle()
                    }
                }
        }
    }
}
