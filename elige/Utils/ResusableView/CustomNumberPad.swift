//
//  CustomNumberPad.swift
//  Homeat
//
//  Created by macbook pro on 29/6/2021.
//

import Foundation
import SwiftUI
import HCalendar

struct PinDots : View{
    var width : CGFloat
    @Binding var codes : [String]
    var maxDigits: Int = 4
    var spacing: Int = 5
    @State var w: CGFloat = 30 ;
    
    var returnWidth : (CGFloat) -> () = { width in }
    
    var body : some View{
        
        HStack {
            ForEach(0..<maxDigits) { index in
                
                VStack(spacing: 5){
                    
                    Text( getChar(index: index))
                        .foregroundColor(.primaryTextColor.opacity(1.0))
                        .font(.system(weight: .bold, size: 22))
                        .frame( minHeight: 30)
                        .frame( height: 30)
                        
                }.frame(width: w , height:  w, alignment: .center)
                    .background(Color.secondaryColor)
                    .cornerRadius(15)
                    
            }
        }.frame(height:  w)
        .onAppear {
            w = CGFloat(((Int(width) - (spacing * maxDigits)) / maxDigits))
            DebugHelper.debug(w)
            returnWidth(w)
        }
    }
    
    func getChar(index: Int) -> String{
        
        if(codes.count > index){
            
            return codes[index]
            
        }else{
            
           return ""
        }
    }
}

struct CustomNumberPad : View {
    
    @Binding var codes : [String]
    var maxDigits: Int = 4
    
    var endAction: (_ code : String) -> () = {_ in }
    
    var body : some View{
        
        VStack(alignment: .leading, spacing: 25){
            
            ForEach(datas){type in
                
                HStack(spacing: -120){
                    
                    Spacer()
                    
                    ForEach(type.row){row in
                        
                        VStack{
                            
                            HStack{
                                
                                Spacer()
                                
                                Button(action: {
                                    
                                    if row.value == "delete.left.fill"{
                                        if(!self.codes.isEmpty){
                                            self.codes.removeLast()
                                        }
                                        DebugHelper.debug(getCode())
                                    }
                                    else{
                                        self.codes.append(row.value)
                                        DebugHelper.debug(getCode())
                                        if self.codes.count == maxDigits{
                                            endAction(getCode())
                                        }
                                    }
                                }) {
                                    
                                    if row.value == "delete.left.fill"{
                                        Image.deleteLeftIcon
                                            .resizable()
                                            .renderingMode(.template)
                                            .colorMultiply(.primaryTextColor)
                                            .frame(width: 30, height: 20, alignment: .center)
                                            //.padding(.vertical)
                                    } else {
                                        Text(row.value)
                                            .foregroundColor(.primaryTextColor)
                                            .font(.system(weight: .bold, size: 24))
                                            .frame(width: 30, height: 30, alignment: .center)
                                            //.padding(.vertical)
                                    }
                                }
                                Spacer()
                                
                            }
                            
                        }
                    }
                    Spacer()
                }
            }
        }.foregroundColor(.white)
    }
    
    func getspacing()->CGFloat{
        
        return UIScreen.main.bounds.width / 3
    }
    
    func getCode()->String{
        
        var code = ""
        
        for i in self.codes{
        
            code += i
            
        }

        return code.replacingOccurrences(of: " ", with: "")
    }
}

// datas...

struct type : Identifiable {
    
    var id : Int
    var row : [row]
}

struct row : Identifiable {
    
    var id : Int
    var value : String
}

var datas = [

type(id: 0, row: [row(id: 0, value: "1"),row(id: 1, value: "2"),row(id: 2, value: "3")]),
type(id: 1, row: [row(id: 0, value: "4"),row(id: 1, value: "5"),row(id: 2, value: "6")]),
type(id: 2, row: [row(id: 0, value: "7"),row(id: 1, value: "8"),row(id: 2, value: "9")]),
type(id: 3, row: [row(id: 0, value: ""),row(id: 1, value: "0"),row(id: 2, value: "delete.left.fill")])
    
]
