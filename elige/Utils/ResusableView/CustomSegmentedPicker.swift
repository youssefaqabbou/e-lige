//
//  CustomSegmentedPicker.swift
//  elige
//
//  Created by user196417 on 11/24/21.
//

import Foundation
import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
}

struct SizePreferenceKey: PreferenceKey {
    typealias Value = CGSize
    static var defaultValue: CGSize = .zero
    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {
        value = nextValue()
    }
}

struct BackgroundGeometryReader: View {
    var body: some View {
        GeometryReader { geometry in
            return Color
                    .clear
                    .preference(key: SizePreferenceKey.self, value: geometry.size)
        }
    }
}

struct SizeAwareViewModifier: ViewModifier {

    @Binding private var viewSize: CGSize

    init(viewSize: Binding<CGSize>) {
        self._viewSize = viewSize
    }

    func body(content: Content) -> some View {
        content
            .background(BackgroundGeometryReader())
            .onPreferenceChange(SizePreferenceKey.self, perform: { if self.viewSize != $0 { self.viewSize = $0 }})
    }
}

struct CustomSegmentedPicker: View {
    private static let ActiveSegmentColor: Color = Color.primaryColor
    private static let BackgroundColor: Color = Color.bgViewSecondaryColor
    private static let ShadowColor: Color = Color.black.opacity(0.2)
    private static let TextColor: Color = Color.secondaryTextColor
    private static let SelectedTextColor: Color = Color.white

    private static let TextFont: Font = .system(weight: .regular, size: 12)
    
    private static let SegmentCornerRadius: CGFloat = .infinity
    private static let ShadowRadius: CGFloat = 4
    private static let SegmentXPadding: CGFloat = 16
    private static let SegmentYPadding: CGFloat = 8
    private static let PickerPadding: CGFloat = 4
    
    private static let AnimationDuration: Double = 0.1
    
    // Stores the size of a segment, used to create the active segment rect
    @State private var segmentSize: CGSize = .zero
    // Rounded rectangle to denote active segment
    private var activeSegmentView: AnyView {
        // Don't show the active segment until we have initialized the view
        // This is required for `.animation()` to display properly, otherwise the animation will fire on init
        let isInitialized: Bool = segmentSize != .zero
        if !isInitialized { return EmptyView().eraseToAnyView() }
        return
            Circle()
            .foregroundColor(CustomSegmentedPicker.ActiveSegmentColor)
            //.shadow(color: SegmentedPicker.ShadowColor, radius: SegmentedPicker.ShadowRadius)
            .frame(width: self.segmentSize.width, height: self.segmentSize.height)
            .offset(x: self.computeActiveSegmentHorizontalOffset(), y: 0)
            .animation(Animation.linear(duration: CustomSegmentedPicker.AnimationDuration))
            .eraseToAnyView()
            
    }
    
    @Binding private var selection: Int
    private let items: [Image]
    private var onSelectionChanged : () -> () = {}
    
    init(items: [Image], selection: Binding<Int>) {
        self._selection = selection
        self.items = items
    }
    
    init(items: [Image], selection: Binding<Int>, action: @escaping () -> () ) {
        self._selection = selection
        self.items = items
        self.onSelectionChanged = action
    }
    
    var body: some View {
        // Align the ZStack to the leading edge to make calculating offset on activeSegmentView easier
        ZStack(alignment: .leading) {
            // activeSegmentView indicates the current selection
            self.activeSegmentView
            HStack {
                ForEach(0..<self.items.count, id: \.self) { index in
                    self.getSegmentView(for: index)
                }
            }
        }
        .padding(CustomSegmentedPicker.PickerPadding)
        .background(CustomSegmentedPicker.BackgroundColor)
        .clipShape(RoundedRectangle(cornerRadius: .infinity))
    }

    // Helper method to compute the offset based on the selected index
    private func computeActiveSegmentHorizontalOffset() -> CGFloat {
        CGFloat(self.selection) * (self.segmentSize.width + CustomSegmentedPicker.SegmentXPadding / 2)
    }

    // Gets text view for the segment
    private func getSegmentView(for index: Int) -> some View {
        guard index < self.items.count else {
            return EmptyView().eraseToAnyView()
        }
        let isSelected = self.selection == index
        return
            self.items[index]
                .resizable()
                .renderingMode(.template)
                .frame(width: 18, height: 18)
                .colorMultiply(isSelected ? CustomSegmentedPicker.SelectedTextColor: CustomSegmentedPicker.TextColor)
                .foregroundColor(isSelected ? CustomSegmentedPicker.SelectedTextColor: CustomSegmentedPicker.TextColor)
                .padding(.vertical, CustomSegmentedPicker.SegmentYPadding)
                .padding(.horizontal, CustomSegmentedPicker.SegmentXPadding)
                .frame(minWidth: 0, maxWidth: .infinity)
                // Watch for the size of the
                .modifier(SizeAwareViewModifier(viewSize: self.$segmentSize))
                .onTapGesture { self.onItemTap(index: index) }
                .eraseToAnyView()
    }

    // On tap to change the selection
    private func onItemTap(index: Int) {
        guard index < self.items.count else {
            return
        }
        self.selection = index
        onSelectionChanged()
    }
    
}
