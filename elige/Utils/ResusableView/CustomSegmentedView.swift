//
//  CustomSegmentedView.swift
//  elige
//
//  Created by macbook pro on 31/1/2022.
//

import Foundation
import SwiftUI

struct CustomSegmentedView : View {
    @State var titles = ["Salon", "À domicile", "Les deux"]
    @Binding var selected : Int
    @State var frame : CGFloat
    var onValueChaged = {}
    @State private var frames = Array<CGRect>(repeating: .zero, count: 3)
    
    func setFrame(index: Int, frame: CGRect) {
        self.frames[index] = frame
      }
    
    var body : some View{
        
        ZStack(){
            HStack(spacing : 0){
                
                ForEach(self.titles.indices, id: \.self) { index in
                    
                    Text(self.titles[index])
                    .font(.system(weight: .bold, size: 14))
                    .foregroundColor(Color.primaryTextColor)
                    .frame(width: frame / CGFloat(self.titles.count) , height: 35)
                    .background(
                        GeometryReader { geo in
                            Color.clear.opacity(0).onAppear { self.setFrame(index: index, frame: geo.frame(in: .global))
                                DebugHelper.debug("clear onAppear")
                            }
                        }
                    ).clipShape(Capsule())
                        .onTapGesture(perform: {
                            withAnimation {
                                self.selected = index
                            }
                            onValueChaged()
                        })
                }
            }.background(
                Capsule().fill(Color.primaryColor)
                    .frame(width: frame / CGFloat(self.titles.count),
                           height: 35, alignment: .topLeading)
                    .offset(x: self.frames[selected].minX - self.frames[0].minX)
                , alignment: .leading
            )
        }
       .padding(4)
        .background(Color.editTextBgColor)
        .clipShape(Capsule())
    }
}
