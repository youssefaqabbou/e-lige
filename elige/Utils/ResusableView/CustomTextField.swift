//
//  ContentView.swift
//  GlooGlooTest
//
//  Created by user179265 on 3/12/21.
//


import SwiftUI

// TODO: add icons and Divider

struct CustomTextField: View {
    var placeholder: String = ""
    @Binding var value: String
    var error: String = ""
    var isSecured: Bool = false
    var isPhone: Bool = false
   // @State var passwordVisibility = true

    var keyboardType: UIKeyboardType = .default
    
    //var icon : Image
//    @State var onFocused: Bool = false

    @State private var isRevealed = false
    @State private var isFocused = false
    
    var body: some View {
        VStack (spacing: 5){
            if isSecured {
                HStack(spacing: 0) {
                    CustomHintSecureField(placeholder: placeholder, text: $value, keyboardType: keyboardType, isFocused: $isFocused)
                        
                }.frame(height: 65)
                    .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                
            } else {
                
                HStack(spacing: 0){
                   
                    HStack(spacing: 0){
                        if(isPhone){
                            Text(AppConstants.phoneIndicator).foregroundColor(Color.primaryTextColor).padding(.trailing, 2).padding(.trailing, 5)
                        }
                        CustomHintTextField(placeholder: placeholder, text: $value, keyboardType: keyboardType,  isFocused: $isFocused)
                            
                    }.frame(height: 65)
                        .background(Color.editTextBgColor.clipShape(AppUtils.CustomCorners(corners: .allCorners, radius: 12)))
                }
            }
//            Divider()
//                .frame(height: self.isFocused ? 2 : 1)
//                .background( self.isFocused ? Color.primaryColor : Color.primaryTextLightColor)
//               // .animation(.linear(duration: 0.7))
            
            CustomErrorText(error: error)
    
        }
    }
}

struct CustomHintSecureField: View {
    var placeholder: String
    @Binding var text: String
    var keyboardType: UIKeyboardType = .default
    
    var editingChanged: (Bool) -> () = { _ in }
    var commit: () -> () = { }
    
    @State private var isRevealed : Bool = false
    @Binding var isFocused : Bool
    var body: some View {
        HStack{
            ZStack(alignment: .leading) {
                if text.isEmpty {
                    Text(placeholder)
                        .textStyle(PlaceHolderStyle())
                        .padding(.horizontal, 25)
                }
                MyTextField(text: $text, isRevealed: $isRevealed, isFocused: $isFocused, keyboardType: keyboardType)
                    .padding(.horizontal, AppConstants.viewExtraMargin)
                    .background(Color.clear)
                    .keyboardType(keyboardType)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .font(.system(weight: .regular, size: 18))
                    .foregroundColor(Color.primaryTextColor)
            }
            Image(self.isRevealed ? "icon_eye_on" : "icon_eye_off")
                .resizable()
                .frame(width: 16, height: 16)
                .foregroundColor(Color.primaryTextColor)
                .onTapGesture {
                    self.isRevealed.toggle()
                }
                .padding(AppConstants.viewSmallMargin)
                .padding(.trailing, AppConstants.viewSmallMargin)
        }
    }
}

struct CustomSearchTextField: View {
    var placeholder: String
    @Binding var text: String
    var keyboardType: UIKeyboardType = .default
    
    var editingChanged: (Bool) -> () = { _ in }
    var commit: () -> () = { }
    
    var body: some View {
        
        ZStack(alignment: .leading) {
            if text.isEmpty {
                Text(placeholder)
                    .textStyle(PlaceHolderStyle())
                    .padding(AppConstants.viewExtraMargin)
            }
            
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit)
                .foregroundColor(Color.primaryTextColor)
                .padding(AppConstants.viewExtraMargin)
                .background(Color.clear)
                .keyboardType(keyboardType)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .font(.system(weight: .regular, size: 18))
        }
    }
}

struct CustomHintTextField: View {
    
    var placeholder: String
    @Binding var text: String
    var keyboardType: UIKeyboardType = .default
    
    var editingChanged: (Bool) -> () = { _ in }
    var commit: () -> () = { }

    @State private var isRevealed = true
    @Binding var isFocused : Bool
    
    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty {
                Text(placeholder)
                    .textStyle(PlaceHolderStyle())
                    .padding(.horizontal, AppConstants.viewExtraMargin)
            }
            
            TextField("", text: $text)
                .padding(.horizontal, AppConstants.viewExtraMargin)
                .background(Color.clear)
                .keyboardType(keyboardType)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .foregroundColor(Color.primaryTextColor)
                .font(.system(weight: .regular, size: 18))
            
            /*
            MyTextField(text: $text,
                                    isRevealed: $isRevealed,
                                    isFocused: $isFocused, keyboardType: keyboardType)
                .padding(.horizontal, AppConstants.viewExtraMargin)
                .background(Color.clear)
                .keyboardType(keyboardType)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .foregroundColor(Color.red)
                //.font(.system(weight: .regular, size: 18))
           */
        }
    }
}

struct CustomHintTextEditor: View {
    
    var placeholder: String
    @Binding var text: String
    var keyboardType: UIKeyboardType = .default
    
    var editingChanged: (Bool) -> () = { _ in }
    var commit: () -> () = { }

    var body: some View {
        
        ZStack(alignment: .topLeading) {

            TextEditor(text: $text)
                .foregroundColor(Color.primaryTextColor)
                .keyboardType(keyboardType)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .font(.system(weight: .regular, size: 18))
                .frame(minHeight: 150)
                
            if text.isEmpty {
                Text(placeholder)
                    .textStyle(PlaceHolderStyle())
                    .padding(AppConstants.viewNormalMargin)
            }
        }.padding(AppConstants.viewSmallMargin)
        //.background(Color.lightGray)
        .onAppear() {
            UITextView.appearance().backgroundColor = UIColor(Color.editTextBgColor)
        }.onDisappear() {
            UITextView.appearance().backgroundColor = nil
        }
    }
}

struct CustomErrorText: View {
    var error: String = ""
    
    var body: some View {
        if (error != "") {
            Text(error)
                .font(.system(weight: .regular, size: 12))
                .foregroundColor(Color.errorColor)
                .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: UIScreen.main.bounds.width, alignment: .leading)
                .transition(AnyTransition.opacity.animation(.easeIn))
                .fixedSize(horizontal: false, vertical: true)
        }else{
            EmptyView()
        }
    }
}


struct MyTextField: UIViewRepresentable {

    // 1
    @Binding var text: String
    @Binding var isRevealed: Bool
    @Binding var isFocused: Bool
    var keyboardType: UIKeyboardType = .default
    
     // 2
    func makeUIView(context: UIViewRepresentableContext<MyTextField>) -> UITextField {
        let tf = UITextField(frame: .zero)
        tf.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        tf.isUserInteractionEnabled = true
        tf.delegate = context.coordinator
        tf.font = UIFont.init(name: FontNameManager.BrownStd.regular, size: 18)
       
        return tf
    }

    func makeCoordinator() -> MyTextField.Coordinator {
        return Coordinator(text: $text, isFocused: $isFocused)
    }

    // 3
    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        uiView.keyboardType = keyboardType
        uiView.isSecureTextEntry = !isRevealed
        uiView.autocorrectionType = .no
    }

    // 4
    class Coordinator: NSObject, UITextFieldDelegate {
        @Binding var text: String
        @Binding var isFocused: Bool

        init(text: Binding<String>, isFocused: Binding<Bool>) {
            _text = text
            _isFocused = isFocused
        }

        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
        }

        func textFieldDidBeginEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
               self.isFocused = true
            }
        }

        func textFieldDidEndEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.isFocused = false
            }
        }

        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return false
        }
    }
}
