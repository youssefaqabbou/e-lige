//
//  MapCalloutView.swift
//  Homeat
//
//  Created by macbook pro on 11/5/2021.
//


import Foundation
import UIKit
import SwiftUI

/**
A custom callout view to be be passed as an MKMarkerAnnotationView, where you can use a SwiftUI View as it's base.
*/
class MapCalloutUIView: UIView {
    
    //create the UIHostingController we need. For now just adding a generic UI
    let body:UIHostingController<AnyView> = UIHostingController(rootView: AnyView(Text("Hello")) )

    
    /**
    An initializer for the callout. You must pass it in your SwiftUI view as the rootView property, wrapped with AnyView. e.g.
    MapCalloutView(rootView: AnyView(YourCustomView))
    
    Obviously you can pass in any properties to your custom view.
    */
    init(rootView: AnyView) {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        body.rootView = AnyView(rootView)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    /**
    Ensures the callout bubble resizes according to the size of the SwiftUI view that's passed in.
    */
    private func setupView() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        body.view.translatesAutoresizingMaskIntoConstraints = false
        body.view.frame = bounds
        body.view.backgroundColor = UIColor(Color.primaryColor)
        body.view.layer.cornerRadius = AppConstants.viewRadius
        
        //add the subview to the map callout
        addSubview(body.view)

        NSLayoutConstraint.activate([
            body.view.topAnchor.constraint(equalTo: topAnchor, constant: -12),
            body.view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 12),
            body.view.leftAnchor.constraint(equalTo: leftAnchor, constant: -10),
            body.view.rightAnchor.constraint(equalTo: rightAnchor, constant: 10)
        ])
        
        sizeToFit()
    
        
    }
}

