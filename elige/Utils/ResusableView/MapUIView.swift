//
//  MapUIView.swift
//  Homeat
//
//  Created by macbook pro on 10/6/2021.
//

import Foundation

import Foundation
import SwiftUI
import MapKit

struct MapUIView : UIViewRepresentable {
    
    @Binding var mapView : MKMapView
    
    @State var canShowCallOut : Bool = false
    var annotationDetail : (_ value: AnyObject) -> () = {_ in }
    var annotationSelect : (_ annotation : MKAnnotation) -> () = {_  in }
    
    func makeUIView(context: Context) -> MKMapView {
        let view = mapView
        view.showsUserLocation = true
        view.delegate = context.coordinator
        
        return view
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
    }
    
    func makeCoordinator() -> Coordinator {
        MapUIView.Coordinator(canShowCallOut: canShowCallOut, detailAction: annotationDetail, selectAction: annotationSelect)
    }
    
    
    
    class Coordinator: NSObject, MKMapViewDelegate {
        var canShowCallOut : Bool
        var annotationDetail : (_ value: AnyObject) -> ()
        var onAnnotationSelected : (_ annotation : MKAnnotation) -> ()
        
        var calloutViews = [MapCalloutUIView]()
        var mkAnnotationViews = [MKAnnotationView]()
        
        init(canShowCallOut : Bool, detailAction : @escaping (AnyObject) -> (), selectAction : @escaping (MKAnnotation) -> ()) {
            //self.vmHome  = vm
            self.canShowCallOut = canShowCallOut
            self.annotationDetail = detailAction
            self.onAnnotationSelected = selectAction
        }
        
        func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
            //DebugHelper.debug(mapView.centerCoordinate)
        }
        
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let lineDashPatterns: [NSNumber]?  = [5,10]

            
            let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
            
            renderer.strokeColor = UIColor(Color.primaryColor)
            renderer.lineDashPattern = lineDashPatterns
            renderer.lineCap = .round
            renderer.lineWidth = 2
            return renderer
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
            let identifier = "barber"
            let clusterIdentifier = "clusterIdentifer"
            
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            view?.clusteringIdentifier = clusterIdentifier
            
            if let cluster = annotation as? MKClusterAnnotation {
                //DebugHelper.debug("MKClusterAnnotation")
                
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view?.image = Text("\(cluster.memberAnnotations.count)").padding().background(Color.primaryColor.clipShape(Circle())).frame(width: 150, height: 150).snapshot()
               
                return view
            } else if annotation is CustomMapAnnotation {
                //DebugHelper.debug("CustomMapAnnotation")
                
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view?.clusteringIdentifier = clusterIdentifier
                view?.canShowCallout = canShowCallOut
                //view?.image = Image.shopMarkerUIImage
                if let mapAnnotation = view?.annotation as? CustomMapAnnotation {
                    if let barber = mapAnnotation.mapModel as? BarberShopModel {
                        DebugHelper.debug("barber annotation", barber.name)
                        view?.image = MapAnnotationView(barber: barber) { loaded in
                            DebugHelper.debug("image loaded", loaded)
                            if(loaded){
                                DispatchQueue.main.async {
                                    view?.image = MapAnnotationView(barber: barber).snapshot()
                                    
                                }
                            }
                        }.snapshot()
                    }
                }
                view?.displayPriority = .required
                return view
               
            }else  if annotation is MKUserLocation {
                DebugHelper.debug("MKUserLocation")
            } else{
                DebugHelper.debug("Else annotation")
                if(view == nil) {
                    view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    view?.canShowCallout = true
                    view?.image = Image.shopMarkerUIImage
                    view?.frame.size = CGSize(width: 25, height: 50)
                    //view?.displayPriority = .defaultHigh
                } else {
                    //view?.annotation = annotation
                }
                return view
            }
            return nil
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            if let cluster = view.annotation as? MKClusterAnnotation {
                DebugHelper.debug("cluster didSelect")
                fitMapViewToAnnotaionList(mapView, annotations: cluster.memberAnnotations)
            } else if view.annotation is CustomMapAnnotation {
                DebugHelper.debug("CustomMapAnnotation didSelect")
                onAnnotationSelected( view.annotation!)
                mapView.deselectAnnotation(view.annotation, animated: false)
            }
        }
        
        func refreshAnnotation(_ mapView : MKMapView, annotation: MKAnnotation ){
            DispatchQueue.main.async {
                // Update UI
                mapView.removeAnnotation(annotation)
                mapView.addAnnotation(annotation)
            }
            
        }
        
        func fitMapViewToAnnotaionList(_ mapView : MKMapView, annotations : [MKAnnotation]) -> Void {
            let mapEdgePadding = UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50)
            var zoomRect:MKMapRect = MKMapRect.null

            for index in 0..<annotations.count {
                let annotation = annotations[index]
               
                let aPoint:MKMapPoint = MKMapPoint(annotation.coordinate)
                let rect:MKMapRect = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)

                if zoomRect.isNull {
                    zoomRect = rect
                } else {
                    zoomRect = zoomRect.union(rect)
                }
                
            }

            mapView.setVisibleMapRect(zoomRect, edgePadding: mapEdgePadding, animated: true)
        }
        
        func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            DebugHelper.debug("calloutAccessoryControlTapped")
        }
    }
}
