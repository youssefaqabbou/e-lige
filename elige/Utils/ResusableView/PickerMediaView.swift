//
//  PickerMediaView.swift
//  elige
//
//  Created by user196417 on 11/8/21.
//

import Foundation
import SwiftUI

struct PickerMediaView: View {
    @Binding var isShowPickerChoice : Bool
    @Binding var pickerType : Int
    @Binding var isShowImagePicker : Bool
    
    var body: some View{
        
        VStack(spacing: 15){
            
            Spacer()
            
            HStack {
                
                Spacer()
                
                Button(action: {
                    self.isShowPickerChoice.toggle()
                }) {
                    Image.iconClose
                        .resizable()
                        .frame(width: 30, height: 30)
                }.buttonStyle(PressedButtonStyle())
                .cornerRadius(10)
            }
            
            VStack(spacing: 20){
                
                Button(action: {
                    self.pickerType = 1
                    self.isShowImagePicker.toggle()
                    self.isShowPickerChoice.toggle()
                }) {
                    
                    HStack{
                        
                        VStack(alignment: .leading, spacing: 5){
                            Text("Prendre une photo avec")
                                .font(.system(weight: .regular, size: 16))
                                .foregroundColor(.secondaryTextColor)
                            
                            Text("Appareil Photo")
                                .font(.system(weight: .regular, size: 24))
                                .foregroundColor(.primaryTextLightColor)
                        }
                        
                        Spacer()
                        
                        Image.iconCameraEdit
                            .resizable()
                            .frame(width: 40, height: 40)
                        
                    }
                }.buttonStyle(PrimaryButtonStyle())
                    
                Button(action: {
                    self.pickerType = 0
                    self.isShowImagePicker.toggle()
                    self.isShowPickerChoice.toggle()
                }) {
                    HStack{
                        
                        VStack(alignment: .leading, spacing: 5){
                            
                            Text("Télécharger a partir")
                                .font(.system(weight: .regular, size: 16))
                                .foregroundColor(.secondaryTextColor)
                            
                            Text("Galerie")
                                .font(.system(weight: .regular, size: 24))
                                .foregroundColor(.primaryTextLightColor)
                        }
                        
                        Spacer()
                        
                        Image.iconGallery
                            .resizable()
                            .frame(width: 40, height: 40)
                        
                    }
                    
                }.buttonStyle(PrimaryButtonStyle())
                
            }
            
            Spacer()
            
        }.frame(width: UIScreen.main.bounds.width - 80, height: 280, alignment: .center)
            .padding()
            .background(Color.primaryTextLightColor)
            .cornerRadius(20)
            .shadow(color: Color.shadowColor, radius: 2, x: 2, y: 3)
            
            
    }
}


struct PickerMediaView_Previews: PreviewProvider {
    static var previews: some View {
        PickerMediaView(isShowPickerChoice: .constant(false), pickerType: .constant(0) , isShowImagePicker: .constant(false))
    }
}

