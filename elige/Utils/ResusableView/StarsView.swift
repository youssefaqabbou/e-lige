//
//  StarsView.swift
//  elige
//
//  Created by macbook pro on 8/11/2021.
//

import Foundation
import SwiftUI

struct StarsView: View {
    
    var rating: CGFloat
    var maxRating: Int
    
    var width : CGFloat = 20
    var spacing : CGFloat = 0

    var body: some View {
        let stars = HStack(spacing: 0) {
            ForEach(0..<maxRating) { _ in
                Image.iconStar
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: width, height: width, alignment: .center)
                    .padding(.trailing, spacing)
            }
        }

        stars.overlay(
            GeometryReader { g in
                let width = rating / CGFloat(maxRating) * g.size.width
                ZStack(alignment: .leading) {
                    Rectangle()
                        .frame(width: width)
                        .foregroundColor(.primaryColor)
                }
            }
            .mask(stars)
        )
        .foregroundColor(.lightGray)
    }
}
