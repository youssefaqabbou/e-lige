//
//  File.swift
//  Homeat
//
//  Created by macbook pro on 30/6/2021.
//

import Foundation
import Stripe
import SwiftUI

struct StripeCardTextField: UIViewRepresentable {
    
    @Binding var cardParams: STPPaymentMethodCardParams
    @Binding var isValid: Bool
    @State var borderColor = Color.clear
    @State var cornerRadius: CGFloat = 12
    
    
    func makeUIView(context: Context) -> STPPaymentCardTextField {
        let input = STPPaymentCardTextField()
        input.borderWidth = 1
        input.borderColor = UIColor(borderColor)
        input.cornerRadius = cornerRadius
        input.backgroundColor = UIColor(Color.bgViewSecondaryColor)
        input.postalCodeEntryEnabled = false
        input.placeholderColor = UIColor(Color.placeHolderTextColor)
        input.textColor = UIColor(Color.primaryTextColor)
        input.textErrorColor = UIColor(Color.errorColor)
        input.numberPlaceholder = "0000000000000000"
        input.delegate = context.coordinator
        return input
    }
    
    func makeCoordinator() -> StripeCardTextField.Coordinator { Coordinator(self) }

    func updateUIView(_ view: STPPaymentCardTextField, context: Context) { }
    
    class Coordinator: NSObject, STPPaymentCardTextFieldDelegate {

        var parent: StripeCardTextField
        init(_ textField: StripeCardTextField) {
            parent = textField
        }
        
        func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
            parent.cardParams = textField.cardParams
            parent.isValid = textField.isValid
            if(parent.isValid){
                textField.resignFirstResponder()
            }
        }
    }
}
