//
//  ListShimmeringView.swift
//  elige
//
//  Created by macbook pro on 31/1/2022.
//

import Foundation
import SwiftUI

struct ListShimmeringView<Content: View>: View {
    var count = 10
    var isVertical : Bool = true
    
    var height : Int = 100
    
    let content: () -> Content
    
    var body: some View {
        
        if(isVertical){
            ScrollView(.vertical, showsIndicators: false) {
               VStack(){
                    ForEach(1...count, id: \.self) { row in
                        //Text("teslkkdslklklkslkdlks").padding()
                        content().redacted(reason: .placeholder)
                            .shimmering()
                    }
                }
            }.disabled(true)
        }else{
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack(){
                    ForEach(1...count, id: \.self) { row in
                        //Text("teslkkdslklklkslkdlks").padding()
                        content().redacted(reason: .placeholder)
                            .shimmering()
                    }
                }//.background(Color.yellow)
                .frame(height: CGFloat(height))
            }.disabled(true)
        }
    }
}
