//
//  UserDefaults.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import Foundation
import SwiftyJSON

extension UserDefaults {

    public struct Keys {
        
        static let isFirstUse = "first_use"
        static let token = "access_token"
        static let refreshToken = "refresh_token"
        static let showLogin = "showLogin"
        static let validAccount = "validAccount"
        static let isBarber = "isBarber"
        static let barberShop = "barberShop"
        static let user = "user"
        static let categories = "categories"
        static let deviceToken = "deviceToken"
        static let deviceTokenSent = "deviceTokenSent"
        static let latitude = "lat"
        static let longitude = "lng"
        static let socialConnect = "socialConnect"
        
        static let showLoginDialog = "showLoginDialog"
    }

    static var isFirstUse: Bool {

        get {
            return UserDefaults.standard.bool(forKey: Keys.isFirstUse)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.isFirstUse)
        }
    }
    
    static var token: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.token) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.token)
        }
    }
    
    static var refreshToken: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.refreshToken) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.refreshToken)
        }
    }
    
    static var deviceToken: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.deviceToken) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.deviceToken)
        }
    }
    static var deviceTokenSent: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.deviceTokenSent) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.deviceTokenSent)
        }
    }
    
    static var showLogin: Bool {

        get {
            return UserDefaults.standard.bool(forKey: Keys.showLogin)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.showLogin)
        }
    }
    
    static var isValidAccount: Bool {

        get {
            return UserDefaults.standard.bool(forKey: Keys.validAccount)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.validAccount)
        }
    }
    
    static var isBarber: Bool {

        get {
            return UserDefaults.standard.bool(forKey: Keys.isBarber)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.isBarber)
        }
    }
    
    static var barberShopJson: String {
        get {
            return UserDefaults.standard.string(forKey: Keys.barberShop) ?? ""
        }
        set {
            DebugHelper.debug("set new shop")
            UserDefaults.standard.set(newValue, forKey: Keys.barberShop)
        }
    }
    
    static var user: String {
        get {
            return UserDefaults.standard.string(forKey: Keys.user) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.user)
        }
    }
    
    static var categories: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.categories) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.categories)
        }
    }
    
    static var latitude: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.latitude) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.latitude)
        }
    }
    
    static var longitude: String {

        get {
            return UserDefaults.standard.string(forKey: Keys.longitude) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.longitude)
        }
    }
    
    static var connectedWithSocial: Bool {

        get {
            return UserDefaults.standard.bool(forKey: Keys.socialConnect) ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.socialConnect)
        }
    }
    
    static var showLoginDialog: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Keys.showLoginDialog)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.showLoginDialog)
        }
    }
   
    static func disconnect() {
        UserDefaults.standard.removeObject(forKey: Keys.token)
        UserDefaults.standard.removeObject(forKey: Keys.refreshToken)
        UserDefaults.standard.removeObject(forKey: Keys.validAccount)
        UserDefaults.standard.removeObject(forKey: Keys.user)
        UserDefaults.standard.removeObject(forKey: Keys.isBarber)
        UserDefaults.showLogin = true
    }

}
