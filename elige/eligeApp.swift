//
//  eligeApp.swift
//  elige
//
//  Created by user196417 on 11/1/21.
//

import SwiftUI
import UIKit
import FBSDKCoreKit

@main
struct eligeApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @Environment(\.scenePhase) private var scenePhase
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onOpenURL { (url) in
                    print("onOpenURL", url)
                }
            //BarberShopScheduleView(barberShop: FakeData.getBarberShopData(id: 1))
        }.onChange(of: scenePhase) { (newScenePhase) in
            switch newScenePhase {
            case .active:
                print("scene is now active!")
            case .inactive:
                print("scene is now inactive!")
            case .background:
                print("scene is now in the background!")
            @unknown default:
                print("Apple must have added something new!")
            }
        }
    }
}

